﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelListControl : MonoBehaviour
{
    public List<LevelButton> levelButtonList;

    private void Awake()
    {
        LevelButton[] levelButtons = transform.Find("Panel").GetComponentsInChildren<LevelButton>();
        levelButtonList = new List<LevelButton>(levelButtons);
    }

    public void CloseOtherSectionMenus(int openStage)
    {
        foreach(LevelButton lb in levelButtonList)
        {
            if(lb.stageIndex != openStage)
            {
                lb.HideSectionMenu();
            }
        }
    }

}
