﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPoint : MonoBehaviour
{
    public float degreesToRotate = 0.0f;
    public float durationOfMovementToNextPoint = 1.0f;
    public bool overrideSpeed = false;
    public bool overrideRotation = false;

    public float GetDistanceToPointFromPosition(Vector2 start)
    {
        Vector2 distance = transform.position - (Vector3)start;
        return distance.magnitude;
    }
}
