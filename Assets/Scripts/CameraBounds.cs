﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraBounds : MonoBehaviour
{
    public RectTransform cameraBorderLeft;
    public RectTransform cameraBorderRight;
    public RectTransform cameraBorderTop;
    public RectTransform cameraBorderBottom;

    public Tuple<float,float> GetXBounds()
    {
        return new Tuple<float, float>(cameraBorderLeft.position.x, 
            cameraBorderRight.position.x);
    }

    public Tuple<float,float> GetYBounds()
    {
        return new Tuple<float, float>(cameraBorderBottom.position.y, 
            cameraBorderTop.position.y);
    }
}
