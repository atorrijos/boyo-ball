﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class DataControl : MonoBehaviour
{
    public static DataControl control;
    const float SAVE_VERSION = 1.135f;
    const string SAVE_FILENAME = "/levelProgress.dat";

    // Data to save progress.
    ProgressData progressData;

    // Awake is the first thing called.
    void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }

        Load();
    }

    public Tuple<LevelID, string> LevelCompleted(int stage, int section)
    {
        return progressData.LevelCompleted(stage, section);
    }

    public string ResumeAfterLastLevelCompleted()
    {
        progressData.ResumeAfterLastLevelCompleted();
        return progressData.GetLevelToResumeString();
    }

    public LevelID GetResumeData()
    {
        return progressData.GetLevelToResume();
    }

    public string GetResumeString()
    {
        return progressData.GetLevelToResumeString();
    }

    public LevelID GetMaxLevelUnlocked()
    {
        return progressData.GetMaxLevelUnlocked();
    }

    public void SetResumeLevel(LevelID level)
    {
        progressData.SetLevelToResume(level.levelStage, level.levelSection);
    }

    public bool HasBeatenGame()
    {
        return progressData.HasBeatenGame();
    }

    public bool IsHatEnabled()
    {
        return progressData.IsHatEnabled();
    }

    public void ToggleHat()
    {
        progressData.SetHatEnabled(
            !progressData.IsHatEnabled());
    }

    public void SetSfxVolumePref(float db)
    {
        progressData.SetSfxVolumePref(db);
    }

    public void SetMusicVolumePref(float db)
    {
        progressData.SetMusicVolumePref(db);
    }

    public float GetSfxVolumePref()
    {
        return progressData.GetSfxVolumePref();
    }
    
    public float GetMusicVolumePref()
    {
        return progressData.GetMusicVolumePref();
    }

    public void SetSfxMutedPref(bool muted)
    {
        progressData.SetSfxMutedPref(muted);
    }

    public void SetMusicMutedPref(bool muted)
    {
        progressData.SetMusicMutedPref(muted);
    }

    public bool GetSfxMutedPref()
    {
        return progressData.GetSfxMutedPref();
    }

    public bool GetMusicMutedPref()
    {
        return progressData.GetMusicMutedPref();
    }
    /*
     * Saving and loading functions.
     */

    public void Save()
    {
        progressData.SetSaveVersion(SAVE_VERSION);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file =
            File.Create(Application.persistentDataPath +
            SAVE_FILENAME);

        bf.Serialize(file, progressData);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + SAVE_FILENAME))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file =
                File.Open(Application.persistentDataPath +
                SAVE_FILENAME, FileMode.Open);
            progressData = (ProgressData)bf.Deserialize(file);
            file.Close();

            if(progressData.GetSaveVersion() < SAVE_VERSION)
            {
                ProgressData oldData = progressData;
                progressData = new ProgressData();
                progressData.SetSaveVersion(SAVE_VERSION);
                progressData.CopyDataFrom(oldData);
                Save();
            }
        }
        else
        {
            progressData = new ProgressData();
        }
    }
}

[System.Serializable]
public class LevelID
{
    public int levelStage = 0;
    public int levelSection = 0;
    public LevelID(int stage, int section)
    {
        levelStage = stage;
        levelSection = section;
    }

    public static bool operator >(LevelID left, LevelID right)
    {
        if (left.levelStage > right.levelStage)
        {
            return true;
        }
        else if (left.levelStage == right.levelStage)
        {
            return left.levelSection > right.levelSection;
        }

        return false;
    }

    public static bool operator <(LevelID left, LevelID right)
    {
        if (left.levelStage < right.levelStage)
        {
            return true;
        }
        else if (left.levelStage == right.levelStage)
        {
            return left.levelSection < right.levelSection;
        }

        return false;
    }

    public static bool operator ==(LevelID left, LevelID right)
    {
        bool leftNull = System.Object.ReferenceEquals(left, null);
        bool rightNull = System.Object.ReferenceEquals(right, null);
        
        if(leftNull || rightNull)
        {
            if(leftNull && rightNull)
            {
                return true;
            }
            return false;
        }

        if (left.levelStage != right.levelStage)
        {
            return false;
        }

        if(left.levelSection != right.levelSection)
        {
            return false;
        }

        return true;
    }

    public static bool operator !=(LevelID left, LevelID right)
    {
        return !(left == right);
    }

    public static bool operator >=(LevelID left, LevelID right)
    {
        return (left > right || left == right);
    }

    public static bool operator <=(LevelID left, LevelID right)
    {
        return (left < right || left == right);
    }
}

[Serializable]
class ProgressData
{
    LevelID lastLevelCompleted = new LevelID(-1, -1);
    LevelID maxLevelUnlocked = new LevelID(0, 0);
    LevelID levelToResume = new LevelID(0, 0);
    LevelID finalLevel = new LevelID(9, 9);
    float saveVersion = 0.0f;
    bool gameCleared = false;
    bool hatEnabled = false;
    float sfxVolumePref = 0.0f;
    float musicVolumePref = 0.0f;
    bool musicMutedPref = false;
    bool sfxMutedPref = false;

    // Contains the count of sections per stage. Used to determine
    // when to switch stages.
    // UPDATE when levels are added.
    int[] sectionCount = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };

    /*
     * Keepts track of maximum level completed and last level completed
     * for progression purposes.
     * 
     * Returns: A tuple containing the stage,level to load next and a 
     * string that refers to the scene to load next.
     */
    public Tuple<LevelID,string> LevelCompleted(int stage, int level)
    {
        lastLevelCompleted = new LevelID(stage,level);

        LevelID nextLevel = GetNextLevel(lastLevelCompleted);
        levelToResume = nextLevel;

        if (nextLevel > maxLevelUnlocked)
        {
            maxLevelUnlocked = nextLevel;
        }

        string sceneString = FormatSceneString(nextLevel);

        // In the case we just completed the last level, load the start
        // menu. Also flag for game completion.
        if (lastLevelCompleted == finalLevel)
        {
            sceneString = "StartMenu";
            gameCleared = true;
        }
        return new Tuple<LevelID, string>(nextLevel, sceneString);
    }

    public void ResumeAfterLastLevelCompleted()
    {
        LevelID nextLevel = GetNextLevel(lastLevelCompleted);
        levelToResume = nextLevel;
    }

    public LevelID GetNextLevel(LevelID current)
    {
        int nextStage = current.levelStage;
        int nextSection = current.levelSection + 1;

        /*
         * Starting case/no save data.
         */
        if(nextStage < 0)
        {
            //Return the first level.
            return new LevelID(0, 0);
        }

        // If we go greater than the number of sections, go to next stage.
        if (nextSection > sectionCount[nextStage] - 1)
        {
            nextStage++;
            nextSection = 0;

            // Last level was completed, next level will be first level
            // for now.
            // TODO: Win condition, show win.
            if (nextStage > sectionCount.Length)
            {
                nextStage = 0;
            }
        }

        // If we go greater than the number of stages, start over.
        if (nextStage > sectionCount.Length - 1)
        {
            nextStage = 0;
            nextSection = 0;
        }

        LevelID nextLevel = new LevelID(nextStage, nextSection);

        return nextLevel;
    }

    /**
     * Scene Strings are 1 - indexed. This is adjusted for in this function.
     */
    public string FormatSceneString(LevelID level)
    {
        string sceneString = (level.levelStage + 1) +
            "_" + 
            (level.levelSection + 1);
        return sceneString;
    }

    public LevelID GetLastLevelCompleted()
    {
        return lastLevelCompleted;
    }

    public string GetLastLevelCompletedString()
    {
        return FormatSceneString(lastLevelCompleted);
    }

    public LevelID GetMaxLevelUnlocked()
    {
        return maxLevelUnlocked;
    }

    public string GetMaxLevelUnlockedString()
    {
        return FormatSceneString(maxLevelUnlocked);
    }

    public LevelID GetLevelToResume()
    {
        return levelToResume;
    }

    public string GetLevelToResumeString()
    {
        return FormatSceneString(levelToResume);
    }

    public float GetSaveVersion()
    {
        return saveVersion;
    }

    public void SetLevelToResume(int stage, int section)
    {
        levelToResume = new LevelID(stage, section);
    }

    public bool HasBeatenGame()
    {
        return gameCleared;
    }

    public bool IsHatEnabled()
    {
        return hatEnabled;
    }

    public void SetHatEnabled(bool enabled)
    {
        hatEnabled = enabled;
    }

    public void SetSfxVolumePref(float vol)
    {
        sfxVolumePref = vol;
    }
    
    public void SetMusicVolumePref(float vol)
    {
        musicVolumePref = vol;
    }

    public float GetSfxVolumePref()
    {
        return sfxVolumePref;
    }

    public float GetMusicVolumePref()
    {
        return musicVolumePref;
    }

    public void SetMusicMutedPref(bool muted)
    {
        musicMutedPref = muted;
    }

    public void SetSfxMutedPref(bool muted)
    {
        sfxMutedPref = muted;
    }

    public bool GetSfxMutedPref()
    {
        return sfxMutedPref;
    }

    public bool GetMusicMutedPref()
    {
        return musicMutedPref;
    }

    /*
     * Might pop this out of the class later. Will be used to
     * resolve save file inconsistencies.
     */
    public void SetSaveVersion(float currentVersion)
    {
        if (currentVersion > saveVersion)
        {
            saveVersion = currentVersion;
        }
    }

    /*
     * Function to copy data from old versions of save files to upgrade
     * to more recent versions.
     */
    public void CopyDataFrom(ProgressData oldData)
    {
        lastLevelCompleted = oldData.GetLastLevelCompleted();
        levelToResume = oldData.GetLevelToResume();
        maxLevelUnlocked = oldData.GetMaxLevelUnlocked();
        gameCleared = oldData.HasBeatenGame();
        musicVolumePref = oldData.GetMusicVolumePref();
        sfxVolumePref = oldData.GetSfxVolumePref();
        musicMutedPref = oldData.GetMusicMutedPref();
        sfxMutedPref = oldData.GetSfxMutedPref();
    }
}

