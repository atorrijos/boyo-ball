﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class EventTrigger : MonoBehaviour
{
    public UnityEvent eventActions;
    public string collisionTag = "";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collisionTag == "")
        {
            eventActions.Invoke();
        }
        else
        {
            if(collision.tag == collisionTag)
            {
                eventActions.Invoke();
            }
        }
    }
}
