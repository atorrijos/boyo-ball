﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquishTrigger : MonoBehaviour
{
    private BallControl ball;

    // Start is called before the first frame update
    void Start()
    {
        ball = GetComponentInParent<BallControl>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
     * Handle a collider entering the squish zone.
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Only want to deal with solid objects. No triggers.
        if (!collision.isTrigger)
        {
            ball.CheckSquish(collision);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.isTrigger)
        {
            ball.CheckClearSquish(collision);
        }
    }
}
