﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MusicSelection : MonoBehaviour
{

    // Variables to keep track of music.
    public List<MusicTrack> musicSelection
        = new List<MusicTrack>();
    public List<LevelID> musicLevelChanges = new List<LevelID>();

    public int currentlyPlayingTrack = -1;

    public MusicTrack GetMusicForLevel(LevelID level)
    {
        int track = GetTrackNumberForLevel(level);
        if(track > -1 && track < musicSelection.Count)
        {
            return musicSelection[track];
        }
        return null;
    }

    /*
     * Determines track for a given level based on the indicated changes.
     */
    public int GetTrackNumberForLevel(LevelID level)
    {
        for (int i = musicLevelChanges.Count - 1; i >= 0; i--)
        {
            // The starting level is the minimum level required to be on
            // to play the specific song. It is assumed that the music selection
            // is ordered so the songs further down the list are played later.
            LevelID startingLevelForMusic = musicLevelChanges[i];
            if (level >= startingLevelForMusic)
            {
                return i;
            }
        }
        return -1;
    }

    /*
     * Directly get a clip for a given track.
     */
     public MusicTrack GetMusicForTrackNumber(int track)
    {
        return musicSelection[track];
    }


}

[Serializable]
public class MusicTrack
{
    public AudioClip musicStart;
    public AudioClip musicLoop;
}
