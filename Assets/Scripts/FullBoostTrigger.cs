﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullBoostTrigger : MonoBehaviour
{
    public ParticleSystem boost;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name == "SurfBoardSurface")
        {
            ParticleSystem.ShapeModule psShape = boost.shape;
            ParticleSystem.EmissionModule psEmission = boost.emission;
            psShape.angle = 70f;
            psEmission.rateOverTimeMultiplier = 200f;
        }
    }
}
