﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitingObject : MoveableObject
{
    MovementPoint orbitCenter;
    float degreesPerSecond;
    float orbitRadius;
    float cycleDuration;
    float currentDegrees;
    float startingDegrees;

    // Variables to sync rotation to orbit.
    public bool matchAngleToOrbit = false;

    new void Start()
    {
        // Always want the rotating object to start at the first index.
        firstIndex = 0;
        base.Start();

        orbitCenter = pathController.GetPointAtIndex(1);

        // Find Radius.
        Vector2 radiusVector =
            firstPosition.transform.position - orbitCenter.transform.position;
        orbitRadius = radiusVector.magnitude;

        // Find the circumference.
        float orbitCircumference = 2 * Mathf.PI * orbitRadius;

        // Find length of cycle given speed value.
        cycleDuration = orbitCircumference / speed;

        // Using the defined speed value, find speed in degrees per second.
        degreesPerSecond = 360f / cycleDuration;

        startingDegrees = Vector2.Angle(new Vector2(1.0f, 0.0f), radiusVector);
        if (radiusVector.y < 0)
        {
            startingDegrees *= -1f;
        }
        currentDegrees = startingDegrees;
        

        if (matchAngleToOrbit)
        {
            rotationalSpeed = degreesPerSecond;
        }

        // Find speed.

    }

    void FixedUpdate()
    {
        if (isMoving && !isStationary)
        {
            timeSinceStart += Time.fixedDeltaTime;
            if(timeSinceStart > delayStart)
            {
                float currX = orbitRadius * Mathf.Cos(Mathf.Deg2Rad * currentDegrees);
                float currY = orbitRadius * Mathf.Sin(Mathf.Deg2Rad * currentDegrees);
                Vector2 currPos = new Vector2(currX, currY) + (Vector2)orbitCenter.transform.position;

                float deltaDegrees = degreesPerSecond * Time.fixedDeltaTime;

                currentDegrees += deltaDegrees;
                float nextX = orbitRadius * Mathf.Cos(Mathf.Deg2Rad * currentDegrees);
                float nextY = orbitRadius * Mathf.Sin(Mathf.Deg2Rad * currentDegrees);
                Vector2 nextPos = new Vector2(nextX, nextY) + (Vector2)orbitCenter.transform.position;
                movingObject.position = nextPos;

                Vector2 pathway = nextPos - currPos;
                movingObject.velocity = pathway / Time.fixedDeltaTime;
            }
        }
    }

    new public void ResetObject()
    {
        base.ResetObject();
        currentDegrees = startingDegrees;
    }
}
