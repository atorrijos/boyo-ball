﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/***
 * Class to manage multiple launchable objects.
 */
public class LaunchableObjectRow : MonoBehaviour
{
    LaunchableObject[] launchableObjects;
    public float maxDelay = 5.0f;
    public float minDelay = 0.0f;
    public float defaultLaunchStrength = 10.0f;
    public float defaultRestInterval = 0.0f;
    public int randomDelaySeed = 1337;
    public MovementTrigger trigger;
    public bool triggeredStart = false;
    public enum Mode
    {
        Random = 0,
        Order = 1,
        Reverse = 2
    };
    public Mode mode = Mode.Random;

    // Start is called before the first frame update
    void Start()
    {
        launchableObjects = GetComponentsInChildren<LaunchableObject>();
    }

    public void GenerateValues()
    {
        SetLaunchDelays(minDelay, maxDelay, randomDelaySeed);
        SetLaunchStrength(defaultLaunchStrength);
        SetRestInterval(defaultRestInterval);
        SetTriggerData(triggeredStart, trigger);
    }

    void SetLaunchDelays(float minDelay, float maxDelay, int delaySeed)
    {
        float totalDelaySpan = maxDelay - minDelay;
        float delayInterval = totalDelaySpan / (float)(launchableObjects.Length - 1);
        switch (mode)
        {
            default:
            case Mode.Random:
                Random.InitState(delaySeed);
                foreach (LaunchableObject l in launchableObjects)
                {
                    float delay = Random.Range(minDelay, maxDelay);
                    l.UpdateLauncherDelay(delay);
                }
                break;
            case Mode.Order:
                for (int i = 0; i < launchableObjects.Length; i++)
                {
                    float delay = minDelay + delayInterval * (float)i;
                    launchableObjects[i].UpdateLauncherDelay(delay);
                }
                break;
            case Mode.Reverse:
                for (int i = 0; i < launchableObjects.Length; i++)
                {
                    float delay = minDelay + delayInterval * (float)(launchableObjects.Length - i - 1);
                    launchableObjects[i].UpdateLauncherDelay(delay);
                }
                break;
        }

    }

    void SetLaunchStrength(float velocity)
    {
        foreach (LaunchableObject l in launchableObjects)
        {
            l.UpdateLaunchStrength(velocity);
        }
    }

    void SetRestInterval(float restInterval)
    {
        foreach (LaunchableObject l in launchableObjects)
        {
            l.UpdateRestInterval(restInterval);
        }
    }

    void SetTriggerData(bool launchByTrigger, MovementTrigger trigger)
    {
        foreach (LaunchableObject l in launchableObjects)
        {
            l.UpdateTriggerData(launchByTrigger, trigger);
        }
    }
}
