﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public int stageIndex = 0;
    Button button;
    GameObject sectionContainer;
    List<Button> sectionList;
    public LevelListControl levelListControl;

    // Start is called before the first frame update
    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ToggleSectionMenu);
        sectionContainer = transform.Find("Levels").gameObject;
        Button[] subLevels = sectionContainer.transform.GetComponentsInChildren<Button>();
        sectionList = new List<Button>(subLevels);
        DisableButton(button);
        foreach(Button b in sectionList)
        {
            b.image.alphaHitTestMinimumThreshold = 0.5f;
            b.onClick.AddListener(
                delegate
                {
                    // Section is gotten from text of button; text is 1 indexed, data is 0 indexed.
                    int section = int.Parse(b.transform.Find("Text").GetComponent<Text>().text) - 1;
                    StartLevel(section);
                });
            DisableButton(b);
        }
        HideSectionMenu();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CheckEnable(LevelID level)
    {
        if(level.levelStage >= stageIndex)
        {
            EnableButton(button);
            foreach (Button b in sectionList)
            {
                // Section is gotten from text of button; text is 1 indexed, data is 0 indexed.
                int section = int.Parse(b.transform.Find("Text").GetComponent<Text>().text) - 1;
                if(level.levelStage > stageIndex 
                    || level.levelSection >= section)
                {
                    EnableButton(b);
                }
            }
        }
    }

    public void EnableButton(Button b)
    {
        Text buttonText = b.transform.Find("Text").GetComponent<Text>();
        buttonText.color = Color.black;
        b.interactable = true;
    }

    public void DisableButton(Button b)
    {
        Text buttonText = b.transform.Find("Text").GetComponent<Text>();
        buttonText.color = Color.grey;
        b.interactable = false;
    }

    void StartLevel(int section)
    {
        DataControl.control.SetResumeLevel(new LevelID(stageIndex,section));
        StartCoroutine(AsyncSceneLoad(DataControl.control.GetResumeString()));
    }

    public void ShowSectionMenu()
    {
        sectionContainer.SetActive(true);
    }

    public void HideSectionMenu()
    {
        sectionContainer.SetActive(false);
    }

    public bool IsSectionMenuVisible()
    {
        return sectionContainer.activeSelf;
    }

    void ToggleSectionMenu()
    {
        if (IsSectionMenuVisible())
        {
            HideSectionMenu();
        }
        else
        {
            ShowSectionMenu();
            levelListControl.CloseOtherSectionMenus(stageIndex);
        }
    }

    /*
     * Load a scene.
     */
    IEnumerator AsyncSceneLoad(string sceneName)
    {
        AsyncOperation asyncLoad =
            SceneManager.LoadSceneAsync(sceneName);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
