﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableObject : MonoBehaviour, StartTrigger
{
    public PathController pathController;

    // Trigger to start movement.
    public MovementTrigger startTrigger;

    // Default platform speed if not overridden.
    public float speed = 1.0f;

    // Default platform rotation if not overridden.
    public float rotationalSpeed = 0.0f;

    // Float to indicate wait time after starting the game to begin movement.
    public float delayStart = 0.0f;
    protected float timeSinceStart = 0.0f;

    public int pathIndex = 0;

    // Used to keep track of velocity in case we need to pause.
    Vector2 currentVelocity = new Vector2(0.0f, 0.0f);
    float currentAngularVelocity = 0.0f;

    // Flagged true if theres only one path point.
    protected bool isStationary = false;

    // Flag for if we want object to ignore rotation instructions
    // given by path.
    public bool freeRotation = false;

    // Flag to cycle object back to first position after using all
    // the path points.
    public bool holdInLastPosition = false;

    // Flag to warp object to next location.
    // Defined by setting move duration to 0.
    bool warpFlag = false;

    // Default mode is automatic.
    // Automatic: Object starts movement cycle after delay start time passes.
    // TriggerOnce: Object starts after ball enters trigger and the delay start
    //              time elapses. Completes 1 full cycle then stops.
    public enum Mode
    {
        Automatic = 0,
        TriggerOnce = 1
    };

    public Mode mode = Mode.Automatic;

    float timeToReachNextPoint = 0.0f;
    float timeElapsedSinceLastPoint = 0.0f;
    float endingRotation = 0.0f;
    protected float startingAngle = 0.0f;
    protected bool isMoving = false;
    protected MovementPoint firstPosition;
    protected int firstIndex;
    private MovementPoint currentDestination;
    protected Rigidbody2D movingObject;
    protected SpriteRenderer objectSprite;
    private Vector2 directionOfTravel;
    private float distanceLeft;
    private bool directionChangeFlag = false;
    private int passCounter = 0;

    // Start is called before the first frame update
    protected void Start()
    {
        movingObject = GetComponent<Rigidbody2D>();
        objectSprite = GetComponent<SpriteRenderer>();

        if(startTrigger != null)
        {
            startTrigger.objectsToTrigger.Add(this);
        }

        int pointCount = pathController.PopulatePoints();
        if(pointCount == 1)
        {
            isStationary = true;
            if (!freeRotation)
            {
                currentAngularVelocity = rotationalSpeed;
            }
        }

        firstPosition = pathController.GetPointAtIndex(pathIndex);
        firstIndex = pathIndex;

        // Start the object on the first point of the path.
        movingObject.position = firstPosition.transform.position;
        directionChangeFlag = true;
        startingAngle = movingObject.rotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Is moving if the game isn't paused.
        if (isMoving && !isStationary)
        {
            timeSinceStart += Time.fixedDeltaTime;

            // Delay first movement till after delay time has elapsed.
            if(timeSinceStart > delayStart) { 

                // Keep track of time.
                timeElapsedSinceLastPoint += Time.fixedDeltaTime;

                // This block is entered when moving object hits a point in the path.
                // By default the block maintains a set speed, but speed can be overridden
                // by points in the path, as well as rotational speed. When overridden,
                // the speed of the platform is calculated by the chunk duration.
                // The platform "arrives" when the duration is elapsed. Then the object
                // is locked to the correct ending values.
                if (directionChangeFlag)
                {
                    movingObject.constraints = RigidbodyConstraints2D.None;

                    if(mode == Mode.TriggerOnce 
                        && pathIndex == firstIndex)
                    {
                        passCounter++;
                        if (passCounter > 1)
                        {
                            // If this is the second pass, reset the trigger and finish the movement
                            // cycle.
                            TriggerReset();
                            return;
                        }

                    }

                    // Get starting and ending point of chunk.
                    MovementPoint currentPoint = pathController.GetPointAtIndex(pathIndex);
                    int nextPathIndex = pathController.GetNextIndex(pathIndex);

                    // Check to see if we want to hold at the end and
                    // check to see if the next index will take us back to the beginning.
                    if(holdInLastPosition && nextPathIndex == firstIndex)
                    {
                        // Stop all movement.
                        isMoving = false;
                        currentVelocity = movingObject.velocity = Vector2.zero;
                        return;
                    }

                    pathIndex = nextPathIndex;
                    MovementPoint nextPoint = pathController.GetPointAtIndex(pathIndex);
                    Vector2 nextPath = nextPoint.transform.position - currentPoint.transform.position;
                    bool holdFlag = nextPath.magnitude == 0f;
                
                    // Make sure object is starting in the right location.
                    if (warpFlag)
                    {
                        movingObject.position = currentPoint.transform.position;
                        warpFlag = false;
                    }
                
                    directionOfTravel = (Vector2)nextPoint.transform.position - movingObject.position;
                
                
                    // Default speed.
                    float chunkSpeed = speed;
                    timeToReachNextPoint = directionOfTravel.magnitude / chunkSpeed;
                
                    // Override speed if flag is set.
                    if (currentPoint.overrideSpeed)
                    {
                        float chunkDistance = nextPoint.GetDistanceToPointFromPosition(movingObject.position);
                
                        // Speed is based on duration to next point.
                        timeToReachNextPoint = currentPoint.durationOfMovementToNextPoint;
                        if (timeToReachNextPoint > 0)
                        {
                            chunkSpeed = chunkDistance / timeToReachNextPoint;
                        }
                        else
                        {
                            warpFlag = true;
                            chunkSpeed = 0.0f;
                        }
                    }
                
                    movingObject.velocity = directionOfTravel.normalized * chunkSpeed;
                    if (holdFlag)
                    {
                        movingObject.position = nextPoint.transform.position;
                        movingObject.constraints = RigidbodyConstraints2D.FreezePosition;
                    }
                
                    timeElapsedSinceLastPoint = 0.0f;
                    currentVelocity = movingObject.velocity;
                
                    // If flag to change rotation is set override here.
                    if (currentPoint.overrideRotation && !freeRotation)
                    {
                        // Mark what the ending rotation should be to snap to correct
                        // angle at the end of the path point.
                        endingRotation = movingObject.rotation + currentPoint.degreesToRotate;
                        movingObject.angularVelocity = currentPoint.degreesToRotate / timeToReachNextPoint;
                    }
                    else if (!freeRotation)
                    {
                        // Default rotation.
                        movingObject.angularVelocity = rotationalSpeed;
                        endingRotation = movingObject.rotation + rotationalSpeed * timeToReachNextPoint;
                    }
                    currentAngularVelocity = movingObject.angularVelocity;
                
                    directionChangeFlag = false;
                }
                
                // Used to slow down and end in right location on physics frame.
                distanceLeft = pathController.GetDistanceToNextPoint(movingObject, pathIndex);
                
                // If object will hit destination within the next frame.
                if (timeToReachNextPoint - timeElapsedSinceLastPoint < Time.fixedDeltaTime)
                {
                    // Slow down the speed so that the change of direction will happen on the
                    // next frame.
                    //float stoppingSpeed = distanceLeft / Time.fixedDeltaTime;
                    //movingObject.velocity = movingObject.velocity.normalized * stoppingSpeed;
                    if (!freeRotation)
                    {
                        movingObject.angularVelocity = 0.0f;
                        movingObject.rotation = endingRotation;
                    }
                    directionChangeFlag = true;
                }
                
            }
        }
    }

    /*
     * Changes the color of the sprite of the moving object, used for launchers 
     * to show when launcher is about to fire.
     */ 
    public void ChangeSpriteColor(Color color)
    {
        objectSprite.color = color;
    }

    /*
     * Returns the index of the start of the path the object is currently on.
     */ 
    public int GetCurrentPathEndIndex()
    {
        return pathIndex;
    }

    /*
     * Gets the percentage of the current path the object is on that it has already
     * completed.
     */ 
    public float GetPercentagePathCompletion()
    {
        // If we have not started moving yet, use the remaining delay
        // time to show path completion.
        if(timeSinceStart == 0.0f)
        {
            return 0.0f;
        }
        if(timeSinceStart < delayStart)
        {
            return timeSinceStart / delayStart;
        }

        // Otherwise use standard path completion to show progress.
        return timeElapsedSinceLastPoint / timeToReachNextPoint;
    }

    public void TriggerEnter()
    {
        isMoving = true;
    }

    public void TriggerReset()
    {
        isMoving = false;
        timeSinceStart = 0.0f;
        passCounter = 0;
    }

    public void ResetObject()
    {
        StopMovement();
        currentVelocity = Vector2.zero;
        currentAngularVelocity = 0.0f;
        movingObject.position = firstPosition.transform.position;
        movingObject.rotation = startingAngle;
        pathIndex = firstIndex;
        directionChangeFlag = true;
        timeElapsedSinceLastPoint = 0.0f;
        warpFlag = false;
        timeSinceStart = 0.0f;
        passCounter = 0;
    }

    public void StartMovement(bool defaultWeightless)
    {
        if (defaultWeightless)
        {
            movingObject.gravityScale = 0f;
        }

        if (!freeRotation)
        {
            movingObject.angularVelocity = rotationalSpeed;
        }

        switch (mode)
        {
            case Mode.Automatic:
                isMoving = true;
                break;
            case Mode.TriggerOnce:
                // Dont start movement flag until trigger is entered.
                break;
            default:
                isMoving = true;
                break;
        }
    }

    public void ResumeMovement()
    {
        isMoving = true;
        movingObject.velocity = currentVelocity;
        movingObject.angularVelocity = currentAngularVelocity;
    }

    public void StopMovement()
    {
        isMoving = false;
        movingObject.velocity = Vector2.zero;
        movingObject.angularVelocity = 0.0f;
    }
}
