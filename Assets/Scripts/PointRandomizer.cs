﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointRandomizer : MonoBehaviour
{
    public Vector2 direction = new Vector2(0.0f, 1.0f);
    public float magnitude = 5.0f;
    public float speed = 5.0f;
    Vector2 start;
    bool isMoving = false;
    // Start is called before the first frame update
    void Start()
    {
        start = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isMoving)
        {
            Vector2 position = transform.position;
            position.x += direction.normalized.x * speed * Time.fixedDeltaTime;
            position.y += direction.normalized.y * speed * Time.fixedDeltaTime;
            Vector2 displacement = position - start;
            if (displacement.magnitude > magnitude)
            {
                speed *= -1;
                position = displacement.normalized * magnitude;
            }

            transform.position = position;
        }
    }

    public void StartMovement(bool defaultWeightless)
    {
        isMoving = true;
    }

    public void ResetObject()
    {
        isMoving = false;
        transform.position = start;
    }
}
