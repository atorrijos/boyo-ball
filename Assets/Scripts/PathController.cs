﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathController : MonoBehaviour
{
    private List<MovementPoint> pathPoints;

    /*
     * Called by the parent MoveableObject before the game starts.
     * Return: Number of elements in path list.
     */
    public int PopulatePoints()
    {
        pathPoints = new List<MovementPoint>();
        MovementPoint[] points = transform.GetComponentsInChildren<MovementPoint>();
        for (int i = 0; i < points.Length; i++)
        {
            pathPoints.Add(points[i]);
        }
        if (points.Length == 0)
        {
            //Use the list transfom as the only point if there are no children.
            pathPoints.Add(gameObject.GetComponent<MovementPoint>());
        }

        return pathPoints.Count;
    }

    /*
     * Returns the whole list of points.
     */
    public List<MovementPoint> GetPathPoints()
    {
        return pathPoints;
    }

    /*
     * Signals to the controller that the platform is ready to continue and returns 
     * the next point in the list. Increments the path index.
     */
    public int GetNextIndex(int currentindex)
    {
        currentindex++;
        if(currentindex > pathPoints.Count - 1)
        {
            currentindex = 0;
        }
        return currentindex;
    }

    /* 
     * Returns the point the platform is heading to currently.
     */
    public MovementPoint GetPointAtIndex(int index)
    {
        return pathPoints[index];
    }


    /*
     * Returns point platform is leaving from.
     */
     public int GetPreviousIndex(int index)
    {
        int previousIndex = index - 1;
        if(previousIndex < 0)
        {
            previousIndex = pathPoints.Count - 1;
        }
        return previousIndex;
    }

    /*
     * Returns direction from given rigidbody to the current point.
     * movingObject: Rigidbody of the object heading to the current point.
     */
    public Vector2 GetDirectionToNextPoint(Rigidbody2D movingObject, int index)
    {
        Vector2 pointPosition = pathPoints[index].transform.position;
        Vector2 directionVector = pointPosition - movingObject.position;
        return directionVector;
    }

    /*
     * Returns distance from given rigidbody to the current point.
     * movingObject: Rigidbody of the object heading to the current point.
     */
    public float GetDistanceToNextPoint(Rigidbody2D movingObject, int index)
    {
        Vector2 distanceVector = GetDirectionToNextPoint(movingObject, index);
        return distanceVector.magnitude;
    }

    /*
     * Sets the path index to the starting position.
     * Returns the transform of the first point.
     */
    public MovementPoint GetStartingPoint()
    {
        return pathPoints[0];
    }

    /*
     * Used to have moveable objects read only from path point speed values.
     */
    public void OverrideSpeedAll()
    {
        foreach(MovementPoint m in pathPoints)
        {
            m.overrideSpeed = true;
        }
    }

    /*
     * Used to modify path duration from the outside.
     */
     public void SetDurationForPoint(int index, float duration)
    {
        pathPoints[index].durationOfMovementToNextPoint = duration;
    }
}
