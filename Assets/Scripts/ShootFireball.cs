﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFireball : MonoBehaviour
{
    public FireballShooter shooter;
    public List<int> shootersToTrigger;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ball")
        {
            foreach (int i in shootersToTrigger)
            {
                shooter.SpawnFireball(i);
            }
        }
    }
}
