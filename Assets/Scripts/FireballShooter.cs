﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballShooter : MonoBehaviour
{
    public List<Transform> fireballSpawns = new List<Transform>();
    public List<GameObject> spawnedFireballs = new List<GameObject>();
    public GameObject fireballPrefab;
    int fireballNumber = 0;
    
    public void SpawnFireball(int index)
    {
        Transform position = fireballSpawns[index];
        GameObject bullet = Instantiate(fireballPrefab, position.position, Quaternion.identity);
        spawnedFireballs.Add(bullet);
        bullet.name = "FireballBullet" + fireballNumber;
        fireballNumber++;
    }

    public void OnReset()
    {
        foreach (GameObject b in spawnedFireballs)
        {
            Destroy(b);
        }
    }
}
