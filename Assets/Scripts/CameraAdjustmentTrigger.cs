﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAdjustmentTrigger : MonoBehaviour
{
    public Vector2 cameraPositionAdjustment = new Vector2(0.0f,0.0f);
    public SmoothFollow smoothCamera;

    private void Start()
    {
        smoothCamera = Camera.main.transform.GetComponent<SmoothFollow>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ball")
        {
            smoothCamera.SetCameraAdjustment(cameraPositionAdjustment);
        }
    }
}
