﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuAnimation : MonoBehaviour
{
    float height;
    float width;
    float xDistance;
    float yDistance;
    float yBigDistance;

    public Sprite ballHat;
    public Sprite ballNoHat;
    Image currentImage;

    // Start is called before the first frame update
    void Start()
    {
        currentImage = GetComponent<Image>();
        height = Screen.height;
        width = Screen.width;
        xDistance = width * 0.3f;
        yDistance = height * 0.1f;
        yBigDistance = height * 0.15f;

        SetHat(DataControl.control.IsHatEnabled());
        AnimationLoop();
    }

    public void SetHat(bool haveHat)
    {
        if (haveHat)
        {
            currentImage.sprite = ballHat;
            return;
        }
        currentImage.sprite = ballNoHat;
    }

    public void ToggleHat()
    {
        DataControl.control.ToggleHat();
        SetHat(DataControl.control.IsHatEnabled());
    }

    void AnimationLoop()
    {
        LeanTween.moveY(gameObject, transform.position.y + yDistance, 1.0f).setEase(LeanTweenType.easeInBounce).setLoopPingPong(2);
        LeanTween.moveX(gameObject, transform.position.x + xDistance, 1.0f).setEase(LeanTweenType.easeInOutCubic).setLoopPingPong(1).setDelay(4f);
        LeanTween.rotateAround(gameObject, Vector3.back, 180f, 1.0f).setEase(LeanTweenType.easeInOutCubic).setLoopPingPong(1).setDelay(4f);
        LeanTween.moveY(gameObject, transform.position.y + yDistance, 1.0f).setEase(LeanTweenType.easeInBounce).setLoopPingPong(1).setDelay(6f);
        LeanTween.moveX(gameObject, transform.position.x - xDistance, 1.0f).setEase(LeanTweenType.easeInOutCubic).setDelay(8f);
        LeanTween.rotateAround(gameObject, Vector3.back, -180f, 1.0f).setEase(LeanTweenType.easeInOutCubic).setDelay(8f);
        LeanTween.moveY(gameObject, transform.position.y + yBigDistance, 1.5f).setEase(LeanTweenType.easeInBounce).setLoopPingPong(1).setDelay(9f);
        LeanTween.moveX(gameObject, transform.position.x + xDistance, 3f).setEase(LeanTweenType.easeInOutCubic).setDelay(9f);
        LeanTween.rotateAround(gameObject, Vector3.back, 360f, 3f).setEase(LeanTweenType.easeInOutCubic).setDelay(9f);
        LeanTween.moveX(gameObject, transform.position.x, 1f).setEase(LeanTweenType.easeInOutCubic).setDelay(12f);
        LeanTween.rotateAround(gameObject, Vector3.back, -180f, 1.0f).setEase(LeanTweenType.easeInOutCubic).setDelay(12f).setOnComplete(AnimationLoop);
    }
}
