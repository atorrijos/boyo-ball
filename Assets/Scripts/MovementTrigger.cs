﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface StartTrigger
{
    void TriggerEnter();
    void TriggerReset();
}

public class MovementTrigger : MonoBehaviour
{
    public List<StartTrigger> objectsToTrigger = new List<StartTrigger>();
    Collider2D trigger;

    // Start is called before the first frame update
    void Start()
    {
        trigger = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Ball")
        {
            foreach (StartTrigger mo in objectsToTrigger)
            {
                mo.TriggerEnter();
            }
        }
    }
    
}
