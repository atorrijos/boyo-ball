﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**************************************
 * Control for ball physics and logic.*
 **************************************/
public class BallControl : MonoBehaviour
{
    // Game Control.
    GameControl gameControl;

    // Rigidbody of the ball.
    Rigidbody2D ballBody;
    Collider2D ballCollider;

    // SpriteRenderer of the ball.
    SpriteRenderer ballSprite;

    // Used to hold the place of one collider which intersects with
    // the squish trigger. 1 trigger can enter with no harm to the ball,
    // but if the space is already occupied then the ball is squished.
    private Collider2D colliderBuffer;

    // Object Ball is stuck to when it makes contact 
    // with a stickable object and its spikes are on.
    Rigidbody2D stuckObjectBody;
    FixedJoint2D stickJoint;

    // Renderer of the spikes.
    SpriteRenderer spikesSprite;
    Animator spikeAnimator;

    // Particle system that emits particles on a bounce.
    ParticleSystem bounceParticles;

    // Booster object.
    Booster booster;
    Transform boosterCenter;

    // Variable to test for spikes.
    bool spikesOn = false;
    bool isStuck = false;

    // Variable for MidairBoost.
    bool hasMidairBoost = true;

    // Flag for zero G
    public bool isWeightless = false;

    // Maximum speed after bouncing off a wall.
    public float maxBounceSpeed = 25.0f;
    float minimumSpeedToBounce = 5f;
    float minimumSpeedForBumpSoundEffect = 1f;

    // Vector to hold input before launch. Used to Help camera predict where
    // to focus.
    Vector2 launchVector = new Vector2(0.0f, 0.0f);
    float launchPercentage = 0.0f;

    // The maximum value the ball can be launched from drag input.
    public float maxStationaryLaunchSpeed = 18.0f;
    public float doubleJumpBoostFactor = 0.5f;

    // Maximum angle to generate a bounce in degrees.
    // 90 Degrees is a tangent collision.
    float bounceAngleThreshold = 75.0f;
    float soundAngleThreshold = 85.0f;

    // Default ball values.
    float bounciness = 0.5f;
    float friction = 0.1f;

    // Flag to show a bounce happened recently.
    bool isBouncing = false;

    // Variables to control how long gravity is turned off during a bounce.
    float timeSinceLastBounce = 0.0f;
    float bounceDuration = 0.5f;
    float spikesOffBounceDelay= 0.05f;
    float timeSinceSpikesOff;

    // Variables to control how long after a launch can the ball stick again.
    float stickGracePeriod = 0.1f;
    float timeSinceLastLaunch;

    // Amount of time before collision to trigger bounce.
    public float bounceWindow = 0.2f;
    public float bounceBoostFactor = 1.25f;

    // Physics Materials to Switch Between
    PhysicsMaterial2D ballMat;
    PhysicsMaterial2D spikyBallMat;

    // Starting position of ball.
    public Vector2 startingPosition;

    // Particle system that plays on deaths.
    public GameObject deathSparks;
    GameObject deathSparksClone;

    // Fields for audio functionality/sound effects.
    AudioSource[] audioSources;
    const int SFX_MAIN = 0;
    public AudioClip audioClipNoBounce;
    public AudioClip audioClipBounce;
    public AudioClip audioClipSpikesStick;
    public AudioClip audioClipSpikesNonstick;
    public AudioClip audioClipBoost;
    public AudioClip audioClipLaunch;

    // --- Standard Functions ---

    // Setup operations.
    void Start()
    {
        gameControl = transform.parent.GetComponent<GameControl>();
        ballBody = transform.GetComponent<Rigidbody2D>();
        ballSprite = transform.GetComponent<SpriteRenderer>();
        stickJoint = transform.GetComponent<FixedJoint2D>();
        boosterCenter = transform.Find("BoosterCenter");
        booster = boosterCenter.Find("Booster").GetComponent<Booster>();

        audioSources = GetComponents<AudioSource>();

        // Pause state.
        ballBody.isKinematic = true;
        ballBody.useFullKinematicContacts = true;
        startingPosition = ballBody.position;

        ballCollider = transform.GetComponent<Collider2D>();
        Transform spikeTransform = transform.Find("Spikes");
        spikesSprite = spikeTransform.GetComponent<SpriteRenderer>();
        spikeAnimator = spikeTransform.GetComponent<Animator>();
        bounceParticles = transform.GetComponent<ParticleSystem>();
        timeSinceLastLaunch = stickGracePeriod;
        timeSinceLastLaunch = bounceDuration;
        ballMat = (PhysicsMaterial2D) Resources.Load("Materials/Ball");
        spikyBallMat = (PhysicsMaterial2D)Resources.Load("Materials/SpikyBall");

        // Set the delay to equal one frame.
        spikesOffBounceDelay = Time.fixedDeltaTime;
        timeSinceSpikesOff = spikesOffBounceDelay;

        // Put on hat if available.
        SetHat(DataControl.control.IsHatEnabled());

        if (isWeightless)
        {
            ballBody.gravityScale = 0f;
        }
    }

    // Update on every physics frame.
    private void FixedUpdate()
    {
        timeSinceLastLaunch += Time.deltaTime;
        timeSinceLastBounce += Time.deltaTime;
        timeSinceSpikesOff += Time.deltaTime;
        if (isBouncing)
        {
            // Turn off bounce mode if the bounce duration has elapsed.
            if(timeSinceLastBounce > bounceDuration)
            {
                isBouncing = false;
                if (!isWeightless)
                {
                    ballBody.gravityScale = 1.0f;
                }
            }
        }
    }

    // --- Ball Bounce Functions ---

    /* 
     * Checks to make sure speed is above the threshold to bounce.
     */
    public void AttemptBounce(Collision2D collision, bool hasBounceAngle)
    {
        bool spikesOffDelayTimePassed = timeSinceSpikesOff > spikesOffBounceDelay;
        float relativeBallSpeed = collision.relativeVelocity.magnitude;

        if (spikesOffDelayTimePassed)
        {
            if (hasBounceAngle && relativeBallSpeed > minimumSpeedToBounce)
            {
                Bounce();
            }
            else
            {
                float volumeModifier = 0.7f;
                audioSources[SFX_MAIN].PlayOneShot(audioClipNoBounce, volumeModifier);
            }
        }
    }

    /*
     * Speeds up the ball and reduces gravity to allow ball to bounce farther
     * and higher than it normally should. This is triggered when player
     * correctly predicts an object collision.
     */
    void Bounce()
    {
        // Turn off gravity.
        ballBody.gravityScale = 0.0f;

        // Speed up ball.
        Vector2 bounceBoostDirection = ballBody.velocity.normalized;
        float newSpeed = ballBody.velocity.magnitude * bounceBoostFactor;

        // Cap ball speed at the max bounce speed.
        if (newSpeed > maxBounceSpeed)
        {
            newSpeed = maxBounceSpeed;
        }
        bounceBoostDirection *= newSpeed;
        ballBody.velocity = bounceBoostDirection;

        // Start a timer to prevent multiple bounces.
        timeSinceLastBounce = 0.0f;
        isBouncing = true;

        // Emit particles to show a bounce.
        bounceParticles.Play();
        audioSources[SFX_MAIN].PlayOneShot(audioClipBounce);

        RefreshBoost();
    }

    // --- Ball Check functions ---

    /*
     * Function to check if a collider entering the squish trigger will
     * trigger a kill. Called by the squish trigger. Assumes collision with solid
     * object.
     */ 
    public void CheckSquish(Collider2D collider)
    {

            if (colliderBuffer == null)
            {
                colliderBuffer = collider;

                // Prevents scenario where ball passes through objects when stuck to others.
                if (isStuck)
                {
                    UnstickFromObject();
                }
            }
            else
            {
                Debug.Log("Squished by " + collider + " and " + colliderBuffer);
                colliderBuffer = null;
                KillBall();
            }
    }

    public void CheckClearSquish(Collider2D collider)
    {
        if (colliderBuffer != null &&
            colliderBuffer == collider)
        {
            colliderBuffer = null;
        }
    }

    /*
     * Returns the collider of an object that the ball can stick to.
     */
    public Collider2D GetFirstStickableSurfaceInContact()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.6f);

        for (int i = 0; i < colliders.Length; i++)
        {
            Transform colliderTransform = colliders[i].transform;

            // Ignore the ball collider and colliders that serve as triggers.
            if (colliderTransform.name != "Ball" &&
                colliders[i].tag == "Stick" &&
                !colliders[i].isTrigger)
            {
                return colliders[i];
            }
        }
        return null;
    }

    /*
     * Check to see if ball is grounded.
     */
    public bool IsGrounded()
    {
        return CheckContact(Vector2.down);
    }

    /*
     * Check to see if the ball connects to an object in the given drection.
     */
    public bool CheckContact(Vector2 direction)
    {
        // Circle used in cast is slightly smaller than the player ball.
        float ballCheckRadius = 0.45f;
        float checkDistance = 0.1f;
        RaycastHit2D[] raycasts = Physics2D.CircleCastAll(transform.position, ballCheckRadius, direction, checkDistance);

        for (int i = 0; i < raycasts.Length; i++)
        {
            //Ignore ball collider and triggers.
            if (raycasts[i].transform.name != "Ball" &&
                !raycasts[i].collider.isTrigger)
            {
                return true;
            }
        }
        return false;
    }

    /*
     * Check to see if the ball is touching a wall.
     */     
    public bool HasHorizontalContact()
    {
        return CheckContact(Vector2.left) || CheckContact(Vector2.right);
    }

    /*
     * Returns whether or not the ball is touching another surface.
     */
    public bool IsTouchingSurface()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.6f);
        for (int i = 0; i < colliders.Length; i++)
        {
            Transform colliderTransform = colliders[i].transform;

            if (colliderTransform.name != "Ball" &&
                !colliders[i].isTrigger)
            {
                return true;
            }
        }
        return false;
    }

    /*
     * Checks to see if the ball is about to collide with another object.
     * t: Amount of time to predict for in seconds.
     */
    public bool WillCollideWithSurfaceInTime(float t)
    {
        // We dont want bounces to happen off of tiny hops.
        if (ballBody.velocity.magnitude < 1.0f)
        {
            return false;
        }

        float distanceTraveledInTSeconds = ballBody.velocity.magnitude * t;

        RaycastHit2D[] raycasts = Physics2D.CircleCastAll(transform.position, 0.5f, ballBody.velocity, distanceTraveledInTSeconds);

        for (int i = 0; i < raycasts.Length; i++)
        {
            //Ignore ball collider and triggers.
            if (raycasts[i].transform.name != "Ball" &&
                !raycasts[i].collider.isTrigger)
            {
                return true;
            }
        }
        return false;
    }

    // --- Ball Death Functions ---

    /*
     * Called when ball touches something that should kill it.
     */
    public void KillBall()
    {
        StopSpikes();
        SpawnDeathSparks();
        colliderBuffer = null;
        booster.ResetBoosterTrigger();
        ballBody.gameObject.SetActive(false);
        gameControl.ResetLevel(1.0f);
    }

    /*
     * Reset values for Ball. Expectation is that ball returns to how it was at
     * the start of the level.
     */
    public void ResetObject()
    {
        ballBody.gameObject.SetActive(true);
        RefreshBoost();
        ballBody.isKinematic = true;
        ballBody.velocity = Vector2.zero;
        ballBody.angularVelocity = 0.0f;
        ballBody.position = startingPosition;


        if (!isWeightless)
        {
            ballBody.gravityScale = 1f;
        }
    }

    /*
     * Spawn the objects needed to show a death.
     */
    public void SpawnDeathSparks()
    {
        // Destroy previous sparks if they exist.
        if (deathSparksClone)
        {
            GameObject.Destroy(deathSparksClone);
        }
        deathSparksClone = Instantiate(deathSparks, ballBody.position, Quaternion.identity);
        deathSparksClone.GetComponent<ParticleSystem>().Play();
    }

    /*
     * Called at the start of the level and on respawns to allow
     * the ball to begin movement.
     */
    public void StartMovement(bool defaultWeightless)
    {
        if (defaultWeightless)
        {
            ballBody.gravityScale = 0f;
            isWeightless = true;
        }
        ballBody.isKinematic = false;
    }

    // --- Ball Launch Functions ---

    /*
     * Function called to set the vector to launch the ball.
     * also used to give information to the camera to follow where
     * the ball will be launched.
     * launchV: Direction to launch the ball when input is released.
     * percentage: Percentage of max launch velocity.
     */
    public void SetLaunchVector(Vector2 launchV, float percentage)
    {
        float launchSpeed = percentage * maxStationaryLaunchSpeed;
        launchVector = launchV.normalized * launchSpeed;
        launchPercentage = percentage;
    }

    /*
     * Function called when a launch is attempted.
     * Checks to see if a launch is okay before launching.
     * v: Direction to launch ball in.
     */
    public void CheckAndLaunch()
    {
        if (isStuck)
        {
            StopSpikes();
            LaunchBall(launchVector);
            return;
        }

        StopSpikes();
        // Dont launch the ball if its in the middle of a bounce.
        if (!isBouncing)
        {
            // If ball is weightless any contact enables launch.
            if (isWeightless)
            {
                if (IsTouchingSurface())
                {
                    LaunchBall(launchVector);
                    return;
                }
            }
            else
            {
                //Only launch ball if ball is in contact with a surface.
                if (IsGrounded())
                {
                    LaunchBall(launchVector);
                    return;
                }

                if (HasHorizontalContact())
                {
                    float horizontalPercentage = Mathf.Abs(launchVector.normalized.x);
                    Vector2 wallLaunchVector = new Vector2();

                    float wallJumpMag = horizontalPercentage * launchVector.magnitude;

                    // Power of the wall launch depends on the amount of horizontal component
                    // involved. Eg. straight up or down wouldnt have any power normally.
                    // However in this case the game does a check to see if a midair boost,
                    // if available would be stronger. In this case it's used.
                    wallLaunchVector = launchVector.normalized * wallJumpMag;

                    float midairBoostMag = (launchVector * doubleJumpBoostFactor).magnitude;
                    

                    if (!hasMidairBoost || wallJumpMag > midairBoostMag)
                    {
                        LaunchBall(wallLaunchVector);
                        return;
                    }
                }
            }
        }

        // If ball has made it to this point, it will attempt a double jump.
        if (hasMidairBoost && launchPercentage > 0.1f)
        {
            launchVector *= doubleJumpBoostFactor;
            MidairBoost(launchVector);
        }
    }

    /*
     * Launch the ball in a given direction.
     * v: Direction to launch ball in.
     */
    public void LaunchBall(Vector2 v)
    {
        audioSources[SFX_MAIN].PlayOneShot(audioClipLaunch, launchPercentage);
        if (isStuck)
        {
            UnstickFromObject();
        }
        ballBody.velocity += v;
        timeSinceLastLaunch = 0.0f;
    }
    
    /*
     * Launch ball midair at a weaker velocity than a normal
     * launch.
     */
    public void MidairBoost(Vector2 v)
    {
        float degrees = Vector2.Angle(Vector2.down, v);
        if(v.x < 0.0f)
        {
            degrees *= -1;
        }
        boosterCenter.eulerAngles = new Vector3(0.0f, 0.0f, degrees);

        audioSources[SFX_MAIN].PlayOneShot(audioClipBoost);
        booster.StartBoost();
        ballBody.velocity += v;
        ballSprite.color = new Color(0.8f, 0.8f, 0.8f);
        hasMidairBoost = false;
    }

    public void RefreshBoost()
    {
        hasMidairBoost = true;
        ballSprite.color = Color.white;
    }

    // --- Ball Spike Functions ---

    /*
     * Refresh the balls collider to enable friction and bounciness changes.
     */
    void RefreshCollider()
    {
        ballCollider.enabled = false;
        ballCollider.enabled = true;
    }

    /*
     * Turn hat off and on.
     */
     public void SetHat(bool enable)
    {
        GameObject hat = transform.Find("Hat").gameObject;
        hat.SetActive(enable);
    }

    /*
     * Enable sticking spikes.
     */
    public void StartSpikes()
    {
        spikeAnimator.SetBool("spikesOn", true);

        //spikesSprite.enabled = true;
        spikesOn = true;

        // The ball with spikes out shouldn't bounce and have a lot of friction.
        ballBody.sharedMaterial.friction = 1f;
        ballBody.sharedMaterial.bounciness = 0f;

        // Refresh collider to set changes.
        RefreshCollider();

        // We want ball to freeze rotation to show the change.
        ballBody.freezeRotation = true;

        Collider2D objectStuckTo = GetFirstStickableSurfaceInContact();

        // Checkt to see if there are stickable surfaces in range and if so
        // stop the ball.
        if (objectStuckTo)
        {
            StickToObject(objectStuckTo);
        }
    }

    /*
     * Disable sticking spikes.
     */
    public void StopSpikes()
    {
        timeSinceSpikesOff = 0.0f;

        // Enable rotation.
        ballBody.freezeRotation = false;

        // Reset ball to normal values of friction and bounciness.
        ballBody.sharedMaterial.friction = friction;
        ballBody.sharedMaterial.bounciness = bounciness;

        // Refresh collider to set changes.
        RefreshCollider();

        spikeAnimator.SetBool("spikesOn", false);
        //spikesSprite.enabled = false;
        spikesOn = false;

        // Only unstick if already stuck. Otherwise we could incorrectly reset 
        // velocity in the air.
        if (isStuck)
        {
            UnstickFromObject();
        }
    }


    /*
     * Stop ball in place to stick it to an object. Childs the ball to
     * the objects transform so it moves with it.
     */
    void StickToObject(Collider2D objectStuckTo)
    {
        // If we trigger a stick in the middle of a bounce. Disregard the bounce.
        isBouncing = false;

        // Rigidbody of the object ball is stuck to.
        stuckObjectBody = objectStuckTo.GetComponent<Rigidbody2D>();

        // Take the ball out of physics.
        if (!isWeightless)
        {
            ballBody.gravityScale = 1.0f;
        }

        isStuck = true;
        // Disable rotational freeze to allow object to react to ball collision.
        ballBody.freezeRotation = false;
        stickJoint.connectedBody = stuckObjectBody;
        stickJoint.enabled = true;

        audioSources[SFX_MAIN].PlayOneShot(audioClipSpikesStick);

        // Refresh midair boost.
        RefreshBoost();
    }

    /*
     * Allow ball to move again once its done being stuck. Returns ball to
     * world space.
     */
    void UnstickFromObject()
    {
        //ballBody.transform.parent = gameControl.transform;
        stickJoint.enabled = false;
        stickJoint.connectedBody = null;

        // Return ball to physics.
        //ballBody.isKinematic = false;
        isStuck = false;

        // Match stuck objects velocity so object wont roll off moving platforms.
        //ballBody.velocity = stuckObjectBody.velocity;
    }

    // --- Collision Functions ---

    /*
     * Check triggers.
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Death")
        {
            KillBall();
            return;
        }

        // Collision with endzone. Ball "goes in".
        if (collision.tag == "Finish")
        {
            ballBody.gameObject.SetActive(false);
        }
    }

    /*
     * Check for bounces or sticks on collision.
     */
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (isWeightless)
        {
            RefreshBoost();
        }

        if (isStuck)
        {
            if (collision.rigidbody != stuckObjectBody)
            {
                UnstickFromObject();
            }
        }

        // Kill the ball if collided with a death object.
        if (collision.collider.tag == "Death")
        {
            KillBall();
            return;
        }

        if (IsGrounded())
        {
            // Refresh midairboost when it hits the ground.
            RefreshBoost();
        }

        bool hasBounceAngle = HasBounceAngle(collision);
        bool hasSoundAngle = HasSoundAngle(collision);
        float relativeBallSpeed = collision.relativeVelocity.magnitude;
        bool isFastEnoughToMakeSound = relativeBallSpeed > minimumSpeedForBumpSoundEffect;
        bool makeContactSound = hasSoundAngle && isFastEnoughToMakeSound;

        // Stick to a sticky object if spikes are on.
        if (spikesOn)
        {
            if ((collision.collider.tag == "Stick") &&
                (timeSinceLastLaunch > stickGracePeriod))
            {
                StickToObject(collision.collider);
            }
            else if (makeContactSound)
            {
                float volumeModifier = 0.9f;
                audioSources[SFX_MAIN].PlayOneShot(audioClipSpikesNonstick, volumeModifier);
            }
        }
        else if(makeContactSound)
        {
            AttemptBounce(collision,hasBounceAngle);
        }
    }

    public bool HasBounceAngle(Collision2D collision)
    {
        ContactPoint2D[] points = new ContactPoint2D[collision.contactCount];
        collision.GetContacts(points);

        for (int i = 0; i < points.Length; i++)
        {
            float collisionAngle = Vector2.Angle(points[i].normal, ballBody.velocity.normalized);
            if (collisionAngle < bounceAngleThreshold)
            {
                return true;
            }
        }

        return false;
    }

    public bool HasSoundAngle(Collision2D collision)
    {
        ContactPoint2D[] points = new ContactPoint2D[collision.contactCount];
        collision.GetContacts(points);

        for (int i = 0; i < points.Length; i++)
        {
            float collisionAngle = Vector2.Angle(points[i].normal, ballBody.velocity.normalized);
            if (collisionAngle < soundAngleThreshold)
            {
                return true;
            }
        }

        return false;
    }
    // ---- Outside access functions ----

    public Vector2 GetBallPosition()
    {
        return ballBody.position;
    }

    public Vector2 GetBallVelocity()
    {
        return ballBody.velocity;
    }

    public Vector2 GetLaunchVector()
    {
        return launchVector;
    }

    public float GetLaunchPercentage()
    {
        return launchPercentage;
    }
}
