﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherRow : MonoBehaviour
{
    Launcher[] launchers;
    public float defaultLaunchDuration = 0.25f;
    public float defaultResetTimes = 0.5f;
    public float defaultRestInterval = 0.5f;
    public float maxDelay = 5.0f;
    public float minDelay = 0.0f;
    public int randomDelaySeed = 1337;
    public enum Mode
    {
        Random = 0,
        Order = 1,
        Reverse = 2
    };
    public Mode mode = Mode.Random;

    // Start is called before the first frame update
    void Start()
    {
        launchers = GetComponentsInChildren<Launcher>();

        // Make sure launchers dont overwrite changes.
        foreach(Launcher l in launchers)
        {
            l.inhereitValuesFromParent = true;
        }
    }

    /*
     * Called by the game to initiate all launcher values
     * before movement starts.
     */
    public void GenerateValues()
    {
        OverridePathSpeeds();
        SetAllLaunchDurations(defaultLaunchDuration);
        SetAllResetTimes(defaultResetTimes);
        SetAllRestIntervals(defaultRestInterval);
        SetLaunchDelays(minDelay, maxDelay, randomDelaySeed);
    }

    void SetAllLaunchDurations(float duration)
    {
        foreach(Launcher l in launchers)
        {
            l.UpdateLaunchDuration(duration);
        }
    }

    void SetAllResetTimes(float time)
    {
        foreach(Launcher l in launchers)
        {
            l.UpdateResetTime(time);
        }
    }

    void SetAllRestIntervals(float time)
    {
        foreach (Launcher l in launchers)
        {
            l.UpdateRestInterval(time);
        }
    }

    void SetLaunchDelays(float minDelay, float maxDelay, int delaySeed)
    {
        float totalDelaySpan = maxDelay - minDelay;
        float delayInterval = totalDelaySpan / (float)(launchers.Length - 1);
        switch (mode)
        {
            default:
            case Mode.Random:
                Random.InitState(delaySeed);
                foreach (Launcher l in launchers)
                {
                    float delay = Random.Range(minDelay, maxDelay);
                    l.UpdateLauncherDelay(delay);
                }
                break;
            case Mode.Order:
                for(int i = 0; i <launchers.Length; i++)
                {
                    float delay = minDelay + delayInterval * (float)i;
                    launchers[i].UpdateLauncherDelay(delay);
                }
                break;
            case Mode.Reverse:
                for (int i = 0; i < launchers.Length; i++)
                {
                    float delay = minDelay + delayInterval * (float)(launchers.Length - i - 1);
                    launchers[i].UpdateLauncherDelay(delay);
                }
                break;
        }
        
    }

    void OverridePathSpeeds()
    {
        foreach(Launcher l in launchers)
        {
            l.OverridePathSpeeds();
        }
    }

    public void SetMode(Mode m)
    {
        mode = m;
    }
}
