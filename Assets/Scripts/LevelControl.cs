﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LevelControl : MonoBehaviour
{
    public static LevelControl control;

    // Reference to GameControl running game.
    GameControl game;

    // Canvas used for UI.
    Canvas canvas;
    float canvasWidth;
    string sceneName;

    // Member variables to keep track of current playthrough
    // progression.
    LevelID currentLevel;

    // Text for level text display.
    Text levelText;
    Text lastLevelText;
    Text winText;
    GameObject levelTitle;
    Text levelTitleText;
    Text levelTitleLevelText;

    // Banner for winning the level.
    RectTransform winBanner;
    public float bannerSpeed = 2.0f;
    float bannerProgress = 0.0f;

    // Time between playing the banner and moving on to the next scene.
    public float bannerDelay = 1.5f;
    Vector2 bannerPosition;
    Vector2 startingBannerPosition;

    bool playBanner = false;
    bool gameFinished = false;

    public bool debugForceCurrentLevelID = false;
    public LevelID levelId;

    // Buttons for sound control.
    Button musicButton;
    Button sfxButton;
    public Sprite musicOn;
    public Sprite musicOff;
    public Sprite sfxOn;
    public Sprite sfxOff;
    bool musicPlaying = true;
    bool sfxPlaying = true;

    // Start is called before the first frame update
    void Awake()
    {
        if(control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }    
        else if(control != this)
        {
            Destroy(gameObject);
        }

        // Set the level text UI.
        canvas = transform.Find("Canvas").GetComponent<Canvas>();

        Transform levelTextTransform = canvas.transform.Find("LevelText");
        Transform winTextTransform = canvas.transform.Find("WinBanner").Find("WinText");
        levelText = levelTextTransform.GetComponent<Text>();
        winText = winTextTransform.GetComponent<Text>();

        levelTitle = canvas.transform.Find("LevelTitle").gameObject;
        levelTitleText = levelTitle.transform.Find("LevelTitleText").GetComponent<Text>();
        levelTitleLevelText = levelTitle.transform.Find("LevelText").GetComponent<Text>();

        musicButton = canvas.transform.Find("MusicButton").GetComponent<Button>();
        sfxButton = canvas.transform.Find("SfxButton").GetComponent<Button>();

        SetupUI();
    }

    void Start()
    {
        // Set up the win banner for level completion.
        winBanner = canvas.transform.Find("WinBanner").GetComponent<RectTransform>();
        RectTransform canvasRect = canvas.GetComponent<RectTransform>();
        canvasWidth = canvasRect.sizeDelta.x;
        bannerPosition = new Vector2(canvasWidth, 0.0f);
        startingBannerPosition = bannerPosition;

        musicPlaying = !DataControl.control.GetMusicMutedPref();
        if (musicPlaying)
        {
            musicButton.image.sprite = musicOn;
        }
        else
        {
            musicButton.image.sprite = musicOff;
        }

        sfxPlaying = !DataControl.control.GetSfxMutedPref();
        if (sfxPlaying)
        {
            sfxButton.image.sprite = sfxOn;
        }
        else
        {
            sfxButton.image.sprite = sfxOff;
        }

        winBanner.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, canvasWidth);
        SetupBanner();
        UpdateLevelAndMusic();
    }

    void UpdateLevelAndMusic()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
        string[] levelParts = sceneName.Split('_');

        // Names are 1 indexed data is 0 indexed.
        int stage = int.Parse(levelParts[0]) - 1;
        int section = int.Parse(levelParts[1]) - 1;

        currentLevel = new LevelID(stage, section);
        PlayMusicForCurrentLevel();
    }

    public void PlayMusicForCurrentLevel()
    {
        MusicControl.control.PlayMusicForLevel(currentLevel);
    }

    // Update is called once per frame
    void Update()
    {
        if (playBanner)
        {
            bannerProgress += Time.deltaTime;

            // Banner should reach center within a half a second given a banner
            // speed of 2.
            bannerPosition.x = canvasWidth - (Mathf.Clamp(bannerProgress * bannerSpeed, 0.0f, 1.0f) * canvasWidth);
            winBanner.anchoredPosition = bannerPosition;

            if (bannerProgress > bannerDelay && !gameFinished)
            {
                gameFinished = true;
                playBanner = false;
                CompleteLevel();
            }

            return;
        }
    }

    void FixedUpdate()
    {
        
    }

    /*
    * Load a scene.
    */
    IEnumerator AsyncSceneLoad(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        if (sceneName == "StartMenu")
        {
            Destroy(gameObject);
            yield break;
        }
        RefreshUI();
        gameFinished = false;
        UpdateLevelAndMusic();
        yield break;
    }

    /*
     * Called when a level is completed, starts loading the next level.
     */
    public void CompleteLevel()
    {
        Tuple<LevelID, string> nextlevel =
            DataControl.control.LevelCompleted(currentLevel.levelStage, currentLevel.levelSection);
        currentLevel = nextlevel.Item1;
        DataControl.control.Save();
        StartCoroutine(AsyncSceneLoad(nextlevel.Item2));
    }

    /*
     * Play the banner that comes onto the screen showing the player has completed
     * a level.
     */
    public void PlayWinBanner()
    {
        winText.text = "You are\na win.";
        playBanner = true;
    }

    public void PlayFinalBanner()
    {
        winText.text = "You are\na BIG win.";
        StartCoroutine(WaitThenPlayBanner(4.0f));
    }

    /*
     * Hide title card shown before play.
     */ 
    public void HideTitle()
    {
        levelTitle.SetActive(false);
    }

    public void ShowTitle()
    {
        levelTitle.SetActive(true);
    }

    /*
     * Used to delay Banner playback, e.g. for final level.
     */
    IEnumerator WaitThenPlayBanner(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        playBanner = true;
    }

    public void SetupBanner()
    {
        playBanner = false;
        bannerProgress = 0.0f;
        bannerPosition = startingBannerPosition;
        if (winBanner)
        {
            winBanner.anchoredPosition = bannerPosition;
        }
    }

    public void SetupUI()
    {
        levelTitle.SetActive(true);
        sceneName = SceneManager.GetActiveScene().name;
        string levelTextString = "Level " + sceneName;
        levelText.text = levelTextString;
        levelTitleLevelText.text = levelTextString;

        // Fit UI to play area.
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = Camera.main;
        canvas.sortingLayerName = "UI";
    }

    public void SetLevelTitle(string title)
    {
        levelTitleText.text = title;
    }

    public void RefreshUI()
    {
        SetupBanner();
        SetupUI();
    }

    public void SetGameControl(GameControl gc)
    {
        game = gc;
    }

    public void ResetButtonOnClick()
    {
        game.ResetLevel(0.0f);
    }

    public void ReturnButtonOnClick()
    {
        // Saves mute preferences.
        DataControl.control.Save();
        StartCoroutine(AsyncSceneLoad("StartMenu"));
    }

    public void ToggleMusic()
    {
        musicPlaying = !musicPlaying;
        if (musicPlaying)
        {
            MusicControl.control.EnableMusic();
            musicButton.image.sprite = musicOn;
        }
        else
        {
            MusicControl.control.MuteMusic();
            musicButton.image.sprite = musicOff;
        }
    }

    public void ToggleSfx()
    {
        sfxPlaying = !sfxPlaying;
        if (sfxPlaying)
        {
            MusicControl.control.EnableSfx();
            sfxButton.image.sprite = sfxOn;
        }
        else
        {
            MusicControl.control.MuteSfx();
            sfxButton.image.sprite = sfxOff;
        }
    }

}

