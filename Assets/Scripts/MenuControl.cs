﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class MenuControl : MonoBehaviour
{
    // Same variables as in Level Control. Ensure consitency.
    const float SAVE_VERSION = 1.0f;
    const string SAVE_FILENAME = "/levelProgress.dat";
    ProgressData progressData;

    public static MenuControl control;

    // Start is called before the first frame update
    void Start()
    {
        control = this;
        DataControl.control.Load();
        MusicControl.control.PlayMusicTrack(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnStartClick()
    {
        Debug.Log("Clicked");
        StartCoroutine(AsyncSceneLoad());
    }

    /*
    * Load a scene.
    */
    IEnumerator AsyncSceneLoad()
    {
        string nextSceneName = DataControl.control.ResumeAfterLastLevelCompleted();
        AsyncOperation asyncLoad = 
            SceneManager.LoadSceneAsync(nextSceneName);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
