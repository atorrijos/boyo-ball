﻿using UnityEngine;
using System;
using System.Collections.Generic;


/******************************************************************************
 * This class controls the camera and allows it to follow the player smoothly.*
 ******************************************************************************/
public class SmoothFollow : MonoBehaviour {

	public float dampTime = 0.1f;
    public float indicatorMaxRange = 25f;
	private Vector3 velocity = Vector3.zero;
    private Vector2 positionAdjustment = new Vector2(0.0f, 0.0f);
	public BallControl target;
    public CameraBounds bounds;
    float dottedLineHalfWidth = 0.1f;
    float inputCameraMovementMagnitude = 3.0f;
    float viewportWidth;
    float viewportHeight;
    private BoxCollider2D cameraBounds;
    private float boundsMargin = 0.5f;

    Vector3 startingPosition;
    bool releaseCamera = false;
    public GameObject indicatorPrefab;
    public List<Rigidbody2D> indicatorObjects;
    public List<Tuple<Rigidbody2D, GameObject>> bodyIndicatorPairs;

    /*
     * Setup the camera. Register its target.
     */
	void Start(){
        startingPosition = transform.position;
        Transform game = GameObject.Find("Game").transform;
        target = game.Find("Ball").GetComponent<BallControl>();
        UpdateDimensions();
        Debug.Log("Viewport height " + viewportHeight + " viewport Width " + viewportWidth);
        bodyIndicatorPairs = new List<Tuple<Rigidbody2D, GameObject>>();

        UpdateCameraBounds();

        StartCamera();

        foreach (Rigidbody2D body in indicatorObjects)
        {
            GameObject indicator = Instantiate(indicatorPrefab, body.position, Quaternion.identity);
            bodyIndicatorPairs.Add(new Tuple<Rigidbody2D, GameObject>(body, indicator));
        }
	}
	
	/*
     * Change position to follow target.
     */
	void FixedUpdate () {
        if (target)
        {
            Vector3 cameraTarget;
            if (releaseCamera)
            {


                // Move the camera to move ahead of the ball's velocity.
                cameraTarget = target.GetBallPosition();
                Vector2 lookAheadVector = target.GetBallVelocity() / 2.0f;
                cameraTarget += (Vector3)(lookAheadVector);

                // Place camera slightly above ball.

                // Move the camera based on the launch line drawn to predict
                // launch.
                Vector2 moveCameraWithInput = target.GetLaunchVector().normalized;
                float launchPercentage = target.GetLaunchPercentage();
                moveCameraWithInput *= launchPercentage * inputCameraMovementMagnitude;

                cameraTarget += (Vector3)positionAdjustment;

                // Find initial position of camera within bounds.
                if (bounds)
                {
                    cameraTarget = KeepCameraInBounds(cameraTarget);
                }

                // Move the camera due to input.
                cameraTarget += (Vector3)moveCameraWithInput;

                // Keep the ball in the frame.
                cameraTarget = KeepBallInCamera(cameraTarget);

                // Constrain the final camera state within bounds.
                if (bounds)
                {
                    cameraTarget = KeepCameraInBounds(cameraTarget);
                }
            }
            else
            {
                cameraTarget = startingPosition;                
            }

            Vector3 point = Camera.main.WorldToViewportPoint(cameraTarget);
            Vector3 delta = cameraTarget - Camera.main.ViewportToWorldPoint(new Vector3(0.5f,
                0.5f, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }

        UpdateIndicators();
	}

    void UpdateIndicators()
    {
        foreach(Tuple<Rigidbody2D,GameObject> pair in bodyIndicatorPairs)
        {
            UpdateIndicator(pair.Item1, pair.Item2);
        }
    }

    void UpdateIndicator(Rigidbody2D obj, GameObject indicator)
    {
        SpriteRenderer spriteRenderer = indicator.GetComponent<SpriteRenderer>();

        Vector2 displacement = (Vector2)transform.position - obj.position;
        if(displacement.magnitude < indicatorMaxRange)
        {

            bool insideXBounds = Mathf.Abs(displacement.x) < viewportWidth;
            bool insideYBounds = Mathf.Abs(displacement.y) < viewportHeight;
            Vector2 indicatorPosition = new Vector2();
            if(insideXBounds && insideYBounds)
            {
                spriteRenderer.enabled = false;
                return;
            }
            float cameraZ = -10.0f;
            int layer = LayerMask.GetMask("CameraBounds");
            RaycastHit2D hit = Physics2D.Raycast(
                obj.position,
                displacement,
                displacement.magnitude,
                layer,
                cameraZ - 1.0f,
                cameraZ + 1.0f);

            indicatorPosition = hit.point;
            Vector3 angle = new Vector3(0.0f,0.0f,Vector2.SignedAngle(Vector2.up, -displacement));

            indicator.transform.eulerAngles = angle;

            spriteRenderer.enabled = true;
            float arrowAlpha = 
                (indicatorMaxRange - displacement.magnitude)
                / indicatorMaxRange;
            spriteRenderer.color = new Color(
                spriteRenderer.color.r,
                spriteRenderer.color.g,
                spriteRenderer.color.b,
                arrowAlpha);
            indicator.transform.position = indicatorPosition;
            return;
        }
        spriteRenderer.enabled = false;
    }

    public void UpdateDimensions()
    {
        viewportHeight = Camera.main.orthographicSize;
        viewportWidth = viewportHeight * Camera.main.aspect;
    }

    public void UpdateCameraBounds()
    {
        cameraBounds = GetComponent<BoxCollider2D>();
        Vector2 colliderSize = new Vector2(
            (viewportWidth - boundsMargin) * 2.0f,
            (viewportHeight - boundsMargin) * 2.0f);
        cameraBounds.size = colliderSize;
    }

    Vector3 KeepCameraInBounds(Vector3 position)
    {
        Tuple<float, float> xBounds = bounds.GetXBounds();
        Tuple<float, float> yBounds = bounds.GetYBounds();
        Vector3 adjustedPosition = new Vector3();
        adjustedPosition.x = Mathf.Clamp(position.x, 
            (xBounds.Item1 + viewportWidth + dottedLineHalfWidth), 
            (xBounds.Item2 - (viewportWidth +dottedLineHalfWidth)));
        adjustedPosition.y = Mathf.Clamp(position.y, 
            (yBounds.Item1 + viewportHeight + dottedLineHalfWidth), 
            (yBounds.Item2 - (viewportHeight + dottedLineHalfWidth)));

        return adjustedPosition;
    }

    Vector3 KeepBallInCamera(Vector3 position)
    {
        CircleCollider2D targetCollider =target.GetComponent<CircleCollider2D>();
        float ballradius = targetCollider.radius;
        Vector2 ballPosition = target.GetBallPosition();

        Vector3 adjustedPosition = new Vector3();
        float maxX = ballPosition.x + (viewportWidth - ballradius);
        float minX = (ballPosition.x - viewportWidth) + ballradius;
        float maxY = ballPosition.y + (viewportHeight - ballradius);
        float minY = (ballPosition.y - viewportHeight) + ballradius;

        adjustedPosition.x = Mathf.Clamp(position.x, minX, maxX);  
        adjustedPosition.y = Mathf.Clamp(position.y, minY, maxY);

        return adjustedPosition;
    }

    public void SetCameraAdjustment(Vector2 adjustment)
    {
        positionAdjustment = adjustment;
    }

    public void StartCamera()
    {
        releaseCamera = false;
        positionAdjustment = new Vector2(0.0f, 0.0f);
    }

    public void GameStart()
    {
        releaseCamera = true;
    }
}
