﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/******************************************************************************
 * This class controls the end zone and the logic of completing a level.*******
 ******************************************************************************/
public class EndZoneControl : MonoBehaviour
{
    public GameControl game;
    public GameObject winParticles;
    bool complete = false;
    public bool isFinal = false;

    /*
     * Detects when the ball enters the end zone collider.
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!complete)
        {
            if (collision.transform.name == "Ball")
            {
                Instantiate(winParticles, transform.position, Quaternion.identity);
                complete = true;

                if (isFinal)
                {
                    // Play final banner.
                    LevelControl.control.PlayFinalBanner();                    
                    return;
                }
                // Play normal banner.
                LevelControl.control.PlayWinBanner();
            }
        }
    }
}
