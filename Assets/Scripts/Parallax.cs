﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private Vector2 startPosition;
    private float width;
    private float height;
    public GameObject cam;
    public float parallaxEffect = 1;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.gameObject;
        startPosition = cam.transform.position - transform.position;
        width = GetComponent<SpriteRenderer>().size.x;
        height = GetComponent<SpriteRenderer>().size.y;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 cameraPosition = cam.transform.position;
        Vector2 displacementVector = cam.transform.position * parallaxEffect;
        Vector2 temp = startPosition + displacementVector;
        float xDistance = cameraPosition.x - temp.x;
        float yDistance = cameraPosition.y - temp.y;
        if(Mathf.Abs(xDistance) > width * 2)
        {
            if (xDistance > 0)
            {
                startPosition.x += 3f * width;
            }
            else
            {
                startPosition.x -= 3f * width; 
            }
        }
        if (Mathf.Abs(yDistance) > height * 2)
        {
            if (yDistance > 0)
            {
                startPosition.y += 3f * height;
            }
            else
            {
                startPosition.y -= 3f * height;
            }
        }

        transform.position = startPosition + displacementVector;
    }
}
