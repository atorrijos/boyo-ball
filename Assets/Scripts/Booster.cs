﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : MonoBehaviour
{
    Animator boosterAnimator;
    SpriteRenderer boosterSprite;
    public BallControl ball;
    // Start is called before the first frame update
    void Start()
    {
        boosterAnimator = GetComponent<Animator>();
        boosterSprite = GetComponent<SpriteRenderer>();
        boosterSprite.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetBoosterTrigger()
    {
        boosterAnimator.ResetTrigger("Boost");
        boosterSprite.enabled = false;
    }

    public void StartBoost()
    {
        boosterSprite.enabled = true;
        boosterAnimator.SetTrigger("Boost");
    }
}
