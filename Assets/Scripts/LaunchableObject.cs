﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchableObject : MonoBehaviour, StartTrigger
{
    public PathController pathController;
    public int startingPathIndex = 0;
    public float launchStrength = 10f;

    // How long the object waits after despawning to launch again.
    public float restInterval = 0.0f;
    bool isResting = false;

    // Forced Respawn Time.
    public bool forceRespawnAfterLaunch = false;
    public float launchableLifetime = 1.0f;
    float lifetimeTimer = 0.0f;

    // Float to indicate wait time after starting the game to begin movement.
    public float delayStart = 0.0f;
    float timeSinceStart = 0.0f;
    float restTimer = 0.0f;

    // Flag comes on when game starts.
    bool isActive = false;
    bool objectLaunched = false;

    // Trigger to start launch.
    public MovementTrigger startTrigger;

    public enum Mode
    {
        Automatic = 0,
        TriggerOnce = 1
    };

    public Mode mode = Mode.Automatic;

    public bool isSolidBody = true;

    Rigidbody2D objectBody;
    Collider2D objectCollider;
    Vector2 startingPosition;
    Vector2 launchAngle;
    float startingAngle;


    // Start is called before the first frame update
    void Start()
    {
        objectBody = GetComponent<Rigidbody2D>();
        objectCollider = GetComponent<Collider2D>();
        objectBody.constraints = RigidbodyConstraints2D.FreezeAll;
        pathController.PopulatePoints();
        MovementPoint start = pathController.GetPointAtIndex(startingPathIndex);
        transform.position = start.transform.position;
        startingPosition = transform.position;
        startingAngle = objectBody.rotation;
        int endIndex = pathController.GetNextIndex(startingPathIndex);
        launchAngle = pathController.GetDirectionToNextPoint(objectBody, endIndex);
        launchAngle = launchAngle.normalized * launchStrength;

        // Make object non collidable to start to layer launches.
        objectCollider.isTrigger = true;
    }

    private void FixedUpdate()
    {
        if (isActive)
        {
            timeSinceStart += Time.fixedDeltaTime;
            restTimer += Time.fixedDeltaTime;
            if (!objectLaunched && timeSinceStart > delayStart)
            {
                // Only take rest time in consideration after first launch.
                isResting = isResting && (restTimer < restInterval);

                if (!isResting)
                {
                    LaunchObject();
                }
            }

            if (objectLaunched && forceRespawnAfterLaunch)
            {
                lifetimeTimer += Time.fixedDeltaTime;
                if(lifetimeTimer > launchableLifetime)
                {
                    ResetPositionAngleAndSpeed();
                }
            }
        }
    }

    void LaunchObject()
    {
        // Allow object to collide with other objects.
        if (isSolidBody)
        {
            objectCollider.isTrigger = false;
        }

        objectBody.constraints = RigidbodyConstraints2D.None;
        objectBody.velocity = launchAngle;
        objectLaunched = true;
    }

    void StartMovement(bool defaultWeightless)
    {
        if (defaultWeightless)
        {
            objectBody.gravityScale = 0f;
        }

        switch (mode)
        {
            case Mode.Automatic:
                isActive = true;
                break;
            case Mode.TriggerOnce:
                // Dont start movement flag until trigger is entered.
                break;
            default:
                isActive = true;
                break;
        }
    }

    void ResetObject()
    {
        ResetPositionAngleAndSpeed();
        timeSinceStart = 0.0f;
        isActive = false;
        isResting = false;
    }

    void ResetPositionAngleAndSpeed()
    {
        objectBody.constraints = RigidbodyConstraints2D.FreezeAll;
        objectBody.angularVelocity = 0f;
        objectBody.rotation = startingAngle;
        transform.position = startingPosition;
        objectBody.velocity = Vector2.zero;
        objectLaunched = false;
        isResting = true;
        restTimer = 0.0f;
        lifetimeTimer = 0.0f;
        if (mode == Mode.TriggerOnce)
        {
            TriggerReset();
        }
    }

    public void UpdateLauncherDelay(float delay)
    {
        delayStart = delay;
    }

    public void UpdateLaunchStrength(float velocity)
    {
        launchStrength = velocity;
        launchAngle = launchAngle.normalized * launchStrength;
    }

    public void UpdateRestInterval(float time)
    {
        restInterval = time;
    }

    public void UpdateTriggerData(bool launchByTrigger, MovementTrigger trigger)
    {
        if (launchByTrigger)
        {
            mode = Mode.TriggerOnce;
            startTrigger = trigger;
            startTrigger.objectsToTrigger.Add(this);
        }
        else
        {
            mode = Mode.Automatic;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "ReSpawn")
        {
            ResetPositionAngleAndSpeed();
        }
    }

    public void TriggerEnter()
    {
        isActive = true;
    }

    public void TriggerReset()
    {
        isActive = false;
        timeSinceStart = 0.0f;
    }
}
