﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstOnPlanet : MonoBehaviour
{
    public GameObject objectToBurst;
    public GameObject explosionPrefab;
    private bool isResetting = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // We check for collision of the moving fireball, and
        // if it matches, disable its parent.
        if (!isResetting && collision.name == "BigFireball")
        {
            Instantiate(explosionPrefab, collision.transform.position, 
                Quaternion.identity);
            objectToBurst.SetActive(false);
        }
    }

    public void StartMovement()
    {
        isResetting = false;
    }

    public void ResetObject()
    {
        isResetting = true;
        if(objectToBurst.activeSelf == false)
        {
            objectToBurst.SetActive(true);
            objectToBurst.BroadcastMessage("ResetObject");
        }
    }
}
