﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitedLifetime : MonoBehaviour
{
    public float lifetime = 10.0f;
    float timeAlive = 0.0f;

    // Update is called once per frame
    private void FixedUpdate()
    {
        timeAlive += Time.fixedDeltaTime;
        if(timeAlive > lifetime)
        {
            Destroy(this.gameObject);
        }
    }
}
