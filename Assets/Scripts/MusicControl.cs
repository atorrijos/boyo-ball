﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class MusicControl : MonoBehaviour
{
    public List<AudioSource> audioSource = new List<AudioSource>(2);
    public float fadeTime = 1.0f;
    public float delayTime = 0.0f;
    MusicSelection musicSelection;
    MusicTrack musicClips;
    int currentAudioSource = 0;
    float onVolume = 1.0f;
    float offVolume = 0.0f;

    public AudioMixer masterMixer;
    public float musicLevel = 0.0f;
    public float sfxLevel = 0.0f;
    public float musicMaxLevel = 0.0f;
    public float sfxMaxLevel = 0.0f;

    public static MusicControl control;
    bool musicMuted = false;
    bool sfxMuted = false;

    // Start is called before the first frame update
    void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }

        musicSelection = GetComponent<MusicSelection>();
        audioSource[currentAudioSource].volume = onVolume;
        audioSource[1 - currentAudioSource].volume = offVolume;  
    }

    void Start()
    {
        sfxLevel = DataControl.control.GetSfxVolumePref();
        musicLevel = DataControl.control.GetMusicVolumePref();
        musicMuted = DataControl.control.GetMusicMutedPref();
        sfxMuted = DataControl.control.GetSfxMutedPref();

        if (musicMuted)
        {
            MuteMusic();
        }
        else
        {
            EnableMusic();
        }

        if (sfxMuted)
        {
            MuteSfx();
        }
        else
        {
            EnableSfx();
        }
    }

    /*
     * Music Control
     */
    public void PlayMusicForLevel(LevelID currentLevel)
    {
        int trackForLevel = musicSelection.GetTrackNumberForLevel(currentLevel);
        PlayMusicTrack(trackForLevel);
    }

    public void PlayMusicTrack(int track)
    {
        // Only change and play music if it should be different that what is
        // currently being played.
        if (!audioSource[currentAudioSource].isPlaying){
            SetupSource(track);
            FadeInCurrentAudio(1.0f);
        }
        else if (track != musicSelection.currentlyPlayingTrack)
        {
            // If another song is playing, crossfade tracks.
            FadeOutCurrentAudio(fadeTime);
            currentAudioSource = 1 - currentAudioSource;
            SetupSource(track);
            FadeInCurrentAudio(fadeTime);
        }
    }
  
    public void SetupSource(int track)
    {
        musicClips = musicSelection.GetMusicForTrackNumber(track);
        audioSource[currentAudioSource].clip = musicClips.musicLoop;
        audioSource[currentAudioSource].loop = true;
        audioSource[currentAudioSource].volume = 0f;
        audioSource[currentAudioSource].PlayOneShot(musicClips.musicStart);
        audioSource[currentAudioSource].PlayScheduled(AudioSettings.dspTime + musicClips.musicStart.length);
        musicSelection.currentlyPlayingTrack = track;
    }

    public void StopMusic()
    {
        audioSource[currentAudioSource].Stop();
        musicSelection.currentlyPlayingTrack = -1;
    }

    public void FadeOutCurrentAudio(float fadeTime)
    {
        StartCoroutine(FadeAudioSource.StartFade(audioSource[currentAudioSource],
                2.0f, offVolume));
    }

    public void FadeInCurrentAudio(float fadeTime)
    {
        StartCoroutine(FadeAudioSource.StartFade(audioSource[currentAudioSource],
                2.0f, onVolume));
    }

    public void MuteMusic()
    {
        masterMixer.SetFloat("musicVol", -80f);
        musicMuted = true;
        DataControl.control.SetMusicMutedPref(true);
    }

    public void MuteSfx()
    {
        masterMixer.SetFloat("sfxVol", -80f);
        sfxMuted = true;
        DataControl.control.SetSfxMutedPref(true);
    }

    public void EnableMusic()
    {
        masterMixer.SetFloat("musicVol", musicLevel);
        musicMuted = false;
        DataControl.control.SetMusicMutedPref(false);
    }

    public void EnableSfx()
    {
        masterMixer.SetFloat("sfxVol", sfxLevel);
        sfxMuted = false;
        DataControl.control.SetSfxMutedPref(false);
    }

    public void SetMusicLevel(float level)
    {
        float scaledLevel = Mathf.Sqrt(level);
        float db = (scaledLevel - 1.0f) * 80.0f;
        musicLevel = db;
        DataControl.control.SetMusicVolumePref(musicLevel);
        if (!musicMuted)
        {
            EnableMusic();
        }
    }

    public void SetSfxLevel(float level)
    {
        float scaledLevel = Mathf.Sqrt(level);
        float db = (scaledLevel - 1.0f) * 80.0f;
        sfxLevel = db;
        DataControl.control.SetSfxVolumePref(sfxLevel);
        if (!sfxMuted)
        {
            EnableSfx();
        }
    }

    public float GetSfxBarValue()
    {
        float level = (sfxLevel + 80.0f) / 80.0f;
        return level;
    }

    public float GetMusicBarValue()
    {
        float level = (musicLevel + 80.0f) / 80.0f;
        return level;
    }
}

public static class FadeAudioSource
{

    public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume)
    {
        float currentTime = 0;
        float start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }

        if (targetVolume == 0.0f)
        {
            audioSource.Stop();
        }
        yield break;
    }
}