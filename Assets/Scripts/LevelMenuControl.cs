﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class LevelMenuControl : MonoBehaviour
{
    VerticalLayoutGroup buttonGroup;
    LevelID maxLevelUnlocked;
    // Start is called before the first frame update
    void Start()
    {
        maxLevelUnlocked = DataControl.control.GetMaxLevelUnlocked();

        buttonGroup = FindObjectOfType<VerticalLayoutGroup>();
        LevelButton[] buttons = buttonGroup.GetComponentsInChildren<LevelButton>();
        foreach(LevelButton b in buttons)
        {
            b.CheckEnable(maxLevelUnlocked);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
