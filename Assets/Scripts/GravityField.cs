﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityField : MonoBehaviour
{
    public float gravityStrength = 9.8f;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Ball")
        {
            Vector2 gravityDirection = transform.position - collision.transform.position;
            gravityDirection = gravityDirection.normalized * gravityStrength;

            collision.GetComponent<Rigidbody2D>().AddForce(gravityDirection);
        }
    }
}
