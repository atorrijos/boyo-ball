﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsControl : MonoBehaviour
{
    public Slider musicSlider;
    public Slider sfxSlider;
    public Button hatButton;
    bool musicPlaying = true;
    bool sfxPlaying = true;

    // Buttons for sound control.
    public Button musicButton;
    public Button sfxButton;
    public Sprite musicOn;
    public Sprite musicOff;
    public Sprite sfxOn;
    public Sprite sfxOff;

    // Start is called before the first frame update
    void Start()
    {
        musicSlider.onValueChanged.AddListener(MusicControl.control.SetMusicLevel);
        sfxSlider.onValueChanged.AddListener(MusicControl.control.SetSfxLevel);

        musicPlaying = !DataControl.control.GetMusicMutedPref();
        if (musicPlaying)
        {
            musicButton.image.sprite = musicOn;
        }
        else
        {
            musicButton.image.sprite = musicOff;
        }

        sfxPlaying = !DataControl.control.GetSfxMutedPref();
        if (sfxPlaying)
        {
            sfxButton.image.sprite = sfxOn;
        }
        else
        {
            sfxButton.image.sprite = sfxOff;
        }
    }

    private void OnEnable()
    {
        float musicSliderValue = MusicControl.control.GetMusicBarValue();
        musicSlider.value = musicSliderValue;
        float sfxSliderValue = MusicControl.control.GetSfxBarValue();
        sfxSlider.value = sfxSliderValue;

        if (DataControl.control.HasBeatenGame())
        {
            hatButton.interactable = true;
            hatButton.GetComponentInChildren<Text>().color = Color.black;
        }
    }

    private void OnDisable()
    {
        DataControl.control.Save();
    }

    public void ToggleMusic()
    {
        musicPlaying = !musicPlaying;
        if (musicPlaying)
        {
            MusicControl.control.EnableMusic();
            musicButton.image.sprite = musicOn;
        }
        else
        {
            MusicControl.control.MuteMusic();
            musicButton.image.sprite = musicOff;
        }
    }

    public void ToggleSfx()
    {
        sfxPlaying = !sfxPlaying;
        if (sfxPlaying)
        {
            MusicControl.control.EnableSfx();
            sfxButton.image.sprite = sfxOn;
        }
        else
        {
            MusicControl.control.MuteSfx();
            sfxButton.image.sprite = sfxOff;
        }
    }


}
