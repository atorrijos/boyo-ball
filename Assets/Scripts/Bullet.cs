﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Vector2 launchSpeed = Vector2.zero;
    public float lifeTime = 0.0f;
    float timeAlive = 0.0f;
    Rigidbody2D bulletBody;
    public GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        bulletBody = transform.GetComponent<Rigidbody2D>();
        bulletBody.velocity = launchSpeed;
    }

    void FixedUpdate()
    {
        timeAlive += Time.fixedDeltaTime;
        if (lifeTime > 0)
        {
            if(timeAlive > lifeTime)
            {
                // Destroy bullet.
                DestroyBullet();
            }
        }
    }

    public void SetSpeed(Vector2 speed)
    {
        launchSpeed = speed;
        bulletBody.velocity = speed;
    }

    public void AddSpeed(Vector2 additionalSpeed)
    {
        bulletBody.velocity += additionalSpeed;
        launchSpeed = bulletBody.velocity;
    }

    public void DestroyBullet()
    {
        Instantiate(explosion, bulletBody.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

    public void ResetObject()
    {
        Destroy(this.gameObject);   
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Stick" || collision.tag == "Nonstick")
        {
            DestroyBullet();
        }
    }
}
