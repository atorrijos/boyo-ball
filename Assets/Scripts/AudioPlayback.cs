﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayback : MonoBehaviour
{
    public AudioSource audioSource;
    public bool interruptable = false;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayAudio()
    {
        if (!interruptable && audioSource.isPlaying)
        {
            return;
        }
        audioSource.Play();
    }

    public void PlayMusicTrack(int track)
    {
        MusicControl.control.PlayMusicTrack(track);
    }
}
