﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;


/******************************************************************************
 * This class controls the logic for the game and any inputs the player makes.*
 ******************************************************************************/
public class GameControl : MonoBehaviour
{

    // Flag to start game.
    bool gameStarted = false;
    bool resetStarted = false;
    public bool defaultWeightless = false;

    // Title of the level being played.
    public string levelTitle = "Default Level Title";

    // Line used to show launch direction.
    LineRenderer aimLineRenderer;
    public float lineThickness = 15.0f;

    // Ball.
    public Transform ball;
    BallControl ballControl;

    // Used to track where the mouse was clicked down before release.
    Vector3 mousePosDown;
    Vector3[] linePositions;

    // The maximum value the ball can be launched from drag input.
    public float maxLaunchInputDistance = 15.0f;

    // Scale to adjust mouse input to launch factor.
    float inputScale = 3.2f;

    // Reference to camera functions.
    SmoothFollow mainCamera;

    // --- Standard Functions ---

    // Start is called before the first frame update
    void Start()
    {
        ballControl = ball.GetComponent<BallControl>();
        LevelControl.control.gameObject.SetActive(true);
        LevelControl.control.SetGameControl(this);
        LevelControl.control.RefreshUI();
        LevelControl.control.SetLevelTitle(levelTitle);

        mainCamera = Camera.main.GetComponent<SmoothFollow>();
        mainCamera.StartCamera();

        // Set up line for launching the ball.
        aimLineRenderer = transform.GetComponent<LineRenderer>();
        linePositions = new Vector3[2];
        SetupLine();
    }

    /* 
     * Update is called once per frame. Used for drawing launch line and
     * taking in player controls.
     */
    void Update()
    {
        if(!gameStarted)
        {
            if (Input.GetButtonDown("Fire1") || Input.GetMouseButtonDown(1))
            {                
                gameStarted = true;
                mainCamera.GameStart();
                LevelControl.control.HideTitle();
                BroadcastMessage("GenerateValues");
                BroadcastMessage("StartMovement", defaultWeightless);
            }
        }

        // Left mouse button is used to control launching.
        
        Vector2 launchVector = new Vector2(0.0f, 0.0f);

        Vector3 mousePos = transform.InverseTransformPoint(Input.mousePosition);

        // The launch button is also used to bounce off walls.
        if (Input.GetButtonDown("Fire1"))
        {        
            mousePosDown = mousePos;            
            aimLineRenderer.enabled = true;
            ballControl.StartSpikes();
        }

        // Update start position if both fingers are on screen.
        if (Input.GetButton("Fire2"))
        {
            mousePosDown = mousePos;
        }

        // When launch button is held aim the launch direction and magnitude.
        if (Input.GetButton("Fire1"))
        {
            transform.InverseTransformPoint(Input.mousePosition);
            linePositions[0] = Camera.main.ScreenToWorldPoint(mousePosDown);
            linePositions[0].z = 10f;
            linePositions[1] = Camera.main.ScreenToWorldPoint(mousePos);
            linePositions[1].z = 10f;
            aimLineRenderer.SetPositions(linePositions);

            // Find angle of launch.
            launchVector = CalculateLaunch(linePositions[1], linePositions[0]);

            // Determine magnitude of launch.
            float inputPercentage = launchVector.magnitude / maxLaunchInputDistance;

            ballControl.SetLaunchVector(launchVector, inputPercentage);

            // Change line color to match magnitude.
            SetLineColor(inputPercentage);
        }
        
        // On launch button release launch ball if possible.
        // Want to check if game started, otherwise we can fire by resetting while holding down
        // launch.
        if (gameStarted && Input.GetButtonUp("Fire1"))
        {
            aimLineRenderer.enabled = false;
            launchVector = CalculateLaunch(linePositions[1], linePositions[0]);
            // Determine magnitude of launch.
            float inputPercentage = launchVector.magnitude / maxLaunchInputDistance;

            ballControl.SetLaunchVector(launchVector, inputPercentage);
            ballControl.CheckAndLaunch();
            ballControl.SetLaunchVector(new Vector2(0.0f, 0.0f), 0.0f);
        }
    }

    /*
     * Called when game needs resetting.
     */
    public void ResetLevel(float waitTime)
    {
        if (!resetStarted)
        {
            LevelControl.control.PlayMusicForCurrentLevel();
            StartCoroutine(WaitThenReset(waitTime));
        }
    }

    /*
     * Delay for restarting game. Broadcasts reset signal to
     * other parts of the game.
     */
    IEnumerator WaitThenReset(float seconds)
    {
        resetStarted = true;
        // Wait to pause and reset Objects.
        yield return new WaitForSeconds(seconds);

        ball.gameObject.SetActive(true);
        BroadcastMessage("ResetObject");
        LevelControl.control.ShowTitle();
        mainCamera.StartCamera();
        aimLineRenderer.enabled = false;
        gameStarted = false;
        resetStarted = false;
    }

    /*
     * Functions used to setup launch line.
     */
    void SetupLine()
    {
        Color startColor = new Color(0.8f, 0.8f, 0.8f);
        aimLineRenderer.startColor = startColor;
        aimLineRenderer.endColor = startColor;
        aimLineRenderer.startWidth = 0.2f;
        aimLineRenderer.endWidth = 1.0f;
    }

    /*
     * Set the color of the launch line.
     * magnitude: 1 means line is full red, 0 means line is full gray.
     */
    void SetLineColor(float magnitude)
    {
        if(magnitude < 0f)
        {
            magnitude = 0f;
        }
        if(magnitude > 1.00f)
        {
            magnitude = 1.00f;
        }
        float greenBlueValue = 0.9f - 0.9f*magnitude;
        Color color = new Color(0.9f, greenBlueValue, greenBlueValue);
        aimLineRenderer.endColor = color;
    }

    /*
     * Calculate and return the vector the ball is to be launched on button release.
     * mousePosEnd: Where the button is released.
     * mousePosBegin: Where the button is pressed.
     */
    public Vector2 CalculateLaunch(Vector3 mousePosEnd, Vector3 mousPosBegin)
    {
        Vector3 inputVector = mousePosEnd - mousPosBegin;

        // Angle is opposite the line drawn.
        Vector2 launchVector = new Vector2(-(inputVector.x), -(inputVector.y));

        // Scale the input to allow for greater granularity in magnitude.
        float magnitude = launchVector.magnitude * inputScale;

        // Cap the launch magnitude.
        if (magnitude > maxLaunchInputDistance)
        {
            magnitude = maxLaunchInputDistance;
        }

        launchVector = launchVector.normalized * magnitude;
        return launchVector;
    }

  




}
