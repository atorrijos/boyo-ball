﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    public float delayStart = 0.0f;
    public float launchDuration = 0.25f;
    public float resetTime = 0.5f;
    public float restInterval = 0.5f;
    public bool inhereitValuesFromParent = false;
    public bool visualizeLaunchBuildup = true;
    private bool buildupReleased = true;
    MoveableObject launcherBody;
    PathController launcherPath;
    const int REST_PATH_START = 2;
    const int LAUNCH_PATH_START = 0;
    const int RESET_PATH_START = 1;    

    private void Awake()
    {
        launcherBody = GetComponentInChildren<MoveableObject>();
        launcherPath = GetComponentInChildren<PathController>();
        launcherBody.delayStart = delayStart;
    }

    public void GenerateValues()
    {
        if (!inhereitValuesFromParent)
        {
            UpdateLaunchDuration(launchDuration);
            UpdateResetTime(resetTime);
            UpdateRestInterval(restInterval);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (visualizeLaunchBuildup)
        {
            int currentPathStartIndex = launcherBody.GetCurrentPathEndIndex();
            if (currentPathStartIndex == LAUNCH_PATH_START)
            {
                buildupReleased = false;
                float percentage = launcherBody.GetPercentagePathCompletion();
                Color buildupColor = new Color(1.0f, 1.0f - percentage, 1.0f - percentage);
                launcherBody.ChangeSpriteColor(buildupColor);
            }

            if(currentPathStartIndex == RESET_PATH_START && !buildupReleased)
            {
                Color defaultColor = new Color(1.0f, 1.0f, 1.0f);
                launcherBody.ChangeSpriteColor(defaultColor);
                buildupReleased = true;
            }
        }
    }

    public void UpdateLauncherDelay(float dStart)
    {
        delayStart = dStart;
        launcherBody.delayStart = delayStart;
    }

    public void UpdateRestInterval(float duration)
    {
        restInterval = duration;
        launcherPath.SetDurationForPoint(REST_PATH_START, duration);
    }

    public void UpdateLaunchDuration(float duration)
    {
        launchDuration = duration;
        launcherPath.SetDurationForPoint(LAUNCH_PATH_START, duration);
    }

    public void UpdateResetTime(float time)
    {
        resetTime = time;
        launcherPath.SetDurationForPoint(RESET_PATH_START, resetTime);
    }

    public void OverridePathSpeeds()
    {
        launcherPath.OverrideSpeedAll();
    }

}
