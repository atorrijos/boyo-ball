﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    public List<GameObject> spawnedObjects = new List<GameObject>();
    public bool repeatSpawn = false;
    public float spawnInterval = 2.0f;
    float timeSinceLastSpawn = 0.0f;
    bool spawnLoopStart = false;
    bool delayStarted = false;

    public void SpawnObject()
    {
        GameObject spawnedObject = 
            Instantiate(objectToSpawn, transform.position, Quaternion.identity);
        spawnedObjects.Add(spawnedObject);
        spawnLoopStart = true;
        timeSinceLastSpawn = 0.0f;
    }

    public void DelayedSpawnObject(float seconds)
    {
        if (delayStarted)
        {
            return;
        }

        if (repeatSpawn)
        {
            if (!spawnLoopStart)
            {
                StartCoroutine(SpawnObjectAfterWait(seconds));
                delayStarted = true;
            }
            return;
        }

        StartCoroutine(SpawnObjectAfterWait(seconds));
        delayStarted = true;
    }

    IEnumerator SpawnObjectAfterWait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        SpawnObject();
        delayStarted = false;
    }

    public void FixedUpdate()
    {
        if (repeatSpawn && spawnLoopStart)
        {
            timeSinceLastSpawn += Time.fixedDeltaTime;
            if(timeSinceLastSpawn > spawnInterval)
            {
                SpawnObject();
            }
        }
    }

    public void ResetObject()
    {
        spawnLoopStart = false;
        delayStarted = false;
        timeSinceLastSpawn = 0.0f;
        foreach (GameObject go in spawnedObjects)
        {
            Destroy(go);
        }
    }
}
