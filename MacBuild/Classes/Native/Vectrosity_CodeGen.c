﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Vectrosity.BrightnessControl::.ctor()
extern void BrightnessControl__ctor_m15FE14F93846E840F4A49EDEBEF9611743DB002C (void);
// 0x00000002 Vectrosity.RefInt Vectrosity.BrightnessControl::get_objectNumber()
extern void BrightnessControl_get_objectNumber_mC8913494AF2D5D4E42D13BC4B97E30DE64DB9B8D (void);
// 0x00000003 System.Void Vectrosity.BrightnessControl::Setup(Vectrosity.VectorLine,System.Boolean)
extern void BrightnessControl_Setup_m1A7604BB2DE92A75632BB4AA67C34E71B0501EC7 (void);
// 0x00000004 System.Void Vectrosity.BrightnessControl::SetUseLine(System.Boolean)
extern void BrightnessControl_SetUseLine_m5CCF7F07EF01A1E5799AD2441F441460E2E7608E (void);
// 0x00000005 System.Void Vectrosity.BrightnessControl::OnBecameVisible()
extern void BrightnessControl_OnBecameVisible_m48F04D45A909D0B03919B96EB67A6C8A67A30262 (void);
// 0x00000006 System.Void Vectrosity.BrightnessControl::OnBecameInvisible()
extern void BrightnessControl_OnBecameInvisible_m5D517F850870C01CD8F9440B43431AA034FF5571 (void);
// 0x00000007 System.Void Vectrosity.BrightnessControl::OnDestroy()
extern void BrightnessControl_OnDestroy_mD27E0FB0C17022887E63D6C80E8F36ED28C14C68 (void);
// 0x00000008 System.Void Vectrosity.CapInfo::.ctor(Vectrosity.EndCap,UnityEngine.Texture,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single[])
extern void CapInfo__ctor_mFC12FCEEFD32F8EA5839C0F17447AA57FB202D87 (void);
// 0x00000009 System.Void Vectrosity.IVectorObject::SetName(System.String)
// 0x0000000A System.Void Vectrosity.IVectorObject::UpdateVerts()
// 0x0000000B System.Void Vectrosity.IVectorObject::UpdateUVs()
// 0x0000000C System.Void Vectrosity.IVectorObject::UpdateColors()
// 0x0000000D System.Void Vectrosity.IVectorObject::UpdateTris()
// 0x0000000E System.Void Vectrosity.IVectorObject::UpdateNormals()
// 0x0000000F System.Void Vectrosity.IVectorObject::UpdateTangents()
// 0x00000010 System.Void Vectrosity.IVectorObject::UpdateMeshAttributes()
// 0x00000011 System.Void Vectrosity.IVectorObject::ClearMesh()
// 0x00000012 System.Void Vectrosity.IVectorObject::SetMaterial(UnityEngine.Material)
// 0x00000013 System.Void Vectrosity.IVectorObject::SetTexture(UnityEngine.Texture)
// 0x00000014 System.Void Vectrosity.IVectorObject::Enable(System.Boolean)
// 0x00000015 System.Void Vectrosity.IVectorObject::SetVectorLine(Vectrosity.VectorLine,UnityEngine.Texture,UnityEngine.Material,System.Boolean)
// 0x00000016 System.Void Vectrosity.IVectorObject::Destroy()
// 0x00000017 System.Int32 Vectrosity.IVectorObject::VertexCount()
// 0x00000018 System.Void Vectrosity.LineManager::.ctor()
extern void LineManager__ctor_mC66A42AB85451269A60F8D3AE60DB560181CCE18 (void);
// 0x00000019 System.Void Vectrosity.LineManager::.cctor()
extern void LineManager__cctor_m8FDF48E19B10B24D472D81743C61CA74A6ED0440 (void);
// 0x0000001A System.Void Vectrosity.LineManager::Awake()
extern void LineManager_Awake_m6333EEA8BCFFB29CCD14EA4047C74DAE15A62A68 (void);
// 0x0000001B System.Void Vectrosity.LineManager::Initialize()
extern void LineManager_Initialize_mE1040C0B72500FB889913D1ECF193B49754498C5 (void);
// 0x0000001C System.Void Vectrosity.LineManager::AddLine(Vectrosity.VectorLine,UnityEngine.Transform,System.Single)
extern void LineManager_AddLine_m48497AB498C6950A6BE4D3FDCBD242AF51251FCA (void);
// 0x0000001D System.Void Vectrosity.LineManager::DisableLine(Vectrosity.VectorLine,System.Single)
extern void LineManager_DisableLine_m6EF07844B003FFBE605DDB93F302024C4F121CA5 (void);
// 0x0000001E System.Collections.IEnumerator Vectrosity.LineManager::DisableLine(Vectrosity.VectorLine,System.Single,System.Boolean)
extern void LineManager_DisableLine_mFF21DD4192F905AE413A0DE1BAC21DF01B04BA09 (void);
// 0x0000001F System.Void Vectrosity.LineManager::LateUpdate()
extern void LineManager_LateUpdate_m168A775D7DD3B46B0FB6768DBD4EFDF68D4F0F4D (void);
// 0x00000020 System.Void Vectrosity.LineManager::RemoveLine(System.Int32)
extern void LineManager_RemoveLine_m5842C7250976916A15361077FF7321CFDAAFC320 (void);
// 0x00000021 System.Void Vectrosity.LineManager::RemoveLine(Vectrosity.VectorLine)
extern void LineManager_RemoveLine_m941BD07322036D8D886799D67AD748791EE97C1D (void);
// 0x00000022 System.Void Vectrosity.LineManager::DisableIfUnused()
extern void LineManager_DisableIfUnused_mCFFB38235D5505D173CBE4B91D596B6283A505FA (void);
// 0x00000023 System.Void Vectrosity.LineManager::EnableIfUsed()
extern void LineManager_EnableIfUsed_m73FDA2A4199FB9F56CE4F89375BDF9C8D2B381E8 (void);
// 0x00000024 System.Void Vectrosity.LineManager::StartCheckDistance()
extern void LineManager_StartCheckDistance_m69BB417780980A3C213ABA7CD0C5B9FBA1EAEEC6 (void);
// 0x00000025 System.Void Vectrosity.LineManager::CheckDistance()
extern void LineManager_CheckDistance_m3ABE139597198720C2E5369DE43675BF47A5A867 (void);
// 0x00000026 System.Void Vectrosity.LineManager::OnDestroy()
extern void LineManager_OnDestroy_m7E55D307BA7DB8638154F41885A09F4F211609D0 (void);
// 0x00000027 System.Void Vectrosity.LineManager_<DisableLine>c__Iterator0::.ctor()
extern void U3CDisableLineU3Ec__Iterator0__ctor_m7C3CC0527424E540C7171AB9D3E63D1E4F306380 (void);
// 0x00000028 System.Object Vectrosity.LineManager_<DisableLine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern void U3CDisableLineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4E358A5BD114BA8582F3DBD751A7B0DE2FFED9B8 (void);
// 0x00000029 System.Object Vectrosity.LineManager_<DisableLine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern void U3CDisableLineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_mB1E265B7781B10315A274977B92F1F96347B6DC7 (void);
// 0x0000002A System.Boolean Vectrosity.LineManager_<DisableLine>c__Iterator0::MoveNext()
extern void U3CDisableLineU3Ec__Iterator0_MoveNext_m80031DD3D256059BE76D15027467F5FA957D6A16 (void);
// 0x0000002B System.Void Vectrosity.LineManager_<DisableLine>c__Iterator0::Dispose()
extern void U3CDisableLineU3Ec__Iterator0_Dispose_m69F6429E931C5B4C9B9F1FAAE78533B0B3B358B9 (void);
// 0x0000002C System.Void Vectrosity.LineManager_<DisableLine>c__Iterator0::Reset()
extern void U3CDisableLineU3Ec__Iterator0_Reset_mB4B023677AE126534F8ECD01B6FD974B9A44A8E1 (void);
// 0x0000002D System.Void Vectrosity.RefInt::.ctor(System.Int32)
extern void RefInt__ctor_mA3DA5388C30EE2966AA4E86148D0E711DFFC9794 (void);
// 0x0000002E UnityEngine.Vector2[][] Vectrosity.VectorChar::get_data()
extern void VectorChar_get_data_m1BF2BF8502A7674FB25201C7B09194625D87853B (void);
// 0x0000002F System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern void VectorLine__ctor_m5186C6DBBC00AFD7EB31A2775F6302A29668E558 (void);
// 0x00000030 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Texture,System.Single)
extern void VectorLine__ctor_mA76E24820F9C54CA6BCF7DB51547FBA65A7B6572 (void);
// 0x00000031 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,Vectrosity.LineType)
extern void VectorLine__ctor_mCF6F9F1D6CBFEE596511C1C431E521B645878B81 (void);
// 0x00000032 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Texture,System.Single,Vectrosity.LineType)
extern void VectorLine__ctor_m3A6502A81C3C996476E1A78068A7DA97B2FD4153 (void);
// 0x00000033 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Texture,System.Single,Vectrosity.LineType,Vectrosity.Joins)
extern void VectorLine__ctor_mFED22F1A7B1AF1CE7E46CD407DA7FA628A26A221 (void);
// 0x00000034 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single)
extern void VectorLine__ctor_mF29F9C3B81B6F7DAA7F2DD7C9C16B60FB7AA4EE5 (void);
// 0x00000035 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Texture,System.Single)
extern void VectorLine__ctor_mFE3D3EBCBB6D36734F2E483301095B3D5CC6908B (void);
// 0x00000036 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single,Vectrosity.LineType)
extern void VectorLine__ctor_m981B4FB629354CEDCC557024D3CE2BF8F6A8394E (void);
// 0x00000037 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Texture,System.Single,Vectrosity.LineType)
extern void VectorLine__ctor_m642E8D9302422588FB033C7D1C84C6AC6D876D51 (void);
// 0x00000038 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single,Vectrosity.LineType,Vectrosity.Joins)
extern void VectorLine__ctor_m72BF598A96DCC3355ED552D7B1C862340CFAE47A (void);
// 0x00000039 System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Texture,System.Single,Vectrosity.LineType,Vectrosity.Joins)
extern void VectorLine__ctor_m43DD0DBCBDC550B0FEE151089D05383B6451DCD4 (void);
// 0x0000003A System.Void Vectrosity.VectorLine::.cctor()
extern void VectorLine__cctor_mD33BE1921623B01237B6CA2B285E597079730503 (void);
// 0x0000003B UnityEngine.Vector3[] Vectrosity.VectorLine::get_lineVertices()
extern void VectorLine_get_lineVertices_mC4D7969376459F3E60138EDDCA9D581F0702D70C (void);
// 0x0000003C UnityEngine.Vector2[] Vectrosity.VectorLine::get_lineUVs()
extern void VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71 (void);
// 0x0000003D UnityEngine.Color32[] Vectrosity.VectorLine::get_lineColors()
extern void VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1 (void);
// 0x0000003E System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorLine::get_lineTriangles()
extern void VectorLine_get_lineTriangles_m6FE31B265CC9E175DBC5BD5D91256EAA7ADE78C2 (void);
// 0x0000003F UnityEngine.RectTransform Vectrosity.VectorLine::get_rectTransform()
extern void VectorLine_get_rectTransform_m2512EBE50E013130E38A8A88FDB20EF789D98E6D (void);
// 0x00000040 UnityEngine.Color32 Vectrosity.VectorLine::get_color()
extern void VectorLine_get_color_mF7E127ACF8E4A6B90DC641629EE40A02D22D20AF (void);
// 0x00000041 System.Void Vectrosity.VectorLine::set_color(UnityEngine.Color32)
extern void VectorLine_set_color_m98D6CA28D53C00FDD4582C72CE82F2AD3A8A84B2 (void);
// 0x00000042 System.Boolean Vectrosity.VectorLine::get_is2D()
extern void VectorLine_get_is2D_mAC08BCA4315C6949F1AF7CE126C4941B2D1B933B (void);
// 0x00000043 System.Collections.Generic.List`1<UnityEngine.Vector2> Vectrosity.VectorLine::get_points2()
extern void VectorLine_get_points2_mBCE99471CD482E8ECBABC5987B0A01B477AC20B4 (void);
// 0x00000044 System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::get_points3()
extern void VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50 (void);
// 0x00000045 System.Void Vectrosity.VectorLine::set_points3(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void VectorLine_set_points3_m2FE6D35A38BD92CFEC2A761D353D5EB904147636 (void);
// 0x00000046 System.Int32 Vectrosity.VectorLine::get_pointsCount()
extern void VectorLine_get_pointsCount_m6B59F9C7889B0E88287C55337EB505FF733B91B8 (void);
// 0x00000047 System.Void Vectrosity.VectorLine::set_lineWidth(System.Single)
extern void VectorLine_set_lineWidth_m6493CEABA0518D25B6B2841848968BC8839CA6D3 (void);
// 0x00000048 System.String Vectrosity.VectorLine::get_name()
extern void VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99 (void);
// 0x00000049 System.Void Vectrosity.VectorLine::set_name(System.String)
extern void VectorLine_set_name_mFE48A2B46F162DF9C7B431D1FB04D972D704757A (void);
// 0x0000004A System.Void Vectrosity.VectorLine::set_material(UnityEngine.Material)
extern void VectorLine_set_material_m80E4743F822E61C1E4B1DAC8E5AA5D5EC0BBE038 (void);
// 0x0000004B UnityEngine.Texture Vectrosity.VectorLine::get_texture()
extern void VectorLine_get_texture_m80B0BB07297D6E6A53789EFF36AFA678FACCA27E (void);
// 0x0000004C System.Void Vectrosity.VectorLine::set_texture(UnityEngine.Texture)
extern void VectorLine_set_texture_m52C12824951FE41F164DABE0C91CE9473443FE79 (void);
// 0x0000004D System.Void Vectrosity.VectorLine::set_layer(System.Int32)
extern void VectorLine_set_layer_mAC1EC9C98E51B720570E971AB28B8A8EABD82B39 (void);
// 0x0000004E System.Boolean Vectrosity.VectorLine::get_active()
extern void VectorLine_get_active_m3B3628759C5C506D16EEFB12D8BB6539B95DB2C5 (void);
// 0x0000004F System.Void Vectrosity.VectorLine::set_active(System.Boolean)
extern void VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484 (void);
// 0x00000050 Vectrosity.LineType Vectrosity.VectorLine::get_lineType()
extern void VectorLine_get_lineType_m0BDEF27EA27FE80EB0F45DA731FA2F924BBDFAB5 (void);
// 0x00000051 System.Void Vectrosity.VectorLine::set_lineType(Vectrosity.LineType)
extern void VectorLine_set_lineType_m4F30A2A4B65EBB3D3F4B5F1844207DDF4483BB5D (void);
// 0x00000052 System.Void Vectrosity.VectorLine::set_capLength(System.Single)
extern void VectorLine_set_capLength_mBA4C11DDE93CD5A965246FC6F8A2161F51A3E00E (void);
// 0x00000053 System.Boolean Vectrosity.VectorLine::get_smoothWidth()
extern void VectorLine_get_smoothWidth_mF8D0CD6A0113B6DDD65D7460381C71AC1E65DF9C (void);
// 0x00000054 System.Boolean Vectrosity.VectorLine::get_smoothColor()
extern void VectorLine_get_smoothColor_m2424D5703ACCC01C345FD966FA45D8906AA0A50E (void);
// 0x00000055 System.Void Vectrosity.VectorLine::set_joins(Vectrosity.Joins)
extern void VectorLine_set_joins_m7652EB4ECA41F82ADDD13FB5BBF778B7308EA8FD (void);
// 0x00000056 System.Boolean Vectrosity.VectorLine::get_isAutoDrawing()
extern void VectorLine_get_isAutoDrawing_m3E9201D7D2D40BC96FB60159D8130E72B03D6756 (void);
// 0x00000057 System.Int32 Vectrosity.VectorLine::get_drawStart()
extern void VectorLine_get_drawStart_mDFA374E9175AC32A8908BC389A707435F94402FD (void);
// 0x00000058 System.Void Vectrosity.VectorLine::set_drawStart(System.Int32)
extern void VectorLine_set_drawStart_m41499CFDF913A402CD6222A390C7D614FB508804 (void);
// 0x00000059 System.Int32 Vectrosity.VectorLine::get_drawEnd()
extern void VectorLine_get_drawEnd_m0F1DCB33137E4C565B906049CC7030E51EE67375 (void);
// 0x0000005A System.Void Vectrosity.VectorLine::set_drawEnd(System.Int32)
extern void VectorLine_set_drawEnd_m12610BC3583F2391E481EB66BD9E3850C9CFA785 (void);
// 0x0000005B System.Void Vectrosity.VectorLine::set_endPointsUpdate(System.Int32)
extern void VectorLine_set_endPointsUpdate_mEF3375E7D07ACA12ECD72038F195E5185858950C (void);
// 0x0000005C System.Void Vectrosity.VectorLine::set_endCap(System.String)
extern void VectorLine_set_endCap_m22A740E34C479CD9AF1E52CA40B6931B10594691 (void);
// 0x0000005D System.Void Vectrosity.VectorLine::set_continuousTexture(System.Boolean)
extern void VectorLine_set_continuousTexture_m425B9D7DE26971A83C0DEC2610E7D2C3231DB47A (void);
// 0x0000005E System.Void Vectrosity.VectorLine::set_drawTransform(UnityEngine.Transform)
extern void VectorLine_set_drawTransform_m64A6D4FFBC0B49EFC72504521A235DB828F62309 (void);
// 0x0000005F System.Void Vectrosity.VectorLine::set_useViewportCoords(System.Boolean)
extern void VectorLine_set_useViewportCoords_mFC89C638ED7189FA21AAD70AA9191680CB680C8E (void);
// 0x00000060 System.Void Vectrosity.VectorLine::set_textureScale(System.Single)
extern void VectorLine_set_textureScale_m0ED3FC9B8B82F3CB23B752F22A727B5E5D36490E (void);
// 0x00000061 System.Void Vectrosity.VectorLine::set_textureOffset(System.Single)
extern void VectorLine_set_textureOffset_mC2ACD00E1018CAE81FE8E4F02C92EB63A0D0CA4D (void);
// 0x00000062 System.Boolean Vectrosity.VectorLine::get_collider()
extern void VectorLine_get_collider_mFDE7C5C00C573D8E7DCF8CE1A9E42435F3EAE875 (void);
// 0x00000063 System.Void Vectrosity.VectorLine::set_collider(System.Boolean)
extern void VectorLine_set_collider_m5A9FEDD8EA796BCD7228552179BAA2FA9764CE9B (void);
// 0x00000064 System.Void Vectrosity.VectorLine::set_physicsMaterial(UnityEngine.PhysicsMaterial2D)
extern void VectorLine_set_physicsMaterial_m03616DE184EC25C18022853A0326512E5CBCAEA6 (void);
// 0x00000065 System.Void Vectrosity.VectorLine::set_alignOddWidthToPixels(System.Boolean)
extern void VectorLine_set_alignOddWidthToPixels_m963FB550EABA19DB12429A2EFB1DBB1D2B075405 (void);
// 0x00000066 UnityEngine.Canvas Vectrosity.VectorLine::get_canvas()
extern void VectorLine_get_canvas_mC9A4C9950A0254C3E05C42531F787BEE373A9223 (void);
// 0x00000067 UnityEngine.Vector3 Vectrosity.VectorLine::get_camTransformPosition()
extern void VectorLine_get_camTransformPosition_mC2DB90AF4A6F753C4D0C669DC528122FCF27527C (void);
// 0x00000068 System.Boolean Vectrosity.VectorLine::get_camTransformExists()
extern void VectorLine_get_camTransformExists_m8E195A7DF3DFEAAE75193FC18D77E46851056FBB (void);
// 0x00000069 Vectrosity.LineManager Vectrosity.VectorLine::get_lineManager()
extern void VectorLine_get_lineManager_m991C1E0A9EF5B315392BC66383A06E71C4037EB1 (void);
// 0x0000006A System.Void Vectrosity.VectorLine::AddColliderIfNeeded()
extern void VectorLine_AddColliderIfNeeded_mE136857BFAF0053EED3C32E7280FA55BDEB21A6A (void);
// 0x0000006B System.Void Vectrosity.VectorLine::SetupLine(System.String,UnityEngine.Texture,System.Single,Vectrosity.LineType,Vectrosity.Joins,System.Boolean)
extern void VectorLine_SetupLine_mC81248EF5A09665E7275BBDCD8E482A7C8DB44BB (void);
// 0x0000006C System.Void Vectrosity.VectorLine::SetupTriangles(System.Int32)
extern void VectorLine_SetupTriangles_m485E5A0CBDD74E3E9DFA3C7DFDB87ECCD1FC209C (void);
// 0x0000006D System.Void Vectrosity.VectorLine::SetLastFillTriangles()
extern void VectorLine_SetLastFillTriangles_mC0964677D0A1CF474365916BF8B4EDD0591EABB6 (void);
// 0x0000006E System.Void Vectrosity.VectorLine::SetupEndCap(System.Single[])
extern void VectorLine_SetupEndCap_m1AF8F7E4FC2026D1E85B4ACAF501AEF522541B75 (void);
// 0x0000006F System.Void Vectrosity.VectorLine::ResetLine()
extern void VectorLine_ResetLine_mB9ECA94280E9ECA70AD820B486404966811CD771 (void);
// 0x00000070 System.Void Vectrosity.VectorLine::SetEndCapUVs()
extern void VectorLine_SetEndCapUVs_m4B6A2A1748CC3B4F32267542B6EBC678458D13A5 (void);
// 0x00000071 System.Void Vectrosity.VectorLine::RemoveEndCap()
extern void VectorLine_RemoveEndCap_mB2CB7582A089D4BADB0AB895B19ADCB71460AF67 (void);
// 0x00000072 System.Void Vectrosity.VectorLine::SetupTransform(UnityEngine.RectTransform)
extern void VectorLine_SetupTransform_mFD551BECE142C8FC1BDBFB3F3FE280B551D6F942 (void);
// 0x00000073 System.Void Vectrosity.VectorLine::ResizeMeshArrays(System.Int32)
extern void VectorLine_ResizeMeshArrays_mA85B233F6968E7939323705E1E3CD50BED2B8777 (void);
// 0x00000074 System.Void Vectrosity.VectorLine::Resize(System.Int32)
extern void VectorLine_Resize_mBD3C2BFDF301D4BCE7B3F52ED5974881752E8457 (void);
// 0x00000075 System.Void Vectrosity.VectorLine::Resize()
extern void VectorLine_Resize_mBD8DDE07DB8E8B0F8ACF875541EDEFB03DDDAE6D (void);
// 0x00000076 System.Void Vectrosity.VectorLine::ResizeLineWidths(System.Int32)
extern void VectorLine_ResizeLineWidths_mAB6A3F63F55F35B5254C6CB5787A2B6CB38A166F (void);
// 0x00000077 System.Void Vectrosity.VectorLine::SetUVs(System.Int32,System.Int32)
extern void VectorLine_SetUVs_m2D1986C499A49876D0C0080B7DEDC78B42768499 (void);
// 0x00000078 System.Boolean Vectrosity.VectorLine::SetVertexCount()
extern void VectorLine_SetVertexCount_m1FFF40FF083EB27DFADD316C491F868BF2B901A8 (void);
// 0x00000079 System.Int32 Vectrosity.VectorLine::MaxPoints()
extern void VectorLine_MaxPoints_m1DA8DB27B519E5B99B76CC3F851DB71D4B31882E (void);
// 0x0000007A UnityEngine.Vector4[] Vectrosity.VectorLine::CalculateTangents(UnityEngine.Vector3[])
extern void VectorLine_CalculateTangents_mCC45C3345D6020C7267838B8616C4E938B031548 (void);
// 0x0000007B UnityEngine.GameObject Vectrosity.VectorLine::SetupVectorCanvas()
extern void VectorLine_SetupVectorCanvas_mF970D0189F2AB50F20B26A583BA05FA4AAF93A81 (void);
// 0x0000007C System.Void Vectrosity.VectorLine::SetCanvasCamera(UnityEngine.Camera)
extern void VectorLine_SetCanvasCamera_mCEA3FB851E347CF9EE3CEF0B992E048904AF5FA9 (void);
// 0x0000007D System.Void Vectrosity.VectorLine::SetMask(UnityEngine.GameObject)
extern void VectorLine_SetMask_m62273CEA69B4215B988B603EA1F04C74ACA16EB1 (void);
// 0x0000007E System.Void Vectrosity.VectorLine::SetMask(UnityEngine.GameObject,System.Boolean)
extern void VectorLine_SetMask_m01707C3C2DE563C2E9AB75AB49C0C830DE3724D3 (void);
// 0x0000007F System.Void Vectrosity.VectorLine::SetMask(UnityEngine.UI.Mask,System.Boolean)
extern void VectorLine_SetMask_m4E5C79CE7F07F15BF6CF13A1F6FA3007A6534561 (void);
// 0x00000080 System.Boolean Vectrosity.VectorLine::CheckCamera3D()
extern void VectorLine_CheckCamera3D_mF55C01A2426FA14FD9ECDAE9C715F74036283E1D (void);
// 0x00000081 System.Void Vectrosity.VectorLine::SetCamera3D()
extern void VectorLine_SetCamera3D_mA766EB77086D6E7DED2227289AAA4F013040212B (void);
// 0x00000082 System.Void Vectrosity.VectorLine::SetCamera3D(UnityEngine.Camera)
extern void VectorLine_SetCamera3D_m38D807920F285B6AEE047D11668B59393132C3AF (void);
// 0x00000083 System.Boolean Vectrosity.VectorLine::CameraHasMoved()
extern void VectorLine_CameraHasMoved_mA922882080F6465C9A77E0C7A41EFF74BECFDCBB (void);
// 0x00000084 System.Void Vectrosity.VectorLine::UpdateCameraInfo()
extern void VectorLine_UpdateCameraInfo_mEB6D9ECDA246E306452384947018891838D3B6FA (void);
// 0x00000085 System.Int32 Vectrosity.VectorLine::GetSegmentNumber()
extern void VectorLine_GetSegmentNumber_mFD71AB492DA3B386A0D89C0E16B9D843A406B1BC (void);
// 0x00000086 System.Void Vectrosity.VectorLine::SetEndCapColors()
extern void VectorLine_SetEndCapColors_m996586C74B40D8C631CADD85FFD5EFF584501045 (void);
// 0x00000087 System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color32)
extern void VectorLine_SetColor_m7A4D856B8A7699488479E0E24757CFF2D9947E98 (void);
// 0x00000088 System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color32,System.Int32)
extern void VectorLine_SetColor_m6784A75290F0417D9107DCCCAC453CC6D5EFDBC9 (void);
// 0x00000089 System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color32,System.Int32,System.Int32)
extern void VectorLine_SetColor_m31BD9E19DC2D5BEF6CE512495733C775DC520346 (void);
// 0x0000008A System.Void Vectrosity.VectorLine::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern void VectorLine_SetColors_m1C38CB6364D8641D09AC69AE2D0B939CB44C76AA (void);
// 0x0000008B System.Void Vectrosity.VectorLine::SetSegmentStartEnd(System.Int32&,System.Int32&)
extern void VectorLine_SetSegmentStartEnd_m1D0DD6E60F18A4A49BE4B4500FC2497DD8308E62 (void);
// 0x0000008C System.Void Vectrosity.VectorLine::SetupWidths(System.Int32)
extern void VectorLine_SetupWidths_mDAA13D5F8E0630851AA239601499571A5E24D8CA (void);
// 0x0000008D System.Void Vectrosity.VectorLine::SetWidths(System.Collections.Generic.List`1<System.Single>)
extern void VectorLine_SetWidths_m40C16DC2D4A3E4351E733B6ABFB33E83584F5D05 (void);
// 0x0000008E System.Void Vectrosity.VectorLine::SetWidths(System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean)
extern void VectorLine_SetWidths_m8F438D1B99E4BB63DDB5815D8C22D98AF85066DC (void);
// 0x0000008F Vectrosity.VectorLine Vectrosity.VectorLine::SetLine(UnityEngine.Color,UnityEngine.Vector2[])
extern void VectorLine_SetLine_mE346724DE5B2FE84E2D95F898B9BE8C99AF0BFED (void);
// 0x00000090 Vectrosity.VectorLine Vectrosity.VectorLine::SetLine(UnityEngine.Color,System.Single,UnityEngine.Vector2[])
extern void VectorLine_SetLine_m41F891C32B7140843825D8BD5E94FC0715704750 (void);
// 0x00000091 System.Void Vectrosity.VectorLine::CheckNormals()
extern void VectorLine_CheckNormals_mFA94845E35FD04007434A5DBD4096B0D971BBA35 (void);
// 0x00000092 System.Void Vectrosity.VectorLine::CheckLine(System.Boolean)
extern void VectorLine_CheckLine_m3088908F463DF07DFB2D244488C4CB6D3AF6E950 (void);
// 0x00000093 System.Void Vectrosity.VectorLine::DrawEndCap(System.Boolean)
extern void VectorLine_DrawEndCap_mD042BC5BD19A70477B9CB3A258AF57DAAC2F0380 (void);
// 0x00000094 System.Void Vectrosity.VectorLine::ScaleCapVertices(System.Int32,System.Single,UnityEngine.Vector3)
extern void VectorLine_ScaleCapVertices_m8E168F56BD3E82E7259159BB4B8483174C5F8DAD (void);
// 0x00000095 System.Void Vectrosity.VectorLine::SetContinuousTexture()
extern void VectorLine_SetContinuousTexture_mD2749B491AEB6EC67B5F3205F76A536D7014E68C (void);
// 0x00000096 System.Boolean Vectrosity.VectorLine::UseMatrix(UnityEngine.Matrix4x4&)
extern void VectorLine_UseMatrix_m7A0F47D8DE335100596DECB3236288CC1D1302F8 (void);
// 0x00000097 System.Boolean Vectrosity.VectorLine::CheckPointCount()
extern void VectorLine_CheckPointCount_m6227536810BB3E1A41A683B5F338602104FE48A4 (void);
// 0x00000098 System.Void Vectrosity.VectorLine::ClearTriangles()
extern void VectorLine_ClearTriangles_m3D40759F9E2E851FCB500FC93ED1ECCD0171C1A2 (void);
// 0x00000099 System.Void Vectrosity.VectorLine::SetupDrawStartEnd(System.Int32&,System.Int32&,System.Boolean)
extern void VectorLine_SetupDrawStartEnd_mD1C039FA000096212EB9BA7146BE447F8E1C3F6A (void);
// 0x0000009A System.Void Vectrosity.VectorLine::ZeroVertices(System.Int32,System.Int32)
extern void VectorLine_ZeroVertices_mFE36F2E91D7F89114DEC9AF5DE9AEB22407E1C5C (void);
// 0x0000009B System.Void Vectrosity.VectorLine::SetupCanvasState(Vectrosity.CanvasState)
extern void VectorLine_SetupCanvasState_m6EBA47AC200EF50527C4B636E09FE7E3B7742105 (void);
// 0x0000009C System.Void Vectrosity.VectorLine::Draw()
extern void VectorLine_Draw_mA2B2502DC0BFD355E1FF3D252DC513B386D9D6C0 (void);
// 0x0000009D System.Void Vectrosity.VectorLine::Line2D(System.Int32,System.Int32,UnityEngine.Matrix4x4,System.Boolean)
extern void VectorLine_Line2D_m14735DBE89365A122066C7EC3F7EA4CC0F63AFD2 (void);
// 0x0000009E System.Void Vectrosity.VectorLine::Line3D(System.Int32,System.Int32,UnityEngine.Matrix4x4,System.Boolean)
extern void VectorLine_Line3D_m5113C4AE79B220D1F0D6AB36EC815E87B1585489 (void);
// 0x0000009F System.Void Vectrosity.VectorLine::CheckDrawStartFill(System.Int32)
extern void VectorLine_CheckDrawStartFill_mB5B9AB1133C15B7024F1317B29E8C7E57296E0FA (void);
// 0x000000A0 System.Void Vectrosity.VectorLine::Draw3D()
extern void VectorLine_Draw3D_m934EFE9DA1927007CE80C86AD4A02D2358D67BD6 (void);
// 0x000000A1 System.Boolean Vectrosity.VectorLine::IntersectAndDoSkip(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single&,UnityEngine.Ray&,UnityEngine.Plane&)
extern void VectorLine_IntersectAndDoSkip_mA23F6A8C74DD7FE4EEE982C4DCB556A029724801 (void);
// 0x000000A2 UnityEngine.Vector3 Vectrosity.VectorLine::PlaneIntersectionPoint(UnityEngine.Ray&,UnityEngine.Plane&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void VectorLine_PlaneIntersectionPoint_mB8B4739D8AD751BE31AF7AB7BCE2AE3717B36E16 (void);
// 0x000000A3 System.Void Vectrosity.VectorLine::DrawPoints()
extern void VectorLine_DrawPoints_m1DEF195519B04C24809CB080DF409CD0831B01F0 (void);
// 0x000000A4 System.Void Vectrosity.VectorLine::DrawPoints3D()
extern void VectorLine_DrawPoints3D_m0F80898FEC93C6FF5DBEBC398F29C9B60526A9BF (void);
// 0x000000A5 System.Void Vectrosity.VectorLine::SkipQuad(System.Int32&,System.Int32&,System.Int32&)
extern void VectorLine_SkipQuad_mDEC1739D41FB0C7B2ECAC1EB6CFD35619FDA6B0D (void);
// 0x000000A6 System.Void Vectrosity.VectorLine::SkipQuad3D(System.Int32&,System.Int32&,System.Int32&)
extern void VectorLine_SkipQuad3D_m2FB947C29890F2600E84AC628B97245DD8312C07 (void);
// 0x000000A7 System.Void Vectrosity.VectorLine::WeldJoins(System.Int32,System.Int32,System.Boolean)
extern void VectorLine_WeldJoins_m7079ADA8BF3767F6E195786602DE78C8773DAF77 (void);
// 0x000000A8 System.Void Vectrosity.VectorLine::WeldJoinsDiscrete(System.Int32,System.Int32,System.Boolean)
extern void VectorLine_WeldJoinsDiscrete_mF5C2D0550D41D6BC50B73AEB14773CD8339EEBF1 (void);
// 0x000000A9 System.Void Vectrosity.VectorLine::SetIntersectionPoint(System.Int32,System.Int32,System.Int32,System.Int32)
extern void VectorLine_SetIntersectionPoint_m8AAC76F59C4BAC1845C6F6EE901DF055C2BD4C52 (void);
// 0x000000AA System.Void Vectrosity.VectorLine::WeldJoins3D(System.Int32,System.Int32,System.Boolean)
extern void VectorLine_WeldJoins3D_m992CCB9840929E8E38695926AE730A35022EE605 (void);
// 0x000000AB System.Void Vectrosity.VectorLine::WeldJoinsDiscrete3D(System.Int32,System.Int32,System.Boolean)
extern void VectorLine_WeldJoinsDiscrete3D_mEF5454B82465CC47256B47E982C052A27C72E295 (void);
// 0x000000AC System.Void Vectrosity.VectorLine::SetIntersectionPoint3D(System.Int32,System.Int32,System.Int32,System.Int32)
extern void VectorLine_SetIntersectionPoint3D_m920049D1AF6DE6BDA56455A436883279F3FF5829 (void);
// 0x000000AD System.Void Vectrosity.VectorLine::LineManagerCheckDistance()
extern void VectorLine_LineManagerCheckDistance_mC373D13D5898B27529FD0A764B01529E2E898E11 (void);
// 0x000000AE System.Void Vectrosity.VectorLine::LineManagerDisable()
extern void VectorLine_LineManagerDisable_mDFC81EA5E94893C402586C2CD5CAE16B6CF4041A (void);
// 0x000000AF System.Void Vectrosity.VectorLine::LineManagerEnable()
extern void VectorLine_LineManagerEnable_m6DC10E4E9DDCE5FD5267CB13BFB258D1AAF6F4D7 (void);
// 0x000000B0 System.Void Vectrosity.VectorLine::Draw3DAuto()
extern void VectorLine_Draw3DAuto_m8DFAB1EB030ACF8EB2EDA0CFC79BAB2F8C5A5CF5 (void);
// 0x000000B1 System.Void Vectrosity.VectorLine::Draw3DAuto(System.Single)
extern void VectorLine_Draw3DAuto_m179354EAAFB85F458AD4AF8D16BC271CFDBDBD63 (void);
// 0x000000B2 System.Void Vectrosity.VectorLine::StopDrawing3DAuto()
extern void VectorLine_StopDrawing3DAuto_mCCAC65DF8DA282D8A07BA726F5384E2336B9AE53 (void);
// 0x000000B3 System.Void Vectrosity.VectorLine::SetTextureScale()
extern void VectorLine_SetTextureScale_mF9F98E2E9F288BF052B17EB617DEC5C56D149A27 (void);
// 0x000000B4 System.Void Vectrosity.VectorLine::ResetTextureScale()
extern void VectorLine_ResetTextureScale_m2CC3461934DFA31FF84FC075BD855AEFAF4BFFBB (void);
// 0x000000B5 System.Void Vectrosity.VectorLine::SetCollider(System.Boolean)
extern void VectorLine_SetCollider_m8924A817379C52F56CB01F08C187DE0DB2D2E75E (void);
// 0x000000B6 System.Void Vectrosity.VectorLine::SetPathVerticesContinuous(System.Int32&,System.Int32&,System.Int32&,UnityEngine.Vector2[])
extern void VectorLine_SetPathVerticesContinuous_mD2DA784D6D86FDE105DE597067B6A0806109263E (void);
// 0x000000B7 System.Void Vectrosity.VectorLine::SetPathWorldVerticesContinuous(System.Int32&,UnityEngine.Vector3&,System.Int32&,System.Int32&,UnityEngine.Vector2[])
extern void VectorLine_SetPathWorldVerticesContinuous_m39AAF33965B74FD57B09E2C485E6315DC6773F3B (void);
// 0x000000B8 System.Void Vectrosity.VectorLine::SetPathVerticesDiscrete(System.Int32&,System.Int32&,UnityEngine.Vector2[],UnityEngine.PolygonCollider2D)
extern void VectorLine_SetPathVerticesDiscrete_m442257B238132AF76EBBDFABC5CC0E435C74B0EF (void);
// 0x000000B9 System.Void Vectrosity.VectorLine::SetPathWorldVerticesDiscrete(System.Int32&,UnityEngine.Vector3&,System.Int32&,UnityEngine.Vector2[],UnityEngine.PolygonCollider2D)
extern void VectorLine_SetPathWorldVerticesDiscrete_mE89980DD5103B25A2BC2DDF5CFC6CDF955951846 (void);
// 0x000000BA System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::BytesToVector3List(System.Byte[])
extern void VectorLine_BytesToVector3List_m92D93F096DC825892828B27099766DECA62E2E24 (void);
// 0x000000BB System.Void Vectrosity.VectorLine::SetupByteBlock()
extern void VectorLine_SetupByteBlock_m5287848984F4F07A21A5B29359FA73F9B446DDBF (void);
// 0x000000BC System.Single Vectrosity.VectorLine::ConvertToFloat(System.Byte[],System.Int32)
extern void VectorLine_ConvertToFloat_m139055584433158D7E470567A31560A9815675A8 (void);
// 0x000000BD System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorLine&)
extern void VectorLine_Destroy_mC44B22F624821712A379ED38A54E638772BB1345 (void);
// 0x000000BE System.Void Vectrosity.VectorLine::DestroyLine(Vectrosity.VectorLine&)
extern void VectorLine_DestroyLine_mD571D169FFD001B6910AAC1F3AAB319776DA4D2D (void);
// 0x000000BF System.Void Vectrosity.VectorLine::SetDistances()
extern void VectorLine_SetDistances_mC45218EDE586EDDDF7A9DD6ED50D72B15EDBFB13 (void);
// 0x000000C0 System.Single Vectrosity.VectorLine::GetLength()
extern void VectorLine_GetLength_mBECDD1C2C54009DCB262D2BFFCBD50D158C19480 (void);
// 0x000000C1 UnityEngine.Vector2 Vectrosity.VectorLine::GetPoint01(System.Single)
extern void VectorLine_GetPoint01_m2FFD624F37CB86F3FD6FF564F93853001109D85C (void);
// 0x000000C2 UnityEngine.Vector2 Vectrosity.VectorLine::GetPoint(System.Single,System.Int32&)
extern void VectorLine_GetPoint_m6935F6C2223BCE6B35F616C46461A5ECD621E339 (void);
// 0x000000C3 UnityEngine.Vector3 Vectrosity.VectorLine::GetPoint3D01(System.Single)
extern void VectorLine_GetPoint3D01_m91E1BF01DE36837BACB94BAD2FD7C70D69BE318B (void);
// 0x000000C4 UnityEngine.Vector3 Vectrosity.VectorLine::GetPoint3D(System.Single,System.Int32&)
extern void VectorLine_GetPoint3D_m60100B844606B06D3290DE66517E73125D3EE692 (void);
// 0x000000C5 System.Void Vectrosity.VectorLine::SetDistanceIndex(System.Int32&,System.Single)
extern void VectorLine_SetDistanceIndex_m63DED4B34EDD0089947000149F62EF0C82876858 (void);
// 0x000000C6 System.Void Vectrosity.VectorLine::SetEndCap(System.String,Vectrosity.EndCap,UnityEngine.Texture2D[])
extern void VectorLine_SetEndCap_m049F8A564A1C48162E576B348AA59D5A3587A784 (void);
// 0x000000C7 System.Void Vectrosity.VectorLine::SetEndCap(System.String,Vectrosity.EndCap,System.Single,System.Single,System.Single,System.Single,UnityEngine.Texture2D[])
extern void VectorLine_SetEndCap_mDFDEF4F5BB544D0E98674FA7FEA2B4E2C8F8E7FE (void);
// 0x000000C8 UnityEngine.Color32[] Vectrosity.VectorLine::GetRowPixels(UnityEngine.Color32[],System.Int32,System.Int32,System.Int32)
extern void VectorLine_GetRowPixels_mAF24EDCA0635137A39A4ADEB97AD1B1933264AED (void);
// 0x000000C9 UnityEngine.Color32[] Vectrosity.VectorLine::GetRotatedPixels(UnityEngine.Texture2D)
extern void VectorLine_GetRotatedPixels_m04915D23250F9A53094E11B0BE03FDE31902E314 (void);
// 0x000000CA System.Void Vectrosity.VectorLine::RemoveEndCap(System.String)
extern void VectorLine_RemoveEndCap_mCABBCC9AF23A507A5697C5065F0FB602CDDA9B92 (void);
// 0x000000CB System.Boolean Vectrosity.VectorLine::Selected(UnityEngine.Vector2,System.Int32,System.Int32&)
extern void VectorLine_Selected_mCD27A972D1B6D4336CD53811B4ECBAEB404C3BA5 (void);
// 0x000000CC System.Boolean Vectrosity.VectorLine::Selected(UnityEngine.Vector2,System.Int32,System.Int32,System.Int32&,UnityEngine.Camera)
extern void VectorLine_Selected_mFCEDC296BA17A8EB3F5FFF261ABDCE07D6A3F89C (void);
// 0x000000CD System.Boolean Vectrosity.VectorLine::Approximately(UnityEngine.Vector2,UnityEngine.Vector2)
extern void VectorLine_Approximately_mA4BE18DE3EAFA1659760BCFF81C067B7935E8291 (void);
// 0x000000CE System.Boolean Vectrosity.VectorLine::Approximately(UnityEngine.Vector3,UnityEngine.Vector3)
extern void VectorLine_Approximately_mDEC01376DAB9CDF181251A1F51E7B36387B55169 (void);
// 0x000000CF System.Boolean Vectrosity.VectorLine::Approximately(System.Single,System.Single)
extern void VectorLine_Approximately_mB98966FB5DC37D3300967816B49E370BF6A11B3D (void);
// 0x000000D0 System.Boolean Vectrosity.VectorLine::WrongArrayLength(System.Int32,Vectrosity.VectorLine_FunctionName)
extern void VectorLine_WrongArrayLength_mC7EDC1E0586585BD55B82D77238D4EF96C48CC30 (void);
// 0x000000D1 System.Boolean Vectrosity.VectorLine::CheckArrayLength(Vectrosity.VectorLine_FunctionName,System.Int32,System.Int32)
extern void VectorLine_CheckArrayLength_mC1FC977B826545DA6432F2C89AEFE32B875F75EC (void);
// 0x000000D2 System.Void Vectrosity.VectorLine::MakeRect(UnityEngine.Rect,System.Int32)
extern void VectorLine_MakeRect_m230F3FF4589FC594085ACE2155E8EF9FDD6A4421 (void);
// 0x000000D3 System.Void Vectrosity.VectorLine::MakeRect(UnityEngine.Vector3,UnityEngine.Vector3)
extern void VectorLine_MakeRect_mE636A899F338C362F4E10FA230CEAB1FA5A1E305 (void);
// 0x000000D4 System.Void Vectrosity.VectorLine::MakeRect(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern void VectorLine_MakeRect_m857C00C4957A49FF452D52DF8DB3D3064A68B9D8 (void);
// 0x000000D5 System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,System.Single)
extern void VectorLine_MakeCircle_m5E30180B8F4FBDDA631A5700BCB467C6F3F2E8F7 (void);
// 0x000000D6 System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,System.Single,System.Int32,System.Int32)
extern void VectorLine_MakeCircle_m8BB150D7ED8AEA1C862449503A377EB54CE15FCB (void);
// 0x000000D7 System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void VectorLine_MakeCircle_mC7DA57812D996657B98A2A7622B2B90B9F88A3AB (void);
// 0x000000D8 System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Int32)
extern void VectorLine_MakeEllipse_m527E5A392020C655EB552822B439619CD009BD56 (void);
// 0x000000D9 System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Single)
extern void VectorLine_MakeEllipse_m7C8335E4BD8EBA81EE71C27CA361CF4CF22C8C48 (void);
// 0x000000DA System.Void Vectrosity.VectorLine::MakeArc(UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single)
extern void VectorLine_MakeArc_m9C138F6D7BDFA7FE6BC33EAA10B16EE086529AC1 (void);
// 0x000000DB System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Int32)
extern void VectorLine_MakeEllipse_m4C4C09F9F44CBB411DE93CEFCF3A55AB8CD6261C (void);
// 0x000000DC System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector2[],System.Int32)
extern void VectorLine_MakeCurve_mE4914E511699875DFF71D53CABFE97F9E15E414D (void);
// 0x000000DD System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector2[],System.Int32,System.Int32)
extern void VectorLine_MakeCurve_m370E54515A638AEC46582A78A3188CEBCFB1184E (void);
// 0x000000DE System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern void VectorLine_MakeCurve_m27DE90D46F08BD708581566DAD6FC4F14AE2F1A3 (void);
// 0x000000DF System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,System.Int32)
extern void VectorLine_MakeCurve_mA008A10040F1C406E6FBCC38F8D70C9724133C7D (void);
// 0x000000E0 UnityEngine.Vector2 Vectrosity.VectorLine::GetBezierPoint(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single)
extern void VectorLine_GetBezierPoint_m3619255712BEF4054AB91B87C7556440039DBCEF (void);
// 0x000000E1 UnityEngine.Vector3 Vectrosity.VectorLine::GetBezierPoint3D(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)
extern void VectorLine_GetBezierPoint3D_m464EB754E6FB97A7A6127493CD21C3CD5275A784 (void);
// 0x000000E2 System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[])
extern void VectorLine_MakeSpline_m55C6629353773E8A75E857119C083ED171F188A3 (void);
// 0x000000E3 System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],System.Int32,System.Boolean)
extern void VectorLine_MakeSpline_m682A295730D63835C37761BB93161307F9F6B278 (void);
// 0x000000E4 System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector3[],System.Int32,System.Boolean)
extern void VectorLine_MakeSpline_m3C45BB34946E46598327A7CA688D7B29538A16F5 (void);
// 0x000000E5 System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],UnityEngine.Vector3[],System.Int32,System.Int32,System.Boolean)
extern void VectorLine_MakeSpline_mF548237B4C6D38F09615B6C63DE65A4324FA47E4 (void);
// 0x000000E6 UnityEngine.Vector2 Vectrosity.VectorLine::GetSplinePoint(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single)
extern void VectorLine_GetSplinePoint_m6FBFFE5699B3D9B6167B73075BB16E990C49353F (void);
// 0x000000E7 UnityEngine.Vector3 Vectrosity.VectorLine::GetSplinePoint3D(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)
extern void VectorLine_GetSplinePoint3D_m4388D9381CA0CA75B3F6B2DB61138EAB9901507E (void);
// 0x000000E8 System.Single Vectrosity.VectorLine::VectorDistanceSquared(UnityEngine.Vector2&,UnityEngine.Vector2&)
extern void VectorLine_VectorDistanceSquared_m7E430ED1F674E61194FEEFE1A3D0F64321F459B3 (void);
// 0x000000E9 System.Single Vectrosity.VectorLine::VectorDistanceSquared(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void VectorLine_VectorDistanceSquared_mD91B066F494949AE96973B4F0EFF7AF78C03A4D5 (void);
// 0x000000EA System.Void Vectrosity.VectorLine::InitNonuniformCatmullRom(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Vector4&)
extern void VectorLine_InitNonuniformCatmullRom_mEEB56854233B62DCA96675BD54174429EC714DF0 (void);
// 0x000000EB System.Single Vectrosity.VectorLine::EvalCubicPoly(UnityEngine.Vector4&,System.Single)
extern void VectorLine_EvalCubicPoly_m4D2025C80233C82C962CB5E5B945809203227A74 (void);
// 0x000000EC System.Void Vectrosity.VectorLine::MakeText(System.String,UnityEngine.Vector3,System.Single)
extern void VectorLine_MakeText_mDE853F81DE512F24048B04F3E12DD2DAE2E829CA (void);
// 0x000000ED System.Void Vectrosity.VectorLine::MakeText(System.String,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Boolean)
extern void VectorLine_MakeText_mF748B4AC57120DA7F818F0408F4BEF61BC033F2C (void);
// 0x000000EE System.Void Vectrosity.VectorManager::.cctor()
extern void VectorManager__cctor_mDBBAD4573C0BA12C1AE71D0FA1B1DD8A5201803A (void);
// 0x000000EF System.Single Vectrosity.VectorManager::GetBrightnessValue(UnityEngine.Vector3)
extern void VectorManager_GetBrightnessValue_mC949BB3C7A6CC28D5D6598269C0588A816C5E387 (void);
// 0x000000F0 System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness)
extern void VectorManager_ObjectSetup_mF7F932263CD9105D67A4277BE6B4EB1D173F20A8 (void);
// 0x000000F1 System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness,System.Boolean)
extern void VectorManager_ObjectSetup_mAF03886A407E27753714B2600BD7F7E5B475043C (void);
// 0x000000F2 System.Void Vectrosity.VectorManager::ResetLinePoints(Vectrosity.VisibilityControlStatic,Vectrosity.VectorLine)
extern void VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142 (void);
// 0x000000F3 System.Int32 Vectrosity.VectorManager::get_arrayCount()
extern void VectorManager_get_arrayCount_m150A26CB0D835707FBEC2B14DC7381A02232A94D (void);
// 0x000000F4 System.Void Vectrosity.VectorManager::VisibilityStaticSetup(Vectrosity.VectorLine,Vectrosity.RefInt&)
extern void VectorManager_VisibilityStaticSetup_mD098A79500A6B1A0E347CAB4DE0A889D28E2E13A (void);
// 0x000000F5 System.Void Vectrosity.VectorManager::VisibilityStaticRemove(System.Int32)
extern void VectorManager_VisibilityStaticRemove_m9B72F2333EE6877D0F29D93BC18F76ADA5B0D237 (void);
// 0x000000F6 System.Int32 Vectrosity.VectorManager::get_arrayCount2()
extern void VectorManager_get_arrayCount2_m17054F8887ECC80587A291E0AE545283AACE03C7 (void);
// 0x000000F7 System.Void Vectrosity.VectorManager::VisibilitySetup(UnityEngine.Transform,Vectrosity.VectorLine,Vectrosity.RefInt&)
extern void VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2 (void);
// 0x000000F8 System.Void Vectrosity.VectorManager::VisibilityRemove(System.Int32)
extern void VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B (void);
// 0x000000F9 System.Void Vectrosity.VectorManager::CheckDistanceSetup(UnityEngine.Transform,Vectrosity.VectorLine,UnityEngine.Color,Vectrosity.RefInt)
extern void VectorManager_CheckDistanceSetup_m5F270965601AD85B1F94AEBDB4DC391FBEBAB788 (void);
// 0x000000FA System.Void Vectrosity.VectorManager::DistanceRemove(System.Int32)
extern void VectorManager_DistanceRemove_mB9D600CD1338D2EC0722800B2BB2BFEE8F6525A9 (void);
// 0x000000FB System.Void Vectrosity.VectorManager::CheckDistance()
extern void VectorManager_CheckDistance_mEE514C5E086101A2B940A07C2DC92BAAE62BCEA2 (void);
// 0x000000FC System.Void Vectrosity.VectorManager::SetOldDistance(System.Int32,System.Int32)
extern void VectorManager_SetOldDistance_m3647DA7ABBBD286740E465A1A309F097FEE3B8B7 (void);
// 0x000000FD System.Void Vectrosity.VectorManager::SetDistanceColor(System.Int32)
extern void VectorManager_SetDistanceColor_m12DACE05CD5D135C7137F3A2A9BFE1E2FB29E402 (void);
// 0x000000FE System.Void Vectrosity.VectorManager::DrawArrayLine(System.Int32)
extern void VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888 (void);
// 0x000000FF System.Void Vectrosity.VectorManager::DrawArrayLine2(System.Int32)
extern void VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732 (void);
// 0x00000100 System.Void Vectrosity.VectorManager::DrawArrayLines()
extern void VectorManager_DrawArrayLines_mCEEBF6298008C43FE803CB36CF9006E666019744 (void);
// 0x00000101 System.Void Vectrosity.VectorManager::DrawArrayLines2()
extern void VectorManager_DrawArrayLines2_mE6A0035BF8AF9A1A4D8B9BC1E335C6697DC5C5EC (void);
// 0x00000102 UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(Vectrosity.VectorLine)
extern void VectorManager_GetBounds_m23D1D082EF71F5B252E32D9E3DCB68E847595026 (void);
// 0x00000103 UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void VectorManager_GetBounds_m9CCD16DC671F020405D42696786AD9DE2EE295DA (void);
// 0x00000104 UnityEngine.Mesh Vectrosity.VectorManager::MakeBoundsMesh(UnityEngine.Bounds)
extern void VectorManager_MakeBoundsMesh_m72D478FD61D161D6DCB279FB54F759EA59CD93E6 (void);
// 0x00000105 System.Void Vectrosity.VectorManager::SetupBoundsMesh(UnityEngine.GameObject,Vectrosity.VectorLine)
extern void VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59 (void);
// 0x00000106 System.Void Vectrosity.VectorObject2D::.ctor()
extern void VectorObject2D__ctor_m704336135C3BC3D39AEE27CBD644E972D242064D (void);
// 0x00000107 System.Void Vectrosity.VectorObject2D::.cctor()
extern void VectorObject2D__cctor_mC43BA63969D868CF6CBB1761B90010A180335FEB (void);
// 0x00000108 System.Void Vectrosity.VectorObject2D::SetVectorLine(Vectrosity.VectorLine,UnityEngine.Texture,UnityEngine.Material,System.Boolean)
extern void VectorObject2D_SetVectorLine_m87D3085F3CF7880E04576D28E11C80349CDE5724 (void);
// 0x00000109 System.Void Vectrosity.VectorObject2D::Destroy()
extern void VectorObject2D_Destroy_m22B58E8F0D320D29B647094B4F05043956F81698 (void);
// 0x0000010A System.Void Vectrosity.VectorObject2D::DestroyNow()
extern void VectorObject2D_DestroyNow_mAFAAAF538F3A01024B05ECA1E8E7F1EDEC8FDEA7 (void);
// 0x0000010B System.Void Vectrosity.VectorObject2D::Enable(System.Boolean)
extern void VectorObject2D_Enable_m3CF7975FFD23E9CC70209AB333EE640200525205 (void);
// 0x0000010C System.Void Vectrosity.VectorObject2D::SetTexture(UnityEngine.Texture)
extern void VectorObject2D_SetTexture_m356FBE3311E759BBD46F75FC3441755089296BC4 (void);
// 0x0000010D System.Void Vectrosity.VectorObject2D::SetMaterial(UnityEngine.Material)
extern void VectorObject2D_SetMaterial_mA7C634438A73116C9CA8F4B95A1B0E535AE61E8F (void);
// 0x0000010E System.Void Vectrosity.VectorObject2D::UpdateGeometry()
extern void VectorObject2D_UpdateGeometry_mAD79F6A576446303BECDCC5FF0D05D75FA0B6FA7 (void);
// 0x0000010F System.Void Vectrosity.VectorObject2D::SetupMesh()
extern void VectorObject2D_SetupMesh_mAFA235E8824EB01E1F7F6D62806B46291A1DCB0C (void);
// 0x00000110 System.Void Vectrosity.VectorObject2D::SetMeshBounds()
extern void VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6 (void);
// 0x00000111 System.Void Vectrosity.VectorObject2D::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void VectorObject2D_OnPopulateMesh_m0406A9A86232C42EBABB5F237E990039CA174800 (void);
// 0x00000112 System.Void Vectrosity.VectorObject2D::SetName(System.String)
extern void VectorObject2D_SetName_mF846BFE2EAE12F81DC7603F0639F2A89B395CE84 (void);
// 0x00000113 System.Void Vectrosity.VectorObject2D::UpdateVerts()
extern void VectorObject2D_UpdateVerts_mCF94DB3DC0536E4587AEB27ADF3351E6C839D3BD (void);
// 0x00000114 System.Void Vectrosity.VectorObject2D::UpdateUVs()
extern void VectorObject2D_UpdateUVs_mC4B07F5C21023FCCEC97A7B2A9F4F195470BECC4 (void);
// 0x00000115 System.Void Vectrosity.VectorObject2D::UpdateColors()
extern void VectorObject2D_UpdateColors_mA9243733BCA7B03C1031A2605898CF0064A29A48 (void);
// 0x00000116 System.Void Vectrosity.VectorObject2D::UpdateNormals()
extern void VectorObject2D_UpdateNormals_mA2DA97A5E41BC7CC838459F93C695A1003ED0206 (void);
// 0x00000117 System.Void Vectrosity.VectorObject2D::UpdateTangents()
extern void VectorObject2D_UpdateTangents_m320C0A81769ADDC4EFE293E6C0B4F8BFDC70F9ED (void);
// 0x00000118 System.Void Vectrosity.VectorObject2D::UpdateTris()
extern void VectorObject2D_UpdateTris_m99E3922F7DB201168938074590B9CF5601BD0D99 (void);
// 0x00000119 System.Void Vectrosity.VectorObject2D::UpdateMeshAttributes()
extern void VectorObject2D_UpdateMeshAttributes_mC480305BD1408E7D8CFF157FBF327AA9A4031659 (void);
// 0x0000011A System.Void Vectrosity.VectorObject2D::ClearMesh()
extern void VectorObject2D_ClearMesh_mB4859A90C0DFABBFC096ED341686BB70AC023763 (void);
// 0x0000011B System.Int32 Vectrosity.VectorObject2D::VertexCount()
extern void VectorObject2D_VertexCount_m88C4663CE98BA9368EA27089C1C5A405AD1A10EB (void);
// 0x0000011C System.Void Vectrosity.VectorObject3D::.ctor()
extern void VectorObject3D__ctor_m5C4B552F6DBA830A65EB9CD7382A83381B9C72B8 (void);
// 0x0000011D System.Void Vectrosity.VectorObject3D::SetVectorLine(Vectrosity.VectorLine,UnityEngine.Texture,UnityEngine.Material,System.Boolean)
extern void VectorObject3D_SetVectorLine_m8EA2D566D669EB28195896F3D557152FC044C6A6 (void);
// 0x0000011E System.Void Vectrosity.VectorObject3D::Destroy()
extern void VectorObject3D_Destroy_m6DAD5F1A059D699F2B05A6B6B54723D56FBD8A8D (void);
// 0x0000011F System.Void Vectrosity.VectorObject3D::Enable(System.Boolean)
extern void VectorObject3D_Enable_m72EEE3ED96AB44836B53AD9A2662CB87D5AB9576 (void);
// 0x00000120 System.Void Vectrosity.VectorObject3D::SetTexture(UnityEngine.Texture)
extern void VectorObject3D_SetTexture_m646C1DE560A2B30B564DD1B4E9E79CEBD5354517 (void);
// 0x00000121 System.Void Vectrosity.VectorObject3D::SetMaterial(UnityEngine.Material)
extern void VectorObject3D_SetMaterial_mFA8CB205105CEDF2EAFB2351619C476C2E726CF3 (void);
// 0x00000122 System.Void Vectrosity.VectorObject3D::SetupMesh()
extern void VectorObject3D_SetupMesh_mEDD447CA9C0FBC3D6408F7D397FAC4B63030E411 (void);
// 0x00000123 System.Void Vectrosity.VectorObject3D::LateUpdate()
extern void VectorObject3D_LateUpdate_m2DF6E4EDD4423FCA03129E57864AB19843A1E76A (void);
// 0x00000124 System.Void Vectrosity.VectorObject3D::SetVerts()
extern void VectorObject3D_SetVerts_m1A386FEC2696DA4E4D4C6795637212987AA49BB1 (void);
// 0x00000125 System.Void Vectrosity.VectorObject3D::SetName(System.String)
extern void VectorObject3D_SetName_m34E91DF41876F47A2CFEB2CA0896CA7687054D59 (void);
// 0x00000126 System.Void Vectrosity.VectorObject3D::UpdateVerts()
extern void VectorObject3D_UpdateVerts_mA2B2185082F33744D62AF725BD6A43FD725E5134 (void);
// 0x00000127 System.Void Vectrosity.VectorObject3D::UpdateUVs()
extern void VectorObject3D_UpdateUVs_m79250BAC83937C89DC45D76F9582E1A51C88B934 (void);
// 0x00000128 System.Void Vectrosity.VectorObject3D::UpdateColors()
extern void VectorObject3D_UpdateColors_mB1F80D91F5887F94F619C6DF3274253F7C6A6730 (void);
// 0x00000129 System.Void Vectrosity.VectorObject3D::UpdateNormals()
extern void VectorObject3D_UpdateNormals_m82C21130E357193304CF7C809068F8919A2D3F55 (void);
// 0x0000012A System.Void Vectrosity.VectorObject3D::UpdateTangents()
extern void VectorObject3D_UpdateTangents_mC12494B53D2475758CEA48315245A7FA58571E58 (void);
// 0x0000012B System.Void Vectrosity.VectorObject3D::UpdateTris()
extern void VectorObject3D_UpdateTris_mEA390BD784FB3EA6B41A28D675951248A4619D10 (void);
// 0x0000012C System.Void Vectrosity.VectorObject3D::UpdateMeshAttributes()
extern void VectorObject3D_UpdateMeshAttributes_m4AE1B7D3D1E8442AD10C0D378778B45827448A65 (void);
// 0x0000012D System.Void Vectrosity.VectorObject3D::ClearMesh()
extern void VectorObject3D_ClearMesh_mF9821537AA239B9A3A5D4D89E07A1CD632810A24 (void);
// 0x0000012E System.Int32 Vectrosity.VectorObject3D::VertexCount()
extern void VectorObject3D_VertexCount_m55658501B729EFF993B0E900ECEC2A1275393C00 (void);
// 0x0000012F System.Void Vectrosity.VisibilityControl::.ctor()
extern void VisibilityControl__ctor_m234E1E2866025823E0BEE6D8CB1E11E8F208CFED (void);
// 0x00000130 Vectrosity.RefInt Vectrosity.VisibilityControl::get_objectNumber()
extern void VisibilityControl_get_objectNumber_mFFDF71E0B885367C7D8F6EB2E1A07A196E5FD7A3 (void);
// 0x00000131 System.Void Vectrosity.VisibilityControl::Setup(Vectrosity.VectorLine,System.Boolean)
extern void VisibilityControl_Setup_mFC214FC710C98CF73C6CA631AC2090811D2BF8B1 (void);
// 0x00000132 System.Collections.IEnumerator Vectrosity.VisibilityControl::VisibilityTest()
extern void VisibilityControl_VisibilityTest_m1376D2161DA0A5636CE0D6CFE36F296C417546ED (void);
// 0x00000133 System.Collections.IEnumerator Vectrosity.VisibilityControl::OnBecameVisible()
extern void VisibilityControl_OnBecameVisible_mF88C687A0EE1D0B8D4026EB2FD2D386859291026 (void);
// 0x00000134 System.Collections.IEnumerator Vectrosity.VisibilityControl::OnBecameInvisible()
extern void VisibilityControl_OnBecameInvisible_mF5C5604BDFA27A20D8B8B89723F3BB753C1AB3BE (void);
// 0x00000135 System.Void Vectrosity.VisibilityControl::OnDestroy()
extern void VisibilityControl_OnDestroy_mE82C1592DD07936C5AC3958E128CB8AB9D4A16C2 (void);
// 0x00000136 System.Void Vectrosity.VisibilityControl::DontDestroyLine()
extern void VisibilityControl_DontDestroyLine_mB14481A1B63B17D7E5843773FB881D82B6C32EE5 (void);
// 0x00000137 System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::.ctor()
extern void U3CVisibilityTestU3Ec__Iterator1__ctor_mA3B4BCE3D8F8C728D75A1B29A3E9A59F66399DE8 (void);
// 0x00000138 System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern void U3CVisibilityTestU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7F1B3610E33046148D4FD293AF55B877CB71E2EA (void);
// 0x00000139 System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern void U3CVisibilityTestU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m78FFCDCE3A8AD3464045FFC94EC6E5D1E04820C9 (void);
// 0x0000013A System.Boolean Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::MoveNext()
extern void U3CVisibilityTestU3Ec__Iterator1_MoveNext_m0B5AADB14FE54C3943C2EC6BFAD29328410B1A8F (void);
// 0x0000013B System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::Dispose()
extern void U3CVisibilityTestU3Ec__Iterator1_Dispose_m3975DC856353BE7A335B0123899D167BC4859553 (void);
// 0x0000013C System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::Reset()
extern void U3CVisibilityTestU3Ec__Iterator1_Reset_m9FF8A84407B2B77BBEC0664EFDA158FD2488CA28 (void);
// 0x0000013D System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::.ctor()
extern void U3COnBecameVisibleU3Ec__Iterator2__ctor_m7D8891F9A9C506D7FB97B598F0376D8DE1A3198F (void);
// 0x0000013E System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern void U3COnBecameVisibleU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9C4AD3A6C5F8A64AEAFC9CE460105C1EBCF6B7C2 (void);
// 0x0000013F System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern void U3COnBecameVisibleU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_mDCFDC2ED90F9DF4277D8794E3371A705B001BACD (void);
// 0x00000140 System.Boolean Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::MoveNext()
extern void U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m5F96417D4C99C7145D0A2E1A75C84EC136147397 (void);
// 0x00000141 System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::Dispose()
extern void U3COnBecameVisibleU3Ec__Iterator2_Dispose_m5E0F062A69EDA202E34ED211D3F9BB812CB88289 (void);
// 0x00000142 System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::Reset()
extern void U3COnBecameVisibleU3Ec__Iterator2_Reset_m5F5390A4426ED12EEA7B6EF4C5E17B25219F452E (void);
// 0x00000143 System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::.ctor()
extern void U3COnBecameInvisibleU3Ec__Iterator3__ctor_m05443508C5AB96FE91ABDC1B593275A6BED217A0 (void);
// 0x00000144 System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern void U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m281FFAD0C7A249DDF0B9CCB7D98F690CA0A8EF50 (void);
// 0x00000145 System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern void U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_mF13911BF6D072BDE853FA1075BA968CEB7E2ABF6 (void);
// 0x00000146 System.Boolean Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::MoveNext()
extern void U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_mE70B1C3FF21A47973237CBABB67A8B40AC99135A (void);
// 0x00000147 System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::Dispose()
extern void U3COnBecameInvisibleU3Ec__Iterator3_Dispose_mA87E7B199FF929EA3BA38ED5BB1368EFDDF0C2F3 (void);
// 0x00000148 System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::Reset()
extern void U3COnBecameInvisibleU3Ec__Iterator3_Reset_m63434CECBC5DF4944317DAA6EC774AD46CD5287A (void);
// 0x00000149 System.Void Vectrosity.VisibilityControlAlways::.ctor()
extern void VisibilityControlAlways__ctor_m32F83386E9619F9359BEB2C1AA055BC883289DF0 (void);
// 0x0000014A Vectrosity.RefInt Vectrosity.VisibilityControlAlways::get_objectNumber()
extern void VisibilityControlAlways_get_objectNumber_m65453C875277B253C002EF4A1FF2F8401CE9033E (void);
// 0x0000014B System.Void Vectrosity.VisibilityControlAlways::Setup(Vectrosity.VectorLine)
extern void VisibilityControlAlways_Setup_m342E892B79D5E722B39337FE70B2E0C45A83712E (void);
// 0x0000014C System.Void Vectrosity.VisibilityControlAlways::OnDestroy()
extern void VisibilityControlAlways_OnDestroy_m64F2672D8F7A7BC724F3BF2DCAAD2A3D6608EEE7 (void);
// 0x0000014D System.Void Vectrosity.VisibilityControlAlways::DontDestroyLine()
extern void VisibilityControlAlways_DontDestroyLine_m4E784734F767E6BEC1984E3E36D3E66D5E872535 (void);
// 0x0000014E System.Void Vectrosity.VisibilityControlStatic::.ctor()
extern void VisibilityControlStatic__ctor_m4E565F840EFBB88A3C5D22322593AE7C20976A40 (void);
// 0x0000014F Vectrosity.RefInt Vectrosity.VisibilityControlStatic::get_objectNumber()
extern void VisibilityControlStatic_get_objectNumber_mEFDD619820364E20C93AE117812F08E117D09BF6 (void);
// 0x00000150 System.Void Vectrosity.VisibilityControlStatic::Setup(Vectrosity.VectorLine,System.Boolean)
extern void VisibilityControlStatic_Setup_mB81BF8A1F5A9DA09AD76430F4F26327F2AAEB928 (void);
// 0x00000151 System.Collections.IEnumerator Vectrosity.VisibilityControlStatic::WaitCheck()
extern void VisibilityControlStatic_WaitCheck_m48175ADD5334C3484E3464A64F8E6F728E9358C7 (void);
// 0x00000152 System.Void Vectrosity.VisibilityControlStatic::OnBecameVisible()
extern void VisibilityControlStatic_OnBecameVisible_m12516B3BF36AEE40B7EADFC6AFA08BD9BA0A1DE4 (void);
// 0x00000153 System.Void Vectrosity.VisibilityControlStatic::OnBecameInvisible()
extern void VisibilityControlStatic_OnBecameInvisible_mA6941F4A753D2AF4256981A553FD9F2A16C208F8 (void);
// 0x00000154 System.Void Vectrosity.VisibilityControlStatic::OnDestroy()
extern void VisibilityControlStatic_OnDestroy_m329A043C2DA7160F29FCD892ACECC20772EFDC28 (void);
// 0x00000155 System.Void Vectrosity.VisibilityControlStatic::DontDestroyLine()
extern void VisibilityControlStatic_DontDestroyLine_mFA2676DFA190FA78B3397881AB85AA267F0607EF (void);
// 0x00000156 UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::GetMatrix()
extern void VisibilityControlStatic_GetMatrix_m72C2A4EB4D860A0B6581A8633B671FD5F73419DD (void);
// 0x00000157 System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::.ctor()
extern void U3CWaitCheckU3Ec__Iterator4__ctor_mC373F298379008231B6CABDCB7F5323393256E4B (void);
// 0x00000158 System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern void U3CWaitCheckU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mE5ECCE300FAD7E88DFA34BDCAE14C2629745E004 (void);
// 0x00000159 System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitCheckU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m4B8B60E5D3BD9A4E109AB5F01AB641CC22E9A1C1 (void);
// 0x0000015A System.Boolean Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::MoveNext()
extern void U3CWaitCheckU3Ec__Iterator4_MoveNext_m1A20A8601DAE22029AF08361494B130EDA3415BB (void);
// 0x0000015B System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::Dispose()
extern void U3CWaitCheckU3Ec__Iterator4_Dispose_mE1CCB0A15D4932E14EB9214C722DDFAEC9439795 (void);
// 0x0000015C System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::Reset()
extern void U3CWaitCheckU3Ec__Iterator4_Reset_mCD91B789ED27DFCAB4939937BFD8114713AE27ED (void);
static Il2CppMethodPointer s_methodPointers[348] = 
{
	BrightnessControl__ctor_m15FE14F93846E840F4A49EDEBEF9611743DB002C,
	BrightnessControl_get_objectNumber_mC8913494AF2D5D4E42D13BC4B97E30DE64DB9B8D,
	BrightnessControl_Setup_m1A7604BB2DE92A75632BB4AA67C34E71B0501EC7,
	BrightnessControl_SetUseLine_m5CCF7F07EF01A1E5799AD2441F441460E2E7608E,
	BrightnessControl_OnBecameVisible_m48F04D45A909D0B03919B96EB67A6C8A67A30262,
	BrightnessControl_OnBecameInvisible_m5D517F850870C01CD8F9440B43431AA034FF5571,
	BrightnessControl_OnDestroy_mD27E0FB0C17022887E63D6C80E8F36ED28C14C68,
	CapInfo__ctor_mFC12FCEEFD32F8EA5839C0F17447AA57FB202D87,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LineManager__ctor_mC66A42AB85451269A60F8D3AE60DB560181CCE18,
	LineManager__cctor_m8FDF48E19B10B24D472D81743C61CA74A6ED0440,
	LineManager_Awake_m6333EEA8BCFFB29CCD14EA4047C74DAE15A62A68,
	LineManager_Initialize_mE1040C0B72500FB889913D1ECF193B49754498C5,
	LineManager_AddLine_m48497AB498C6950A6BE4D3FDCBD242AF51251FCA,
	LineManager_DisableLine_m6EF07844B003FFBE605DDB93F302024C4F121CA5,
	LineManager_DisableLine_mFF21DD4192F905AE413A0DE1BAC21DF01B04BA09,
	LineManager_LateUpdate_m168A775D7DD3B46B0FB6768DBD4EFDF68D4F0F4D,
	LineManager_RemoveLine_m5842C7250976916A15361077FF7321CFDAAFC320,
	LineManager_RemoveLine_m941BD07322036D8D886799D67AD748791EE97C1D,
	LineManager_DisableIfUnused_mCFFB38235D5505D173CBE4B91D596B6283A505FA,
	LineManager_EnableIfUsed_m73FDA2A4199FB9F56CE4F89375BDF9C8D2B381E8,
	LineManager_StartCheckDistance_m69BB417780980A3C213ABA7CD0C5B9FBA1EAEEC6,
	LineManager_CheckDistance_m3ABE139597198720C2E5369DE43675BF47A5A867,
	LineManager_OnDestroy_m7E55D307BA7DB8638154F41885A09F4F211609D0,
	U3CDisableLineU3Ec__Iterator0__ctor_m7C3CC0527424E540C7171AB9D3E63D1E4F306380,
	U3CDisableLineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4E358A5BD114BA8582F3DBD751A7B0DE2FFED9B8,
	U3CDisableLineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_mB1E265B7781B10315A274977B92F1F96347B6DC7,
	U3CDisableLineU3Ec__Iterator0_MoveNext_m80031DD3D256059BE76D15027467F5FA957D6A16,
	U3CDisableLineU3Ec__Iterator0_Dispose_m69F6429E931C5B4C9B9F1FAAE78533B0B3B358B9,
	U3CDisableLineU3Ec__Iterator0_Reset_mB4B023677AE126534F8ECD01B6FD974B9A44A8E1,
	RefInt__ctor_mA3DA5388C30EE2966AA4E86148D0E711DFFC9794,
	VectorChar_get_data_m1BF2BF8502A7674FB25201C7B09194625D87853B,
	VectorLine__ctor_m5186C6DBBC00AFD7EB31A2775F6302A29668E558,
	VectorLine__ctor_mA76E24820F9C54CA6BCF7DB51547FBA65A7B6572,
	VectorLine__ctor_mCF6F9F1D6CBFEE596511C1C431E521B645878B81,
	VectorLine__ctor_m3A6502A81C3C996476E1A78068A7DA97B2FD4153,
	VectorLine__ctor_mFED22F1A7B1AF1CE7E46CD407DA7FA628A26A221,
	VectorLine__ctor_mF29F9C3B81B6F7DAA7F2DD7C9C16B60FB7AA4EE5,
	VectorLine__ctor_mFE3D3EBCBB6D36734F2E483301095B3D5CC6908B,
	VectorLine__ctor_m981B4FB629354CEDCC557024D3CE2BF8F6A8394E,
	VectorLine__ctor_m642E8D9302422588FB033C7D1C84C6AC6D876D51,
	VectorLine__ctor_m72BF598A96DCC3355ED552D7B1C862340CFAE47A,
	VectorLine__ctor_m43DD0DBCBDC550B0FEE151089D05383B6451DCD4,
	VectorLine__cctor_mD33BE1921623B01237B6CA2B285E597079730503,
	VectorLine_get_lineVertices_mC4D7969376459F3E60138EDDCA9D581F0702D70C,
	VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71,
	VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1,
	VectorLine_get_lineTriangles_m6FE31B265CC9E175DBC5BD5D91256EAA7ADE78C2,
	VectorLine_get_rectTransform_m2512EBE50E013130E38A8A88FDB20EF789D98E6D,
	VectorLine_get_color_mF7E127ACF8E4A6B90DC641629EE40A02D22D20AF,
	VectorLine_set_color_m98D6CA28D53C00FDD4582C72CE82F2AD3A8A84B2,
	VectorLine_get_is2D_mAC08BCA4315C6949F1AF7CE126C4941B2D1B933B,
	VectorLine_get_points2_mBCE99471CD482E8ECBABC5987B0A01B477AC20B4,
	VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50,
	VectorLine_set_points3_m2FE6D35A38BD92CFEC2A761D353D5EB904147636,
	VectorLine_get_pointsCount_m6B59F9C7889B0E88287C55337EB505FF733B91B8,
	VectorLine_set_lineWidth_m6493CEABA0518D25B6B2841848968BC8839CA6D3,
	VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99,
	VectorLine_set_name_mFE48A2B46F162DF9C7B431D1FB04D972D704757A,
	VectorLine_set_material_m80E4743F822E61C1E4B1DAC8E5AA5D5EC0BBE038,
	VectorLine_get_texture_m80B0BB07297D6E6A53789EFF36AFA678FACCA27E,
	VectorLine_set_texture_m52C12824951FE41F164DABE0C91CE9473443FE79,
	VectorLine_set_layer_mAC1EC9C98E51B720570E971AB28B8A8EABD82B39,
	VectorLine_get_active_m3B3628759C5C506D16EEFB12D8BB6539B95DB2C5,
	VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484,
	VectorLine_get_lineType_m0BDEF27EA27FE80EB0F45DA731FA2F924BBDFAB5,
	VectorLine_set_lineType_m4F30A2A4B65EBB3D3F4B5F1844207DDF4483BB5D,
	VectorLine_set_capLength_mBA4C11DDE93CD5A965246FC6F8A2161F51A3E00E,
	VectorLine_get_smoothWidth_mF8D0CD6A0113B6DDD65D7460381C71AC1E65DF9C,
	VectorLine_get_smoothColor_m2424D5703ACCC01C345FD966FA45D8906AA0A50E,
	VectorLine_set_joins_m7652EB4ECA41F82ADDD13FB5BBF778B7308EA8FD,
	VectorLine_get_isAutoDrawing_m3E9201D7D2D40BC96FB60159D8130E72B03D6756,
	VectorLine_get_drawStart_mDFA374E9175AC32A8908BC389A707435F94402FD,
	VectorLine_set_drawStart_m41499CFDF913A402CD6222A390C7D614FB508804,
	VectorLine_get_drawEnd_m0F1DCB33137E4C565B906049CC7030E51EE67375,
	VectorLine_set_drawEnd_m12610BC3583F2391E481EB66BD9E3850C9CFA785,
	VectorLine_set_endPointsUpdate_mEF3375E7D07ACA12ECD72038F195E5185858950C,
	VectorLine_set_endCap_m22A740E34C479CD9AF1E52CA40B6931B10594691,
	VectorLine_set_continuousTexture_m425B9D7DE26971A83C0DEC2610E7D2C3231DB47A,
	VectorLine_set_drawTransform_m64A6D4FFBC0B49EFC72504521A235DB828F62309,
	VectorLine_set_useViewportCoords_mFC89C638ED7189FA21AAD70AA9191680CB680C8E,
	VectorLine_set_textureScale_m0ED3FC9B8B82F3CB23B752F22A727B5E5D36490E,
	VectorLine_set_textureOffset_mC2ACD00E1018CAE81FE8E4F02C92EB63A0D0CA4D,
	VectorLine_get_collider_mFDE7C5C00C573D8E7DCF8CE1A9E42435F3EAE875,
	VectorLine_set_collider_m5A9FEDD8EA796BCD7228552179BAA2FA9764CE9B,
	VectorLine_set_physicsMaterial_m03616DE184EC25C18022853A0326512E5CBCAEA6,
	VectorLine_set_alignOddWidthToPixels_m963FB550EABA19DB12429A2EFB1DBB1D2B075405,
	VectorLine_get_canvas_mC9A4C9950A0254C3E05C42531F787BEE373A9223,
	VectorLine_get_camTransformPosition_mC2DB90AF4A6F753C4D0C669DC528122FCF27527C,
	VectorLine_get_camTransformExists_m8E195A7DF3DFEAAE75193FC18D77E46851056FBB,
	VectorLine_get_lineManager_m991C1E0A9EF5B315392BC66383A06E71C4037EB1,
	VectorLine_AddColliderIfNeeded_mE136857BFAF0053EED3C32E7280FA55BDEB21A6A,
	VectorLine_SetupLine_mC81248EF5A09665E7275BBDCD8E482A7C8DB44BB,
	VectorLine_SetupTriangles_m485E5A0CBDD74E3E9DFA3C7DFDB87ECCD1FC209C,
	VectorLine_SetLastFillTriangles_mC0964677D0A1CF474365916BF8B4EDD0591EABB6,
	VectorLine_SetupEndCap_m1AF8F7E4FC2026D1E85B4ACAF501AEF522541B75,
	VectorLine_ResetLine_mB9ECA94280E9ECA70AD820B486404966811CD771,
	VectorLine_SetEndCapUVs_m4B6A2A1748CC3B4F32267542B6EBC678458D13A5,
	VectorLine_RemoveEndCap_mB2CB7582A089D4BADB0AB895B19ADCB71460AF67,
	VectorLine_SetupTransform_mFD551BECE142C8FC1BDBFB3F3FE280B551D6F942,
	VectorLine_ResizeMeshArrays_mA85B233F6968E7939323705E1E3CD50BED2B8777,
	VectorLine_Resize_mBD3C2BFDF301D4BCE7B3F52ED5974881752E8457,
	VectorLine_Resize_mBD8DDE07DB8E8B0F8ACF875541EDEFB03DDDAE6D,
	VectorLine_ResizeLineWidths_mAB6A3F63F55F35B5254C6CB5787A2B6CB38A166F,
	VectorLine_SetUVs_m2D1986C499A49876D0C0080B7DEDC78B42768499,
	VectorLine_SetVertexCount_m1FFF40FF083EB27DFADD316C491F868BF2B901A8,
	VectorLine_MaxPoints_m1DA8DB27B519E5B99B76CC3F851DB71D4B31882E,
	VectorLine_CalculateTangents_mCC45C3345D6020C7267838B8616C4E938B031548,
	VectorLine_SetupVectorCanvas_mF970D0189F2AB50F20B26A583BA05FA4AAF93A81,
	VectorLine_SetCanvasCamera_mCEA3FB851E347CF9EE3CEF0B992E048904AF5FA9,
	VectorLine_SetMask_m62273CEA69B4215B988B603EA1F04C74ACA16EB1,
	VectorLine_SetMask_m01707C3C2DE563C2E9AB75AB49C0C830DE3724D3,
	VectorLine_SetMask_m4E5C79CE7F07F15BF6CF13A1F6FA3007A6534561,
	VectorLine_CheckCamera3D_mF55C01A2426FA14FD9ECDAE9C715F74036283E1D,
	VectorLine_SetCamera3D_mA766EB77086D6E7DED2227289AAA4F013040212B,
	VectorLine_SetCamera3D_m38D807920F285B6AEE047D11668B59393132C3AF,
	VectorLine_CameraHasMoved_mA922882080F6465C9A77E0C7A41EFF74BECFDCBB,
	VectorLine_UpdateCameraInfo_mEB6D9ECDA246E306452384947018891838D3B6FA,
	VectorLine_GetSegmentNumber_mFD71AB492DA3B386A0D89C0E16B9D843A406B1BC,
	VectorLine_SetEndCapColors_m996586C74B40D8C631CADD85FFD5EFF584501045,
	VectorLine_SetColor_m7A4D856B8A7699488479E0E24757CFF2D9947E98,
	VectorLine_SetColor_m6784A75290F0417D9107DCCCAC453CC6D5EFDBC9,
	VectorLine_SetColor_m31BD9E19DC2D5BEF6CE512495733C775DC520346,
	VectorLine_SetColors_m1C38CB6364D8641D09AC69AE2D0B939CB44C76AA,
	VectorLine_SetSegmentStartEnd_m1D0DD6E60F18A4A49BE4B4500FC2497DD8308E62,
	VectorLine_SetupWidths_mDAA13D5F8E0630851AA239601499571A5E24D8CA,
	VectorLine_SetWidths_m40C16DC2D4A3E4351E733B6ABFB33E83584F5D05,
	VectorLine_SetWidths_m8F438D1B99E4BB63DDB5815D8C22D98AF85066DC,
	VectorLine_SetLine_mE346724DE5B2FE84E2D95F898B9BE8C99AF0BFED,
	VectorLine_SetLine_m41F891C32B7140843825D8BD5E94FC0715704750,
	VectorLine_CheckNormals_mFA94845E35FD04007434A5DBD4096B0D971BBA35,
	VectorLine_CheckLine_m3088908F463DF07DFB2D244488C4CB6D3AF6E950,
	VectorLine_DrawEndCap_mD042BC5BD19A70477B9CB3A258AF57DAAC2F0380,
	VectorLine_ScaleCapVertices_m8E168F56BD3E82E7259159BB4B8483174C5F8DAD,
	VectorLine_SetContinuousTexture_mD2749B491AEB6EC67B5F3205F76A536D7014E68C,
	VectorLine_UseMatrix_m7A0F47D8DE335100596DECB3236288CC1D1302F8,
	VectorLine_CheckPointCount_m6227536810BB3E1A41A683B5F338602104FE48A4,
	VectorLine_ClearTriangles_m3D40759F9E2E851FCB500FC93ED1ECCD0171C1A2,
	VectorLine_SetupDrawStartEnd_mD1C039FA000096212EB9BA7146BE447F8E1C3F6A,
	VectorLine_ZeroVertices_mFE36F2E91D7F89114DEC9AF5DE9AEB22407E1C5C,
	VectorLine_SetupCanvasState_m6EBA47AC200EF50527C4B636E09FE7E3B7742105,
	VectorLine_Draw_mA2B2502DC0BFD355E1FF3D252DC513B386D9D6C0,
	VectorLine_Line2D_m14735DBE89365A122066C7EC3F7EA4CC0F63AFD2,
	VectorLine_Line3D_m5113C4AE79B220D1F0D6AB36EC815E87B1585489,
	VectorLine_CheckDrawStartFill_mB5B9AB1133C15B7024F1317B29E8C7E57296E0FA,
	VectorLine_Draw3D_m934EFE9DA1927007CE80C86AD4A02D2358D67BD6,
	VectorLine_IntersectAndDoSkip_mA23F6A8C74DD7FE4EEE982C4DCB556A029724801,
	VectorLine_PlaneIntersectionPoint_mB8B4739D8AD751BE31AF7AB7BCE2AE3717B36E16,
	VectorLine_DrawPoints_m1DEF195519B04C24809CB080DF409CD0831B01F0,
	VectorLine_DrawPoints3D_m0F80898FEC93C6FF5DBEBC398F29C9B60526A9BF,
	VectorLine_SkipQuad_mDEC1739D41FB0C7B2ECAC1EB6CFD35619FDA6B0D,
	VectorLine_SkipQuad3D_m2FB947C29890F2600E84AC628B97245DD8312C07,
	VectorLine_WeldJoins_m7079ADA8BF3767F6E195786602DE78C8773DAF77,
	VectorLine_WeldJoinsDiscrete_mF5C2D0550D41D6BC50B73AEB14773CD8339EEBF1,
	VectorLine_SetIntersectionPoint_m8AAC76F59C4BAC1845C6F6EE901DF055C2BD4C52,
	VectorLine_WeldJoins3D_m992CCB9840929E8E38695926AE730A35022EE605,
	VectorLine_WeldJoinsDiscrete3D_mEF5454B82465CC47256B47E982C052A27C72E295,
	VectorLine_SetIntersectionPoint3D_m920049D1AF6DE6BDA56455A436883279F3FF5829,
	VectorLine_LineManagerCheckDistance_mC373D13D5898B27529FD0A764B01529E2E898E11,
	VectorLine_LineManagerDisable_mDFC81EA5E94893C402586C2CD5CAE16B6CF4041A,
	VectorLine_LineManagerEnable_m6DC10E4E9DDCE5FD5267CB13BFB258D1AAF6F4D7,
	VectorLine_Draw3DAuto_m8DFAB1EB030ACF8EB2EDA0CFC79BAB2F8C5A5CF5,
	VectorLine_Draw3DAuto_m179354EAAFB85F458AD4AF8D16BC271CFDBDBD63,
	VectorLine_StopDrawing3DAuto_mCCAC65DF8DA282D8A07BA726F5384E2336B9AE53,
	VectorLine_SetTextureScale_mF9F98E2E9F288BF052B17EB617DEC5C56D149A27,
	VectorLine_ResetTextureScale_m2CC3461934DFA31FF84FC075BD855AEFAF4BFFBB,
	VectorLine_SetCollider_m8924A817379C52F56CB01F08C187DE0DB2D2E75E,
	VectorLine_SetPathVerticesContinuous_mD2DA784D6D86FDE105DE597067B6A0806109263E,
	VectorLine_SetPathWorldVerticesContinuous_m39AAF33965B74FD57B09E2C485E6315DC6773F3B,
	VectorLine_SetPathVerticesDiscrete_m442257B238132AF76EBBDFABC5CC0E435C74B0EF,
	VectorLine_SetPathWorldVerticesDiscrete_mE89980DD5103B25A2BC2DDF5CFC6CDF955951846,
	VectorLine_BytesToVector3List_m92D93F096DC825892828B27099766DECA62E2E24,
	VectorLine_SetupByteBlock_m5287848984F4F07A21A5B29359FA73F9B446DDBF,
	VectorLine_ConvertToFloat_m139055584433158D7E470567A31560A9815675A8,
	VectorLine_Destroy_mC44B22F624821712A379ED38A54E638772BB1345,
	VectorLine_DestroyLine_mD571D169FFD001B6910AAC1F3AAB319776DA4D2D,
	VectorLine_SetDistances_mC45218EDE586EDDDF7A9DD6ED50D72B15EDBFB13,
	VectorLine_GetLength_mBECDD1C2C54009DCB262D2BFFCBD50D158C19480,
	VectorLine_GetPoint01_m2FFD624F37CB86F3FD6FF564F93853001109D85C,
	VectorLine_GetPoint_m6935F6C2223BCE6B35F616C46461A5ECD621E339,
	VectorLine_GetPoint3D01_m91E1BF01DE36837BACB94BAD2FD7C70D69BE318B,
	VectorLine_GetPoint3D_m60100B844606B06D3290DE66517E73125D3EE692,
	VectorLine_SetDistanceIndex_m63DED4B34EDD0089947000149F62EF0C82876858,
	VectorLine_SetEndCap_m049F8A564A1C48162E576B348AA59D5A3587A784,
	VectorLine_SetEndCap_mDFDEF4F5BB544D0E98674FA7FEA2B4E2C8F8E7FE,
	VectorLine_GetRowPixels_mAF24EDCA0635137A39A4ADEB97AD1B1933264AED,
	VectorLine_GetRotatedPixels_m04915D23250F9A53094E11B0BE03FDE31902E314,
	VectorLine_RemoveEndCap_mCABBCC9AF23A507A5697C5065F0FB602CDDA9B92,
	VectorLine_Selected_mCD27A972D1B6D4336CD53811B4ECBAEB404C3BA5,
	VectorLine_Selected_mFCEDC296BA17A8EB3F5FFF261ABDCE07D6A3F89C,
	VectorLine_Approximately_mA4BE18DE3EAFA1659760BCFF81C067B7935E8291,
	VectorLine_Approximately_mDEC01376DAB9CDF181251A1F51E7B36387B55169,
	VectorLine_Approximately_mB98966FB5DC37D3300967816B49E370BF6A11B3D,
	VectorLine_WrongArrayLength_mC7EDC1E0586585BD55B82D77238D4EF96C48CC30,
	VectorLine_CheckArrayLength_mC1FC977B826545DA6432F2C89AEFE32B875F75EC,
	VectorLine_MakeRect_m230F3FF4589FC594085ACE2155E8EF9FDD6A4421,
	VectorLine_MakeRect_mE636A899F338C362F4E10FA230CEAB1FA5A1E305,
	VectorLine_MakeRect_m857C00C4957A49FF452D52DF8DB3D3064A68B9D8,
	VectorLine_MakeCircle_m5E30180B8F4FBDDA631A5700BCB467C6F3F2E8F7,
	VectorLine_MakeCircle_m8BB150D7ED8AEA1C862449503A377EB54CE15FCB,
	VectorLine_MakeCircle_mC7DA57812D996657B98A2A7622B2B90B9F88A3AB,
	VectorLine_MakeEllipse_m527E5A392020C655EB552822B439619CD009BD56,
	VectorLine_MakeEllipse_m7C8335E4BD8EBA81EE71C27CA361CF4CF22C8C48,
	VectorLine_MakeArc_m9C138F6D7BDFA7FE6BC33EAA10B16EE086529AC1,
	VectorLine_MakeEllipse_m4C4C09F9F44CBB411DE93CEFCF3A55AB8CD6261C,
	VectorLine_MakeCurve_mE4914E511699875DFF71D53CABFE97F9E15E414D,
	VectorLine_MakeCurve_m370E54515A638AEC46582A78A3188CEBCFB1184E,
	VectorLine_MakeCurve_m27DE90D46F08BD708581566DAD6FC4F14AE2F1A3,
	VectorLine_MakeCurve_mA008A10040F1C406E6FBCC38F8D70C9724133C7D,
	VectorLine_GetBezierPoint_m3619255712BEF4054AB91B87C7556440039DBCEF,
	VectorLine_GetBezierPoint3D_m464EB754E6FB97A7A6127493CD21C3CD5275A784,
	VectorLine_MakeSpline_m55C6629353773E8A75E857119C083ED171F188A3,
	VectorLine_MakeSpline_m682A295730D63835C37761BB93161307F9F6B278,
	VectorLine_MakeSpline_m3C45BB34946E46598327A7CA688D7B29538A16F5,
	VectorLine_MakeSpline_mF548237B4C6D38F09615B6C63DE65A4324FA47E4,
	VectorLine_GetSplinePoint_m6FBFFE5699B3D9B6167B73075BB16E990C49353F,
	VectorLine_GetSplinePoint3D_m4388D9381CA0CA75B3F6B2DB61138EAB9901507E,
	VectorLine_VectorDistanceSquared_m7E430ED1F674E61194FEEFE1A3D0F64321F459B3,
	VectorLine_VectorDistanceSquared_mD91B066F494949AE96973B4F0EFF7AF78C03A4D5,
	VectorLine_InitNonuniformCatmullRom_mEEB56854233B62DCA96675BD54174429EC714DF0,
	VectorLine_EvalCubicPoly_m4D2025C80233C82C962CB5E5B945809203227A74,
	VectorLine_MakeText_mDE853F81DE512F24048B04F3E12DD2DAE2E829CA,
	VectorLine_MakeText_mF748B4AC57120DA7F818F0408F4BEF61BC033F2C,
	VectorManager__cctor_mDBBAD4573C0BA12C1AE71D0FA1B1DD8A5201803A,
	VectorManager_GetBrightnessValue_mC949BB3C7A6CC28D5D6598269C0588A816C5E387,
	VectorManager_ObjectSetup_mF7F932263CD9105D67A4277BE6B4EB1D173F20A8,
	VectorManager_ObjectSetup_mAF03886A407E27753714B2600BD7F7E5B475043C,
	VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142,
	VectorManager_get_arrayCount_m150A26CB0D835707FBEC2B14DC7381A02232A94D,
	VectorManager_VisibilityStaticSetup_mD098A79500A6B1A0E347CAB4DE0A889D28E2E13A,
	VectorManager_VisibilityStaticRemove_m9B72F2333EE6877D0F29D93BC18F76ADA5B0D237,
	VectorManager_get_arrayCount2_m17054F8887ECC80587A291E0AE545283AACE03C7,
	VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2,
	VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B,
	VectorManager_CheckDistanceSetup_m5F270965601AD85B1F94AEBDB4DC391FBEBAB788,
	VectorManager_DistanceRemove_mB9D600CD1338D2EC0722800B2BB2BFEE8F6525A9,
	VectorManager_CheckDistance_mEE514C5E086101A2B940A07C2DC92BAAE62BCEA2,
	VectorManager_SetOldDistance_m3647DA7ABBBD286740E465A1A309F097FEE3B8B7,
	VectorManager_SetDistanceColor_m12DACE05CD5D135C7137F3A2A9BFE1E2FB29E402,
	VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888,
	VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732,
	VectorManager_DrawArrayLines_mCEEBF6298008C43FE803CB36CF9006E666019744,
	VectorManager_DrawArrayLines2_mE6A0035BF8AF9A1A4D8B9BC1E335C6697DC5C5EC,
	VectorManager_GetBounds_m23D1D082EF71F5B252E32D9E3DCB68E847595026,
	VectorManager_GetBounds_m9CCD16DC671F020405D42696786AD9DE2EE295DA,
	VectorManager_MakeBoundsMesh_m72D478FD61D161D6DCB279FB54F759EA59CD93E6,
	VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59,
	VectorObject2D__ctor_m704336135C3BC3D39AEE27CBD644E972D242064D,
	VectorObject2D__cctor_mC43BA63969D868CF6CBB1761B90010A180335FEB,
	VectorObject2D_SetVectorLine_m87D3085F3CF7880E04576D28E11C80349CDE5724,
	VectorObject2D_Destroy_m22B58E8F0D320D29B647094B4F05043956F81698,
	VectorObject2D_DestroyNow_mAFAAAF538F3A01024B05ECA1E8E7F1EDEC8FDEA7,
	VectorObject2D_Enable_m3CF7975FFD23E9CC70209AB333EE640200525205,
	VectorObject2D_SetTexture_m356FBE3311E759BBD46F75FC3441755089296BC4,
	VectorObject2D_SetMaterial_mA7C634438A73116C9CA8F4B95A1B0E535AE61E8F,
	VectorObject2D_UpdateGeometry_mAD79F6A576446303BECDCC5FF0D05D75FA0B6FA7,
	VectorObject2D_SetupMesh_mAFA235E8824EB01E1F7F6D62806B46291A1DCB0C,
	VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6,
	VectorObject2D_OnPopulateMesh_m0406A9A86232C42EBABB5F237E990039CA174800,
	VectorObject2D_SetName_mF846BFE2EAE12F81DC7603F0639F2A89B395CE84,
	VectorObject2D_UpdateVerts_mCF94DB3DC0536E4587AEB27ADF3351E6C839D3BD,
	VectorObject2D_UpdateUVs_mC4B07F5C21023FCCEC97A7B2A9F4F195470BECC4,
	VectorObject2D_UpdateColors_mA9243733BCA7B03C1031A2605898CF0064A29A48,
	VectorObject2D_UpdateNormals_mA2DA97A5E41BC7CC838459F93C695A1003ED0206,
	VectorObject2D_UpdateTangents_m320C0A81769ADDC4EFE293E6C0B4F8BFDC70F9ED,
	VectorObject2D_UpdateTris_m99E3922F7DB201168938074590B9CF5601BD0D99,
	VectorObject2D_UpdateMeshAttributes_mC480305BD1408E7D8CFF157FBF327AA9A4031659,
	VectorObject2D_ClearMesh_mB4859A90C0DFABBFC096ED341686BB70AC023763,
	VectorObject2D_VertexCount_m88C4663CE98BA9368EA27089C1C5A405AD1A10EB,
	VectorObject3D__ctor_m5C4B552F6DBA830A65EB9CD7382A83381B9C72B8,
	VectorObject3D_SetVectorLine_m8EA2D566D669EB28195896F3D557152FC044C6A6,
	VectorObject3D_Destroy_m6DAD5F1A059D699F2B05A6B6B54723D56FBD8A8D,
	VectorObject3D_Enable_m72EEE3ED96AB44836B53AD9A2662CB87D5AB9576,
	VectorObject3D_SetTexture_m646C1DE560A2B30B564DD1B4E9E79CEBD5354517,
	VectorObject3D_SetMaterial_mFA8CB205105CEDF2EAFB2351619C476C2E726CF3,
	VectorObject3D_SetupMesh_mEDD447CA9C0FBC3D6408F7D397FAC4B63030E411,
	VectorObject3D_LateUpdate_m2DF6E4EDD4423FCA03129E57864AB19843A1E76A,
	VectorObject3D_SetVerts_m1A386FEC2696DA4E4D4C6795637212987AA49BB1,
	VectorObject3D_SetName_m34E91DF41876F47A2CFEB2CA0896CA7687054D59,
	VectorObject3D_UpdateVerts_mA2B2185082F33744D62AF725BD6A43FD725E5134,
	VectorObject3D_UpdateUVs_m79250BAC83937C89DC45D76F9582E1A51C88B934,
	VectorObject3D_UpdateColors_mB1F80D91F5887F94F619C6DF3274253F7C6A6730,
	VectorObject3D_UpdateNormals_m82C21130E357193304CF7C809068F8919A2D3F55,
	VectorObject3D_UpdateTangents_mC12494B53D2475758CEA48315245A7FA58571E58,
	VectorObject3D_UpdateTris_mEA390BD784FB3EA6B41A28D675951248A4619D10,
	VectorObject3D_UpdateMeshAttributes_m4AE1B7D3D1E8442AD10C0D378778B45827448A65,
	VectorObject3D_ClearMesh_mF9821537AA239B9A3A5D4D89E07A1CD632810A24,
	VectorObject3D_VertexCount_m55658501B729EFF993B0E900ECEC2A1275393C00,
	VisibilityControl__ctor_m234E1E2866025823E0BEE6D8CB1E11E8F208CFED,
	VisibilityControl_get_objectNumber_mFFDF71E0B885367C7D8F6EB2E1A07A196E5FD7A3,
	VisibilityControl_Setup_mFC214FC710C98CF73C6CA631AC2090811D2BF8B1,
	VisibilityControl_VisibilityTest_m1376D2161DA0A5636CE0D6CFE36F296C417546ED,
	VisibilityControl_OnBecameVisible_mF88C687A0EE1D0B8D4026EB2FD2D386859291026,
	VisibilityControl_OnBecameInvisible_mF5C5604BDFA27A20D8B8B89723F3BB753C1AB3BE,
	VisibilityControl_OnDestroy_mE82C1592DD07936C5AC3958E128CB8AB9D4A16C2,
	VisibilityControl_DontDestroyLine_mB14481A1B63B17D7E5843773FB881D82B6C32EE5,
	U3CVisibilityTestU3Ec__Iterator1__ctor_mA3B4BCE3D8F8C728D75A1B29A3E9A59F66399DE8,
	U3CVisibilityTestU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7F1B3610E33046148D4FD293AF55B877CB71E2EA,
	U3CVisibilityTestU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m78FFCDCE3A8AD3464045FFC94EC6E5D1E04820C9,
	U3CVisibilityTestU3Ec__Iterator1_MoveNext_m0B5AADB14FE54C3943C2EC6BFAD29328410B1A8F,
	U3CVisibilityTestU3Ec__Iterator1_Dispose_m3975DC856353BE7A335B0123899D167BC4859553,
	U3CVisibilityTestU3Ec__Iterator1_Reset_m9FF8A84407B2B77BBEC0664EFDA158FD2488CA28,
	U3COnBecameVisibleU3Ec__Iterator2__ctor_m7D8891F9A9C506D7FB97B598F0376D8DE1A3198F,
	U3COnBecameVisibleU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9C4AD3A6C5F8A64AEAFC9CE460105C1EBCF6B7C2,
	U3COnBecameVisibleU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_mDCFDC2ED90F9DF4277D8794E3371A705B001BACD,
	U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m5F96417D4C99C7145D0A2E1A75C84EC136147397,
	U3COnBecameVisibleU3Ec__Iterator2_Dispose_m5E0F062A69EDA202E34ED211D3F9BB812CB88289,
	U3COnBecameVisibleU3Ec__Iterator2_Reset_m5F5390A4426ED12EEA7B6EF4C5E17B25219F452E,
	U3COnBecameInvisibleU3Ec__Iterator3__ctor_m05443508C5AB96FE91ABDC1B593275A6BED217A0,
	U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m281FFAD0C7A249DDF0B9CCB7D98F690CA0A8EF50,
	U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_mF13911BF6D072BDE853FA1075BA968CEB7E2ABF6,
	U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_mE70B1C3FF21A47973237CBABB67A8B40AC99135A,
	U3COnBecameInvisibleU3Ec__Iterator3_Dispose_mA87E7B199FF929EA3BA38ED5BB1368EFDDF0C2F3,
	U3COnBecameInvisibleU3Ec__Iterator3_Reset_m63434CECBC5DF4944317DAA6EC774AD46CD5287A,
	VisibilityControlAlways__ctor_m32F83386E9619F9359BEB2C1AA055BC883289DF0,
	VisibilityControlAlways_get_objectNumber_m65453C875277B253C002EF4A1FF2F8401CE9033E,
	VisibilityControlAlways_Setup_m342E892B79D5E722B39337FE70B2E0C45A83712E,
	VisibilityControlAlways_OnDestroy_m64F2672D8F7A7BC724F3BF2DCAAD2A3D6608EEE7,
	VisibilityControlAlways_DontDestroyLine_m4E784734F767E6BEC1984E3E36D3E66D5E872535,
	VisibilityControlStatic__ctor_m4E565F840EFBB88A3C5D22322593AE7C20976A40,
	VisibilityControlStatic_get_objectNumber_mEFDD619820364E20C93AE117812F08E117D09BF6,
	VisibilityControlStatic_Setup_mB81BF8A1F5A9DA09AD76430F4F26327F2AAEB928,
	VisibilityControlStatic_WaitCheck_m48175ADD5334C3484E3464A64F8E6F728E9358C7,
	VisibilityControlStatic_OnBecameVisible_m12516B3BF36AEE40B7EADFC6AFA08BD9BA0A1DE4,
	VisibilityControlStatic_OnBecameInvisible_mA6941F4A753D2AF4256981A553FD9F2A16C208F8,
	VisibilityControlStatic_OnDestroy_m329A043C2DA7160F29FCD892ACECC20772EFDC28,
	VisibilityControlStatic_DontDestroyLine_mFA2676DFA190FA78B3397881AB85AA267F0607EF,
	VisibilityControlStatic_GetMatrix_m72C2A4EB4D860A0B6581A8633B671FD5F73419DD,
	U3CWaitCheckU3Ec__Iterator4__ctor_mC373F298379008231B6CABDCB7F5323393256E4B,
	U3CWaitCheckU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mE5ECCE300FAD7E88DFA34BDCAE14C2629745E004,
	U3CWaitCheckU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m4B8B60E5D3BD9A4E109AB5F01AB641CC22E9A1C1,
	U3CWaitCheckU3Ec__Iterator4_MoveNext_m1A20A8601DAE22029AF08361494B130EDA3415BB,
	U3CWaitCheckU3Ec__Iterator4_Dispose_mE1CCB0A15D4932E14EB9214C722DDFAEC9439795,
	U3CWaitCheckU3Ec__Iterator4_Reset_mCD91B789ED27DFCAB4939937BFD8114713AE27ED,
};
static const int32_t s_InvokerIndices[348] = 
{
	23,
	14,
	382,
	31,
	23,
	23,
	23,
	1540,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	31,
	843,
	23,
	10,
	23,
	3,
	23,
	23,
	1541,
	827,
	1542,
	23,
	32,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	102,
	23,
	23,
	32,
	4,
	1541,
	1543,
	1544,
	1545,
	1546,
	1541,
	1543,
	1544,
	1545,
	1547,
	1546,
	3,
	14,
	14,
	14,
	14,
	14,
	1548,
	1386,
	102,
	14,
	14,
	26,
	10,
	275,
	14,
	26,
	26,
	14,
	26,
	32,
	102,
	31,
	10,
	32,
	275,
	102,
	102,
	32,
	102,
	10,
	32,
	10,
	32,
	32,
	26,
	31,
	26,
	31,
	275,
	275,
	102,
	31,
	26,
	31,
	4,
	1098,
	49,
	4,
	23,
	1549,
	32,
	23,
	26,
	23,
	23,
	23,
	111,
	32,
	32,
	23,
	32,
	157,
	102,
	10,
	28,
	4,
	111,
	26,
	382,
	382,
	102,
	3,
	111,
	49,
	3,
	10,
	23,
	1386,
	1550,
	1551,
	26,
	502,
	32,
	26,
	663,
	1552,
	1553,
	23,
	31,
	31,
	1554,
	23,
	756,
	102,
	23,
	1555,
	157,
	32,
	23,
	1556,
	1556,
	32,
	23,
	1557,
	1558,
	23,
	23,
	648,
	648,
	971,
	971,
	279,
	971,
	971,
	279,
	3,
	3,
	3,
	23,
	275,
	23,
	23,
	23,
	31,
	1559,
	1560,
	1561,
	1562,
	0,
	3,
	92,
	17,
	17,
	23,
	653,
	1563,
	1564,
	1000,
	1565,
	1161,
	424,
	1566,
	153,
	0,
	111,
	1567,
	1568,
	1274,
	1569,
	1570,
	52,
	690,
	1571,
	994,
	1572,
	1159,
	1573,
	1160,
	1574,
	1575,
	1576,
	1577,
	124,
	35,
	1578,
	1579,
	1580,
	1581,
	26,
	296,
	296,
	1582,
	1580,
	1581,
	1583,
	1583,
	1584,
	776,
	1585,
	1586,
	3,
	1097,
	145,
	1587,
	122,
	513,
	326,
	121,
	513,
	862,
	121,
	1588,
	121,
	3,
	123,
	121,
	121,
	121,
	3,
	3,
	1589,
	1589,
	1590,
	122,
	23,
	3,
	843,
	23,
	23,
	31,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	23,
	843,
	23,
	31,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	23,
	14,
	382,
	14,
	14,
	14,
	23,
	23,
	23,
	14,
	14,
	102,
	23,
	23,
	23,
	14,
	14,
	102,
	23,
	23,
	23,
	14,
	14,
	102,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	23,
	14,
	382,
	14,
	23,
	23,
	23,
	23,
	1083,
	23,
	14,
	14,
	102,
	23,
	23,
};
extern const Il2CppCodeGenModule g_VectrosityCodeGenModule;
const Il2CppCodeGenModule g_VectrosityCodeGenModule = 
{
	"Vectrosity.dll",
	348,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
