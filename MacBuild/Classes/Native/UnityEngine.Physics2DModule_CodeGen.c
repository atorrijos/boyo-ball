﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.PhysicsScene2D::ToString()
extern void PhysicsScene2D_ToString_mDA6F499BD218AA31A450D918BB6C1890A9CE1109_AdjustorThunk (void);
// 0x00000002 System.Int32 UnityEngine.PhysicsScene2D::GetHashCode()
extern void PhysicsScene2D_GetHashCode_m4B5D8DCBA0AD6E5767C4D7A6AD6BC789EB19C8F5_AdjustorThunk (void);
// 0x00000003 System.Boolean UnityEngine.PhysicsScene2D::Equals(System.Object)
extern void PhysicsScene2D_Equals_m0C61F175C3B1BB308ADBC2AB323CEC45D1B5E99C_AdjustorThunk (void);
// 0x00000004 System.Boolean UnityEngine.PhysicsScene2D::Equals(UnityEngine.PhysicsScene2D)
extern void PhysicsScene2D_Equals_m581586F404E7A3BD3B6F0A05362974A6B523A2DA_AdjustorThunk (void);
// 0x00000005 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void PhysicsScene2D_Raycast_m22F55CAAA1B34A02757A5C6E2B573F6464B32723_AdjustorThunk (void);
// 0x00000006 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_m167DAEC271F46133013EB0AAF2C7807064EBB3F2_AdjustorThunk (void);
// 0x00000007 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_Internal_m933E452FA1E36FFC87A0EC896EE4D36859935C91 (void);
// 0x00000008 System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_Raycast_m3B011D4A6CA691739178E253665799A7AD0CB423_AdjustorThunk (void);
// 0x00000009 System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_mB02544544B0F0AA17972D1D9ABC7CA0171537D9D (void);
// 0x0000000A System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_Raycast_m6FB2BBC4E3BE53114D7F6EFA975F5AF703ADCDC0_AdjustorThunk (void);
// 0x0000000B System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_m10A64653E2C399B6523B2D04B1E1C8626BF68B24 (void);
// 0x0000000C System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersection(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit2D[],System.Int32)
extern void PhysicsScene2D_GetRayIntersection_mB6F14C8BB95609094BE9BDB218483EAAC4117B5A_AdjustorThunk (void);
// 0x0000000D System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_m44D4B8638806360B19BDCFDCC090C1AD1267539E (void);
// 0x0000000E System.Void UnityEngine.PhysicsScene2D::Raycast_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern void PhysicsScene2D_Raycast_Internal_Injected_m6CEAF96A977FEDD926FBAAA74FFA2D571D75C422 (void);
// 0x0000000F System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_Injected_mE7F2FCB47E154BB4062BCEFECD74385275109D76 (void);
// 0x00000010 System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_Injected_m0866D19E97A4D9267F9E91C3CE33B7D34AB2DFBC (void);
// 0x00000011 System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_mAF80210771484878DF20775EE91ADF1D19CCA99D (void);
// 0x00000012 UnityEngine.PhysicsScene2D UnityEngine.Physics2D::get_defaultPhysicsScene()
extern void Physics2D_get_defaultPhysicsScene_m067B3BAED5AD5E0E71C1462480BFC107ED901A83 (void);
// 0x00000013 System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern void Physics2D_get_queriesHitTriggers_mA735707551D682297446435DFC7963E97F562E44 (void);
// 0x00000014 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Physics2D_Raycast_m9DF2A5F7852F9C736CA4F1BABA52DC0AB24ED983 (void);
// 0x00000015 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Physics2D_Raycast_m684AD52FAD1E3BF5AE53F8E48AF92202114053C2 (void);
// 0x00000016 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void Physics2D_Raycast_mE1F849D695803D7409790B7C736D00FD9724F65A (void);
// 0x00000017 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern void Physics2D_Raycast_mCA2AD72EC8244B2208955E072BD0B619F9376143 (void);
// 0x00000018 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern void Physics2D_Raycast_mF01414D1A1EF5BA1B012AB14583518BAD0A65CB0 (void);
// 0x00000019 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void Physics2D_Raycast_mEB1CCF6AF3D9FC18576F70C95E2FD82C8CF840A4 (void);
// 0x0000001A System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_Raycast_m007A72E26CA79C348434B46F9D04DD915CE6CE67 (void);
// 0x0000001B System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>,System.Single)
extern void Physics2D_Raycast_m2A909F3065F530329891F5B8D1B80202300786AA (void);
// 0x0000001C UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single)
extern void Physics2D_CircleCastAll_m17E08543ECA28B853FD867F6C7844985137FEF7F (void);
// 0x0000001D UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void Physics2D_CircleCastAll_Internal_m300197FC6212AAE4C069898FBC43A6BABB1EB046 (void);
// 0x0000001E UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern void Physics2D_GetRayIntersectionAll_m233595BB5435D09B5F38773D14C01C9A3BD8C04D (void);
// 0x0000001F UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern void Physics2D_GetRayIntersectionAll_m496B28AE025E60DE4DA540159C10AEBFE6B1A28A (void);
// 0x00000020 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_m71FB456B2AA9C909119CF5399AD8F4CB6EDED7F2 (void);
// 0x00000021 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_mA2BD81667462FF3C9E67A7E0445CC874009CF337 (void);
// 0x00000022 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[])
extern void Physics2D_GetRayIntersectionNonAlloc_m5E63F05DC3EE41BEE3B869614CEE27DA4A0340F4 (void);
// 0x00000023 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_GetRayIntersectionNonAlloc_m866F0C065F53C6CDD6BDD8EEB9CE1EB213763548 (void);
// 0x00000024 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionNonAlloc_m8EED1B31A7E557C3D7B6C67D2D3AD77A8BA31B5F (void);
// 0x00000025 UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single)
extern void Physics2D_OverlapCircleAll_m9D7CF119EA9F260FBAD101EA8A6916A643551C0D (void);
// 0x00000026 UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void Physics2D_OverlapCircleAll_Internal_m5E0B68FBDC36985268B1A0D0ED3B6EC102C1BA89 (void);
// 0x00000027 System.Void UnityEngine.Physics2D::.cctor()
extern void Physics2D__cctor_m2C742CE7156A78480229016DEEF403B45E4DAFEE (void);
// 0x00000028 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern void Physics2D_CircleCastAll_Internal_Injected_mCF3F01B71AD484E0720EFA0FEC22F4DC23DAB8AF (void);
// 0x00000029 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_Injected_m87038811DE210AFEEAD218D82268AF482BBD24C7 (void);
// 0x0000002A UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern void Physics2D_OverlapCircleAll_Internal_Injected_mA8F3E6FC493512102940D0B04FFA037315CBA224 (void);
// 0x0000002B System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern void ContactFilter2D_CheckConsistency_m4B6DAA0197DC017E3B7A8B8F661729431504C5D1_AdjustorThunk (void);
// 0x0000002C System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern void ContactFilter2D_SetLayerMask_m925C98BC2EEAA78349B3ED3654BC3C362743BBDE_AdjustorThunk (void);
// 0x0000002D System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern void ContactFilter2D_SetDepth_m63872B3F8BBDB962AF44D064BA328599C74D840F_AdjustorThunk (void);
// 0x0000002E UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern void ContactFilter2D_CreateLegacyFilter_mA293F293D35236A5564D57574012590BD6DF385A (void);
// 0x0000002F System.Void UnityEngine.ContactFilter2D::CheckConsistency_Injected(UnityEngine.ContactFilter2D&)
extern void ContactFilter2D_CheckConsistency_Injected_mFF4A5178E9F60CCF59DDE6B6F0013D182294573E (void);
// 0x00000030 UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::GetContacts_Internal()
extern void Collision2D_GetContacts_Internal_m503F55971ED6F03B10CA4D9C1CF455EFD7792D53 (void);
// 0x00000031 UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern void Collision2D_get_collider_mA7687EDB0D47A2F211BFE8DB89266B9AA05CFDDD (void);
// 0x00000032 UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern void Collision2D_get_rigidbody_m82AF533E110DFDBDED6D6C74EB479902E813D42E (void);
// 0x00000033 UnityEngine.Vector2 UnityEngine.Collision2D::get_relativeVelocity()
extern void Collision2D_get_relativeVelocity_m021626A3853F4D1E2DA016479096DB6CE9999628 (void);
// 0x00000034 System.Int32 UnityEngine.Collision2D::get_contactCount()
extern void Collision2D_get_contactCount_m2C74A32BE18A7DDFABD28CD2DCCB65D5283691DF (void);
// 0x00000035 System.Int32 UnityEngine.Collision2D::GetContacts(UnityEngine.ContactPoint2D[])
extern void Collision2D_GetContacts_m5E2550321ED4BF04E4B3E7F2E87A185F0EB13D4E (void);
// 0x00000036 UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_normal()
extern void ContactPoint2D_get_normal_mC001581D9A15A2E6DA20B96C05482385621EF8DB_AdjustorThunk (void);
// 0x00000037 UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern void RaycastHit2D_get_point_m10D5AB3B26EAE62583BE35CFA13A3E40BDAF30AE_AdjustorThunk (void);
// 0x00000038 UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern void RaycastHit2D_get_normal_m6F8B9F4018EFA126CC33126E8E42B09BB5A82637_AdjustorThunk (void);
// 0x00000039 System.Single UnityEngine.RaycastHit2D::get_distance()
extern void RaycastHit2D_get_distance_mA910B45BD349A8F70139F6BC1E686F47F40A1662_AdjustorThunk (void);
// 0x0000003A UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern void RaycastHit2D_get_collider_m00F7EC55C36F39E2ED64B31354FB4D9C8938D563_AdjustorThunk (void);
// 0x0000003B UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern void RaycastHit2D_get_rigidbody_m798FC5415CBA164090855FACC9F43379F08F37C6_AdjustorThunk (void);
// 0x0000003C UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern void RaycastHit2D_get_transform_m0CD992BD94BCE7F1CFB44D987924328B6C0E8864_AdjustorThunk (void);
// 0x0000003D UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern void Rigidbody2D_get_position_mEC7D07E3478BEF5A2A0E22C91CA54935376F84C2 (void);
// 0x0000003E System.Void UnityEngine.Rigidbody2D::set_position(UnityEngine.Vector2)
extern void Rigidbody2D_set_position_m1604084713EB195D44B8B411D4BCAFA5941E3413 (void);
// 0x0000003F System.Single UnityEngine.Rigidbody2D::get_rotation()
extern void Rigidbody2D_get_rotation_mD58E62EDB334FCDF7914A94C940F7903E8ADBBFF (void);
// 0x00000040 System.Void UnityEngine.Rigidbody2D::set_rotation(System.Single)
extern void Rigidbody2D_set_rotation_m4982A0057C1FEB61323B58C7A791E8A0D0CD8C77 (void);
// 0x00000041 UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern void Rigidbody2D_get_velocity_m138328DCC01EB876FB5EA025BF08728030D93D66 (void);
// 0x00000042 System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern void Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA (void);
// 0x00000043 System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern void Rigidbody2D_get_angularVelocity_m5A202B05024DE0C8F9A8C86900F7EF111574D8A3 (void);
// 0x00000044 System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern void Rigidbody2D_set_angularVelocity_m53F03D07251B600A01A05A27F16D2D8C0D3C5A66 (void);
// 0x00000045 UnityEngine.PhysicsMaterial2D UnityEngine.Rigidbody2D::get_sharedMaterial()
extern void Rigidbody2D_get_sharedMaterial_mE0B1677540E85930DCE9A0CD3B12F12C80047ABF (void);
// 0x00000046 System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
extern void Rigidbody2D_set_gravityScale_mEDC82EE2ED74DA9C5AB8A0C4A929B09149522BBF (void);
// 0x00000047 System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
extern void Rigidbody2D_set_bodyType_m8D34999918D42B2DF16FAAB4F237A8663EA8406B (void);
// 0x00000048 System.Void UnityEngine.Rigidbody2D::set_useFullKinematicContacts(System.Boolean)
extern void Rigidbody2D_set_useFullKinematicContacts_m1A9E419994A8B098543825A4301A96289CCDDECF (void);
// 0x00000049 System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern void Rigidbody2D_set_isKinematic_m82FBA7C4F4EB2569AC77D4767061F9CE0F8C3FEB (void);
// 0x0000004A System.Void UnityEngine.Rigidbody2D::set_freezeRotation(System.Boolean)
extern void Rigidbody2D_set_freezeRotation_mF8E01A84694933A5E21F71869CAA42DB247CE987 (void);
// 0x0000004B System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
extern void Rigidbody2D_set_constraints_mEB2202406AB063F57E273F4313E7E4D58D613263 (void);
// 0x0000004C System.Void UnityEngine.Rigidbody2D::WakeUp()
extern void Rigidbody2D_WakeUp_mB5A3852660DB19F546F0651ADD80BAA2D96D81C0 (void);
// 0x0000004D System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
extern void Rigidbody2D_AddForce_mB4754FC98ED65E5381854CDC858D12F0504FB3A2 (void);
// 0x0000004E System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern void Rigidbody2D_AddForce_m2360EEDAF4E9F279AAB77DBD785A7F7161865343 (void);
// 0x0000004F System.Void UnityEngine.Rigidbody2D::.ctor()
extern void Rigidbody2D__ctor_m6CE72A013ED172CA657C0E00E8C04C0B523C78C5 (void);
// 0x00000050 System.Void UnityEngine.Rigidbody2D::get_position_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_position_Injected_m1B97DC47EDF69F0506CEC24603C3A330B7E8FAB4 (void);
// 0x00000051 System.Void UnityEngine.Rigidbody2D::set_position_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_set_position_Injected_mB05AB62CD1D243D071F60E48C9905587ACB94933 (void);
// 0x00000052 System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_velocity_Injected_m2102CD221C46BA3C36127D7413D8C35A402A8903 (void);
// 0x00000053 System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_set_velocity_Injected_m198FAA0E7A328E1B236FC5835C9711987D3C691E (void);
// 0x00000054 System.Void UnityEngine.Rigidbody2D::AddForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern void Rigidbody2D_AddForce_Injected_m238B89F81818A2A5A0CBD3BADE376151EA7243C7 (void);
// 0x00000055 System.Boolean UnityEngine.Collider2D::get_isTrigger()
extern void Collider2D_get_isTrigger_m2D8D206E7F91ED3AF8AC552E17414CBD7279F6FF (void);
// 0x00000056 System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern void Collider2D_set_isTrigger_m129C3059CC21789EFA198E153A3C82662E3BFF52 (void);
// 0x00000057 UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern void Collider2D_get_attachedRigidbody_m3E930FC14E58085493B6CC1E4228BEF634A6D5AD (void);
// 0x00000058 System.Void UnityEngine.Collider2D::set_sharedMaterial(UnityEngine.PhysicsMaterial2D)
extern void Collider2D_set_sharedMaterial_m5DF75EB2B6BE7C7A36D791BDFDCDEDDCFE9E4117 (void);
// 0x00000059 System.Void UnityEngine.Collider2D::.ctor()
extern void Collider2D__ctor_mB3AFF9CA18C8FA8E4F75DC45CA2D4A423A40F23D (void);
// 0x0000005A System.Single UnityEngine.CircleCollider2D::get_radius()
extern void CircleCollider2D_get_radius_m879CF754EBD72E22F29664045632BC8E7DEC5F49 (void);
// 0x0000005B System.Void UnityEngine.EdgeCollider2D::set_points(UnityEngine.Vector2[])
extern void EdgeCollider2D_set_points_m8A6DD1D62CCF4CE90AA079BC59FA62796E1667E3 (void);
// 0x0000005C System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
extern void BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760 (void);
// 0x0000005D System.Void UnityEngine.BoxCollider2D::set_size_Injected(UnityEngine.Vector2&)
extern void BoxCollider2D_set_size_Injected_mB8DEA9F8BBB15256BAE3092D37FDA1AE9D081401 (void);
// 0x0000005E System.Void UnityEngine.PolygonCollider2D::set_pathCount(System.Int32)
extern void PolygonCollider2D_set_pathCount_mCB8738EC16C8EB539A8DF1F44429AE15DBA79CD4 (void);
// 0x0000005F System.Void UnityEngine.PolygonCollider2D::SetPath(System.Int32,UnityEngine.Vector2[])
extern void PolygonCollider2D_SetPath_m77B8966A290430F608BE6B207652B55B522151CF (void);
// 0x00000060 System.Void UnityEngine.PolygonCollider2D::SetPath_Internal(System.Int32,UnityEngine.Vector2[])
extern void PolygonCollider2D_SetPath_Internal_mD78EEDF737B1B7A9C7393F68F226CF85BA5C753E (void);
// 0x00000061 System.Void UnityEngine.Joint2D::set_connectedBody(UnityEngine.Rigidbody2D)
extern void Joint2D_set_connectedBody_mDFAD1EA275D93EDFBCCD52FC5562DED5A556C4F7 (void);
// 0x00000062 System.Void UnityEngine.PhysicsMaterial2D::.ctor()
extern void PhysicsMaterial2D__ctor_m18313D6E7E05873C43A80AC81F521FEB7BCD0743 (void);
// 0x00000063 System.Void UnityEngine.PhysicsMaterial2D::Create_Internal(UnityEngine.PhysicsMaterial2D,System.String)
extern void PhysicsMaterial2D_Create_Internal_m4E32D46C8B2D5478DFF629EF320C5779918F5FCE (void);
// 0x00000064 System.Void UnityEngine.PhysicsMaterial2D::set_bounciness(System.Single)
extern void PhysicsMaterial2D_set_bounciness_m382DA6E2E421457D5159812EC65405E5624CEF55 (void);
// 0x00000065 System.Void UnityEngine.PhysicsMaterial2D::set_friction(System.Single)
extern void PhysicsMaterial2D_set_friction_m477B1C8F1C9B981C3D6F20DCC3A2693F2D99FFBB (void);
static Il2CppMethodPointer s_methodPointers[101] = 
{
	PhysicsScene2D_ToString_mDA6F499BD218AA31A450D918BB6C1890A9CE1109_AdjustorThunk,
	PhysicsScene2D_GetHashCode_m4B5D8DCBA0AD6E5767C4D7A6AD6BC789EB19C8F5_AdjustorThunk,
	PhysicsScene2D_Equals_m0C61F175C3B1BB308ADBC2AB323CEC45D1B5E99C_AdjustorThunk,
	PhysicsScene2D_Equals_m581586F404E7A3BD3B6F0A05362974A6B523A2DA_AdjustorThunk,
	PhysicsScene2D_Raycast_m22F55CAAA1B34A02757A5C6E2B573F6464B32723_AdjustorThunk,
	PhysicsScene2D_Raycast_m167DAEC271F46133013EB0AAF2C7807064EBB3F2_AdjustorThunk,
	PhysicsScene2D_Raycast_Internal_m933E452FA1E36FFC87A0EC896EE4D36859935C91,
	PhysicsScene2D_Raycast_m3B011D4A6CA691739178E253665799A7AD0CB423_AdjustorThunk,
	PhysicsScene2D_RaycastArray_Internal_mB02544544B0F0AA17972D1D9ABC7CA0171537D9D,
	PhysicsScene2D_Raycast_m6FB2BBC4E3BE53114D7F6EFA975F5AF703ADCDC0_AdjustorThunk,
	PhysicsScene2D_RaycastList_Internal_m10A64653E2C399B6523B2D04B1E1C8626BF68B24,
	PhysicsScene2D_GetRayIntersection_mB6F14C8BB95609094BE9BDB218483EAAC4117B5A_AdjustorThunk,
	PhysicsScene2D_GetRayIntersectionArray_Internal_m44D4B8638806360B19BDCFDCC090C1AD1267539E,
	PhysicsScene2D_Raycast_Internal_Injected_m6CEAF96A977FEDD926FBAAA74FFA2D571D75C422,
	PhysicsScene2D_RaycastArray_Internal_Injected_mE7F2FCB47E154BB4062BCEFECD74385275109D76,
	PhysicsScene2D_RaycastList_Internal_Injected_m0866D19E97A4D9267F9E91C3CE33B7D34AB2DFBC,
	PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_mAF80210771484878DF20775EE91ADF1D19CCA99D,
	Physics2D_get_defaultPhysicsScene_m067B3BAED5AD5E0E71C1462480BFC107ED901A83,
	Physics2D_get_queriesHitTriggers_mA735707551D682297446435DFC7963E97F562E44,
	Physics2D_Raycast_m9DF2A5F7852F9C736CA4F1BABA52DC0AB24ED983,
	Physics2D_Raycast_m684AD52FAD1E3BF5AE53F8E48AF92202114053C2,
	Physics2D_Raycast_mE1F849D695803D7409790B7C736D00FD9724F65A,
	Physics2D_Raycast_mCA2AD72EC8244B2208955E072BD0B619F9376143,
	Physics2D_Raycast_mF01414D1A1EF5BA1B012AB14583518BAD0A65CB0,
	Physics2D_Raycast_mEB1CCF6AF3D9FC18576F70C95E2FD82C8CF840A4,
	Physics2D_Raycast_m007A72E26CA79C348434B46F9D04DD915CE6CE67,
	Physics2D_Raycast_m2A909F3065F530329891F5B8D1B80202300786AA,
	Physics2D_CircleCastAll_m17E08543ECA28B853FD867F6C7844985137FEF7F,
	Physics2D_CircleCastAll_Internal_m300197FC6212AAE4C069898FBC43A6BABB1EB046,
	Physics2D_GetRayIntersectionAll_m233595BB5435D09B5F38773D14C01C9A3BD8C04D,
	Physics2D_GetRayIntersectionAll_m496B28AE025E60DE4DA540159C10AEBFE6B1A28A,
	Physics2D_GetRayIntersectionAll_m71FB456B2AA9C909119CF5399AD8F4CB6EDED7F2,
	Physics2D_GetRayIntersectionAll_Internal_mA2BD81667462FF3C9E67A7E0445CC874009CF337,
	Physics2D_GetRayIntersectionNonAlloc_m5E63F05DC3EE41BEE3B869614CEE27DA4A0340F4,
	Physics2D_GetRayIntersectionNonAlloc_m866F0C065F53C6CDD6BDD8EEB9CE1EB213763548,
	Physics2D_GetRayIntersectionNonAlloc_m8EED1B31A7E557C3D7B6C67D2D3AD77A8BA31B5F,
	Physics2D_OverlapCircleAll_m9D7CF119EA9F260FBAD101EA8A6916A643551C0D,
	Physics2D_OverlapCircleAll_Internal_m5E0B68FBDC36985268B1A0D0ED3B6EC102C1BA89,
	Physics2D__cctor_m2C742CE7156A78480229016DEEF403B45E4DAFEE,
	Physics2D_CircleCastAll_Internal_Injected_mCF3F01B71AD484E0720EFA0FEC22F4DC23DAB8AF,
	Physics2D_GetRayIntersectionAll_Internal_Injected_m87038811DE210AFEEAD218D82268AF482BBD24C7,
	Physics2D_OverlapCircleAll_Internal_Injected_mA8F3E6FC493512102940D0B04FFA037315CBA224,
	ContactFilter2D_CheckConsistency_m4B6DAA0197DC017E3B7A8B8F661729431504C5D1_AdjustorThunk,
	ContactFilter2D_SetLayerMask_m925C98BC2EEAA78349B3ED3654BC3C362743BBDE_AdjustorThunk,
	ContactFilter2D_SetDepth_m63872B3F8BBDB962AF44D064BA328599C74D840F_AdjustorThunk,
	ContactFilter2D_CreateLegacyFilter_mA293F293D35236A5564D57574012590BD6DF385A,
	ContactFilter2D_CheckConsistency_Injected_mFF4A5178E9F60CCF59DDE6B6F0013D182294573E,
	Collision2D_GetContacts_Internal_m503F55971ED6F03B10CA4D9C1CF455EFD7792D53,
	Collision2D_get_collider_mA7687EDB0D47A2F211BFE8DB89266B9AA05CFDDD,
	Collision2D_get_rigidbody_m82AF533E110DFDBDED6D6C74EB479902E813D42E,
	Collision2D_get_relativeVelocity_m021626A3853F4D1E2DA016479096DB6CE9999628,
	Collision2D_get_contactCount_m2C74A32BE18A7DDFABD28CD2DCCB65D5283691DF,
	Collision2D_GetContacts_m5E2550321ED4BF04E4B3E7F2E87A185F0EB13D4E,
	ContactPoint2D_get_normal_mC001581D9A15A2E6DA20B96C05482385621EF8DB_AdjustorThunk,
	RaycastHit2D_get_point_m10D5AB3B26EAE62583BE35CFA13A3E40BDAF30AE_AdjustorThunk,
	RaycastHit2D_get_normal_m6F8B9F4018EFA126CC33126E8E42B09BB5A82637_AdjustorThunk,
	RaycastHit2D_get_distance_mA910B45BD349A8F70139F6BC1E686F47F40A1662_AdjustorThunk,
	RaycastHit2D_get_collider_m00F7EC55C36F39E2ED64B31354FB4D9C8938D563_AdjustorThunk,
	RaycastHit2D_get_rigidbody_m798FC5415CBA164090855FACC9F43379F08F37C6_AdjustorThunk,
	RaycastHit2D_get_transform_m0CD992BD94BCE7F1CFB44D987924328B6C0E8864_AdjustorThunk,
	Rigidbody2D_get_position_mEC7D07E3478BEF5A2A0E22C91CA54935376F84C2,
	Rigidbody2D_set_position_m1604084713EB195D44B8B411D4BCAFA5941E3413,
	Rigidbody2D_get_rotation_mD58E62EDB334FCDF7914A94C940F7903E8ADBBFF,
	Rigidbody2D_set_rotation_m4982A0057C1FEB61323B58C7A791E8A0D0CD8C77,
	Rigidbody2D_get_velocity_m138328DCC01EB876FB5EA025BF08728030D93D66,
	Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA,
	Rigidbody2D_get_angularVelocity_m5A202B05024DE0C8F9A8C86900F7EF111574D8A3,
	Rigidbody2D_set_angularVelocity_m53F03D07251B600A01A05A27F16D2D8C0D3C5A66,
	Rigidbody2D_get_sharedMaterial_mE0B1677540E85930DCE9A0CD3B12F12C80047ABF,
	Rigidbody2D_set_gravityScale_mEDC82EE2ED74DA9C5AB8A0C4A929B09149522BBF,
	Rigidbody2D_set_bodyType_m8D34999918D42B2DF16FAAB4F237A8663EA8406B,
	Rigidbody2D_set_useFullKinematicContacts_m1A9E419994A8B098543825A4301A96289CCDDECF,
	Rigidbody2D_set_isKinematic_m82FBA7C4F4EB2569AC77D4767061F9CE0F8C3FEB,
	Rigidbody2D_set_freezeRotation_mF8E01A84694933A5E21F71869CAA42DB247CE987,
	Rigidbody2D_set_constraints_mEB2202406AB063F57E273F4313E7E4D58D613263,
	Rigidbody2D_WakeUp_mB5A3852660DB19F546F0651ADD80BAA2D96D81C0,
	Rigidbody2D_AddForce_mB4754FC98ED65E5381854CDC858D12F0504FB3A2,
	Rigidbody2D_AddForce_m2360EEDAF4E9F279AAB77DBD785A7F7161865343,
	Rigidbody2D__ctor_m6CE72A013ED172CA657C0E00E8C04C0B523C78C5,
	Rigidbody2D_get_position_Injected_m1B97DC47EDF69F0506CEC24603C3A330B7E8FAB4,
	Rigidbody2D_set_position_Injected_mB05AB62CD1D243D071F60E48C9905587ACB94933,
	Rigidbody2D_get_velocity_Injected_m2102CD221C46BA3C36127D7413D8C35A402A8903,
	Rigidbody2D_set_velocity_Injected_m198FAA0E7A328E1B236FC5835C9711987D3C691E,
	Rigidbody2D_AddForce_Injected_m238B89F81818A2A5A0CBD3BADE376151EA7243C7,
	Collider2D_get_isTrigger_m2D8D206E7F91ED3AF8AC552E17414CBD7279F6FF,
	Collider2D_set_isTrigger_m129C3059CC21789EFA198E153A3C82662E3BFF52,
	Collider2D_get_attachedRigidbody_m3E930FC14E58085493B6CC1E4228BEF634A6D5AD,
	Collider2D_set_sharedMaterial_m5DF75EB2B6BE7C7A36D791BDFDCDEDDCFE9E4117,
	Collider2D__ctor_mB3AFF9CA18C8FA8E4F75DC45CA2D4A423A40F23D,
	CircleCollider2D_get_radius_m879CF754EBD72E22F29664045632BC8E7DEC5F49,
	EdgeCollider2D_set_points_m8A6DD1D62CCF4CE90AA079BC59FA62796E1667E3,
	BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760,
	BoxCollider2D_set_size_Injected_mB8DEA9F8BBB15256BAE3092D37FDA1AE9D081401,
	PolygonCollider2D_set_pathCount_mCB8738EC16C8EB539A8DF1F44429AE15DBA79CD4,
	PolygonCollider2D_SetPath_m77B8966A290430F608BE6B207652B55B522151CF,
	PolygonCollider2D_SetPath_Internal_mD78EEDF737B1B7A9C7393F68F226CF85BA5C753E,
	Joint2D_set_connectedBody_mDFAD1EA275D93EDFBCCD52FC5562DED5A556C4F7,
	PhysicsMaterial2D__ctor_m18313D6E7E05873C43A80AC81F521FEB7BCD0743,
	PhysicsMaterial2D_Create_Internal_m4E32D46C8B2D5478DFF629EF320C5779918F5FCE,
	PhysicsMaterial2D_set_bounciness_m382DA6E2E421457D5159812EC65405E5624CEF55,
	PhysicsMaterial2D_set_friction_m477B1C8F1C9B981C3D6F20DCC3A2693F2D99FFBB,
};
static const int32_t s_InvokerIndices[101] = 
{
	14,
	10,
	9,
	1237,
	1238,
	1239,
	1240,
	1241,
	1242,
	1241,
	1242,
	1243,
	1244,
	1245,
	1246,
	1246,
	1247,
	1248,
	49,
	1249,
	1250,
	1251,
	1252,
	1253,
	1254,
	1255,
	1255,
	1256,
	1257,
	1258,
	1259,
	1260,
	1261,
	1262,
	1263,
	1264,
	1265,
	1266,
	3,
	1267,
	1268,
	1269,
	23,
	1270,
	976,
	1271,
	17,
	14,
	14,
	14,
	1004,
	10,
	104,
	1004,
	1004,
	1004,
	653,
	14,
	14,
	14,
	1004,
	1005,
	653,
	275,
	1004,
	1005,
	653,
	275,
	14,
	275,
	32,
	31,
	31,
	31,
	32,
	23,
	1005,
	1272,
	23,
	6,
	6,
	6,
	6,
	64,
	102,
	31,
	14,
	26,
	23,
	653,
	26,
	1005,
	6,
	32,
	62,
	62,
	26,
	23,
	122,
	275,
	275,
};
extern const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule = 
{
	"UnityEngine.Physics2DModule.dll",
	101,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
