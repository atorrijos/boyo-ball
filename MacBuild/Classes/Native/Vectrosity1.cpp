﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// Vectrosity.BrightnessControl
struct BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// Vectrosity.IVectorObject
struct IVectorObject_t42AC6F8419FF898F4F87E8462D63AB43FA3DD059;
// Vectrosity.LineManager
struct LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// Vectrosity.RefInt
struct RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// Vectrosity.VectorLine
struct VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775;
// Vectrosity.VectorObject2D
struct VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5;
// Vectrosity.VectorObject3D
struct VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC;
// Vectrosity.VisibilityControl
struct VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10;
// Vectrosity.VisibilityControlAlways
struct VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F;
// Vectrosity.VisibilityControlStatic
struct VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Vectrosity.LineManager/<DisableLine>c__Iterator0
struct U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// Vectrosity.VisibilityControl/<OnBecameInvisible>c__Iterator3
struct U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B;
// Vectrosity.VisibilityControl/<OnBecameVisible>c__Iterator2
struct U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0;
// Vectrosity.VisibilityControl/<VisibilityTest>c__Iterator1
struct U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA;
// Vectrosity.VisibilityControlStatic/<WaitCheck>c__Iterator4
struct U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo>
struct Dictionary_2_tEE2DF6F30CB4C856D739A00EEBB4B139451D60EB;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>
struct Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_tDBC849B8248C833C53F1762E771EFC477EB8AF18;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.Mesh>
struct KeyCollection_t97ABDDC7C4A50E399A2A6EF308DA850312953C80;
// System.Collections.Generic.List`1<Vectrosity.RefInt>
struct List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344;
// System.Collections.Generic.List`1<Vectrosity.VectorLine>
struct List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.Mesh>
struct ValueCollection_t88D14A25EF3DD6338779EDE40DAAF01463E9C139;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color32[]
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Collections.Generic.Dictionary`2/Entry<System.String,UnityEngine.Mesh>[]
struct EntryU5BU5D_tD0C6513C243AC3CD0B1EF3096B58483245B4F186;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MeshFilter
struct MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A;
// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_tBB400C4D07B2AA7F4D3B7060E588C6D46B63EBBD;
// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// Vectrosity.RefInt[]
struct RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
// Vectrosity.VectorLine[]
struct VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;

IL2CPP_EXTERN_C RuntimeClass* BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1621373437091DE73D152553E425FB66A03B5F27;
IL2CPP_EXTERN_C String_t* _stringLiteral30738EA834CC7433C88B12A40783EE08FA5066D7;
IL2CPP_EXTERN_C String_t* _stringLiteral4E5C9140DE7AD25CB01C92DE3153A14590195A07;
IL2CPP_EXTERN_C String_t* _stringLiteral7837EF2A788ACAC95713E45C5054E759F3642277;
IL2CPP_EXTERN_C String_t* _stringLiteralF141E5CFAFD2AC23EC22AD5EB48EEA5FCF523C2A;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m3090C39E5790BE4D40623B28FF117F9157181983_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mC6D73DFD73A2A194D712ED95C27413F05C8BD1CA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m89E829D13C760C66899A0034D2D31EBC11BC801F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m9AA27A646CA2E339C23E3B02A90B179D38A58F01_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mD924C79C3DA2B286ACB091AAC225DA91675E62A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m776A2FA26D0AA2185665AC714486953B3DBA5D9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m7E6935F5AAD512A36BA46D28BA43762A22FEBB0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB47F013C62B967199614B3D07228B381C0562448_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDisableLineU3Ec__Iterator0_Reset_mB4B023677AE126534F8ECD01B6FD974B9A44A8E1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3COnBecameInvisibleU3Ec__Iterator3_Reset_m63434CECBC5DF4944317DAA6EC774AD46CD5287A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3COnBecameVisibleU3Ec__Iterator2_Reset_m5F5390A4426ED12EEA7B6EF4C5E17B25219F452E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CVisibilityTestU3Ec__Iterator1_Reset_m9FF8A84407B2B77BBEC0664EFDA158FD2488CA28_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CWaitCheckU3Ec__Iterator4_Reset_mCD91B789ED27DFCAB4939937BFD8114713AE27ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t U3CDisableLineU3Ec__Iterator0_MoveNext_m80031DD3D256059BE76D15027467F5FA957D6A16_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CDisableLineU3Ec__Iterator0_Reset_mB4B023677AE126534F8ECD01B6FD974B9A44A8E1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_mE70B1C3FF21A47973237CBABB67A8B40AC99135A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3COnBecameInvisibleU3Ec__Iterator3_Reset_m63434CECBC5DF4944317DAA6EC774AD46CD5287A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m5F96417D4C99C7145D0A2E1A75C84EC136147397_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3COnBecameVisibleU3Ec__Iterator2_Reset_m5F5390A4426ED12EEA7B6EF4C5E17B25219F452E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CVisibilityTestU3Ec__Iterator1_MoveNext_m0B5AADB14FE54C3943C2EC6BFAD29328410B1A8F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CVisibilityTestU3Ec__Iterator1_Reset_m9FF8A84407B2B77BBEC0664EFDA158FD2488CA28_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CWaitCheckU3Ec__Iterator4_MoveNext_m1A20A8601DAE22029AF08361494B130EDA3415BB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CWaitCheckU3Ec__Iterator4_Reset_mCD91B789ED27DFCAB4939937BFD8114713AE27ED_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_CheckDistanceSetup_m5F270965601AD85B1F94AEBDB4DC391FBEBAB788_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_CheckDistance_mEE514C5E086101A2B940A07C2DC92BAAE62BCEA2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_DistanceRemove_mB9D600CD1338D2EC0722800B2BB2BFEE8F6525A9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_DrawArrayLines2_mE6A0035BF8AF9A1A4D8B9BC1E335C6697DC5C5EC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_DrawArrayLines_mCEEBF6298008C43FE803CB36CF9006E666019744_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_GetBounds_m23D1D082EF71F5B252E32D9E3DCB68E847595026_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_GetBounds_m9CCD16DC671F020405D42696786AD9DE2EE295DA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_GetBrightnessValue_mC949BB3C7A6CC28D5D6598269C0588A816C5E387_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_MakeBoundsMesh_m72D478FD61D161D6DCB279FB54F759EA59CD93E6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_ObjectSetup_mAF03886A407E27753714B2600BD7F7E5B475043C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_ObjectSetup_mF7F932263CD9105D67A4277BE6B4EB1D173F20A8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_SetDistanceColor_m12DACE05CD5D135C7137F3A2A9BFE1E2FB29E402_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_SetOldDistance_m3647DA7ABBBD286740E465A1A309F097FEE3B8B7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_VisibilityStaticRemove_m9B72F2333EE6877D0F29D93BC18F76ADA5B0D237_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_VisibilityStaticSetup_mD098A79500A6B1A0E347CAB4DE0A889D28E2E13A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager__cctor_mDBBAD4573C0BA12C1AE71D0FA1B1DD8A5201803A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_get_arrayCount2_m17054F8887ECC80587A291E0AE545283AACE03C7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorManager_get_arrayCount_m150A26CB0D835707FBEC2B14DC7381A02232A94D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_ClearMesh_mB4859A90C0DFABBFC096ED341686BB70AC023763_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_DestroyNow_mAFAAAF538F3A01024B05ECA1E8E7F1EDEC8FDEA7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_Destroy_m22B58E8F0D320D29B647094B4F05043956F81698_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_Enable_m3CF7975FFD23E9CC70209AB333EE640200525205_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_OnPopulateMesh_m0406A9A86232C42EBABB5F237E990039CA174800_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_SetName_mF846BFE2EAE12F81DC7603F0639F2A89B395CE84_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_SetupMesh_mAFA235E8824EB01E1F7F6D62806B46291A1DCB0C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_UpdateGeometry_mAD79F6A576446303BECDCC5FF0D05D75FA0B6FA7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D_UpdateMeshAttributes_mC480305BD1408E7D8CFF157FBF327AA9A4031659_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject2D__cctor_mC43BA63969D868CF6CBB1761B90010A180335FEB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_ClearMesh_mF9821537AA239B9A3A5D4D89E07A1CD632810A24_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_Destroy_m6DAD5F1A059D699F2B05A6B6B54723D56FBD8A8D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_Enable_m72EEE3ED96AB44836B53AD9A2662CB87D5AB9576_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_SetMaterial_mFA8CB205105CEDF2EAFB2351619C476C2E726CF3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_SetName_m34E91DF41876F47A2CFEB2CA0896CA7687054D59_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_SetTexture_m646C1DE560A2B30B564DD1B4E9E79CEBD5354517_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_SetVectorLine_m8EA2D566D669EB28195896F3D557152FC044C6A6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VectorObject3D_SetupMesh_mEDD447CA9C0FBC3D6408F7D397FAC4B63030E411_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControlAlways_OnDestroy_m64F2672D8F7A7BC724F3BF2DCAAD2A3D6608EEE7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControlAlways_Setup_m342E892B79D5E722B39337FE70B2E0C45A83712E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControlStatic_OnBecameVisible_m12516B3BF36AEE40B7EADFC6AFA08BD9BA0A1DE4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControlStatic_OnDestroy_m329A043C2DA7160F29FCD892ACECC20772EFDC28_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControlStatic_Setup_mB81BF8A1F5A9DA09AD76430F4F26327F2AAEB928_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControlStatic_WaitCheck_m48175ADD5334C3484E3464A64F8E6F728E9358C7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControl_OnBecameInvisible_mF5C5604BDFA27A20D8B8B89723F3BB753C1AB3BE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControl_OnBecameVisible_mF88C687A0EE1D0B8D4026EB2FD2D386859291026_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControl_OnDestroy_mE82C1592DD07936C5AC3958E128CB8AB9D4A16C2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControl_Setup_mFC214FC710C98CF73C6CA631AC2090811D2BF8B1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VisibilityControl_VisibilityTest_m1376D2161DA0A5636CE0D6CFE36F296C417546ED_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2;
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>
struct  Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tD0C6513C243AC3CD0B1EF3096B58483245B4F186* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t97ABDDC7C4A50E399A2A6EF308DA850312953C80 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t88D14A25EF3DD6338779EDE40DAAF01463E9C139 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___entries_1)); }
	inline EntryU5BU5D_tD0C6513C243AC3CD0B1EF3096B58483245B4F186* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tD0C6513C243AC3CD0B1EF3096B58483245B4F186** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tD0C6513C243AC3CD0B1EF3096B58483245B4F186* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___keys_7)); }
	inline KeyCollection_t97ABDDC7C4A50E399A2A6EF308DA850312953C80 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t97ABDDC7C4A50E399A2A6EF308DA850312953C80 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t97ABDDC7C4A50E399A2A6EF308DA850312953C80 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ___values_8)); }
	inline ValueCollection_t88D14A25EF3DD6338779EDE40DAAF01463E9C139 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t88D14A25EF3DD6338779EDE40DAAF01463E9C139 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t88D14A25EF3DD6338779EDE40DAAF01463E9C139 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Color>
struct  List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____items_1)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get__items_1() const { return ____items_1; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_StaticFields, ____emptyArray_5)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Transform>
struct  List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____items_1)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields, ____emptyArray_5)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____items_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Vectrosity.RefInt>
struct  List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344, ____items_1)); }
	inline RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248* get__items_1() const { return ____items_1; }
	inline RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344_StaticFields, ____emptyArray_5)); }
	inline RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248* get__emptyArray_5() const { return ____emptyArray_5; }
	inline RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(RefIntU5BU5D_tD7962D13C2732F4A5209A6D5134B8EF51FAE2248* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Vectrosity.VectorLine>
struct  List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB, ____items_1)); }
	inline VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241* get__items_1() const { return ____items_1; }
	inline VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB_StaticFields, ____emptyArray_5)); }
	inline VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241* get__emptyArray_5() const { return ____emptyArray_5; }
	inline VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(VectorLineU5BU5D_tF42DC9F226434BD48BAA58E2E55893E829FA2241* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// Vectrosity.LineManager_<DisableLine>c__Iterator0
struct  U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5  : public RuntimeObject
{
public:
	// System.Single Vectrosity.LineManager_<DisableLine>c__Iterator0::time
	float ___time_0;
	// System.Boolean Vectrosity.LineManager_<DisableLine>c__Iterator0::remove
	bool ___remove_1;
	// Vectrosity.VectorLine Vectrosity.LineManager_<DisableLine>c__Iterator0::vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___vectorLine_2;
	// System.Int32 Vectrosity.LineManager_<DisableLine>c__Iterator0::U24PC
	int32_t ___U24PC_3;
	// System.Object Vectrosity.LineManager_<DisableLine>c__Iterator0::U24current
	RuntimeObject * ___U24current_4;
	// System.Single Vectrosity.LineManager_<DisableLine>c__Iterator0::<U24>time
	float ___U3CU24U3Etime_5;
	// System.Boolean Vectrosity.LineManager_<DisableLine>c__Iterator0::<U24>remove
	bool ___U3CU24U3Eremove_6;
	// Vectrosity.VectorLine Vectrosity.LineManager_<DisableLine>c__Iterator0::<U24>vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___U3CU24U3EvectorLine_7;
	// Vectrosity.LineManager Vectrosity.LineManager_<DisableLine>c__Iterator0::<>f__this
	LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_remove_1() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___remove_1)); }
	inline bool get_remove_1() const { return ___remove_1; }
	inline bool* get_address_of_remove_1() { return &___remove_1; }
	inline void set_remove_1(bool value)
	{
		___remove_1 = value;
	}

	inline static int32_t get_offset_of_vectorLine_2() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___vectorLine_2)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_vectorLine_2() const { return ___vectorLine_2; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_vectorLine_2() { return &___vectorLine_2; }
	inline void set_vectorLine_2(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___vectorLine_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vectorLine_2), (void*)value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U24current_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etime_5() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___U3CU24U3Etime_5)); }
	inline float get_U3CU24U3Etime_5() const { return ___U3CU24U3Etime_5; }
	inline float* get_address_of_U3CU24U3Etime_5() { return &___U3CU24U3Etime_5; }
	inline void set_U3CU24U3Etime_5(float value)
	{
		___U3CU24U3Etime_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eremove_6() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___U3CU24U3Eremove_6)); }
	inline bool get_U3CU24U3Eremove_6() const { return ___U3CU24U3Eremove_6; }
	inline bool* get_address_of_U3CU24U3Eremove_6() { return &___U3CU24U3Eremove_6; }
	inline void set_U3CU24U3Eremove_6(bool value)
	{
		___U3CU24U3Eremove_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EvectorLine_7() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___U3CU24U3EvectorLine_7)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_U3CU24U3EvectorLine_7() const { return ___U3CU24U3EvectorLine_7; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_U3CU24U3EvectorLine_7() { return &___U3CU24U3EvectorLine_7; }
	inline void set_U3CU24U3EvectorLine_7(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___U3CU24U3EvectorLine_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU24U3EvectorLine_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5, ___U3CU3Ef__this_8)); }
	inline LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__this_8), (void*)value);
	}
};


// Vectrosity.RefInt
struct  RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.RefInt::i
	int32_t ___i_0;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}
};


// Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3
struct  U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::<>f__this
	VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U24current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__this_2), (void*)value);
	}
};


// Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2
struct  U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::<>f__this
	VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U24current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__this_2), (void*)value);
	}
};


// Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1
struct  U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControl Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::<>f__this
	VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U24current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA, ___U3CU3Ef__this_2)); }
	inline VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__this_2), (void*)value);
	}
};


// Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4
struct  U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6  : public RuntimeObject
{
public:
	// System.Int32 Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::U24PC
	int32_t ___U24PC_0;
	// System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::U24current
	RuntimeObject * ___U24current_1;
	// Vectrosity.VisibilityControlStatic Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::<>f__this
	VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U24current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6, ___U3CU3Ef__this_2)); }
	inline VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__this_2), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct  UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct  Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// UnityEngine.Matrix4x4
struct  Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:

public:
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.Bounds
struct  Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.HideFlags
struct  HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.VertexHelper
struct  VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Positions_0)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Colors_1)); }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv0S_2)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv0S_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv1S_3)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv1S_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv2S_4)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv2S_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv3S_5)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv3S_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Normals_6)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Tangents_7)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tangents_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Indices_8)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Indices_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___s_DefaultNormal_10 = value;
	}
};


// Vectrosity.Brightness
struct  Brightness_tB4D2433A2B088F8423F358F2010DF78FECC5F20C 
{
public:
	// System.Int32 Vectrosity.Brightness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Brightness_tB4D2433A2B088F8423F358F2010DF78FECC5F20C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vectrosity.CanvasState
struct  CanvasState_t199A9646B67896D6C15A4D506A3EE87B2A4AEA79 
{
public:
	// System.Int32 Vectrosity.CanvasState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CanvasState_t199A9646B67896D6C15A4D506A3EE87B2A4AEA79, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vectrosity.EndCap
struct  EndCap_t271B7D86737C40C3D85A07FE806952DB94FE66EC 
{
public:
	// System.Int32 Vectrosity.EndCap::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EndCap_t271B7D86737C40C3D85A07FE806952DB94FE66EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vectrosity.Joins
struct  Joins_tC3BF6C602B56E1207764339CBDBBC3F35043B13D 
{
public:
	// System.Int32 Vectrosity.Joins::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Joins_tC3BF6C602B56E1207764339CBDBBC3F35043B13D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vectrosity.LineType
struct  LineType_t91D12D91DE32FDB954E8D3E5688B24E4FFB01A8F 
{
public:
	// System.Int32 Vectrosity.LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t91D12D91DE32FDB954E8D3E5688B24E4FFB01A8F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vectrosity.VectorLine_FunctionName
struct  FunctionName_t86C663697DB8A81D520D7C2B89E2CBACD6E4A231 
{
public:
	// System.Int32 Vectrosity.VectorLine_FunctionName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FunctionName_t86C663697DB8A81D520D7C2B89E2CBACD6E4A231, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vectrosity.VectorManager
struct  VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108  : public RuntimeObject
{
public:

public:
};

struct VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields
{
public:
	// System.Single Vectrosity.VectorManager::minBrightnessDistance
	float ___minBrightnessDistance_0;
	// System.Single Vectrosity.VectorManager::maxBrightnessDistance
	float ___maxBrightnessDistance_1;
	// System.Int32 Vectrosity.VectorManager::brightnessLevels
	int32_t ___brightnessLevels_2;
	// System.Single Vectrosity.VectorManager::distanceCheckFrequency
	float ___distanceCheckFrequency_3;
	// UnityEngine.Color Vectrosity.VectorManager::fogColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___fogColor_4;
	// System.Boolean Vectrosity.VectorManager::useDraw3D
	bool ___useDraw3D_5;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines
	List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * ___vectorLines_6;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers
	List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * ___objectNumbers_7;
	// System.Int32 Vectrosity.VectorManager::_arrayCount
	int32_t ____arrayCount_8;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines2
	List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * ___vectorLines2_9;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers2
	List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * ___objectNumbers2_10;
	// System.Int32 Vectrosity.VectorManager::_arrayCount2
	int32_t ____arrayCount2_11;
	// System.Collections.Generic.List`1<UnityEngine.Transform> Vectrosity.VectorManager::transforms3
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___transforms3_12;
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.VectorManager::vectorLines3
	List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * ___vectorLines3_13;
	// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorManager::oldDistances
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___oldDistances_14;
	// System.Collections.Generic.List`1<UnityEngine.Color> Vectrosity.VectorManager::colors
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___colors_15;
	// System.Collections.Generic.List`1<Vectrosity.RefInt> Vectrosity.VectorManager::objectNumbers3
	List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * ___objectNumbers3_16;
	// System.Int32 Vectrosity.VectorManager::_arrayCount3
	int32_t ____arrayCount3_17;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh> Vectrosity.VectorManager::meshTable
	Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * ___meshTable_18;

public:
	inline static int32_t get_offset_of_minBrightnessDistance_0() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___minBrightnessDistance_0)); }
	inline float get_minBrightnessDistance_0() const { return ___minBrightnessDistance_0; }
	inline float* get_address_of_minBrightnessDistance_0() { return &___minBrightnessDistance_0; }
	inline void set_minBrightnessDistance_0(float value)
	{
		___minBrightnessDistance_0 = value;
	}

	inline static int32_t get_offset_of_maxBrightnessDistance_1() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___maxBrightnessDistance_1)); }
	inline float get_maxBrightnessDistance_1() const { return ___maxBrightnessDistance_1; }
	inline float* get_address_of_maxBrightnessDistance_1() { return &___maxBrightnessDistance_1; }
	inline void set_maxBrightnessDistance_1(float value)
	{
		___maxBrightnessDistance_1 = value;
	}

	inline static int32_t get_offset_of_brightnessLevels_2() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___brightnessLevels_2)); }
	inline int32_t get_brightnessLevels_2() const { return ___brightnessLevels_2; }
	inline int32_t* get_address_of_brightnessLevels_2() { return &___brightnessLevels_2; }
	inline void set_brightnessLevels_2(int32_t value)
	{
		___brightnessLevels_2 = value;
	}

	inline static int32_t get_offset_of_distanceCheckFrequency_3() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___distanceCheckFrequency_3)); }
	inline float get_distanceCheckFrequency_3() const { return ___distanceCheckFrequency_3; }
	inline float* get_address_of_distanceCheckFrequency_3() { return &___distanceCheckFrequency_3; }
	inline void set_distanceCheckFrequency_3(float value)
	{
		___distanceCheckFrequency_3 = value;
	}

	inline static int32_t get_offset_of_fogColor_4() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___fogColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_fogColor_4() const { return ___fogColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_fogColor_4() { return &___fogColor_4; }
	inline void set_fogColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___fogColor_4 = value;
	}

	inline static int32_t get_offset_of_useDraw3D_5() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___useDraw3D_5)); }
	inline bool get_useDraw3D_5() const { return ___useDraw3D_5; }
	inline bool* get_address_of_useDraw3D_5() { return &___useDraw3D_5; }
	inline void set_useDraw3D_5(bool value)
	{
		___useDraw3D_5 = value;
	}

	inline static int32_t get_offset_of_vectorLines_6() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___vectorLines_6)); }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * get_vectorLines_6() const { return ___vectorLines_6; }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB ** get_address_of_vectorLines_6() { return &___vectorLines_6; }
	inline void set_vectorLines_6(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * value)
	{
		___vectorLines_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vectorLines_6), (void*)value);
	}

	inline static int32_t get_offset_of_objectNumbers_7() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___objectNumbers_7)); }
	inline List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * get_objectNumbers_7() const { return ___objectNumbers_7; }
	inline List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 ** get_address_of_objectNumbers_7() { return &___objectNumbers_7; }
	inline void set_objectNumbers_7(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * value)
	{
		___objectNumbers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectNumbers_7), (void*)value);
	}

	inline static int32_t get_offset_of__arrayCount_8() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ____arrayCount_8)); }
	inline int32_t get__arrayCount_8() const { return ____arrayCount_8; }
	inline int32_t* get_address_of__arrayCount_8() { return &____arrayCount_8; }
	inline void set__arrayCount_8(int32_t value)
	{
		____arrayCount_8 = value;
	}

	inline static int32_t get_offset_of_vectorLines2_9() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___vectorLines2_9)); }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * get_vectorLines2_9() const { return ___vectorLines2_9; }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB ** get_address_of_vectorLines2_9() { return &___vectorLines2_9; }
	inline void set_vectorLines2_9(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * value)
	{
		___vectorLines2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vectorLines2_9), (void*)value);
	}

	inline static int32_t get_offset_of_objectNumbers2_10() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___objectNumbers2_10)); }
	inline List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * get_objectNumbers2_10() const { return ___objectNumbers2_10; }
	inline List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 ** get_address_of_objectNumbers2_10() { return &___objectNumbers2_10; }
	inline void set_objectNumbers2_10(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * value)
	{
		___objectNumbers2_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectNumbers2_10), (void*)value);
	}

	inline static int32_t get_offset_of__arrayCount2_11() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ____arrayCount2_11)); }
	inline int32_t get__arrayCount2_11() const { return ____arrayCount2_11; }
	inline int32_t* get_address_of__arrayCount2_11() { return &____arrayCount2_11; }
	inline void set__arrayCount2_11(int32_t value)
	{
		____arrayCount2_11 = value;
	}

	inline static int32_t get_offset_of_transforms3_12() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___transforms3_12)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_transforms3_12() const { return ___transforms3_12; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_transforms3_12() { return &___transforms3_12; }
	inline void set_transforms3_12(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___transforms3_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___transforms3_12), (void*)value);
	}

	inline static int32_t get_offset_of_vectorLines3_13() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___vectorLines3_13)); }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * get_vectorLines3_13() const { return ___vectorLines3_13; }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB ** get_address_of_vectorLines3_13() { return &___vectorLines3_13; }
	inline void set_vectorLines3_13(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * value)
	{
		___vectorLines3_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vectorLines3_13), (void*)value);
	}

	inline static int32_t get_offset_of_oldDistances_14() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___oldDistances_14)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_oldDistances_14() const { return ___oldDistances_14; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_oldDistances_14() { return &___oldDistances_14; }
	inline void set_oldDistances_14(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___oldDistances_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oldDistances_14), (void*)value);
	}

	inline static int32_t get_offset_of_colors_15() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___colors_15)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_colors_15() const { return ___colors_15; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_colors_15() { return &___colors_15; }
	inline void set_colors_15(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___colors_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colors_15), (void*)value);
	}

	inline static int32_t get_offset_of_objectNumbers3_16() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___objectNumbers3_16)); }
	inline List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * get_objectNumbers3_16() const { return ___objectNumbers3_16; }
	inline List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 ** get_address_of_objectNumbers3_16() { return &___objectNumbers3_16; }
	inline void set_objectNumbers3_16(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * value)
	{
		___objectNumbers3_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectNumbers3_16), (void*)value);
	}

	inline static int32_t get_offset_of__arrayCount3_17() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ____arrayCount3_17)); }
	inline int32_t get__arrayCount3_17() const { return ____arrayCount3_17; }
	inline int32_t* get_address_of__arrayCount3_17() { return &____arrayCount3_17; }
	inline void set__arrayCount3_17(int32_t value)
	{
		____arrayCount3_17 = value;
	}

	inline static int32_t get_offset_of_meshTable_18() { return static_cast<int32_t>(offsetof(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields, ___meshTable_18)); }
	inline Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * get_meshTable_18() const { return ___meshTable_18; }
	inline Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE ** get_address_of_meshTable_18() { return &___meshTable_18; }
	inline void set_meshTable_18(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * value)
	{
		___meshTable_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___meshTable_18), (void*)value);
	}
};


// Vectrosity.Visibility
struct  Visibility_t2774E5AABC91F7C2F088F58C16352B47CE973665 
{
public:
	// System.Int32 Vectrosity.Visibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Visibility_t2774E5AABC91F7C2F088F58C16352B47CE973665, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct  Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Mesh
struct  Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// Vectrosity.VectorLine
struct  VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_lineVertices
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_lineVertices_0;
	// UnityEngine.Vector2[] Vectrosity.VectorLine::m_lineUVs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_lineUVs_1;
	// UnityEngine.Color32[] Vectrosity.VectorLine::m_lineColors
	Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* ___m_lineColors_2;
	// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorLine::m_lineTriangles
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_lineTriangles_3;
	// System.Int32 Vectrosity.VectorLine::m_vertexCount
	int32_t ___m_vertexCount_4;
	// UnityEngine.GameObject Vectrosity.VectorLine::m_go
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_go_5;
	// UnityEngine.RectTransform Vectrosity.VectorLine::m_rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_rectTransform_6;
	// Vectrosity.IVectorObject Vectrosity.VectorLine::m_vectorObject
	RuntimeObject* ___m_vectorObject_7;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_color
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_color_8;
	// Vectrosity.CanvasState Vectrosity.VectorLine::m_canvasState
	int32_t ___m_canvasState_9;
	// System.Boolean Vectrosity.VectorLine::m_is2D
	bool ___m_is2D_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Vectrosity.VectorLine::m_points2
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___m_points2_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::m_points3
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_points3_12;
	// System.Int32 Vectrosity.VectorLine::m_pointsCount
	int32_t ___m_pointsCount_13;
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_screenPoints
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_screenPoints_14;
	// System.Single[] Vectrosity.VectorLine::m_lineWidths
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___m_lineWidths_15;
	// System.Single Vectrosity.VectorLine::m_lineWidth
	float ___m_lineWidth_16;
	// System.Single Vectrosity.VectorLine::m_maxWeldDistance
	float ___m_maxWeldDistance_17;
	// System.Single[] Vectrosity.VectorLine::m_distances
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___m_distances_18;
	// System.String Vectrosity.VectorLine::m_name
	String_t* ___m_name_19;
	// UnityEngine.Material Vectrosity.VectorLine::m_material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_material_20;
	// UnityEngine.Texture Vectrosity.VectorLine::m_originalTexture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_originalTexture_21;
	// UnityEngine.Texture Vectrosity.VectorLine::m_texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_texture_22;
	// System.Boolean Vectrosity.VectorLine::m_active
	bool ___m_active_23;
	// Vectrosity.LineType Vectrosity.VectorLine::m_lineType
	int32_t ___m_lineType_24;
	// System.Single Vectrosity.VectorLine::m_capLength
	float ___m_capLength_25;
	// System.Boolean Vectrosity.VectorLine::m_smoothWidth
	bool ___m_smoothWidth_26;
	// System.Boolean Vectrosity.VectorLine::m_smoothColor
	bool ___m_smoothColor_27;
	// Vectrosity.Joins Vectrosity.VectorLine::m_joins
	int32_t ___m_joins_28;
	// System.Boolean Vectrosity.VectorLine::m_isAutoDrawing
	bool ___m_isAutoDrawing_29;
	// System.Int32 Vectrosity.VectorLine::m_drawStart
	int32_t ___m_drawStart_30;
	// System.Int32 Vectrosity.VectorLine::m_drawEnd
	int32_t ___m_drawEnd_31;
	// System.Int32 Vectrosity.VectorLine::m_endPointsUpdate
	int32_t ___m_endPointsUpdate_32;
	// System.Boolean Vectrosity.VectorLine::m_useNormals
	bool ___m_useNormals_33;
	// System.Boolean Vectrosity.VectorLine::m_useTangents
	bool ___m_useTangents_34;
	// System.Boolean Vectrosity.VectorLine::m_normalsCalculated
	bool ___m_normalsCalculated_35;
	// System.Boolean Vectrosity.VectorLine::m_tangentsCalculated
	bool ___m_tangentsCalculated_36;
	// Vectrosity.EndCap Vectrosity.VectorLine::m_capType
	int32_t ___m_capType_37;
	// System.String Vectrosity.VectorLine::m_endCap
	String_t* ___m_endCap_38;
	// System.Boolean Vectrosity.VectorLine::m_useCapColors
	bool ___m_useCapColors_39;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_frontColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_frontColor_40;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_backColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_backColor_41;
	// System.Int32 Vectrosity.VectorLine::m_frontEndCapIndex
	int32_t ___m_frontEndCapIndex_42;
	// System.Int32 Vectrosity.VectorLine::m_backEndCapIndex
	int32_t ___m_backEndCapIndex_43;
	// System.Single Vectrosity.VectorLine::m_lineUVBottom
	float ___m_lineUVBottom_44;
	// System.Single Vectrosity.VectorLine::m_lineUVTop
	float ___m_lineUVTop_45;
	// System.Single Vectrosity.VectorLine::m_frontCapUVBottom
	float ___m_frontCapUVBottom_46;
	// System.Single Vectrosity.VectorLine::m_frontCapUVTop
	float ___m_frontCapUVTop_47;
	// System.Single Vectrosity.VectorLine::m_backCapUVBottom
	float ___m_backCapUVBottom_48;
	// System.Single Vectrosity.VectorLine::m_backCapUVTop
	float ___m_backCapUVTop_49;
	// System.Boolean Vectrosity.VectorLine::m_continuousTexture
	bool ___m_continuousTexture_50;
	// UnityEngine.Transform Vectrosity.VectorLine::m_drawTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_drawTransform_51;
	// System.Boolean Vectrosity.VectorLine::m_viewportDraw
	bool ___m_viewportDraw_52;
	// System.Single Vectrosity.VectorLine::m_textureScale
	float ___m_textureScale_53;
	// System.Boolean Vectrosity.VectorLine::m_useTextureScale
	bool ___m_useTextureScale_54;
	// System.Single Vectrosity.VectorLine::m_textureOffset
	float ___m_textureOffset_55;
	// System.Boolean Vectrosity.VectorLine::m_useMatrix
	bool ___m_useMatrix_56;
	// UnityEngine.Matrix4x4 Vectrosity.VectorLine::m_matrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___m_matrix_57;
	// System.Boolean Vectrosity.VectorLine::m_collider
	bool ___m_collider_58;
	// System.Boolean Vectrosity.VectorLine::m_trigger
	bool ___m_trigger_59;
	// UnityEngine.PhysicsMaterial2D Vectrosity.VectorLine::m_physicsMaterial
	PhysicsMaterial2D_tBB400C4D07B2AA7F4D3B7060E588C6D46B63EBBD * ___m_physicsMaterial_60;
	// System.Boolean Vectrosity.VectorLine::m_alignOddWidthToPixels
	bool ___m_alignOddWidthToPixels_61;

public:
	inline static int32_t get_offset_of_m_lineVertices_0() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineVertices_0)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_lineVertices_0() const { return ___m_lineVertices_0; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_lineVertices_0() { return &___m_lineVertices_0; }
	inline void set_m_lineVertices_0(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_lineVertices_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_lineVertices_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_lineUVs_1() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineUVs_1)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_lineUVs_1() const { return ___m_lineUVs_1; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_lineUVs_1() { return &___m_lineUVs_1; }
	inline void set_m_lineUVs_1(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_lineUVs_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_lineUVs_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_lineColors_2() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineColors_2)); }
	inline Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* get_m_lineColors_2() const { return ___m_lineColors_2; }
	inline Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2** get_address_of_m_lineColors_2() { return &___m_lineColors_2; }
	inline void set_m_lineColors_2(Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* value)
	{
		___m_lineColors_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_lineColors_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_lineTriangles_3() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineTriangles_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_lineTriangles_3() const { return ___m_lineTriangles_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_lineTriangles_3() { return &___m_lineTriangles_3; }
	inline void set_m_lineTriangles_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_lineTriangles_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_lineTriangles_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_vertexCount_4() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_vertexCount_4)); }
	inline int32_t get_m_vertexCount_4() const { return ___m_vertexCount_4; }
	inline int32_t* get_address_of_m_vertexCount_4() { return &___m_vertexCount_4; }
	inline void set_m_vertexCount_4(int32_t value)
	{
		___m_vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_m_go_5() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_go_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_go_5() const { return ___m_go_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_go_5() { return &___m_go_5; }
	inline void set_m_go_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_go_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_go_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_rectTransform_6() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_rectTransform_6)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_rectTransform_6() const { return ___m_rectTransform_6; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_rectTransform_6() { return &___m_rectTransform_6; }
	inline void set_m_rectTransform_6(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_rectTransform_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_vectorObject_7() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_vectorObject_7)); }
	inline RuntimeObject* get_m_vectorObject_7() const { return ___m_vectorObject_7; }
	inline RuntimeObject** get_address_of_m_vectorObject_7() { return &___m_vectorObject_7; }
	inline void set_m_vectorObject_7(RuntimeObject* value)
	{
		___m_vectorObject_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vectorObject_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_color_8() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_color_8)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_color_8() const { return ___m_color_8; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_color_8() { return &___m_color_8; }
	inline void set_m_color_8(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_color_8 = value;
	}

	inline static int32_t get_offset_of_m_canvasState_9() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_canvasState_9)); }
	inline int32_t get_m_canvasState_9() const { return ___m_canvasState_9; }
	inline int32_t* get_address_of_m_canvasState_9() { return &___m_canvasState_9; }
	inline void set_m_canvasState_9(int32_t value)
	{
		___m_canvasState_9 = value;
	}

	inline static int32_t get_offset_of_m_is2D_10() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_is2D_10)); }
	inline bool get_m_is2D_10() const { return ___m_is2D_10; }
	inline bool* get_address_of_m_is2D_10() { return &___m_is2D_10; }
	inline void set_m_is2D_10(bool value)
	{
		___m_is2D_10 = value;
	}

	inline static int32_t get_offset_of_m_points2_11() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_points2_11)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_m_points2_11() const { return ___m_points2_11; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_m_points2_11() { return &___m_points2_11; }
	inline void set_m_points2_11(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___m_points2_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_points2_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_points3_12() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_points3_12)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_points3_12() const { return ___m_points3_12; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_points3_12() { return &___m_points3_12; }
	inline void set_m_points3_12(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_points3_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_points3_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_pointsCount_13() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_pointsCount_13)); }
	inline int32_t get_m_pointsCount_13() const { return ___m_pointsCount_13; }
	inline int32_t* get_address_of_m_pointsCount_13() { return &___m_pointsCount_13; }
	inline void set_m_pointsCount_13(int32_t value)
	{
		___m_pointsCount_13 = value;
	}

	inline static int32_t get_offset_of_m_screenPoints_14() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_screenPoints_14)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_screenPoints_14() const { return ___m_screenPoints_14; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_screenPoints_14() { return &___m_screenPoints_14; }
	inline void set_m_screenPoints_14(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_screenPoints_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_screenPoints_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_lineWidths_15() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineWidths_15)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_m_lineWidths_15() const { return ___m_lineWidths_15; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_m_lineWidths_15() { return &___m_lineWidths_15; }
	inline void set_m_lineWidths_15(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___m_lineWidths_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_lineWidths_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_lineWidth_16() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineWidth_16)); }
	inline float get_m_lineWidth_16() const { return ___m_lineWidth_16; }
	inline float* get_address_of_m_lineWidth_16() { return &___m_lineWidth_16; }
	inline void set_m_lineWidth_16(float value)
	{
		___m_lineWidth_16 = value;
	}

	inline static int32_t get_offset_of_m_maxWeldDistance_17() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_maxWeldDistance_17)); }
	inline float get_m_maxWeldDistance_17() const { return ___m_maxWeldDistance_17; }
	inline float* get_address_of_m_maxWeldDistance_17() { return &___m_maxWeldDistance_17; }
	inline void set_m_maxWeldDistance_17(float value)
	{
		___m_maxWeldDistance_17 = value;
	}

	inline static int32_t get_offset_of_m_distances_18() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_distances_18)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_m_distances_18() const { return ___m_distances_18; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_m_distances_18() { return &___m_distances_18; }
	inline void set_m_distances_18(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___m_distances_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_distances_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_19() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_name_19)); }
	inline String_t* get_m_name_19() const { return ___m_name_19; }
	inline String_t** get_address_of_m_name_19() { return &___m_name_19; }
	inline void set_m_name_19(String_t* value)
	{
		___m_name_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_material_20() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_material_20)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_material_20() const { return ___m_material_20; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_material_20() { return &___m_material_20; }
	inline void set_m_material_20(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_material_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_material_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_originalTexture_21() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_originalTexture_21)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_originalTexture_21() const { return ___m_originalTexture_21; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_originalTexture_21() { return &___m_originalTexture_21; }
	inline void set_m_originalTexture_21(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_originalTexture_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_originalTexture_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_texture_22() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_texture_22)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_texture_22() const { return ___m_texture_22; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_texture_22() { return &___m_texture_22; }
	inline void set_m_texture_22(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_texture_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_texture_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_active_23() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_active_23)); }
	inline bool get_m_active_23() const { return ___m_active_23; }
	inline bool* get_address_of_m_active_23() { return &___m_active_23; }
	inline void set_m_active_23(bool value)
	{
		___m_active_23 = value;
	}

	inline static int32_t get_offset_of_m_lineType_24() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineType_24)); }
	inline int32_t get_m_lineType_24() const { return ___m_lineType_24; }
	inline int32_t* get_address_of_m_lineType_24() { return &___m_lineType_24; }
	inline void set_m_lineType_24(int32_t value)
	{
		___m_lineType_24 = value;
	}

	inline static int32_t get_offset_of_m_capLength_25() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_capLength_25)); }
	inline float get_m_capLength_25() const { return ___m_capLength_25; }
	inline float* get_address_of_m_capLength_25() { return &___m_capLength_25; }
	inline void set_m_capLength_25(float value)
	{
		___m_capLength_25 = value;
	}

	inline static int32_t get_offset_of_m_smoothWidth_26() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_smoothWidth_26)); }
	inline bool get_m_smoothWidth_26() const { return ___m_smoothWidth_26; }
	inline bool* get_address_of_m_smoothWidth_26() { return &___m_smoothWidth_26; }
	inline void set_m_smoothWidth_26(bool value)
	{
		___m_smoothWidth_26 = value;
	}

	inline static int32_t get_offset_of_m_smoothColor_27() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_smoothColor_27)); }
	inline bool get_m_smoothColor_27() const { return ___m_smoothColor_27; }
	inline bool* get_address_of_m_smoothColor_27() { return &___m_smoothColor_27; }
	inline void set_m_smoothColor_27(bool value)
	{
		___m_smoothColor_27 = value;
	}

	inline static int32_t get_offset_of_m_joins_28() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_joins_28)); }
	inline int32_t get_m_joins_28() const { return ___m_joins_28; }
	inline int32_t* get_address_of_m_joins_28() { return &___m_joins_28; }
	inline void set_m_joins_28(int32_t value)
	{
		___m_joins_28 = value;
	}

	inline static int32_t get_offset_of_m_isAutoDrawing_29() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_isAutoDrawing_29)); }
	inline bool get_m_isAutoDrawing_29() const { return ___m_isAutoDrawing_29; }
	inline bool* get_address_of_m_isAutoDrawing_29() { return &___m_isAutoDrawing_29; }
	inline void set_m_isAutoDrawing_29(bool value)
	{
		___m_isAutoDrawing_29 = value;
	}

	inline static int32_t get_offset_of_m_drawStart_30() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_drawStart_30)); }
	inline int32_t get_m_drawStart_30() const { return ___m_drawStart_30; }
	inline int32_t* get_address_of_m_drawStart_30() { return &___m_drawStart_30; }
	inline void set_m_drawStart_30(int32_t value)
	{
		___m_drawStart_30 = value;
	}

	inline static int32_t get_offset_of_m_drawEnd_31() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_drawEnd_31)); }
	inline int32_t get_m_drawEnd_31() const { return ___m_drawEnd_31; }
	inline int32_t* get_address_of_m_drawEnd_31() { return &___m_drawEnd_31; }
	inline void set_m_drawEnd_31(int32_t value)
	{
		___m_drawEnd_31 = value;
	}

	inline static int32_t get_offset_of_m_endPointsUpdate_32() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_endPointsUpdate_32)); }
	inline int32_t get_m_endPointsUpdate_32() const { return ___m_endPointsUpdate_32; }
	inline int32_t* get_address_of_m_endPointsUpdate_32() { return &___m_endPointsUpdate_32; }
	inline void set_m_endPointsUpdate_32(int32_t value)
	{
		___m_endPointsUpdate_32 = value;
	}

	inline static int32_t get_offset_of_m_useNormals_33() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_useNormals_33)); }
	inline bool get_m_useNormals_33() const { return ___m_useNormals_33; }
	inline bool* get_address_of_m_useNormals_33() { return &___m_useNormals_33; }
	inline void set_m_useNormals_33(bool value)
	{
		___m_useNormals_33 = value;
	}

	inline static int32_t get_offset_of_m_useTangents_34() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_useTangents_34)); }
	inline bool get_m_useTangents_34() const { return ___m_useTangents_34; }
	inline bool* get_address_of_m_useTangents_34() { return &___m_useTangents_34; }
	inline void set_m_useTangents_34(bool value)
	{
		___m_useTangents_34 = value;
	}

	inline static int32_t get_offset_of_m_normalsCalculated_35() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_normalsCalculated_35)); }
	inline bool get_m_normalsCalculated_35() const { return ___m_normalsCalculated_35; }
	inline bool* get_address_of_m_normalsCalculated_35() { return &___m_normalsCalculated_35; }
	inline void set_m_normalsCalculated_35(bool value)
	{
		___m_normalsCalculated_35 = value;
	}

	inline static int32_t get_offset_of_m_tangentsCalculated_36() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_tangentsCalculated_36)); }
	inline bool get_m_tangentsCalculated_36() const { return ___m_tangentsCalculated_36; }
	inline bool* get_address_of_m_tangentsCalculated_36() { return &___m_tangentsCalculated_36; }
	inline void set_m_tangentsCalculated_36(bool value)
	{
		___m_tangentsCalculated_36 = value;
	}

	inline static int32_t get_offset_of_m_capType_37() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_capType_37)); }
	inline int32_t get_m_capType_37() const { return ___m_capType_37; }
	inline int32_t* get_address_of_m_capType_37() { return &___m_capType_37; }
	inline void set_m_capType_37(int32_t value)
	{
		___m_capType_37 = value;
	}

	inline static int32_t get_offset_of_m_endCap_38() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_endCap_38)); }
	inline String_t* get_m_endCap_38() const { return ___m_endCap_38; }
	inline String_t** get_address_of_m_endCap_38() { return &___m_endCap_38; }
	inline void set_m_endCap_38(String_t* value)
	{
		___m_endCap_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_endCap_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_useCapColors_39() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_useCapColors_39)); }
	inline bool get_m_useCapColors_39() const { return ___m_useCapColors_39; }
	inline bool* get_address_of_m_useCapColors_39() { return &___m_useCapColors_39; }
	inline void set_m_useCapColors_39(bool value)
	{
		___m_useCapColors_39 = value;
	}

	inline static int32_t get_offset_of_m_frontColor_40() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_frontColor_40)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_frontColor_40() const { return ___m_frontColor_40; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_frontColor_40() { return &___m_frontColor_40; }
	inline void set_m_frontColor_40(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_frontColor_40 = value;
	}

	inline static int32_t get_offset_of_m_backColor_41() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_backColor_41)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_backColor_41() const { return ___m_backColor_41; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_backColor_41() { return &___m_backColor_41; }
	inline void set_m_backColor_41(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_backColor_41 = value;
	}

	inline static int32_t get_offset_of_m_frontEndCapIndex_42() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_frontEndCapIndex_42)); }
	inline int32_t get_m_frontEndCapIndex_42() const { return ___m_frontEndCapIndex_42; }
	inline int32_t* get_address_of_m_frontEndCapIndex_42() { return &___m_frontEndCapIndex_42; }
	inline void set_m_frontEndCapIndex_42(int32_t value)
	{
		___m_frontEndCapIndex_42 = value;
	}

	inline static int32_t get_offset_of_m_backEndCapIndex_43() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_backEndCapIndex_43)); }
	inline int32_t get_m_backEndCapIndex_43() const { return ___m_backEndCapIndex_43; }
	inline int32_t* get_address_of_m_backEndCapIndex_43() { return &___m_backEndCapIndex_43; }
	inline void set_m_backEndCapIndex_43(int32_t value)
	{
		___m_backEndCapIndex_43 = value;
	}

	inline static int32_t get_offset_of_m_lineUVBottom_44() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineUVBottom_44)); }
	inline float get_m_lineUVBottom_44() const { return ___m_lineUVBottom_44; }
	inline float* get_address_of_m_lineUVBottom_44() { return &___m_lineUVBottom_44; }
	inline void set_m_lineUVBottom_44(float value)
	{
		___m_lineUVBottom_44 = value;
	}

	inline static int32_t get_offset_of_m_lineUVTop_45() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_lineUVTop_45)); }
	inline float get_m_lineUVTop_45() const { return ___m_lineUVTop_45; }
	inline float* get_address_of_m_lineUVTop_45() { return &___m_lineUVTop_45; }
	inline void set_m_lineUVTop_45(float value)
	{
		___m_lineUVTop_45 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVBottom_46() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_frontCapUVBottom_46)); }
	inline float get_m_frontCapUVBottom_46() const { return ___m_frontCapUVBottom_46; }
	inline float* get_address_of_m_frontCapUVBottom_46() { return &___m_frontCapUVBottom_46; }
	inline void set_m_frontCapUVBottom_46(float value)
	{
		___m_frontCapUVBottom_46 = value;
	}

	inline static int32_t get_offset_of_m_frontCapUVTop_47() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_frontCapUVTop_47)); }
	inline float get_m_frontCapUVTop_47() const { return ___m_frontCapUVTop_47; }
	inline float* get_address_of_m_frontCapUVTop_47() { return &___m_frontCapUVTop_47; }
	inline void set_m_frontCapUVTop_47(float value)
	{
		___m_frontCapUVTop_47 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVBottom_48() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_backCapUVBottom_48)); }
	inline float get_m_backCapUVBottom_48() const { return ___m_backCapUVBottom_48; }
	inline float* get_address_of_m_backCapUVBottom_48() { return &___m_backCapUVBottom_48; }
	inline void set_m_backCapUVBottom_48(float value)
	{
		___m_backCapUVBottom_48 = value;
	}

	inline static int32_t get_offset_of_m_backCapUVTop_49() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_backCapUVTop_49)); }
	inline float get_m_backCapUVTop_49() const { return ___m_backCapUVTop_49; }
	inline float* get_address_of_m_backCapUVTop_49() { return &___m_backCapUVTop_49; }
	inline void set_m_backCapUVTop_49(float value)
	{
		___m_backCapUVTop_49 = value;
	}

	inline static int32_t get_offset_of_m_continuousTexture_50() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_continuousTexture_50)); }
	inline bool get_m_continuousTexture_50() const { return ___m_continuousTexture_50; }
	inline bool* get_address_of_m_continuousTexture_50() { return &___m_continuousTexture_50; }
	inline void set_m_continuousTexture_50(bool value)
	{
		___m_continuousTexture_50 = value;
	}

	inline static int32_t get_offset_of_m_drawTransform_51() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_drawTransform_51)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_drawTransform_51() const { return ___m_drawTransform_51; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_drawTransform_51() { return &___m_drawTransform_51; }
	inline void set_m_drawTransform_51(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_drawTransform_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_drawTransform_51), (void*)value);
	}

	inline static int32_t get_offset_of_m_viewportDraw_52() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_viewportDraw_52)); }
	inline bool get_m_viewportDraw_52() const { return ___m_viewportDraw_52; }
	inline bool* get_address_of_m_viewportDraw_52() { return &___m_viewportDraw_52; }
	inline void set_m_viewportDraw_52(bool value)
	{
		___m_viewportDraw_52 = value;
	}

	inline static int32_t get_offset_of_m_textureScale_53() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_textureScale_53)); }
	inline float get_m_textureScale_53() const { return ___m_textureScale_53; }
	inline float* get_address_of_m_textureScale_53() { return &___m_textureScale_53; }
	inline void set_m_textureScale_53(float value)
	{
		___m_textureScale_53 = value;
	}

	inline static int32_t get_offset_of_m_useTextureScale_54() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_useTextureScale_54)); }
	inline bool get_m_useTextureScale_54() const { return ___m_useTextureScale_54; }
	inline bool* get_address_of_m_useTextureScale_54() { return &___m_useTextureScale_54; }
	inline void set_m_useTextureScale_54(bool value)
	{
		___m_useTextureScale_54 = value;
	}

	inline static int32_t get_offset_of_m_textureOffset_55() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_textureOffset_55)); }
	inline float get_m_textureOffset_55() const { return ___m_textureOffset_55; }
	inline float* get_address_of_m_textureOffset_55() { return &___m_textureOffset_55; }
	inline void set_m_textureOffset_55(float value)
	{
		___m_textureOffset_55 = value;
	}

	inline static int32_t get_offset_of_m_useMatrix_56() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_useMatrix_56)); }
	inline bool get_m_useMatrix_56() const { return ___m_useMatrix_56; }
	inline bool* get_address_of_m_useMatrix_56() { return &___m_useMatrix_56; }
	inline void set_m_useMatrix_56(bool value)
	{
		___m_useMatrix_56 = value;
	}

	inline static int32_t get_offset_of_m_matrix_57() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_matrix_57)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_m_matrix_57() const { return ___m_matrix_57; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_m_matrix_57() { return &___m_matrix_57; }
	inline void set_m_matrix_57(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___m_matrix_57 = value;
	}

	inline static int32_t get_offset_of_m_collider_58() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_collider_58)); }
	inline bool get_m_collider_58() const { return ___m_collider_58; }
	inline bool* get_address_of_m_collider_58() { return &___m_collider_58; }
	inline void set_m_collider_58(bool value)
	{
		___m_collider_58 = value;
	}

	inline static int32_t get_offset_of_m_trigger_59() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_trigger_59)); }
	inline bool get_m_trigger_59() const { return ___m_trigger_59; }
	inline bool* get_address_of_m_trigger_59() { return &___m_trigger_59; }
	inline void set_m_trigger_59(bool value)
	{
		___m_trigger_59 = value;
	}

	inline static int32_t get_offset_of_m_physicsMaterial_60() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_physicsMaterial_60)); }
	inline PhysicsMaterial2D_tBB400C4D07B2AA7F4D3B7060E588C6D46B63EBBD * get_m_physicsMaterial_60() const { return ___m_physicsMaterial_60; }
	inline PhysicsMaterial2D_tBB400C4D07B2AA7F4D3B7060E588C6D46B63EBBD ** get_address_of_m_physicsMaterial_60() { return &___m_physicsMaterial_60; }
	inline void set_m_physicsMaterial_60(PhysicsMaterial2D_tBB400C4D07B2AA7F4D3B7060E588C6D46B63EBBD * value)
	{
		___m_physicsMaterial_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_physicsMaterial_60), (void*)value);
	}

	inline static int32_t get_offset_of_m_alignOddWidthToPixels_61() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775, ___m_alignOddWidthToPixels_61)); }
	inline bool get_m_alignOddWidthToPixels_61() const { return ___m_alignOddWidthToPixels_61; }
	inline bool* get_address_of_m_alignOddWidthToPixels_61() { return &___m_alignOddWidthToPixels_61; }
	inline void set_m_alignOddWidthToPixels_61(bool value)
	{
		___m_alignOddWidthToPixels_61 = value;
	}
};

struct VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields
{
public:
	// UnityEngine.Vector3 Vectrosity.VectorLine::v3zero
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v3zero_62;
	// UnityEngine.Canvas Vectrosity.VectorLine::m_canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_canvas_63;
	// UnityEngine.Transform Vectrosity.VectorLine::camTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___camTransform_64;
	// UnityEngine.Camera Vectrosity.VectorLine::cam3D
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cam3D_65;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oldPosition_66;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldRotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oldRotation_67;
	// System.Boolean Vectrosity.VectorLine::lineManagerCreated
	bool ___lineManagerCreated_68;
	// Vectrosity.LineManager Vectrosity.VectorLine::m_lineManager
	LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * ___m_lineManager_69;
	// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo> Vectrosity.VectorLine::capDictionary
	Dictionary_2_tEE2DF6F30CB4C856D739A00EEBB4B139451D60EB * ___capDictionary_70;
	// System.Int32 Vectrosity.VectorLine::endianDiff1
	int32_t ___endianDiff1_71;
	// System.Int32 Vectrosity.VectorLine::endianDiff2
	int32_t ___endianDiff2_72;
	// System.Byte[] Vectrosity.VectorLine::byteBlock
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___byteBlock_73;
	// System.String[] Vectrosity.VectorLine::functionNames
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___functionNames_74;

public:
	inline static int32_t get_offset_of_v3zero_62() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___v3zero_62)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_v3zero_62() const { return ___v3zero_62; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_v3zero_62() { return &___v3zero_62; }
	inline void set_v3zero_62(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___v3zero_62 = value;
	}

	inline static int32_t get_offset_of_m_canvas_63() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___m_canvas_63)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_canvas_63() const { return ___m_canvas_63; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_canvas_63() { return &___m_canvas_63; }
	inline void set_m_canvas_63(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_canvas_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_canvas_63), (void*)value);
	}

	inline static int32_t get_offset_of_camTransform_64() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___camTransform_64)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_camTransform_64() const { return ___camTransform_64; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_camTransform_64() { return &___camTransform_64; }
	inline void set_camTransform_64(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___camTransform_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___camTransform_64), (void*)value);
	}

	inline static int32_t get_offset_of_cam3D_65() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___cam3D_65)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_cam3D_65() const { return ___cam3D_65; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_cam3D_65() { return &___cam3D_65; }
	inline void set_cam3D_65(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___cam3D_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cam3D_65), (void*)value);
	}

	inline static int32_t get_offset_of_oldPosition_66() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___oldPosition_66)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oldPosition_66() const { return ___oldPosition_66; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oldPosition_66() { return &___oldPosition_66; }
	inline void set_oldPosition_66(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oldPosition_66 = value;
	}

	inline static int32_t get_offset_of_oldRotation_67() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___oldRotation_67)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oldRotation_67() const { return ___oldRotation_67; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oldRotation_67() { return &___oldRotation_67; }
	inline void set_oldRotation_67(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oldRotation_67 = value;
	}

	inline static int32_t get_offset_of_lineManagerCreated_68() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___lineManagerCreated_68)); }
	inline bool get_lineManagerCreated_68() const { return ___lineManagerCreated_68; }
	inline bool* get_address_of_lineManagerCreated_68() { return &___lineManagerCreated_68; }
	inline void set_lineManagerCreated_68(bool value)
	{
		___lineManagerCreated_68 = value;
	}

	inline static int32_t get_offset_of_m_lineManager_69() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___m_lineManager_69)); }
	inline LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * get_m_lineManager_69() const { return ___m_lineManager_69; }
	inline LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED ** get_address_of_m_lineManager_69() { return &___m_lineManager_69; }
	inline void set_m_lineManager_69(LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * value)
	{
		___m_lineManager_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_lineManager_69), (void*)value);
	}

	inline static int32_t get_offset_of_capDictionary_70() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___capDictionary_70)); }
	inline Dictionary_2_tEE2DF6F30CB4C856D739A00EEBB4B139451D60EB * get_capDictionary_70() const { return ___capDictionary_70; }
	inline Dictionary_2_tEE2DF6F30CB4C856D739A00EEBB4B139451D60EB ** get_address_of_capDictionary_70() { return &___capDictionary_70; }
	inline void set_capDictionary_70(Dictionary_2_tEE2DF6F30CB4C856D739A00EEBB4B139451D60EB * value)
	{
		___capDictionary_70 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___capDictionary_70), (void*)value);
	}

	inline static int32_t get_offset_of_endianDiff1_71() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___endianDiff1_71)); }
	inline int32_t get_endianDiff1_71() const { return ___endianDiff1_71; }
	inline int32_t* get_address_of_endianDiff1_71() { return &___endianDiff1_71; }
	inline void set_endianDiff1_71(int32_t value)
	{
		___endianDiff1_71 = value;
	}

	inline static int32_t get_offset_of_endianDiff2_72() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___endianDiff2_72)); }
	inline int32_t get_endianDiff2_72() const { return ___endianDiff2_72; }
	inline int32_t* get_address_of_endianDiff2_72() { return &___endianDiff2_72; }
	inline void set_endianDiff2_72(int32_t value)
	{
		___endianDiff2_72 = value;
	}

	inline static int32_t get_offset_of_byteBlock_73() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___byteBlock_73)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_byteBlock_73() const { return ___byteBlock_73; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_byteBlock_73() { return &___byteBlock_73; }
	inline void set_byteBlock_73(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___byteBlock_73 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___byteBlock_73), (void*)value);
	}

	inline static int32_t get_offset_of_functionNames_74() { return static_cast<int32_t>(offsetof(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_StaticFields, ___functionNames_74)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_functionNames_74() const { return ___functionNames_74; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_functionNames_74() { return &___functionNames_74; }
	inline void set_functionNames_74(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___functionNames_74 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___functionNames_74), (void*)value);
	}
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.CanvasRenderer
struct  CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisMaskU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E, ___U3CisMaskU3Ek__BackingField_4)); }
	inline bool get_U3CisMaskU3Ek__BackingField_4() const { return ___U3CisMaskU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisMaskU3Ek__BackingField_4() { return &___U3CisMaskU3Ek__BackingField_4; }
	inline void set_U3CisMaskU3Ek__BackingField_4(bool value)
	{
		___U3CisMaskU3Ek__BackingField_4 = value;
	}
};


// UnityEngine.MeshFilter
struct  MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MeshRenderer
struct  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Vectrosity.BrightnessControl
struct  BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Vectrosity.RefInt Vectrosity.BrightnessControl::m_objectNumber
	RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.BrightnessControl::m_vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___m_vectorLine_5;
	// System.Boolean Vectrosity.BrightnessControl::m_useLine
	bool ___m_useLine_6;
	// System.Boolean Vectrosity.BrightnessControl::m_destroyed
	bool ___m_destroyed_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984, ___m_objectNumber_4)); }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_objectNumber_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984, ___m_vectorLine_5)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vectorLine_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_useLine_6() { return static_cast<int32_t>(offsetof(BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984, ___m_useLine_6)); }
	inline bool get_m_useLine_6() const { return ___m_useLine_6; }
	inline bool* get_address_of_m_useLine_6() { return &___m_useLine_6; }
	inline void set_m_useLine_6(bool value)
	{
		___m_useLine_6 = value;
	}

	inline static int32_t get_offset_of_m_destroyed_7() { return static_cast<int32_t>(offsetof(BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984, ___m_destroyed_7)); }
	inline bool get_m_destroyed_7() const { return ___m_destroyed_7; }
	inline bool* get_address_of_m_destroyed_7() { return &___m_destroyed_7; }
	inline void set_m_destroyed_7(bool value)
	{
		___m_destroyed_7 = value;
	}
};


// Vectrosity.LineManager
struct  LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Vectrosity.LineManager::destroyed
	bool ___destroyed_7;

public:
	inline static int32_t get_offset_of_destroyed_7() { return static_cast<int32_t>(offsetof(LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED, ___destroyed_7)); }
	inline bool get_destroyed_7() const { return ___destroyed_7; }
	inline bool* get_address_of_destroyed_7() { return &___destroyed_7; }
	inline void set_destroyed_7(bool value)
	{
		___destroyed_7 = value;
	}
};

struct LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED_StaticFields
{
public:
	// System.Collections.Generic.List`1<Vectrosity.VectorLine> Vectrosity.LineManager::lines
	List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * ___lines_4;
	// System.Collections.Generic.List`1<UnityEngine.Transform> Vectrosity.LineManager::transforms
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___transforms_5;
	// System.Int32 Vectrosity.LineManager::lineCount
	int32_t ___lineCount_6;

public:
	inline static int32_t get_offset_of_lines_4() { return static_cast<int32_t>(offsetof(LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED_StaticFields, ___lines_4)); }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * get_lines_4() const { return ___lines_4; }
	inline List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB ** get_address_of_lines_4() { return &___lines_4; }
	inline void set_lines_4(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * value)
	{
		___lines_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lines_4), (void*)value);
	}

	inline static int32_t get_offset_of_transforms_5() { return static_cast<int32_t>(offsetof(LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED_StaticFields, ___transforms_5)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_transforms_5() const { return ___transforms_5; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_transforms_5() { return &___transforms_5; }
	inline void set_transforms_5(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___transforms_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___transforms_5), (void*)value);
	}

	inline static int32_t get_offset_of_lineCount_6() { return static_cast<int32_t>(offsetof(LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED_StaticFields, ___lineCount_6)); }
	inline int32_t get_lineCount_6() const { return ___lineCount_6; }
	inline int32_t* get_address_of_lineCount_6() { return &___lineCount_6; }
	inline void set_lineCount_6(int32_t value)
	{
		___lineCount_6 = value;
	}
};


// Vectrosity.VectorObject3D
struct  VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Vectrosity.VectorObject3D::m_updateVerts
	bool ___m_updateVerts_4;
	// System.Boolean Vectrosity.VectorObject3D::m_updateUVs
	bool ___m_updateUVs_5;
	// System.Boolean Vectrosity.VectorObject3D::m_updateColors
	bool ___m_updateColors_6;
	// System.Boolean Vectrosity.VectorObject3D::m_updateNormals
	bool ___m_updateNormals_7;
	// System.Boolean Vectrosity.VectorObject3D::m_updateTangents
	bool ___m_updateTangents_8;
	// System.Boolean Vectrosity.VectorObject3D::m_updateTris
	bool ___m_updateTris_9;
	// UnityEngine.Mesh Vectrosity.VectorObject3D::m_mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_mesh_10;
	// Vectrosity.VectorLine Vectrosity.VectorObject3D::m_vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___m_vectorLine_11;
	// UnityEngine.Material Vectrosity.VectorObject3D::m_material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_material_12;
	// System.Boolean Vectrosity.VectorObject3D::m_useCustomMaterial
	bool ___m_useCustomMaterial_13;

public:
	inline static int32_t get_offset_of_m_updateVerts_4() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_updateVerts_4)); }
	inline bool get_m_updateVerts_4() const { return ___m_updateVerts_4; }
	inline bool* get_address_of_m_updateVerts_4() { return &___m_updateVerts_4; }
	inline void set_m_updateVerts_4(bool value)
	{
		___m_updateVerts_4 = value;
	}

	inline static int32_t get_offset_of_m_updateUVs_5() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_updateUVs_5)); }
	inline bool get_m_updateUVs_5() const { return ___m_updateUVs_5; }
	inline bool* get_address_of_m_updateUVs_5() { return &___m_updateUVs_5; }
	inline void set_m_updateUVs_5(bool value)
	{
		___m_updateUVs_5 = value;
	}

	inline static int32_t get_offset_of_m_updateColors_6() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_updateColors_6)); }
	inline bool get_m_updateColors_6() const { return ___m_updateColors_6; }
	inline bool* get_address_of_m_updateColors_6() { return &___m_updateColors_6; }
	inline void set_m_updateColors_6(bool value)
	{
		___m_updateColors_6 = value;
	}

	inline static int32_t get_offset_of_m_updateNormals_7() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_updateNormals_7)); }
	inline bool get_m_updateNormals_7() const { return ___m_updateNormals_7; }
	inline bool* get_address_of_m_updateNormals_7() { return &___m_updateNormals_7; }
	inline void set_m_updateNormals_7(bool value)
	{
		___m_updateNormals_7 = value;
	}

	inline static int32_t get_offset_of_m_updateTangents_8() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_updateTangents_8)); }
	inline bool get_m_updateTangents_8() const { return ___m_updateTangents_8; }
	inline bool* get_address_of_m_updateTangents_8() { return &___m_updateTangents_8; }
	inline void set_m_updateTangents_8(bool value)
	{
		___m_updateTangents_8 = value;
	}

	inline static int32_t get_offset_of_m_updateTris_9() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_updateTris_9)); }
	inline bool get_m_updateTris_9() const { return ___m_updateTris_9; }
	inline bool* get_address_of_m_updateTris_9() { return &___m_updateTris_9; }
	inline void set_m_updateTris_9(bool value)
	{
		___m_updateTris_9 = value;
	}

	inline static int32_t get_offset_of_m_mesh_10() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_mesh_10)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_mesh_10() const { return ___m_mesh_10; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_mesh_10() { return &___m_mesh_10; }
	inline void set_m_mesh_10(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_mesh_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_mesh_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_vectorLine_11() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_vectorLine_11)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_m_vectorLine_11() const { return ___m_vectorLine_11; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_m_vectorLine_11() { return &___m_vectorLine_11; }
	inline void set_m_vectorLine_11(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___m_vectorLine_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vectorLine_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_material_12() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_material_12)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_material_12() const { return ___m_material_12; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_material_12() { return &___m_material_12; }
	inline void set_m_material_12(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_material_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_material_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_useCustomMaterial_13() { return static_cast<int32_t>(offsetof(VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC, ___m_useCustomMaterial_13)); }
	inline bool get_m_useCustomMaterial_13() const { return ___m_useCustomMaterial_13; }
	inline bool* get_address_of_m_useCustomMaterial_13() { return &___m_useCustomMaterial_13; }
	inline void set_m_useCustomMaterial_13(bool value)
	{
		___m_useCustomMaterial_13 = value;
	}
};


// Vectrosity.VisibilityControl
struct  VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControl::m_objectNumber
	RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControl::m_vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControl::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControl::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10, ___m_objectNumber_4)); }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_objectNumber_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10, ___m_vectorLine_5)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vectorLine_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}
};


// Vectrosity.VisibilityControlAlways
struct  VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControlAlways::m_objectNumber
	RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControlAlways::m_vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControlAlways::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControlAlways::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F, ___m_objectNumber_4)); }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_objectNumber_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F, ___m_vectorLine_5)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vectorLine_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}
};


// Vectrosity.VisibilityControlStatic
struct  VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Vectrosity.RefInt Vectrosity.VisibilityControlStatic::m_objectNumber
	RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * ___m_objectNumber_4;
	// Vectrosity.VectorLine Vectrosity.VisibilityControlStatic::m_vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___m_vectorLine_5;
	// System.Boolean Vectrosity.VisibilityControlStatic::m_destroyed
	bool ___m_destroyed_6;
	// System.Boolean Vectrosity.VisibilityControlStatic::m_dontDestroyLine
	bool ___m_dontDestroyLine_7;
	// UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::m_originalMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___m_originalMatrix_8;

public:
	inline static int32_t get_offset_of_m_objectNumber_4() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D, ___m_objectNumber_4)); }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * get_m_objectNumber_4() const { return ___m_objectNumber_4; }
	inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** get_address_of_m_objectNumber_4() { return &___m_objectNumber_4; }
	inline void set_m_objectNumber_4(RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * value)
	{
		___m_objectNumber_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_objectNumber_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_vectorLine_5() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D, ___m_vectorLine_5)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_m_vectorLine_5() const { return ___m_vectorLine_5; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_m_vectorLine_5() { return &___m_vectorLine_5; }
	inline void set_m_vectorLine_5(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___m_vectorLine_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vectorLine_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_destroyed_6() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D, ___m_destroyed_6)); }
	inline bool get_m_destroyed_6() const { return ___m_destroyed_6; }
	inline bool* get_address_of_m_destroyed_6() { return &___m_destroyed_6; }
	inline void set_m_destroyed_6(bool value)
	{
		___m_destroyed_6 = value;
	}

	inline static int32_t get_offset_of_m_dontDestroyLine_7() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D, ___m_dontDestroyLine_7)); }
	inline bool get_m_dontDestroyLine_7() const { return ___m_dontDestroyLine_7; }
	inline bool* get_address_of_m_dontDestroyLine_7() { return &___m_dontDestroyLine_7; }
	inline void set_m_dontDestroyLine_7(bool value)
	{
		___m_dontDestroyLine_7 = value;
	}

	inline static int32_t get_offset_of_m_originalMatrix_8() { return static_cast<int32_t>(offsetof(VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D, ___m_originalMatrix_8)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_m_originalMatrix_8() const { return ___m_originalMatrix_8; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_m_originalMatrix_8() { return &___m_originalMatrix_8; }
	inline void set_m_originalMatrix_8(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___m_originalMatrix_8 = value;
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.RawImage
struct  RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_Texture_36;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___m_UVRect_37;

public:
	inline static int32_t get_offset_of_m_Texture_36() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_Texture_36)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_Texture_36() const { return ___m_Texture_36; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_Texture_36() { return &___m_Texture_36; }
	inline void set_m_Texture_36(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_Texture_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Texture_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_UVRect_37() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_UVRect_37)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_m_UVRect_37() const { return ___m_UVRect_37; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_m_UVRect_37() { return &___m_UVRect_37; }
	inline void set_m_UVRect_37(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___m_UVRect_37 = value;
	}
};


// Vectrosity.VectorObject2D
struct  VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5  : public RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A
{
public:
	// System.Boolean Vectrosity.VectorObject2D::m_updateVerts
	bool ___m_updateVerts_38;
	// System.Boolean Vectrosity.VectorObject2D::m_updateUVs
	bool ___m_updateUVs_39;
	// System.Boolean Vectrosity.VectorObject2D::m_updateColors
	bool ___m_updateColors_40;
	// System.Boolean Vectrosity.VectorObject2D::m_updateNormals
	bool ___m_updateNormals_41;
	// System.Boolean Vectrosity.VectorObject2D::m_updateTangents
	bool ___m_updateTangents_42;
	// System.Boolean Vectrosity.VectorObject2D::m_updateTris
	bool ___m_updateTris_43;
	// UnityEngine.Mesh Vectrosity.VectorObject2D::m_mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_mesh_44;
	// Vectrosity.VectorLine Vectrosity.VectorObject2D::vectorLine
	VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___vectorLine_45;

public:
	inline static int32_t get_offset_of_m_updateVerts_38() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___m_updateVerts_38)); }
	inline bool get_m_updateVerts_38() const { return ___m_updateVerts_38; }
	inline bool* get_address_of_m_updateVerts_38() { return &___m_updateVerts_38; }
	inline void set_m_updateVerts_38(bool value)
	{
		___m_updateVerts_38 = value;
	}

	inline static int32_t get_offset_of_m_updateUVs_39() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___m_updateUVs_39)); }
	inline bool get_m_updateUVs_39() const { return ___m_updateUVs_39; }
	inline bool* get_address_of_m_updateUVs_39() { return &___m_updateUVs_39; }
	inline void set_m_updateUVs_39(bool value)
	{
		___m_updateUVs_39 = value;
	}

	inline static int32_t get_offset_of_m_updateColors_40() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___m_updateColors_40)); }
	inline bool get_m_updateColors_40() const { return ___m_updateColors_40; }
	inline bool* get_address_of_m_updateColors_40() { return &___m_updateColors_40; }
	inline void set_m_updateColors_40(bool value)
	{
		___m_updateColors_40 = value;
	}

	inline static int32_t get_offset_of_m_updateNormals_41() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___m_updateNormals_41)); }
	inline bool get_m_updateNormals_41() const { return ___m_updateNormals_41; }
	inline bool* get_address_of_m_updateNormals_41() { return &___m_updateNormals_41; }
	inline void set_m_updateNormals_41(bool value)
	{
		___m_updateNormals_41 = value;
	}

	inline static int32_t get_offset_of_m_updateTangents_42() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___m_updateTangents_42)); }
	inline bool get_m_updateTangents_42() const { return ___m_updateTangents_42; }
	inline bool* get_address_of_m_updateTangents_42() { return &___m_updateTangents_42; }
	inline void set_m_updateTangents_42(bool value)
	{
		___m_updateTangents_42 = value;
	}

	inline static int32_t get_offset_of_m_updateTris_43() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___m_updateTris_43)); }
	inline bool get_m_updateTris_43() const { return ___m_updateTris_43; }
	inline bool* get_address_of_m_updateTris_43() { return &___m_updateTris_43; }
	inline void set_m_updateTris_43(bool value)
	{
		___m_updateTris_43 = value;
	}

	inline static int32_t get_offset_of_m_mesh_44() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___m_mesh_44)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_mesh_44() const { return ___m_mesh_44; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_mesh_44() { return &___m_mesh_44; }
	inline void set_m_mesh_44(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_mesh_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_mesh_44), (void*)value);
	}

	inline static int32_t get_offset_of_vectorLine_45() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5, ___vectorLine_45)); }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * get_vectorLine_45() const { return ___vectorLine_45; }
	inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** get_address_of_vectorLine_45() { return &___vectorLine_45; }
	inline void set_vectorLine_45(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * value)
	{
		___vectorLine_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vectorLine_45), (void*)value);
	}
};

struct VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_StaticFields
{
public:
	// UnityEngine.UI.VertexHelper Vectrosity.VectorObject2D::vertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vertexHelper_46;

public:
	inline static int32_t get_offset_of_vertexHelper_46() { return static_cast<int32_t>(offsetof(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_StaticFields, ___vertexHelper_46)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_vertexHelper_46() const { return ___vertexHelper_46; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_vertexHelper_46() { return &___vertexHelper_46; }
	inline void set_vertexHelper_46(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___vertexHelper_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vertexHelper_46), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  m_Items[1];

public:
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  m_Items[1];

public:
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  m_Items[1];

public:
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  m_Items[1];

public:
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_gshared (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_gshared (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m7E6935F5AAD512A36BA46D28BA43762A22FEBB0A_gshared (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_gshared_inline (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mB47F013C62B967199614B3D07228B381C0562448_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);

// System.Boolean Vectrosity.VectorLine::get_camTransformExists()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VectorLine_get_camTransformExists_m8E195A7DF3DFEAAE75193FC18D77E46851056FBB (const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::SetCamera3D()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_SetCamera3D_mA766EB77086D6E7DED2227289AAA4F013040212B (const RuntimeMethod* method);
// UnityEngine.Vector3 Vectrosity.VectorLine::get_camTransformPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  VectorLine_get_camTransformPosition_mC2DB90AF4A6F753C4D0C669DC528122FCF27527C (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B (float ___a0, float ___b1, float ___value2, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_ObjectSetup_mAF03886A407E27753714B2600BD7F7E5B475043C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___go0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, int32_t ___visibility2, int32_t ___brightness3, bool ___makeBounds4, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlStatic::DontDestroyLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic_DontDestroyLine_mFA2676DFA190FA78B3397881AB85AA267F0607EF (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::ResetLinePoints(Vectrosity.VisibilityControlStatic,Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * ___vcs0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlAlways::DontDestroyLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlAlways_DontDestroyLine_m4E784734F767E6BEC1984E3E36D3E66D5E872535 (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * __this, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, Type_t * ___componentType0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl::Setup(Vectrosity.VectorLine,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControl_Setup_mFC214FC710C98CF73C6CA631AC2090811D2BF8B1 (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, bool ___makeBounds1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void Vectrosity.BrightnessControl::SetUseLine(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BrightnessControl_SetUseLine_m5CCF7F07EF01A1E5799AD2441F441460E2E7608E_inline (BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * __this, bool ___useLine0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl::DontDestroyLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControl_DontDestroyLine_mB14481A1B63B17D7E5843773FB881D82B6C32EE5 (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlStatic::Setup(Vectrosity.VectorLine,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic_Setup_mB81BF8A1F5A9DA09AD76430F4F26327F2AAEB928 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, bool ___makeBounds1, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlAlways::Setup(Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlAlways_Setup_m342E892B79D5E722B39337FE70B2E0C45A83712E (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, const RuntimeMethod* method);
// System.Void Vectrosity.BrightnessControl::Setup(Vectrosity.VectorLine,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BrightnessControl_Setup_m1A7604BB2DE92A75632BB4AA67C34E71B0501EC7 (BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, bool ___m_useLine1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::GetMatrix()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  VisibilityControlStatic_GetMatrix_m72C2A4EB4D860A0B6581A8633B671FD5F73419DD_inline (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_get_inverse_mFA34ECC790B269522F60FC32370D628DAFCAE225 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::get_points3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, const RuntimeMethod*))List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Matrix4x4_MultiplyPoint3x4_mA0A34C5FD162DA8E5421596F1F921436F3E7B2FC (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_gshared)(__this, ___index0, ___value1, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.VectorLine>::.ctor()
inline void List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4 (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.RefInt>::.ctor()
inline void List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046 (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void Vectrosity.VectorLine::set_drawTransform(UnityEngine.Transform)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VectorLine_set_drawTransform_m64A6D4FFBC0B49EFC72504521A235DB828F62309_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Vectrosity.VectorLine>::Add(!0)
inline void List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6 (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void Vectrosity.RefInt::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RefInt__ctor_mA3DA5388C30EE2966AA4E86148D0E711DFFC9794 (RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Vectrosity.RefInt>::Add(!0)
inline void List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935 (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * __this, RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 *, RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void Vectrosity.VectorLine::LineManagerEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_LineManagerEnable_m6DC10E4E9DDCE5FD5267CB13BFB258D1AAF6F4D7 (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Vectrosity.VectorLine>::get_Count()
inline int32_t List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_inline (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Vectrosity.RefInt>::get_Item(System.Int32)
inline RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_inline (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * (*) (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.VectorLine>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691 (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<Vectrosity.RefInt>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75 (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared)(__this, ___index0, method);
}
// System.Void Vectrosity.VectorLine::LineManagerDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_LineManagerDisable_mDFC81EA5E94893C402586C2CD5CAE16B6CF4041A (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
inline void List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
inline void List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1 (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *, const RuntimeMethod*))List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_gshared)(__this, method);
}
// System.Void Vectrosity.VectorLine::LineManagerCheckDistance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_LineManagerCheckDistance_mC373D13D5898B27529FD0A764B01529E2E898E11 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0)
inline void List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982 (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
inline void List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 , const RuntimeMethod*))List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m776A2FA26D0AA2185665AC714486953B3DBA5D9E (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221 (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_gshared)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m7E6935F5AAD512A36BA46D28BA43762A22FEBB0A (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m7E6935F5AAD512A36BA46D28BA43762A22FEBB0A_gshared)(__this, ___index0, method);
}
// System.Void Vectrosity.VectorManager::SetDistanceColor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_SetDistanceColor_m12DACE05CD5D135C7137F3A2A9BFE1E2FB29E402 (int32_t ___i0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9 (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, int32_t, const RuntimeMethod*))List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9_gshared)(__this, ___index0, ___value1, method);
}
// !0 System.Collections.Generic.List`1<Vectrosity.VectorLine>::get_Item(System.Int32)
inline VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * (*) (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Boolean Vectrosity.VectorLine::get_active()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool VectorLine_get_active_m3B3628759C5C506D16EEFB12D8BB6539B95DB2C5_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Single Vectrosity.VectorManager::GetBrightnessValue(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float VectorManager_GetBrightnessValue_mC949BB3C7A6CC28D5D6598269C0588A816C5E387 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline)(__this, ___index0, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_inline (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  (*) (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *, int32_t, const RuntimeMethod*))List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___a0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  Color32_op_Implicit_mD17E8145D2D32EF369EFE349C4D32E839F7D7AA4 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___c0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_SetColor_m7A4D856B8A7699488479E0E24757CFF2D9947E98 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::Draw3D()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_Draw3D_m934EFE9DA1927007CE80C86AD4A02D2358D67BD6 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::Draw()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_Draw_mA2B2502DC0BFD355E1FF3D252DC513B386D9D6C0 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  VectorManager_GetBounds_m9CCD16DC671F020405D42696786AD9DE2EE295DA (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___points30, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds_set_min_mE513EAA10EEF244BD01BFF55EA3B36BD116EF3F4 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds_set_max_m488E9C4F112B524DAB49FDE6284B22AFC6F956C9 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * GameObject_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mD924C79C3DA2B286ACB091AAC225DA91675E62A4 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::.ctor()
inline void Dictionary_2__ctor_m89E829D13C760C66899A0034D2D31EBC11BC801F (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.String Vectrosity.VectorLine::get_name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_mC6D73DFD73A2A194D712ED95C27413F05C8BD1CA (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared)(__this, ___key0, method);
}
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  VectorManager_GetBounds_m23D1D082EF71F5B252E32D9E3DCB68E847595026 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, const RuntimeMethod* method);
// UnityEngine.Mesh Vectrosity.VectorManager::MakeBoundsMesh(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * VectorManager_MakeBoundsMesh_m72D478FD61D161D6DCB279FB54F759EA59CD93E6 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___bounds0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::Add(!0,!1)
inline void Dictionary_2_Add_m3090C39E5790BE4D40623B28FF117F9157181983 (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * __this, String_t* ___key0, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE *, String_t*, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *, const RuntimeMethod*))Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared)(__this, ___key0, ___value1, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Mesh>::get_Item(!0)
inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * Dictionary_2_get_Item_m9AA27A646CA2E339C23E3B02A90B179D38A58F01 (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * (*) (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared)(__this, ___key0, method);
}
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshFilter_set_mesh_m13177C1A6C29D76DDCD858CEF2B28C2AA7CC46FC (MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.RawImage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawImage__ctor_mF745F95A5854034191E5787E9A22F2267943FDFE (RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetTexture_m356FBE3311E759BBD46F75FC3441755089296BC4 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___tex0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetMaterial_mA7C634438A73116C9CA8F4B95A1B0E535AE61E8F (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99 (RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetupMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetupMesh_mAFA235E8824EB01E1F7F6D62806B46291A1DCB0C (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method);
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * Graphic_get_rectTransform_m87D5A808474C6B71649CBB153DEBF5F268189EFF (Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::get_canvasRenderer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * Graphic_get_canvasRenderer_m33EC3A53310593E87C540654486C7A73A66FCF4A (Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CanvasRenderer_SetMesh_mB506682F318E5D8D8FE3888BF50E40DC34B726DF (CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject2D::SetMeshBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE (const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds__ctor_m8356472A177F4B22FFCE8911EBC8547A65A07CA3 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___size1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_bounds(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_bounds_m9752E145EA6D719B417AA27555DDC2A388AB4E0A (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3[] Vectrosity.VectorLine::get_lineVertices()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* VectorLine_get_lineVertices_mC4D7969376459F3E60138EDDCA9D581F0702D70C_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2[] Vectrosity.VectorLine::get_lineUVs()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mesh::get_vertexCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv_mF6FED6DDACBAE3EAF28BFBF257A0D5356FCF3AAC (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___value0, const RuntimeMethod* method);
// UnityEngine.Color32[] Vectrosity.VectorLine::get_lineColors()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_colors32_m0EA838C15BBD01710A4EE1A475DB585EB9B5BD91 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* ___value0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.Int32> Vectrosity.VectorLine::get_lineTriangles()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * VectorLine_get_lineTriangles_m6FE31B265CC9E175DBC5BD5D91256EAA7ADE78C2_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_SetTriangles_mF74536E3A39AECF33809A7D23AFF54A1CFC37129 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___triangles0, int32_t ___submesh1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::RecalculateNormals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_RecalculateNormals_mEBF9ED932D0B463E4EF3947D232CC8BEECAE1A4A (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Vector4[] Vectrosity.VectorLine::CalculateTangents(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* VectorLine_CalculateTangents_mCC45C3345D6020C7267838B8616C4E938B031548 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___normals0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_tangents(UnityEngine.Vector4[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_tangents_mFA4E0A26B52C1FCF80FA5DA642B28716249ACF67 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_Clear_m7500ECE6209E14CC750CB16B48301B8D2A57ACCE (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___value0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorObject3D::SetupMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetupMesh_mEDD447CA9C0FBC3D6408F7D397FAC4B63030E411 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method);
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// UnityEngine.Texture Vectrosity.VectorLine::get_texture()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * VectorLine_get_texture_m80B0BB07297D6E6A53789EFF36AFA678FACCA27E_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Vectrosity.VectorObject3D::SetVerts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetVerts_m1A386FEC2696DA4E4D4C6795637212987AA49BB1 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::RecalculateBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_RecalculateBounds_mC39556595CFE3E4D8EFA777476ECD22B97FC2737 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::SetupBoundsMesh(UnityEngine.GameObject,Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___go0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilitySetup(UnityEngine.Transform,Vectrosity.VectorLine,Vectrosity.RefInt&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___thisTransform0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** ___objectNum2, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::DrawArrayLine2(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732 (int32_t ___i0, const RuntimeMethod* method);
// System.Collections.IEnumerator Vectrosity.VisibilityControl::VisibilityTest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_VisibilityTest_m1376D2161DA0A5636CE0D6CFE36F296C417546ED (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl/<VisibilityTest>c__Iterator1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1__ctor_mA3B4BCE3D8F8C728D75A1B29A3E9A59F66399DE8 (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl/<OnBecameVisible>c__Iterator2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2__ctor_m7D8891F9A9C506D7FB97B598F0376D8DE1A3198F (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControl/<OnBecameInvisible>c__Iterator3::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3__ctor_m05443508C5AB96FE91ABDC1B593275A6BED217A0 (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilityRemove(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B (int32_t ___objectNumber0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorLine&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_Destroy_mC44B22F624821712A379ED38A54E638772BB1345 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** ___line0, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_mB47F013C62B967199614B3D07228B381C0562448 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_mB47F013C62B967199614B3D07228B381C0562448_gshared)(__this, ___collection0, method);
}
// System.Void Vectrosity.VectorLine::set_points3(System.Collections.Generic.List`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_set_points3_m2FE6D35A38BD92CFEC2A761D353D5EB904147636 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___value0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilityStaticSetup(Vectrosity.VectorLine,Vectrosity.RefInt&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticSetup_mD098A79500A6B1A0E347CAB4DE0A889D28E2E13A (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** ___objectNum1, const RuntimeMethod* method);
// System.Collections.IEnumerator Vectrosity.VisibilityControlStatic::WaitCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControlStatic_WaitCheck_m48175ADD5334C3484E3464A64F8E6F728E9358C7 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VisibilityControlStatic/<WaitCheck>c__Iterator4::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4__ctor_mC373F298379008231B6CABDCB7F5323393256E4B (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * __this, const RuntimeMethod* method);
// System.Void Vectrosity.VectorLine::set_active(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::DrawArrayLine(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888 (int32_t ___i0, const RuntimeMethod* method);
// System.Void Vectrosity.VectorManager::VisibilityStaticRemove(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticRemove_m9B72F2333EE6877D0F29D93BC18F76ADA5B0D237 (int32_t ___objectNumber0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Void Vectrosity.LineManager::RemoveLine(Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LineManager_RemoveLine_m941BD07322036D8D886799D67AD748791EE97C1D (LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___vectorLine0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Renderer::get_isVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Renderer_get_isVisible_mE424F7FFEA9D78BC657B3F54FDFBE1EB8461CCB7 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VectorManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager__cctor_mDBBAD4573C0BA12C1AE71D0FA1B1DD8A5201803A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager__cctor_mDBBAD4573C0BA12C1AE71D0FA1B1DD8A5201803A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_minBrightnessDistance_0((500.0f));
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_maxBrightnessDistance_1((250.0f));
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_brightnessLevels_2(((int32_t)32));
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_distanceCheckFrequency_3((0.200000003f));
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_useDraw3D_5((bool)0);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount_8(0);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount2_11(0);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount3_17(0);
		return;
	}
}
// System.Single Vectrosity.VectorManager::GetBrightnessValue(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float VectorManager_GetBrightnessValue_mC949BB3C7A6CC28D5D6598269C0588A816C5E387 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_GetBrightnessValue_mC949BB3C7A6CC28D5D6598269C0588A816C5E387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		bool L_0 = VectorLine_get_camTransformExists_m8E195A7DF3DFEAAE75193FC18D77E46851056FBB(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_SetCamera3D_mA766EB77086D6E7DED2227289AAA4F013040212B(/*hidden argument*/NULL);
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		float L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_minBrightnessDistance_0();
		float L_2 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_maxBrightnessDistance_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___pos0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = VectorLine_get_camTransformPosition_mC2DB90AF4A6F753C4D0C669DC528122FCF27527C(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_7 = Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B(L_1, L_2, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_ObjectSetup_mF7F932263CD9105D67A4277BE6B4EB1D173F20A8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___go0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, int32_t ___visibility2, int32_t ___brightness3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_ObjectSetup_mF7F932263CD9105D67A4277BE6B4EB1D173F20A8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___go0;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_1 = ___line1;
		int32_t L_2 = ___visibility2;
		int32_t L_3 = ___brightness3;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_ObjectSetup_mAF03886A407E27753714B2600BD7F7E5B475043C(L_0, L_1, L_2, L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_ObjectSetup_mAF03886A407E27753714B2600BD7F7E5B475043C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___go0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, int32_t ___visibility2, int32_t ___brightness3, bool ___makeBounds4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_ObjectSetup_mAF03886A407E27753714B2600BD7F7E5B475043C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * V_0 = NULL;
	VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * V_1 = NULL;
	VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * V_2 = NULL;
	BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * V_3 = NULL;
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_3 = GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473(L_0, L_2, /*hidden argument*/NULL);
		V_0 = ((VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 *)IsInstClass((RuntimeObject*)L_3, VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10_il2cpp_TypeInfo_var));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_5 = { reinterpret_cast<intptr_t> (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D_0_0_0_var) };
		Type_t * L_6 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_7 = GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473(L_4, L_6, /*hidden argument*/NULL);
		V_1 = ((VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D *)IsInstClass((RuntimeObject*)L_7, VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D_il2cpp_TypeInfo_var));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_9 = { reinterpret_cast<intptr_t> (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F_0_0_0_var) };
		Type_t * L_10 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_11 = GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473(L_8, L_10, /*hidden argument*/NULL);
		V_2 = ((VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F *)IsInstClass((RuntimeObject*)L_11, VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F_il2cpp_TypeInfo_var));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984_0_0_0_var) };
		Type_t * L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_15 = GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473(L_12, L_14, /*hidden argument*/NULL);
		V_3 = ((BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 *)IsInstClass((RuntimeObject*)L_15, BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984_il2cpp_TypeInfo_var));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_17 = { reinterpret_cast<intptr_t> (MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var) };
		Type_t * L_18 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_19 = GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473(L_16, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(((MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A *)IsInstSealed((RuntimeObject*)L_19, MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_il2cpp_TypeInfo_var)), (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21 = ___go0;
		NullCheck(L_21);
		GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2(L_21, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2_RuntimeMethod_var);
	}

IL_007f:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_23 = { reinterpret_cast<intptr_t> (MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_25 = GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473(L_22, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(((MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B *)IsInstClass((RuntimeObject*)L_25, MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_il2cpp_TypeInfo_var)), (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00a6;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_27 = ___go0;
		NullCheck(L_27);
		GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47(L_27, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47_RuntimeMethod_var);
	}

IL_00a6:
	{
		int32_t L_28 = ___visibility2;
		if (L_28)
		{
			goto IL_0124;
		}
	}
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00ca;
		}
	}
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_31 = V_1;
		NullCheck(L_31);
		VisibilityControlStatic_DontDestroyLine_mFA2676DFA190FA78B3397881AB85AA267F0607EF(L_31, /*hidden argument*/NULL);
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_32, /*hidden argument*/NULL);
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_33 = V_1;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_34 = ___line1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142(L_33, L_34, /*hidden argument*/NULL);
	}

IL_00ca:
	{
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00e1;
		}
	}
	{
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_37 = V_2;
		NullCheck(L_37);
		VisibilityControlAlways_DontDestroyLine_m4E784734F767E6BEC1984E3E36D3E66D5E872535(L_37, /*hidden argument*/NULL);
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_38 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_38, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_39 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_40 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_39, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_011f;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_41 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_42 = { reinterpret_cast<intptr_t> (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_44 = GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8(L_41, L_43, /*hidden argument*/NULL);
		V_0 = ((VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 *)IsInstClass((RuntimeObject*)L_44, VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10_il2cpp_TypeInfo_var));
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_45 = V_0;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_46 = ___line1;
		bool L_47 = ___makeBounds4;
		NullCheck(L_45);
		VisibilityControl_Setup_mFC214FC710C98CF73C6CA631AC2090811D2BF8B1(L_45, L_46, L_47, /*hidden argument*/NULL);
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_48 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_48, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_011f;
		}
	}
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_50 = V_3;
		NullCheck(L_50);
		BrightnessControl_SetUseLine_m5CCF7F07EF01A1E5799AD2441F441460E2E7608E_inline(L_50, (bool)0, /*hidden argument*/NULL);
	}

IL_011f:
	{
		goto IL_0214;
	}

IL_0124:
	{
		int32_t L_51 = ___visibility2;
		if ((!(((uint32_t)L_51) == ((uint32_t)1))))
		{
			goto IL_019c;
		}
	}
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_52 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_53 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0142;
		}
	}
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_54 = V_0;
		NullCheck(L_54);
		VisibilityControl_DontDestroyLine_mB14481A1B63B17D7E5843773FB881D82B6C32EE5(L_54, /*hidden argument*/NULL);
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_55 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_55, /*hidden argument*/NULL);
	}

IL_0142:
	{
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_56 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_57 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0159;
		}
	}
	{
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_58 = V_2;
		NullCheck(L_58);
		VisibilityControlAlways_DontDestroyLine_m4E784734F767E6BEC1984E3E36D3E66D5E872535(L_58, /*hidden argument*/NULL);
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_59 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_59, /*hidden argument*/NULL);
	}

IL_0159:
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_60 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_61 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_60, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0197;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_62 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_63 = { reinterpret_cast<intptr_t> (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_64 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_65 = GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8(L_62, L_64, /*hidden argument*/NULL);
		V_1 = ((VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D *)IsInstClass((RuntimeObject*)L_65, VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D_il2cpp_TypeInfo_var));
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_66 = V_1;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_67 = ___line1;
		bool L_68 = ___makeBounds4;
		NullCheck(L_66);
		VisibilityControlStatic_Setup_mB81BF8A1F5A9DA09AD76430F4F26327F2AAEB928(L_66, L_67, L_68, /*hidden argument*/NULL);
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_69 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_70 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_69, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_0197;
		}
	}
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_71 = V_3;
		NullCheck(L_71);
		BrightnessControl_SetUseLine_m5CCF7F07EF01A1E5799AD2441F441460E2E7608E_inline(L_71, (bool)0, /*hidden argument*/NULL);
	}

IL_0197:
	{
		goto IL_0214;
	}

IL_019c:
	{
		int32_t L_72 = ___visibility2;
		if ((!(((uint32_t)L_72) == ((uint32_t)2))))
		{
			goto IL_0214;
		}
	}
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_73 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_74 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_01ba;
		}
	}
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_75 = V_0;
		NullCheck(L_75);
		VisibilityControl_DontDestroyLine_mB14481A1B63B17D7E5843773FB881D82B6C32EE5(L_75, /*hidden argument*/NULL);
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_76 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_76, /*hidden argument*/NULL);
	}

IL_01ba:
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_77 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_78 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_77, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_01d8;
		}
	}
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_79 = V_1;
		NullCheck(L_79);
		VisibilityControlStatic_DontDestroyLine_mFA2676DFA190FA78B3397881AB85AA267F0607EF(L_79, /*hidden argument*/NULL);
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_80 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_80, /*hidden argument*/NULL);
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_81 = V_1;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_82 = ___line1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142(L_81, L_82, /*hidden argument*/NULL);
	}

IL_01d8:
	{
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_83 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_83, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0214;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_85 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_86 = { reinterpret_cast<intptr_t> (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_87 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_86, /*hidden argument*/NULL);
		NullCheck(L_85);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_88 = GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8(L_85, L_87, /*hidden argument*/NULL);
		V_2 = ((VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F *)IsInstClass((RuntimeObject*)L_88, VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F_il2cpp_TypeInfo_var));
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_89 = V_2;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_90 = ___line1;
		NullCheck(L_89);
		VisibilityControlAlways_Setup_m342E892B79D5E722B39337FE70B2E0C45A83712E(L_89, L_90, /*hidden argument*/NULL);
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_91 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_92 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_91, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_0214;
		}
	}
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_93 = V_3;
		NullCheck(L_93);
		BrightnessControl_SetUseLine_m5CCF7F07EF01A1E5799AD2441F441460E2E7608E_inline(L_93, (bool)0, /*hidden argument*/NULL);
	}

IL_0214:
	{
		int32_t L_94 = ___brightness3;
		if (L_94)
		{
			goto IL_027a;
		}
	}
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_95 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_96 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_95, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_0275;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_97 = ___go0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_98 = { reinterpret_cast<intptr_t> (BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_98, /*hidden argument*/NULL);
		NullCheck(L_97);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_100 = GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8(L_97, L_99, /*hidden argument*/NULL);
		V_3 = ((BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 *)IsInstClass((RuntimeObject*)L_100, BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984_il2cpp_TypeInfo_var));
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_101 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_102 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_101, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_026d;
		}
	}
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_103 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_104 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_103, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_026d;
		}
	}
	{
		VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * L_105 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_106 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_105, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_026d;
		}
	}
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_107 = V_3;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_108 = ___line1;
		NullCheck(L_107);
		BrightnessControl_Setup_m1A7604BB2DE92A75632BB4AA67C34E71B0501EC7(L_107, L_108, (bool)1, /*hidden argument*/NULL);
		goto IL_0275;
	}

IL_026d:
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_109 = V_3;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_110 = ___line1;
		NullCheck(L_109);
		BrightnessControl_Setup_m1A7604BB2DE92A75632BB4AA67C34E71B0501EC7(L_109, L_110, (bool)0, /*hidden argument*/NULL);
	}

IL_0275:
	{
		goto IL_028b;
	}

IL_027a:
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_111 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_112 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_111, /*hidden argument*/NULL);
		if (!L_112)
		{
			goto IL_028b;
		}
	}
	{
		BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * L_113 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_113, /*hidden argument*/NULL);
	}

IL_028b:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::ResetLinePoints(Vectrosity.VisibilityControlStatic,Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * ___vcs0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_ResetLinePoints_m51C85A223B694EEE14DA69D4088CFF53B54AA142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_0 = ___vcs0;
		NullCheck(L_0);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_1 = VisibilityControlStatic_GetMatrix_m72C2A4EB4D860A0B6581A8633B671FD5F73419DD_inline(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_2 = Matrix4x4_get_inverse_mFA34ECC790B269522F60FC32370D628DAFCAE225((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		V_2 = 0;
		goto IL_0039;
	}

IL_0016:
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = ___line1;
		NullCheck(L_3);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_4 = VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_6 = ___line1;
		NullCheck(L_6);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_7 = VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = Matrix4x4_MultiplyPoint3x4_mA0A34C5FD162DA8E5421596F1F921436F3E7B2FC((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&V_0), L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406(L_4, L_5, L_10, /*hidden argument*/List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var);
		int32_t L_11 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0039:
	{
		int32_t L_12 = V_2;
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_13 = ___line1;
		NullCheck(L_13);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_14 = VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_14, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_15)))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// System.Int32 Vectrosity.VectorManager::get_arrayCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t VectorManager_get_arrayCount_m150A26CB0D835707FBEC2B14DC7381A02232A94D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_get_arrayCount_m150A26CB0D835707FBEC2B14DC7381A02232A94D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount_8();
		return L_0;
	}
}
// System.Void Vectrosity.VectorManager::VisibilityStaticSetup(Vectrosity.VectorLine,Vectrosity.RefInt&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticSetup_mD098A79500A6B1A0E347CAB4DE0A889D28E2E13A (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** ___objectNum1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilityStaticSetup_mD098A79500A6B1A0E347CAB4DE0A889D28E2E13A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *)il2cpp_codegen_object_new(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB_il2cpp_TypeInfo_var);
		List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4(L_1, /*hidden argument*/List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_vectorLines_6(L_1);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_2 = (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 *)il2cpp_codegen_object_new(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344_il2cpp_TypeInfo_var);
		List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046(L_2, /*hidden argument*/List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046_RuntimeMethod_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_objectNumbers_7(L_2);
	}

IL_001e:
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = ___line0;
		NullCheck(L_3);
		VectorLine_set_drawTransform_m64A6D4FFBC0B49EFC72504521A235DB828F62309_inline(L_3, (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_4 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_5 = ___line0;
		NullCheck(L_4);
		List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6(L_4, L_5, /*hidden argument*/List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6_RuntimeMethod_var);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** L_6 = ___objectNum1;
		int32_t L_7 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount_8();
		int32_t L_8 = L_7;
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount_8(((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1)));
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_9 = (RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE *)il2cpp_codegen_object_new(RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE_il2cpp_TypeInfo_var);
		RefInt__ctor_mA3DA5388C30EE2966AA4E86148D0E711DFFC9794(L_9, L_8, /*hidden argument*/NULL);
		*((RuntimeObject **)L_6) = (RuntimeObject *)L_9;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_6, (void*)(RuntimeObject *)L_9);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_10 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers_7();
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** L_11 = ___objectNum1;
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_12 = *((RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE **)L_11);
		NullCheck(L_10);
		List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935(L_10, L_12, /*hidden argument*/List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_LineManagerEnable_m6DC10E4E9DDCE5FD5267CB13BFB258D1AAF6F4D7(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::VisibilityStaticRemove(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilityStaticRemove_m9B72F2333EE6877D0F29D93BC18F76ADA5B0D237 (int32_t ___objectNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilityStaticRemove_m9B72F2333EE6877D0F29D93BC18F76ADA5B0D237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___objectNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_inline(L_1, /*hidden argument*/List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteralF141E5CFAFD2AC23EC22AD5EB48EEA5FCF523C2A, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		int32_t L_3 = ___objectNumber0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		goto IL_0040;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_4 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers_7();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_6 = List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_RuntimeMethod_var);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_7 = L_6;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		NullCheck(L_7);
		L_7->set_i_0(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_11 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount_8();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_12 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_13 = ___objectNumber0;
		NullCheck(L_12);
		List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691(L_12, L_13, /*hidden argument*/List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691_RuntimeMethod_var);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_14 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers_7();
		int32_t L_15 = ___objectNumber0;
		NullCheck(L_14);
		List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75(L_14, L_15, /*hidden argument*/List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75_RuntimeMethod_var);
		int32_t L_16 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount_8();
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_LineManagerDisable_mDFC81EA5E94893C402586C2CD5CAE16B6CF4041A(/*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vectrosity.VectorManager::get_arrayCount2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t VectorManager_get_arrayCount2_m17054F8887ECC80587A291E0AE545283AACE03C7 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_get_arrayCount2_m17054F8887ECC80587A291E0AE545283AACE03C7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		return L_0;
	}
}
// System.Void Vectrosity.VectorManager::VisibilitySetup(UnityEngine.Transform,Vectrosity.VectorLine,Vectrosity.RefInt&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___thisTransform0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** ___objectNum2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *)il2cpp_codegen_object_new(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB_il2cpp_TypeInfo_var);
		List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4(L_1, /*hidden argument*/List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_vectorLines2_9(L_1);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_2 = (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 *)il2cpp_codegen_object_new(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344_il2cpp_TypeInfo_var);
		List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046(L_2, /*hidden argument*/List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046_RuntimeMethod_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_objectNumbers2_10(L_2);
	}

IL_001e:
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = ___line1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = ___thisTransform0;
		NullCheck(L_3);
		VectorLine_set_drawTransform_m64A6D4FFBC0B49EFC72504521A235DB828F62309_inline(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_5 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_6 = ___line1;
		NullCheck(L_5);
		List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6(L_5, L_6, /*hidden argument*/List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6_RuntimeMethod_var);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** L_7 = ___objectNum2;
		int32_t L_8 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		int32_t L_9 = L_8;
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount2_11(((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)));
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_10 = (RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE *)il2cpp_codegen_object_new(RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE_il2cpp_TypeInfo_var);
		RefInt__ctor_mA3DA5388C30EE2966AA4E86148D0E711DFFC9794(L_10, L_9, /*hidden argument*/NULL);
		*((RuntimeObject **)L_7) = (RuntimeObject *)L_10;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_7, (void*)(RuntimeObject *)L_10);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_11 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers2_10();
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** L_12 = ___objectNum2;
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_13 = *((RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE **)L_12);
		NullCheck(L_11);
		List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935(L_11, L_13, /*hidden argument*/List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_LineManagerEnable_m6DC10E4E9DDCE5FD5267CB13BFB258D1AAF6F4D7(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::VisibilityRemove(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B (int32_t ___objectNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___objectNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_inline(L_1, /*hidden argument*/List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral30738EA834CC7433C88B12A40783EE08FA5066D7, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		int32_t L_3 = ___objectNumber0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		goto IL_0040;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_4 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers2_10();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_6 = List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_RuntimeMethod_var);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_7 = L_6;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		NullCheck(L_7);
		L_7->set_i_0(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_11 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_12 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_13 = ___objectNumber0;
		NullCheck(L_12);
		List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691(L_12, L_13, /*hidden argument*/List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691_RuntimeMethod_var);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_14 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers2_10();
		int32_t L_15 = ___objectNumber0;
		NullCheck(L_14);
		List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75(L_14, L_15, /*hidden argument*/List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75_RuntimeMethod_var);
		int32_t L_16 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount2_11(((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_LineManagerDisable_mDFC81EA5E94893C402586C2CD5CAE16B6CF4041A(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorManager::CheckDistanceSetup(UnityEngine.Transform,Vectrosity.VectorLine,UnityEngine.Color,Vectrosity.RefInt)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_CheckDistanceSetup_m5F270965601AD85B1F94AEBDB4DC391FBEBAB788 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___thisTransform0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * ___objectNum3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_CheckDistanceSetup_m5F270965601AD85B1F94AEBDB4DC391FBEBAB788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_LineManagerEnable_m6DC10E4E9DDCE5FD5267CB13BFB258D1AAF6F4D7(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		if (L_0)
		{
			goto IL_0046;
		}
	}
	{
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = (List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB *)il2cpp_codegen_object_new(List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB_il2cpp_TypeInfo_var);
		List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4(L_1, /*hidden argument*/List_1__ctor_m5E8FEB4A3FD0E59381D7DB6DBEA8E28295DFEAC4_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_vectorLines3_13(L_1);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_2 = (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *)il2cpp_codegen_object_new(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var);
		List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F(L_2, /*hidden argument*/List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_transforms3_12(L_2);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_3, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_oldDistances_14(L_3);
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_4 = (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *)il2cpp_codegen_object_new(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_il2cpp_TypeInfo_var);
		List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1(L_4, /*hidden argument*/List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_RuntimeMethod_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_colors_15(L_4);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_5 = (List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 *)il2cpp_codegen_object_new(List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344_il2cpp_TypeInfo_var);
		List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046(L_5, /*hidden argument*/List_1__ctor_m9D3677383FEB12277BF88A3851B93255A7492046_RuntimeMethod_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_objectNumbers3_16(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_LineManagerCheckDistance_mC373D13D5898B27529FD0A764B01529E2E898E11(/*hidden argument*/NULL);
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_6 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_transforms3_12();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = ___thisTransform0;
		NullCheck(L_6);
		List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982(L_6, L_7, /*hidden argument*/List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982_RuntimeMethod_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_8 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_9 = ___line1;
		NullCheck(L_8);
		List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6(L_8, L_9, /*hidden argument*/List_1_Add_mF48570D06CD2A51086125A98854874E9B6DDA1A6_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_10 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_oldDistances_14();
		NullCheck(L_10);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_10, (-1), /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_11 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_colors_15();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_12 = ___color2;
		NullCheck(L_11);
		List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C(L_11, L_12, /*hidden argument*/List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_13 = ___objectNum3;
		int32_t L_14 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		int32_t L_15 = L_14;
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount3_17(((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)));
		NullCheck(L_13);
		L_13->set_i_0(L_15);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_16 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers3_16();
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_17 = ___objectNum3;
		NullCheck(L_16);
		List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935(L_16, L_17, /*hidden argument*/List_1_Add_mD7B51B8CAF021E50156C9A4F02ADF8DC0F5BE935_RuntimeMethod_var);
		return;
	}
}
// System.Void Vectrosity.VectorManager::DistanceRemove(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_DistanceRemove_mB9D600CD1338D2EC0722800B2BB2BFEE8F6525A9 (int32_t ___objectNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DistanceRemove_mB9D600CD1338D2EC0722800B2BB2BFEE8F6525A9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___objectNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_inline(L_1, /*hidden argument*/List_1_get_Count_m96385CC6BB07BB355CEA04E7E077713B069976A1_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral4E5C9140DE7AD25CB01C92DE3153A14590195A07, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		int32_t L_3 = ___objectNumber0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		goto IL_0040;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_4 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers3_16();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_6 = List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mE956DF4F62D008FF2BD5289708000D303EBAFD51_RuntimeMethod_var);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_7 = L_6;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		NullCheck(L_7);
		L_7->set_i_0(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_11 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_12 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_transforms3_12();
		int32_t L_13 = ___objectNumber0;
		NullCheck(L_12);
		List_1_RemoveAt_m776A2FA26D0AA2185665AC714486953B3DBA5D9E(L_12, L_13, /*hidden argument*/List_1_RemoveAt_m776A2FA26D0AA2185665AC714486953B3DBA5D9E_RuntimeMethod_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_14 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		int32_t L_15 = ___objectNumber0;
		NullCheck(L_14);
		List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691(L_14, L_15, /*hidden argument*/List_1_RemoveAt_m3DC32A24D9801FE4F36ACACD5BF24CBB1807F691_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_16 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_17 = ___objectNumber0;
		NullCheck(L_16);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_16, L_17, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_18 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_colors_15();
		int32_t L_19 = ___objectNumber0;
		NullCheck(L_18);
		List_1_RemoveAt_m7E6935F5AAD512A36BA46D28BA43762A22FEBB0A(L_18, L_19, /*hidden argument*/List_1_RemoveAt_m7E6935F5AAD512A36BA46D28BA43762A22FEBB0A_RuntimeMethod_var);
		List_1_tDF5213C752B2618030E7D5BE4F0CB2E4CF376344 * L_20 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_objectNumbers3_16();
		int32_t L_21 = ___objectNumber0;
		NullCheck(L_20);
		List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75(L_20, L_21, /*hidden argument*/List_1_RemoveAt_m269EDEEA03002005F4C4B5FC962E35D56E6CBF75_RuntimeMethod_var);
		int32_t L_22 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set__arrayCount3_17(((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)1)));
		return;
	}
}
// System.Void Vectrosity.VectorManager::CheckDistance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_CheckDistance_mEE514C5E086101A2B940A07C2DC92BAAE62BCEA2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_CheckDistance_mEE514C5E086101A2B940A07C2DC92BAAE62BCEA2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0011;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_SetDistanceColor_m12DACE05CD5D135C7137F3A2A9BFE1E2FB29E402(L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1));
	}

IL_0011:
	{
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_3 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount3_17();
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::SetOldDistance(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_SetOldDistance_m3647DA7ABBBD286740E465A1A309F097FEE3B8B7 (int32_t ___objectNumber0, int32_t ___val1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_SetOldDistance_m3647DA7ABBBD286740E465A1A309F097FEE3B8B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_1 = ___objectNumber0;
		int32_t L_2 = ___val1;
		NullCheck(L_0);
		List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9(L_0, L_1, L_2, /*hidden argument*/List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9_RuntimeMethod_var);
		return;
	}
}
// System.Void Vectrosity.VectorManager::SetDistanceColor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_SetDistanceColor_m12DACE05CD5D135C7137F3A2A9BFE1E2FB29E402 (int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_SetDistanceColor_m12DACE05CD5D135C7137F3A2A9BFE1E2FB29E402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3 = VectorLine_get_active_m3B3628759C5C506D16EEFB12D8BB6539B95DB2C5_inline(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_4 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_transforms3_12();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		NullCheck(L_6);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = VectorManager_GetBrightnessValue_mC949BB3C7A6CC28D5D6598269C0588A816C5E387(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = V_0;
		int32_t L_10 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_brightnessLevels_2();
		V_1 = (((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)L_9, (float)(((float)((float)L_10))))))));
		int32_t L_11 = V_1;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_12 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_13 = ___i0;
		NullCheck(L_12);
		int32_t L_14 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_12, L_13, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		if ((((int32_t)L_11) == ((int32_t)L_14)))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_15 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines3_13();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_17 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_15, L_16, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_18 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_fogColor_4();
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_19 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_colors_15();
		int32_t L_20 = ___i0;
		NullCheck(L_19);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_21 = List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_inline(L_19, L_20, /*hidden argument*/List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_RuntimeMethod_var);
		float L_22 = V_0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_23 = Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197(L_18, L_21, L_22, /*hidden argument*/NULL);
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_24 = Color32_op_Implicit_mD17E8145D2D32EF369EFE349C4D32E839F7D7AA4(L_23, /*hidden argument*/NULL);
		NullCheck(L_17);
		VectorLine_SetColor_m7A4D856B8A7699488479E0E24757CFF2D9947E98(L_17, L_24, /*hidden argument*/NULL);
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_25 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_oldDistances_14();
		int32_t L_26 = ___i0;
		int32_t L_27 = V_1;
		NullCheck(L_25);
		List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9(L_25, L_26, L_27, /*hidden argument*/List_1_set_Item_m85EC6079AE402A592416119251D7CB59B8A0E4A9_RuntimeMethod_var);
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLine(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888 (int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_1, L_2, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m934EFE9DA1927007CE80C86AD4A02D2358D67BD6(L_3, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_4 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_6 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_6);
		VectorLine_Draw_mA2B2502DC0BFD355E1FF3D252DC513B386D9D6C0(L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLine2(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732 (int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_1, L_2, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m934EFE9DA1927007CE80C86AD4A02D2358D67BD6(L_3, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_4 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_6 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_6);
		VectorLine_Draw_mA2B2502DC0BFD355E1FF3D252DC513B386D9D6C0(L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLines()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLines_mCEEBF6298008C43FE803CB36CF9006E666019744 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLines_mCEEBF6298008C43FE803CB36CF9006E666019744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		V_0 = 0;
		goto IL_0025;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_1, L_2, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m934EFE9DA1927007CE80C86AD4A02D2358D67BD6(L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0025:
	{
		int32_t L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_6 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount_8();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0011;
		}
	}
	{
		goto IL_005b;
	}

IL_0035:
	{
		V_1 = 0;
		goto IL_0050;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_7 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines_6();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_9 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_9);
		VectorLine_Draw_mA2B2502DC0BFD355E1FF3D252DC513B386D9D6C0(L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0050:
	{
		int32_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_12 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount_8();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_003c;
		}
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vectrosity.VectorManager::DrawArrayLines2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_DrawArrayLines2_mE6A0035BF8AF9A1A4D8B9BC1E335C6697DC5C5EC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_DrawArrayLines2_mE6A0035BF8AF9A1A4D8B9BC1E335C6697DC5C5EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		bool L_0 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_useDraw3D_5();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		V_0 = 0;
		goto IL_0025;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_1 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_1, L_2, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_3);
		VectorLine_Draw3D_m934EFE9DA1927007CE80C86AD4A02D2358D67BD6(L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0025:
	{
		int32_t L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_6 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0011;
		}
	}
	{
		goto IL_005b;
	}

IL_0035:
	{
		V_1 = 0;
		goto IL_0050;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		List_1_t740BC1F2A9BC648F7F9452C3981E77CFF863B4AB * L_7 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_vectorLines2_9();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_9 = List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_mCAB7800CD8AF6C92C428A9E7AF2317075BD15D9F_RuntimeMethod_var);
		NullCheck(L_9);
		VectorLine_Draw_mA2B2502DC0BFD355E1FF3D252DC513B386D9D6C0(L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0050:
	{
		int32_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		int32_t L_12 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get__arrayCount2_11();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_003c;
		}
	}

IL_005b:
	{
		return;
	}
}
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  VectorManager_GetBounds_m23D1D082EF71F5B252E32D9E3DCB68E847595026 (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_GetBounds_m23D1D082EF71F5B252E32D9E3DCB68E847595026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_0 = ___line0;
		NullCheck(L_0);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_1 = VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral1621373437091DE73D152553E425FB66A03B5F27, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_0), sizeof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 ));
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2 = V_0;
		return L_2;
	}

IL_001f:
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_3 = ___line0;
		NullCheck(L_3);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_4 = VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_5 = VectorManager_GetBounds_m9CCD16DC671F020405D42696786AD9DE2EE295DA(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Bounds Vectrosity.VectorManager::GetBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  VectorManager_GetBounds_m9CCD16DC671F020405D42696786AD9DE2EE295DA (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___points30, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_GetBounds_m9CCD16DC671F020405D42696786AD9DE2EE295DA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_8;
	memset((&V_8), 0, sizeof(V_8));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_9;
	memset((&V_9), 0, sizeof(V_9));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_10;
	memset((&V_10), 0, sizeof(V_10));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_11;
	memset((&V_11), 0, sizeof(V_11));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_12;
	memset((&V_12), 0, sizeof(V_12));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_13;
	memset((&V_13), 0, sizeof(V_13));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_14;
	memset((&V_14), 0, sizeof(V_14));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_15;
	memset((&V_15), 0, sizeof(V_15));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_16;
	memset((&V_16), 0, sizeof(V_16));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 ));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), ((std::numeric_limits<float>::max)()), ((std::numeric_limits<float>::max)()), ((std::numeric_limits<float>::max)()), /*hidden argument*/NULL);
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), (-(std::numeric_limits<float>::max)()), (-(std::numeric_limits<float>::max)()), (-(std::numeric_limits<float>::max)()), /*hidden argument*/NULL);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_0 = ___points30;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_0, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		V_3 = L_1;
		V_4 = 0;
		goto IL_0196;
	}

IL_0043:
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_2 = ___points30;
		int32_t L_3 = V_4;
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_5 = L_4;
		float L_5 = (&V_5)->get_x_2();
		float L_6 = (&V_1)->get_x_2();
		if ((!(((float)L_5) < ((float)L_6))))
		{
			goto IL_007d;
		}
	}
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_7 = ___points30;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_6 = L_9;
		float L_10 = (&V_6)->get_x_2();
		(&V_1)->set_x_2(L_10);
		goto IL_00b2;
	}

IL_007d:
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_11 = ___points30;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_7 = L_13;
		float L_14 = (&V_7)->get_x_2();
		float L_15 = (&V_2)->get_x_2();
		if ((!(((float)L_14) > ((float)L_15))))
		{
			goto IL_00b2;
		}
	}
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_16 = ___points30;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_16, L_17, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_8 = L_18;
		float L_19 = (&V_8)->get_x_2();
		(&V_2)->set_x_2(L_19);
	}

IL_00b2:
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_20 = ___points30;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_20, L_21, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_9 = L_22;
		float L_23 = (&V_9)->get_y_3();
		float L_24 = (&V_1)->get_y_3();
		if ((!(((float)L_23) < ((float)L_24))))
		{
			goto IL_00ec;
		}
	}
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_25 = ___points30;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_25, L_26, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_10 = L_27;
		float L_28 = (&V_10)->get_y_3();
		(&V_1)->set_y_3(L_28);
		goto IL_0121;
	}

IL_00ec:
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_29 = ___points30;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_29, L_30, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_11 = L_31;
		float L_32 = (&V_11)->get_y_3();
		float L_33 = (&V_2)->get_y_3();
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_0121;
		}
	}
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_34 = ___points30;
		int32_t L_35 = V_4;
		NullCheck(L_34);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_34, L_35, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_12 = L_36;
		float L_37 = (&V_12)->get_y_3();
		(&V_2)->set_y_3(L_37);
	}

IL_0121:
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_38 = ___points30;
		int32_t L_39 = V_4;
		NullCheck(L_38);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_38, L_39, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_13 = L_40;
		float L_41 = (&V_13)->get_z_4();
		float L_42 = (&V_1)->get_z_4();
		if ((!(((float)L_41) < ((float)L_42))))
		{
			goto IL_015b;
		}
	}
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_43 = ___points30;
		int32_t L_44 = V_4;
		NullCheck(L_43);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_43, L_44, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_14 = L_45;
		float L_46 = (&V_14)->get_z_4();
		(&V_1)->set_z_4(L_46);
		goto IL_0190;
	}

IL_015b:
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_47 = ___points30;
		int32_t L_48 = V_4;
		NullCheck(L_47);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_47, L_48, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_15 = L_49;
		float L_50 = (&V_15)->get_z_4();
		float L_51 = (&V_2)->get_z_4();
		if ((!(((float)L_50) > ((float)L_51))))
		{
			goto IL_0190;
		}
	}
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_52 = ___points30;
		int32_t L_53 = V_4;
		NullCheck(L_52);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_54 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_52, L_53, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_16 = L_54;
		float L_55 = (&V_16)->get_z_4();
		(&V_2)->set_z_4(L_55);
	}

IL_0190:
	{
		int32_t L_56 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)1));
	}

IL_0196:
	{
		int32_t L_57 = V_4;
		int32_t L_58 = V_3;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_0043;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59 = V_1;
		Bounds_set_min_mE513EAA10EEF244BD01BFF55EA3B36BD116EF3F4((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), L_59, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_60 = V_2;
		Bounds_set_max_m488E9C4F112B524DAB49FDE6284B22AFC6F956C9((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), L_60, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_61 = V_0;
		return L_61;
	}
}
// UnityEngine.Mesh Vectrosity.VectorManager::MakeBoundsMesh(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * VectorManager_MakeBoundsMesh_m72D478FD61D161D6DCB279FB54F759EA59CD93E6 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___bounds0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_MakeBoundsMesh_m72D478FD61D161D6DCB279FB54F759EA59CD93E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * V_0 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_8;
	memset((&V_8), 0, sizeof(V_8));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_9;
	memset((&V_9), 0, sizeof(V_9));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_10;
	memset((&V_10), 0, sizeof(V_10));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_11;
	memset((&V_11), 0, sizeof(V_11));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_12;
	memset((&V_12), 0, sizeof(V_12));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_13;
	memset((&V_13), 0, sizeof(V_13));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_14;
	memset((&V_14), 0, sizeof(V_14));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_15;
	memset((&V_15), 0, sizeof(V_15));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_16;
	memset((&V_16), 0, sizeof(V_16));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_17;
	memset((&V_17), 0, sizeof(V_17));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_18;
	memset((&V_18), 0, sizeof(V_18));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_19;
	memset((&V_19), 0, sizeof(V_19));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_20;
	memset((&V_20), 0, sizeof(V_20));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_21;
	memset((&V_21), 0, sizeof(V_21));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_22;
	memset((&V_22), 0, sizeof(V_22));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_23;
	memset((&V_23), 0, sizeof(V_23));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_24;
	memset((&V_24), 0, sizeof(V_24));
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *)il2cpp_codegen_object_new(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var);
		Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = V_0;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)8);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_3 = L_2;
		NullCheck(L_3);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = (&V_3)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_11), ((-L_6)), L_8, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_4, L_11, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_12;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_13 = L_3;
		NullCheck(L_13);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = (&V_4)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_5 = L_17;
		float L_18 = (&V_5)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_6 = L_19;
		float L_20 = (&V_6)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		memset((&L_21), 0, sizeof(L_21));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_21), L_16, L_18, L_20, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_14, L_21, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_22;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_23 = L_13;
		NullCheck(L_23);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_7 = L_25;
		float L_26 = (&V_7)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_8 = L_27;
		float L_28 = (&V_8)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_9 = L_29;
		float L_30 = (&V_9)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31;
		memset((&L_31), 0, sizeof(L_31));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_31), ((-L_26)), L_28, ((-L_30)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_24, L_31, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_32;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_33 = L_23;
		NullCheck(L_33);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_10 = L_35;
		float L_36 = (&V_10)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_11 = L_37;
		float L_38 = (&V_11)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_12 = L_39;
		float L_40 = (&V_12)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		memset((&L_41), 0, sizeof(L_41));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_41), L_36, L_38, ((-L_40)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_34, L_41, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_42;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_43 = L_33;
		NullCheck(L_43);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_13 = L_45;
		float L_46 = (&V_13)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_14 = L_47;
		float L_48 = (&V_14)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_15 = L_49;
		float L_50 = (&V_15)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
		memset((&L_51), 0, sizeof(L_51));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_51), ((-L_46)), ((-L_48)), L_50, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_44, L_51, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(4))) = L_52;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_53 = L_43;
		NullCheck(L_53);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_54 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_55 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_16 = L_55;
		float L_56 = (&V_16)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_57 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_17 = L_57;
		float L_58 = (&V_17)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_18 = L_59;
		float L_60 = (&V_18)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_61;
		memset((&L_61), 0, sizeof(L_61));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_61), L_56, ((-L_58)), L_60, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_62 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_54, L_61, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_53)->GetAddressAt(static_cast<il2cpp_array_size_t>(5))) = L_62;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_63 = L_53;
		NullCheck(L_63);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_64 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_65 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_19 = L_65;
		float L_66 = (&V_19)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_67 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_20 = L_67;
		float L_68 = (&V_20)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_69 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_21 = L_69;
		float L_70 = (&V_21)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_71;
		memset((&L_71), 0, sizeof(L_71));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_71), ((-L_66)), ((-L_68)), ((-L_70)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_72 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_64, L_71, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_63)->GetAddressAt(static_cast<il2cpp_array_size_t>(6))) = L_72;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_73 = L_63;
		NullCheck(L_73);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_74 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_75 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_22 = L_75;
		float L_76 = (&V_22)->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_77 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_23 = L_77;
		float L_78 = (&V_23)->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_79 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&___bounds0), /*hidden argument*/NULL);
		V_24 = L_79;
		float L_80 = (&V_24)->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_81;
		memset((&L_81), 0, sizeof(L_81));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_81), L_76, ((-L_78)), ((-L_80)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_82 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_74, L_81, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_73)->GetAddressAt(static_cast<il2cpp_array_size_t>(7))) = L_82;
		NullCheck(L_1);
		Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD(L_1, L_73, /*hidden argument*/NULL);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_83 = V_0;
		return L_83;
	}
}
// System.Void Vectrosity.VectorManager::SetupBoundsMesh(UnityEngine.GameObject,Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___go0, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * V_0 = NULL;
	MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * V_1 = NULL;
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___go0;
		NullCheck(L_0);
		MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * L_1 = GameObject_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mD924C79C3DA2B286ACB091AAC225DA91675E62A4(L_0, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mD924C79C3DA2B286ACB091AAC225DA91675E62A4_RuntimeMethod_var);
		V_0 = L_1;
		MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___go0;
		NullCheck(L_4);
		MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * L_5 = GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2(L_4, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2_RuntimeMethod_var);
		V_0 = L_5;
	}

IL_001a:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = ___go0;
		NullCheck(L_6);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_7 = GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B(L_6, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var);
		V_1 = L_7;
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_8, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0034;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = ___go0;
		NullCheck(L_10);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_11 = GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47(L_10, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47_RuntimeMethod_var);
		V_1 = L_11;
	}

IL_0034:
	{
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_12 = V_1;
		NullCheck(L_12);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_12, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * L_13 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_meshTable_18();
		if (L_13)
		{
			goto IL_004f;
		}
	}
	{
		Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * L_14 = (Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE *)il2cpp_codegen_object_new(Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m89E829D13C760C66899A0034D2D31EBC11BC801F(L_14, /*hidden argument*/Dictionary_2__ctor_m89E829D13C760C66899A0034D2D31EBC11BC801F_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->set_meshTable_18(L_14);
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * L_15 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_16 = ___line1;
		NullCheck(L_16);
		String_t* L_17 = VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		bool L_18 = Dictionary_2_ContainsKey_mC6D73DFD73A2A194D712ED95C27413F05C8BD1CA(L_15, L_17, /*hidden argument*/Dictionary_2_ContainsKey_mC6D73DFD73A2A194D712ED95C27413F05C8BD1CA_RuntimeMethod_var);
		if (L_18)
		{
			goto IL_00a4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * L_19 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_20 = ___line1;
		NullCheck(L_20);
		String_t* L_21 = VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline(L_20, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_22 = ___line1;
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_23 = VectorManager_GetBounds_m23D1D082EF71F5B252E32D9E3DCB68E847595026(L_22, /*hidden argument*/NULL);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_24 = VectorManager_MakeBoundsMesh_m72D478FD61D161D6DCB279FB54F759EA59CD93E6(L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		Dictionary_2_Add_m3090C39E5790BE4D40623B28FF117F9157181983(L_19, L_21, L_24, /*hidden argument*/Dictionary_2_Add_m3090C39E5790BE4D40623B28FF117F9157181983_RuntimeMethod_var);
		Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * L_25 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_26 = ___line1;
		NullCheck(L_26);
		String_t* L_27 = VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_28 = Dictionary_2_get_Item_m9AA27A646CA2E339C23E3B02A90B179D38A58F01(L_25, L_27, /*hidden argument*/Dictionary_2_get_Item_m9AA27A646CA2E339C23E3B02A90B179D38A58F01_RuntimeMethod_var);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_29 = ___line1;
		NullCheck(L_29);
		String_t* L_30 = VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline(L_29, /*hidden argument*/NULL);
		String_t* L_31 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_30, _stringLiteral7837EF2A788ACAC95713E45C5054E759F3642277, /*hidden argument*/NULL);
		NullCheck(L_28);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_28, L_31, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		Dictionary_2_t24FC4F7345767E16851199C5E021E0A4E17FEBBE * L_33 = ((VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_StaticFields*)il2cpp_codegen_static_fields_for(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var))->get_meshTable_18();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_34 = ___line1;
		NullCheck(L_34);
		String_t* L_35 = VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_36 = Dictionary_2_get_Item_m9AA27A646CA2E339C23E3B02A90B179D38A58F01(L_33, L_35, /*hidden argument*/Dictionary_2_get_Item_m9AA27A646CA2E339C23E3B02A90B179D38A58F01_RuntimeMethod_var);
		NullCheck(L_32);
		MeshFilter_set_mesh_m13177C1A6C29D76DDCD858CEF2B28C2AA7CC46FC(L_32, L_36, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VectorObject2D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D__ctor_m704336135C3BC3D39AEE27CBD644E972D242064D (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_38((bool)1);
		__this->set_m_updateUVs_39((bool)1);
		__this->set_m_updateColors_40((bool)1);
		__this->set_m_updateNormals_41((bool)0);
		__this->set_m_updateTangents_42((bool)0);
		__this->set_m_updateTris_43((bool)1);
		RawImage__ctor_mF745F95A5854034191E5787E9A22F2267943FDFE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D__cctor_mC43BA63969D868CF6CBB1761B90010A180335FEB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D__cctor_mC43BA63969D868CF6CBB1761B90010A180335FEB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_StaticFields*)il2cpp_codegen_static_fields_for(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_il2cpp_TypeInfo_var))->set_vertexHelper_46((VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 *)NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetVectorLine(Vectrosity.VectorLine,UnityEngine.Texture,UnityEngine.Material,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetVectorLine_m87D3085F3CF7880E04576D28E11C80349CDE5724 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___vectorLine0, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___tex1, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat2, bool ___useCustomMaterial3, const RuntimeMethod* method)
{
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_0 = ___vectorLine0;
		__this->set_vectorLine_45(L_0);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_1 = ___tex1;
		VectorObject2D_SetTexture_m356FBE3311E759BBD46F75FC3441755089296BC4(__this, L_1, /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___mat2;
		VectorObject2D_SetMaterial_mA7C634438A73116C9CA8F4B95A1B0E535AE61E8F(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::Destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_Destroy_m22B58E8F0D320D29B647094B4F05043956F81698 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_Destroy_m22B58E8F0D320D29B647094B4F05043956F81698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::DestroyNow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_DestroyNow_mAFAAAF538F3A01024B05ECA1E8E7F1EDEC8FDEA7 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_DestroyNow_mAFAAAF538F3A01024B05ECA1E8E7F1EDEC8FDEA7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::Enable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_Enable_m3CF7975FFD23E9CC70209AB333EE640200525205 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, bool ___enable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_Enable_m3CF7975FFD23E9CC70209AB333EE640200525205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_0 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(__this, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_1 = ___enable0;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetTexture_m356FBE3311E759BBD46F75FC3441755089296BC4 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___tex0, const RuntimeMethod* method)
{
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = ___tex0;
		RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetMaterial_mA7C634438A73116C9CA8F4B95A1B0E535AE61E8F (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat0, const RuntimeMethod* method)
{
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___mat0;
		VirtActionInvoker1< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, __this, L_0);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateGeometry()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateGeometry_mAD79F6A576446303BECDCC5FF0D05D75FA0B6FA7 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_UpdateGeometry_mAD79F6A576446303BECDCC5FF0D05D75FA0B6FA7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		VectorObject2D_SetupMesh_mAFA235E8824EB01E1F7F6D62806B46291A1DCB0C(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_2 = Graphic_get_rectTransform_m87D5A808474C6B71649CBB153DEBF5F268189EFF(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_006d;
		}
	}
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_4 = Graphic_get_rectTransform_m87D5A808474C6B71649CBB153DEBF5F268189EFF(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_5 = RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_6) >= ((float)(0.0f)))))
		{
			goto IL_006d;
		}
	}
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_7 = Graphic_get_rectTransform_m87D5A808474C6B71649CBB153DEBF5F268189EFF(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_8 = RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)(0.0f)))))
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_il2cpp_TypeInfo_var);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_10 = ((VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_StaticFields*)il2cpp_codegen_static_fields_for(VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5_il2cpp_TypeInfo_var))->get_vertexHelper_46();
		VirtActionInvoker1< VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * >::Invoke(44 /* System.Void UnityEngine.UI.Graphic::OnPopulateMesh(UnityEngine.UI.VertexHelper) */, __this, L_10);
	}

IL_006d:
	{
		CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * L_11 = Graphic_get_canvasRenderer_m33EC3A53310593E87C540654486C7A73A66FCF4A(__this, /*hidden argument*/NULL);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_12 = __this->get_m_mesh_44();
		NullCheck(L_11);
		CanvasRenderer_SetMesh_mB506682F318E5D8D8FE3888BF50E40DC34B726DF(L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetupMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetupMesh_mAFA235E8824EB01E1F7F6D62806B46291A1DCB0C (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_SetupMesh_mAFA235E8824EB01E1F7F6D62806B46291A1DCB0C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *)il2cpp_codegen_object_new(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var);
		Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6(L_0, /*hidden argument*/NULL);
		__this->set_m_mesh_44(L_0);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = __this->get_m_mesh_44();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = __this->get_vectorLine_45();
		NullCheck(L_2);
		String_t* L_3 = VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_1, L_3, /*hidden argument*/NULL);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_4 = __this->get_m_mesh_44();
		NullCheck(L_4);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_4, ((int32_t)61), /*hidden argument*/NULL);
		VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetMeshBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = __this->get_m_mesh_44();
		int32_t L_3 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_5), (((float)((float)((int32_t)((int32_t)L_3/(int32_t)2))))), (((float)((float)((int32_t)((int32_t)L_4/(int32_t)2))))), (0.0f), /*hidden argument*/NULL);
		int32_t L_6 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		int32_t L_7 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_8), (((float)((float)L_6))), (((float)((float)L_7))), (0.0f), /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Bounds__ctor_m8356472A177F4B22FFCE8911EBC8547A65A07CA3((&L_9), L_5, L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		Mesh_set_bounds_m9752E145EA6D719B417AA27555DDC2A388AB4E0A(L_2, L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::OnPopulateMesh(UnityEngine.UI.VertexHelper)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_OnPopulateMesh_m0406A9A86232C42EBABB5F237E990039CA174800 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_OnPopulateMesh_m0406A9A86232C42EBABB5F237E990039CA174800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_updateVerts_38();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = __this->get_m_mesh_44();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = __this->get_vectorLine_45();
		NullCheck(L_2);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_3 = VectorLine_get_lineVertices_mC4D7969376459F3E60138EDDCA9D581F0702D70C_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD(L_1, L_3, /*hidden argument*/NULL);
		__this->set_m_updateVerts_38((bool)0);
	}

IL_0028:
	{
		bool L_4 = __this->get_m_updateUVs_39();
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_5 = __this->get_vectorLine_45();
		NullCheck(L_5);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_6 = VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_7 = __this->get_m_mesh_44();
		NullCheck(L_7);
		int32_t L_8 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))) == ((uint32_t)L_8))))
		{
			goto IL_0066;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_9 = __this->get_m_mesh_44();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_10 = __this->get_vectorLine_45();
		NullCheck(L_10);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_11 = VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71_inline(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Mesh_set_uv_mF6FED6DDACBAE3EAF28BFBF257A0D5356FCF3AAC(L_9, L_11, /*hidden argument*/NULL);
	}

IL_0066:
	{
		__this->set_m_updateUVs_39((bool)0);
	}

IL_006d:
	{
		bool L_12 = __this->get_m_updateColors_40();
		if (!L_12)
		{
			goto IL_00b2;
		}
	}
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_13 = __this->get_vectorLine_45();
		NullCheck(L_13);
		Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* L_14 = VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1_inline(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_15 = __this->get_m_mesh_44();
		NullCheck(L_15);
		int32_t L_16 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_15, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))) == ((uint32_t)L_16))))
		{
			goto IL_00ab;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_17 = __this->get_m_mesh_44();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_18 = __this->get_vectorLine_45();
		NullCheck(L_18);
		Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* L_19 = VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1_inline(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Mesh_set_colors32_m0EA838C15BBD01710A4EE1A475DB585EB9B5BD91(L_17, L_19, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		__this->set_m_updateColors_40((bool)0);
	}

IL_00b2:
	{
		bool L_20 = __this->get_m_updateTris_43();
		if (!L_20)
		{
			goto IL_00e1;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_21 = __this->get_m_mesh_44();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_22 = __this->get_vectorLine_45();
		NullCheck(L_22);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_23 = VectorLine_get_lineTriangles_m6FE31B265CC9E175DBC5BD5D91256EAA7ADE78C2_inline(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Mesh_SetTriangles_mF74536E3A39AECF33809A7D23AFF54A1CFC37129(L_21, L_23, 0, /*hidden argument*/NULL);
		__this->set_m_updateTris_43((bool)0);
		VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6(__this, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		bool L_24 = __this->get_m_updateNormals_41();
		if (!L_24)
		{
			goto IL_0115;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_25 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_25, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0115;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_27 = __this->get_m_mesh_44();
		NullCheck(L_27);
		Mesh_RecalculateNormals_mEBF9ED932D0B463E4EF3947D232CC8BEECAE1A4A(L_27, /*hidden argument*/NULL);
		__this->set_m_updateNormals_41((bool)0);
		VirtActionInvoker0::Invoke(41 /* System.Void UnityEngine.UI.Graphic::UpdateGeometry() */, __this);
	}

IL_0115:
	{
		bool L_28 = __this->get_m_updateTangents_42();
		if (!L_28)
		{
			goto IL_0159;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_29 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_29, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0159;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_31 = __this->get_m_mesh_44();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_32 = __this->get_vectorLine_45();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_33 = __this->get_m_mesh_44();
		NullCheck(L_33);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_34 = Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_35 = VectorLine_CalculateTangents_mCC45C3345D6020C7267838B8616C4E938B031548(L_32, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		Mesh_set_tangents_mFA4E0A26B52C1FCF80FA5DA642B28716249ACF67(L_31, L_35, /*hidden argument*/NULL);
		__this->set_m_updateTangents_42((bool)0);
	}

IL_0159:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::SetName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_SetName_mF846BFE2EAE12F81DC7603F0639F2A89B395CE84 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_SetName_mF846BFE2EAE12F81DC7603F0639F2A89B395CE84_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = __this->get_m_mesh_44();
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateVerts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateVerts_mCF94DB3DC0536E4587AEB27ADF3351E6C839D3BD (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_38((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateUVs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateUVs_mC4B07F5C21023FCCEC97A7B2A9F4F195470BECC4 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateUVs_39((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateColors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateColors_mA9243733BCA7B03C1031A2605898CF0064A29A48 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateColors_40((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateNormals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateNormals_mA2DA97A5E41BC7CC838459F93C695A1003ED0206 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateNormals_41((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateTangents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateTangents_m320C0A81769ADDC4EFE293E6C0B4F8BFDC70F9ED (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTangents_42((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateTris()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateTris_m99E3922F7DB201168938074590B9CF5601BD0D99 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTris_43((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::UpdateMeshAttributes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_UpdateMeshAttributes_mC480305BD1408E7D8CFF157FBF327AA9A4031659 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_UpdateMeshAttributes_mC480305BD1408E7D8CFF157FBF327AA9A4031659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = __this->get_m_mesh_44();
		NullCheck(L_2);
		Mesh_Clear_m7500ECE6209E14CC750CB16B48301B8D2A57ACCE(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		__this->set_m_updateVerts_38((bool)1);
		__this->set_m_updateUVs_39((bool)1);
		__this->set_m_updateColors_40((bool)1);
		__this->set_m_updateTris_43((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VectorObject2D_SetMeshBounds_m1083EDEA991C1E5C8C9047B0269A89C5E031D2E6(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject2D::ClearMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject2D_ClearMesh_mB4859A90C0DFABBFC096ED341686BB70AC023763 (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject2D_ClearMesh_mB4859A90C0DFABBFC096ED341686BB70AC023763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = __this->get_m_mesh_44();
		NullCheck(L_2);
		Mesh_Clear_m7500ECE6209E14CC750CB16B48301B8D2A57ACCE(L_2, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(41 /* System.Void UnityEngine.UI.Graphic::UpdateGeometry() */, __this);
		return;
	}
}
// System.Int32 Vectrosity.VectorObject2D::VertexCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t VectorObject2D_VertexCount_m88C4663CE98BA9368EA27089C1C5A405AD1A10EB (VectorObject2D_t69171C1E6224D44255A91911147B302BE111CCA5 * __this, const RuntimeMethod* method)
{
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_44();
		NullCheck(L_0);
		int32_t L_1 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VectorObject3D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D__ctor_m5C4B552F6DBA830A65EB9CD7382A83381B9C72B8 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_4((bool)1);
		__this->set_m_updateUVs_5((bool)1);
		__this->set_m_updateColors_6((bool)1);
		__this->set_m_updateNormals_7((bool)0);
		__this->set_m_updateTangents_8((bool)0);
		__this->set_m_updateTris_9((bool)1);
		__this->set_m_useCustomMaterial_13((bool)0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetVectorLine(Vectrosity.VectorLine,UnityEngine.Texture,UnityEngine.Material,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetVectorLine_m8EA2D566D669EB28195896F3D557152FC044C6A6 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___vectorLine0, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___tex1, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat2, bool ___useCustomMaterial3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetVectorLine_m8EA2D566D669EB28195896F3D557152FC044C6A6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47(L_0, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_mD5BD4B507E470AFA16BAD4B418DC15AE59A9FC47_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2(L_1, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_mA5802EF007058E65CCD414C3EB2518474D17A2F2_RuntimeMethod_var);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = ___vectorLine0;
		__this->set_m_vectorLine_11(L_2);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___mat2;
		__this->set_m_material_12(L_3);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_4 = __this->get_m_material_12();
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_5 = ___tex1;
		NullCheck(L_4);
		Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5(L_4, L_5, /*hidden argument*/NULL);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_6 = Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537_RuntimeMethod_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = __this->get_m_material_12();
		NullCheck(L_6);
		Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = ___useCustomMaterial3;
		__this->set_m_useCustomMaterial_13(L_8);
		VectorObject3D_SetupMesh_mEDD447CA9C0FBC3D6408F7D397FAC4B63030E411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::Destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_Destroy_m6DAD5F1A059D699F2B05A6B6B54723D56FBD8A8D (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_Destroy_m6DAD5F1A059D699F2B05A6B6B54723D56FBD8A8D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_m_useCustomMaterial_13();
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = __this->get_m_material_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::Enable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_Enable_m72EEE3ED96AB44836B53AD9A2662CB87D5AB9576 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, bool ___enable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_Enable_m72EEE3ED96AB44836B53AD9A2662CB87D5AB9576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_0 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(__this, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_1 = Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537_RuntimeMethod_var);
		bool L_2 = ___enable0;
		NullCheck(L_1);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetTexture_m646C1DE560A2B30B564DD1B4E9E79CEBD5354517 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___tex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetTexture_m646C1DE560A2B30B564DD1B4E9E79CEBD5354517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_0 = Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537_RuntimeMethod_var);
		NullCheck(L_0);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC(L_0, /*hidden argument*/NULL);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_2 = ___tex0;
		NullCheck(L_1);
		Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetMaterial_mFA8CB205105CEDF2EAFB2351619C476C2E726CF3 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetMaterial_mFA8CB205105CEDF2EAFB2351619C476C2E726CF3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___mat0;
		__this->set_m_material_12(L_0);
		__this->set_m_useCustomMaterial_13((bool)1);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_1 = Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537_RuntimeMethod_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___mat0;
		NullCheck(L_1);
		Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B(L_1, L_2, /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___mat0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_5 = Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m87EEABB28FFB9E9553015DD36B7C6F7C45A7F537_RuntimeMethod_var);
		NullCheck(L_5);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_6 = Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC(L_5, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_7 = __this->get_m_vectorLine_11();
		NullCheck(L_7);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_8 = VectorLine_get_texture_m80B0BB07297D6E6A53789EFF36AFA678FACCA27E_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5(L_6, L_8, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetupMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetupMesh_mEDD447CA9C0FBC3D6408F7D397FAC4B63030E411 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetupMesh_mEDD447CA9C0FBC3D6408F7D397FAC4B63030E411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *)il2cpp_codegen_object_new(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var);
		Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6(L_0, /*hidden argument*/NULL);
		__this->set_m_mesh_10(L_0);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = __this->get_m_mesh_10();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = __this->get_m_vectorLine_11();
		NullCheck(L_2);
		String_t* L_3 = VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_1, L_3, /*hidden argument*/NULL);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_4 = __this->get_m_mesh_10();
		NullCheck(L_4);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_4, ((int32_t)61), /*hidden argument*/NULL);
		MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * L_5 = Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC_RuntimeMethod_var);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_6 = __this->get_m_mesh_10();
		NullCheck(L_5);
		MeshFilter_set_mesh_m13177C1A6C29D76DDCD858CEF2B28C2AA7CC46FC(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_LateUpdate_m2DF6E4EDD4423FCA03129E57864AB19843A1E76A (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_updateVerts_4();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		VectorObject3D_SetVerts_m1A386FEC2696DA4E4D4C6795637212987AA49BB1(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_m_updateUVs_5();
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = __this->get_m_vectorLine_11();
		NullCheck(L_2);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_3 = VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_4 = __this->get_m_mesh_10();
		NullCheck(L_4);
		int32_t L_5 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length))))) == ((uint32_t)L_5))))
		{
			goto IL_004f;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_6 = __this->get_m_mesh_10();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_7 = __this->get_m_vectorLine_11();
		NullCheck(L_7);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_8 = VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Mesh_set_uv_mF6FED6DDACBAE3EAF28BFBF257A0D5356FCF3AAC(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004f:
	{
		__this->set_m_updateUVs_5((bool)0);
	}

IL_0056:
	{
		bool L_9 = __this->get_m_updateColors_6();
		if (!L_9)
		{
			goto IL_009b;
		}
	}
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_10 = __this->get_m_vectorLine_11();
		NullCheck(L_10);
		Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* L_11 = VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1_inline(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_12 = __this->get_m_mesh_10();
		NullCheck(L_12);
		int32_t L_13 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))) == ((uint32_t)L_13))))
		{
			goto IL_0094;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_14 = __this->get_m_mesh_10();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_15 = __this->get_m_vectorLine_11();
		NullCheck(L_15);
		Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* L_16 = VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1_inline(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Mesh_set_colors32_m0EA838C15BBD01710A4EE1A475DB585EB9B5BD91(L_14, L_16, /*hidden argument*/NULL);
	}

IL_0094:
	{
		__this->set_m_updateColors_6((bool)0);
	}

IL_009b:
	{
		bool L_17 = __this->get_m_updateTris_9();
		if (!L_17)
		{
			goto IL_00c4;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_18 = __this->get_m_mesh_10();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_19 = __this->get_m_vectorLine_11();
		NullCheck(L_19);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_20 = VectorLine_get_lineTriangles_m6FE31B265CC9E175DBC5BD5D91256EAA7ADE78C2_inline(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Mesh_SetTriangles_mF74536E3A39AECF33809A7D23AFF54A1CFC37129(L_18, L_20, 0, /*hidden argument*/NULL);
		__this->set_m_updateTris_9((bool)0);
	}

IL_00c4:
	{
		bool L_21 = __this->get_m_updateNormals_7();
		if (!L_21)
		{
			goto IL_00e1;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_22 = __this->get_m_mesh_10();
		NullCheck(L_22);
		Mesh_RecalculateNormals_mEBF9ED932D0B463E4EF3947D232CC8BEECAE1A4A(L_22, /*hidden argument*/NULL);
		__this->set_m_updateNormals_7((bool)0);
	}

IL_00e1:
	{
		bool L_23 = __this->get_m_updateTangents_8();
		if (!L_23)
		{
			goto IL_0114;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_24 = __this->get_m_mesh_10();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_25 = __this->get_m_vectorLine_11();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_26 = __this->get_m_mesh_10();
		NullCheck(L_26);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_27 = Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_28 = VectorLine_CalculateTangents_mCC45C3345D6020C7267838B8616C4E938B031548(L_25, L_27, /*hidden argument*/NULL);
		NullCheck(L_24);
		Mesh_set_tangents_mFA4E0A26B52C1FCF80FA5DA642B28716249ACF67(L_24, L_28, /*hidden argument*/NULL);
		__this->set_m_updateTangents_8((bool)0);
	}

IL_0114:
	{
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetVerts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetVerts_m1A386FEC2696DA4E4D4C6795637212987AA49BB1 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_10();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_1 = __this->get_m_vectorLine_11();
		NullCheck(L_1);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = VectorLine_get_lineVertices_mC4D7969376459F3E60138EDDCA9D581F0702D70C_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD(L_0, L_2, /*hidden argument*/NULL);
		__this->set_m_updateVerts_4((bool)0);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_3 = __this->get_m_mesh_10();
		NullCheck(L_3);
		Mesh_RecalculateBounds_mC39556595CFE3E4D8EFA777476ECD22B97FC2737(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::SetName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_SetName_m34E91DF41876F47A2CFEB2CA0896CA7687054D59 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_SetName_m34E91DF41876F47A2CFEB2CA0896CA7687054D59_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = __this->get_m_mesh_10();
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateVerts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_UpdateVerts_mA2B2185082F33744D62AF725BD6A43FD725E5134 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateVerts_4((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateUVs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_UpdateUVs_m79250BAC83937C89DC45D76F9582E1A51C88B934 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateUVs_5((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateColors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_UpdateColors_mB1F80D91F5887F94F619C6DF3274253F7C6A6730 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateColors_6((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateNormals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_UpdateNormals_m82C21130E357193304CF7C809068F8919A2D3F55 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateNormals_7((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateTangents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_UpdateTangents_mC12494B53D2475758CEA48315245A7FA58571E58 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTangents_8((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateTris()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_UpdateTris_mEA390BD784FB3EA6B41A28D675951248A4619D10 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_updateTris_9((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::UpdateMeshAttributes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_UpdateMeshAttributes_m4AE1B7D3D1E8442AD10C0D378778B45827448A65 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_10();
		NullCheck(L_0);
		Mesh_Clear_m7500ECE6209E14CC750CB16B48301B8D2A57ACCE(L_0, /*hidden argument*/NULL);
		__this->set_m_updateVerts_4((bool)1);
		__this->set_m_updateUVs_5((bool)1);
		__this->set_m_updateColors_6((bool)1);
		__this->set_m_updateTris_9((bool)1);
		return;
	}
}
// System.Void Vectrosity.VectorObject3D::ClearMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VectorObject3D_ClearMesh_mF9821537AA239B9A3A5D4D89E07A1CD632810A24 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VectorObject3D_ClearMesh_mF9821537AA239B9A3A5D4D89E07A1CD632810A24_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = __this->get_m_mesh_10();
		NullCheck(L_2);
		Mesh_Clear_m7500ECE6209E14CC750CB16B48301B8D2A57ACCE(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vectrosity.VectorObject3D::VertexCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t VectorObject3D_VertexCount_m55658501B729EFF993B0E900ECEC2A1275393C00 (VectorObject3D_tC00FE76D7E267CC113D7F29483C0BB867515CBFC * __this, const RuntimeMethod* method)
{
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_mesh_10();
		NullCheck(L_0);
		int32_t L_1 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControl__ctor_m234E1E2866025823E0BEE6D8CB1E11E8F208CFED (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_destroyed_6((bool)0);
		__this->set_m_dontDestroyLine_7((bool)0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vectrosity.RefInt Vectrosity.VisibilityControl::get_objectNumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * VisibilityControl_get_objectNumber_mFFDF71E0B885367C7D8F6EB2E1A07A196E5FD7A3 (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method)
{
	{
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_0 = __this->get_m_objectNumber_4();
		return L_0;
	}
}
// System.Void Vectrosity.VisibilityControl::Setup(Vectrosity.VectorLine,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControl_Setup_mFC214FC710C98CF73C6CA631AC2090811D2BF8B1 (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, bool ___makeBounds1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_Setup_mFC214FC710C98CF73C6CA631AC2090811D2BF8B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___makeBounds1;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = ___line0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_4 = ___line0;
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** L_5 = __this->get_address_of_m_objectNumber_4();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2(L_3, L_4, (RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE **)L_5, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_6 = ___line0;
		__this->set_m_vectorLine_5(L_6);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_7 = __this->get_m_objectNumber_4();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_i_0();
		VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732(L_8, /*hidden argument*/NULL);
		RuntimeObject* L_9 = VisibilityControl_VisibilityTest_m1376D2161DA0A5636CE0D6CFE36F296C417546ED(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControl::VisibilityTest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_VisibilityTest_m1376D2161DA0A5636CE0D6CFE36F296C417546ED (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_VisibilityTest_m1376D2161DA0A5636CE0D6CFE36F296C417546ED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * V_0 = NULL;
	{
		U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * L_0 = (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA *)il2cpp_codegen_object_new(U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA_il2cpp_TypeInfo_var);
		U3CVisibilityTestU3Ec__Iterator1__ctor_mA3B4BCE3D8F8C728D75A1B29A3E9A59F66399DE8(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControl::OnBecameVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_OnBecameVisible_mF88C687A0EE1D0B8D4026EB2FD2D386859291026 (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_OnBecameVisible_mF88C687A0EE1D0B8D4026EB2FD2D386859291026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * V_0 = NULL;
	{
		U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * L_0 = (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 *)il2cpp_codegen_object_new(U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0_il2cpp_TypeInfo_var);
		U3COnBecameVisibleU3Ec__Iterator2__ctor_m7D8891F9A9C506D7FB97B598F0376D8DE1A3198F(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControl::OnBecameInvisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControl_OnBecameInvisible_mF5C5604BDFA27A20D8B8B89723F3BB753C1AB3BE (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_OnBecameInvisible_mF5C5604BDFA27A20D8B8B89723F3BB753C1AB3BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * V_0 = NULL;
	{
		U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * L_0 = (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B *)il2cpp_codegen_object_new(U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B_il2cpp_TypeInfo_var);
		U3COnBecameInvisibleU3Ec__Iterator3__ctor_m05443508C5AB96FE91ABDC1B593275A6BED217A0(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * L_2 = V_0;
		return L_2;
	}
}
// System.Void Vectrosity.VisibilityControl::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControl_OnDestroy_mE82C1592DD07936C5AC3958E128CB8AB9D4A16C2 (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControl_OnDestroy_mE82C1592DD07936C5AC3958E128CB8AB9D4A16C2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_destroyed_6();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_m_destroyed_6((bool)1);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B(L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_m_dontDestroyLine_7();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** L_4 = __this->get_address_of_m_vectorLine_5();
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_Destroy_mC44B22F624821712A379ED38A54E638772BB1345((VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 **)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControl::DontDestroyLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControl_DontDestroyLine_mB14481A1B63B17D7E5843773FB881D82B6C32EE5 (VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_dontDestroyLine_7((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControlAlways::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlAlways__ctor_m32F83386E9619F9359BEB2C1AA055BC883289DF0 (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_destroyed_6((bool)0);
		__this->set_m_dontDestroyLine_7((bool)0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vectrosity.RefInt Vectrosity.VisibilityControlAlways::get_objectNumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * VisibilityControlAlways_get_objectNumber_m65453C875277B253C002EF4A1FF2F8401CE9033E (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * __this, const RuntimeMethod* method)
{
	{
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_0 = __this->get_m_objectNumber_4();
		return L_0;
	}
}
// System.Void Vectrosity.VisibilityControlAlways::Setup(Vectrosity.VectorLine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlAlways_Setup_m342E892B79D5E722B39337FE70B2E0C45A83712E (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlAlways_Setup_m342E892B79D5E722B39337FE70B2E0C45A83712E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_1 = ___line0;
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** L_2 = __this->get_address_of_m_objectNumber_4();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_VisibilitySetup_mB4AA606C410D8327A11F7835184C265AA3E745C2(L_0, L_1, (RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE **)L_2, /*hidden argument*/NULL);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_3 = __this->get_m_objectNumber_4();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_i_0();
		VectorManager_DrawArrayLine2_m311A17F0A108BFD7938A57EE1DC2291D2FA8F732(L_4, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_5 = ___line0;
		__this->set_m_vectorLine_5(L_5);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlAlways::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlAlways_OnDestroy_m64F2672D8F7A7BC724F3BF2DCAAD2A3D6608EEE7 (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlAlways_OnDestroy_m64F2672D8F7A7BC724F3BF2DCAAD2A3D6608EEE7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_destroyed_6();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_m_destroyed_6((bool)1);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_VisibilityRemove_m101BE7AF67F0D0F84D910C6C7C8D48554553556B(L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_m_dontDestroyLine_7();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** L_4 = __this->get_address_of_m_vectorLine_5();
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_Destroy_mC44B22F624821712A379ED38A54E638772BB1345((VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 **)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlAlways::DontDestroyLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlAlways_DontDestroyLine_m4E784734F767E6BEC1984E3E36D3E66D5E872535 (VisibilityControlAlways_t91A41A63F939FC39E6FBD0E24F8279FC7C31AA5F * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_dontDestroyLine_7((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControlStatic::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic__ctor_m4E565F840EFBB88A3C5D22322593AE7C20976A40 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_destroyed_6((bool)0);
		__this->set_m_dontDestroyLine_7((bool)0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vectrosity.RefInt Vectrosity.VisibilityControlStatic::get_objectNumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * VisibilityControlStatic_get_objectNumber_mEFDD619820364E20C93AE117812F08E117D09BF6 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	{
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_0 = __this->get_m_objectNumber_4();
		return L_0;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::Setup(Vectrosity.VectorLine,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic_Setup_mB81BF8A1F5A9DA09AD76430F4F26327F2AAEB928 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * ___line0, bool ___makeBounds1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_Setup_mB81BF8A1F5A9DA09AD76430F4F26327F2AAEB928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		bool L_0 = ___makeBounds1;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_2 = ___line0;
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_SetupBoundsMesh_m76D7728D02EEC0724543AD3F2B33E1E3E683BB59(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_4 = Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC(L_3, /*hidden argument*/NULL);
		__this->set_m_originalMatrix_8(L_4);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_5 = ___line0;
		NullCheck(L_5);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_6 = VectorLine_get_points3_m193E2FEFD72026674A56766F63150184E9DF4F50(L_5, /*hidden argument*/NULL);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_7 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mB47F013C62B967199614B3D07228B381C0562448(L_7, L_6, /*hidden argument*/List_1__ctor_mB47F013C62B967199614B3D07228B381C0562448_RuntimeMethod_var);
		V_0 = L_7;
		V_1 = 0;
		goto IL_0053;
	}

IL_0036:
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_8 = V_0;
		int32_t L_9 = V_1;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_10 = __this->get_address_of_m_originalMatrix_8();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_11 = V_0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = Matrix4x4_MultiplyPoint3x4_mA0A34C5FD162DA8E5421596F1F921436F3E7B2FC((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_10, L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406(L_8, L_9, L_14, /*hidden argument*/List_1_set_Item_m47BD350016A981C60BB5DA25E124EE6F98450406_RuntimeMethod_var);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0053:
	{
		int32_t L_16 = V_1;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_17, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0036;
		}
	}
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_19 = ___line0;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_20 = V_0;
		NullCheck(L_19);
		VectorLine_set_points3_m2FE6D35A38BD92CFEC2A761D353D5EB904147636(L_19, L_20, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_21 = ___line0;
		__this->set_m_vectorLine_5(L_21);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_22 = ___line0;
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE ** L_23 = __this->get_address_of_m_objectNumber_4();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_VisibilityStaticSetup_mD098A79500A6B1A0E347CAB4DE0A889D28E2E13A(L_22, (RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE **)L_23, /*hidden argument*/NULL);
		RuntimeObject* L_24 = VisibilityControlStatic_WaitCheck_m48175ADD5334C3484E3464A64F8E6F728E9358C7(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Vectrosity.VisibilityControlStatic::WaitCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VisibilityControlStatic_WaitCheck_m48175ADD5334C3484E3464A64F8E6F728E9358C7 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_WaitCheck_m48175ADD5334C3484E3464A64F8E6F728E9358C7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * V_0 = NULL;
	{
		U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * L_0 = (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 *)il2cpp_codegen_object_new(U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6_il2cpp_TypeInfo_var);
		U3CWaitCheckU3Ec__Iterator4__ctor_mC373F298379008231B6CABDCB7F5323393256E4B(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::OnBecameVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic_OnBecameVisible_m12516B3BF36AEE40B7EADFC6AFA08BD9BA0A1DE4 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_OnBecameVisible_m12516B3BF36AEE40B7EADFC6AFA08BD9BA0A1DE4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_0 = __this->get_m_vectorLine_5();
		NullCheck(L_0);
		VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484(L_0, (bool)1, /*hidden argument*/NULL);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::OnBecameInvisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic_OnBecameInvisible_mA6941F4A753D2AF4256981A553FD9F2A16C208F8 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_0 = __this->get_m_vectorLine_5();
		NullCheck(L_0);
		VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic_OnDestroy_m329A043C2DA7160F29FCD892ACECC20772EFDC28 (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisibilityControlStatic_OnDestroy_m329A043C2DA7160F29FCD892ACECC20772EFDC28_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_destroyed_6();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_m_destroyed_6((bool)1);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_1 = __this->get_m_objectNumber_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_VisibilityStaticRemove_m9B72F2333EE6877D0F29D93BC18F76ADA5B0D237(L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_m_dontDestroyLine_7();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** L_4 = __this->get_address_of_m_vectorLine_5();
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_Destroy_mC44B22F624821712A379ED38A54E638772BB1345((VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 **)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic::DontDestroyLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibilityControlStatic_DontDestroyLine_mFA2676DFA190FA78B3397881AB85AA267F0607EF (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_dontDestroyLine_7((bool)1);
		return;
	}
}
// UnityEngine.Matrix4x4 Vectrosity.VisibilityControlStatic::GetMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  VisibilityControlStatic_GetMatrix_m72C2A4EB4D860A0B6581A8633B671FD5F73419DD (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	{
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_0 = __this->get_m_originalMatrix_8();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.LineManager_<DisableLine>c__Iterator0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDisableLineU3Ec__Iterator0__ctor_m7C3CC0527424E540C7171AB9D3E63D1E4F306380 (U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.LineManager_<DisableLine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDisableLineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4E358A5BD114BA8582F3DBD751A7B0DE2FFED9B8 (U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Vectrosity.LineManager_<DisableLine>c__Iterator0::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDisableLineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_mB1E265B7781B10315A274977B92F1F96347B6DC7 (U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean Vectrosity.LineManager_<DisableLine>c__Iterator0::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDisableLineU3Ec__Iterator0_MoveNext_m80031DD3D256059BE76D15027467F5FA957D6A16 (U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDisableLineU3Ec__Iterator0_MoveNext_m80031DD3D256059BE76D15027467F5FA957D6A16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_003e;
			}
		}
	}
	{
		goto IL_0089;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_3 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_4(L_3);
		__this->set_U24PC_3(1);
		goto IL_008b;
	}

IL_003e:
	{
		bool L_4 = __this->get_remove_1();
		if (!L_4)
		{
			goto IL_005f;
		}
	}
	{
		LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * L_5 = __this->get_U3CU3Ef__this_8();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_6 = __this->get_vectorLine_2();
		NullCheck(L_5);
		LineManager_RemoveLine_m941BD07322036D8D886799D67AD748791EE97C1D(L_5, L_6, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_005f:
	{
		LineManager_t4BA0606446ACC46468067C02B8A92E01D287CDED * L_7 = __this->get_U3CU3Ef__this_8();
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_8 = __this->get_vectorLine_2();
		NullCheck(L_7);
		LineManager_RemoveLine_m941BD07322036D8D886799D67AD748791EE97C1D(L_7, L_8, /*hidden argument*/NULL);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 ** L_9 = __this->get_address_of_vectorLine_2();
		IL2CPP_RUNTIME_CLASS_INIT(VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775_il2cpp_TypeInfo_var);
		VectorLine_Destroy_mC44B22F624821712A379ED38A54E638772BB1345((VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 **)L_9, /*hidden argument*/NULL);
	}

IL_007b:
	{
		__this->set_vectorLine_2((VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 *)NULL);
		__this->set_U24PC_3((-1));
	}

IL_0089:
	{
		return (bool)0;
	}

IL_008b:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.LineManager_<DisableLine>c__Iterator0::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDisableLineU3Ec__Iterator0_Dispose_m69F6429E931C5B4C9B9F1FAAE78533B0B3B358B9 (U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Vectrosity.LineManager_<DisableLine>c__Iterator0::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDisableLineU3Ec__Iterator0_Reset_mB4B023677AE126534F8ECD01B6FD974B9A44A8E1 (U3CDisableLineU3Ec__Iterator0_t93A339AD4398EA53751E8DF54696B58483A892D5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDisableLineU3Ec__Iterator0_Reset_mB4B023677AE126534F8ECD01B6FD974B9A44A8E1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CDisableLineU3Ec__Iterator0_Reset_mB4B023677AE126534F8ECD01B6FD974B9A44A8E1_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3__ctor_m05443508C5AB96FE91ABDC1B593275A6BED217A0 (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m281FFAD0C7A249DDF0B9CCB7D98F690CA0A8EF50 (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameInvisibleU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_mF13911BF6D072BDE853FA1075BA968CEB7E2ABF6 (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_mE70B1C3FF21A47973237CBABB67A8B40AC99135A (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameInvisibleU3Ec__Iterator3_MoveNext_mE70B1C3FF21A47973237CBABB67A8B40AC99135A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0038;
			}
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_2 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0052;
	}

IL_0038:
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_4 = L_3->get_m_vectorLine_5();
		NullCheck(L_4);
		VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0050:
	{
		return (bool)0;
	}

IL_0052:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3_Dispose_mA87E7B199FF929EA3BA38ED5BB1368EFDDF0C2F3 (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameInvisible>c__Iterator3::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameInvisibleU3Ec__Iterator3_Reset_m63434CECBC5DF4944317DAA6EC774AD46CD5287A (U3COnBecameInvisibleU3Ec__Iterator3_t146D23E19C0B964CC6DC96FF8F4A507BF95C567B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameInvisibleU3Ec__Iterator3_Reset_m63434CECBC5DF4944317DAA6EC774AD46CD5287A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3COnBecameInvisibleU3Ec__Iterator3_Reset_m63434CECBC5DF4944317DAA6EC774AD46CD5287A_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2__ctor_m7D8891F9A9C506D7FB97B598F0376D8DE1A3198F (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameVisibleU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9C4AD3A6C5F8A64AEAFC9CE460105C1EBCF6B7C2 (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3COnBecameVisibleU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_mDCFDC2ED90F9DF4277D8794E3371A705B001BACD (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m5F96417D4C99C7145D0A2E1A75C84EC136147397 (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameVisibleU3Ec__Iterator2_MoveNext_m5F96417D4C99C7145D0A2E1A75C84EC136147397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0038;
			}
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_2 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0052;
	}

IL_0038:
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_4 = L_3->get_m_vectorLine_5();
		NullCheck(L_4);
		VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484(L_4, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0050:
	{
		return (bool)0;
	}

IL_0052:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2_Dispose_m5E0F062A69EDA202E34ED211D3F9BB812CB88289 (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControl_<OnBecameVisible>c__Iterator2::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3COnBecameVisibleU3Ec__Iterator2_Reset_m5F5390A4426ED12EEA7B6EF4C5E17B25219F452E (U3COnBecameVisibleU3Ec__Iterator2_t6D2797766E32245177C1DEAABE7FE93EEAC387D0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnBecameVisibleU3Ec__Iterator2_Reset_m5F5390A4426ED12EEA7B6EF4C5E17B25219F452E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3COnBecameVisibleU3Ec__Iterator2_Reset_m5F5390A4426ED12EEA7B6EF4C5E17B25219F452E_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1__ctor_mA3B4BCE3D8F8C728D75A1B29A3E9A59F66399DE8 (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CVisibilityTestU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7F1B3610E33046148D4FD293AF55B877CB71E2EA (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CVisibilityTestU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m78FFCDCE3A8AD3464045FFC94EC6E5D1E04820C9 (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CVisibilityTestU3Ec__Iterator1_MoveNext_m0B5AADB14FE54C3943C2EC6BFAD29328410B1A8F (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CVisibilityTestU3Ec__Iterator1_MoveNext_m0B5AADB14FE54C3943C2EC6BFAD29328410B1A8F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_004b;
			}
		}
	}
	{
		goto IL_0078;
	}

IL_0025:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_007a;
	}

IL_0038:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(2);
		goto IL_007a;
	}

IL_004b:
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_3 = Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019(L_2, /*hidden argument*/Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019_RuntimeMethod_var);
		NullCheck(L_3);
		bool L_4 = Renderer_get_isVisible_mE424F7FFEA9D78BC657B3F54FDFBE1EB8461CCB7(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0071;
		}
	}
	{
		VisibilityControl_t0EBCFACE28AAC2DB61E9BFC8EB4E62897864DA10 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_6 = L_5->get_m_vectorLine_5();
		NullCheck(L_6);
		VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484(L_6, (bool)0, /*hidden argument*/NULL);
	}

IL_0071:
	{
		__this->set_U24PC_0((-1));
	}

IL_0078:
	{
		return (bool)0;
	}

IL_007a:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1_Dispose_m3975DC856353BE7A335B0123899D167BC4859553 (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControl_<VisibilityTest>c__Iterator1::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVisibilityTestU3Ec__Iterator1_Reset_m9FF8A84407B2B77BBEC0664EFDA158FD2488CA28 (U3CVisibilityTestU3Ec__Iterator1_t11A85FF5B727BCC1158891DDE7C4DD2D78B818DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CVisibilityTestU3Ec__Iterator1_Reset_m9FF8A84407B2B77BBEC0664EFDA158FD2488CA28_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CVisibilityTestU3Ec__Iterator1_Reset_m9FF8A84407B2B77BBEC0664EFDA158FD2488CA28_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4__ctor_mC373F298379008231B6CABDCB7F5323393256E4B (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitCheckU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mE5ECCE300FAD7E88DFA34BDCAE14C2629745E004 (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitCheckU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m4B8B60E5D3BD9A4E109AB5F01AB641CC22E9A1C1 (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CWaitCheckU3Ec__Iterator4_MoveNext_m1A20A8601DAE22029AF08361494B130EDA3415BB (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitCheckU3Ec__Iterator4_MoveNext_m1A20A8601DAE22029AF08361494B130EDA3415BB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_004d;
			}
			case 2:
			{
				goto IL_0060;
			}
		}
	}
	{
		goto IL_008d;
	}

IL_0025:
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		RefInt_t76276C0E52C03B7018A40D77232CF118E2EB2ADE * L_3 = L_2->get_m_objectNumber_4();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_i_0();
		IL2CPP_RUNTIME_CLASS_INIT(VectorManager_tD7C7CB626CB1680522C851891D187CD795C27108_il2cpp_TypeInfo_var);
		VectorManager_DrawArrayLine_m6B2FB8AC00AA6C6928ACA7B32B38F1A0742F9888(L_4, /*hidden argument*/NULL);
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_008f;
	}

IL_004d:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(2);
		goto IL_008f;
	}

IL_0060:
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_6 = Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019(L_5, /*hidden argument*/Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019_RuntimeMethod_var);
		NullCheck(L_6);
		bool L_7 = Renderer_get_isVisible_mE424F7FFEA9D78BC657B3F54FDFBE1EB8461CCB7(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0086;
		}
	}
	{
		VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * L_9 = L_8->get_m_vectorLine_5();
		NullCheck(L_9);
		VectorLine_set_active_mC36854853066B0E2F40DC52EF8DA6A10C072A484(L_9, (bool)0, /*hidden argument*/NULL);
	}

IL_0086:
	{
		__this->set_U24PC_0((-1));
	}

IL_008d:
	{
		return (bool)0;
	}

IL_008f:
	{
		return (bool)1;
	}
}
// System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4_Dispose_mE1CCB0A15D4932E14EB9214C722DDFAEC9439795 (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Vectrosity.VisibilityControlStatic_<WaitCheck>c__Iterator4::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitCheckU3Ec__Iterator4_Reset_mCD91B789ED27DFCAB4939937BFD8114713AE27ED (U3CWaitCheckU3Ec__Iterator4_t639C12EB4F6E85005E4E9ABC2B88C744A34023A6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitCheckU3Ec__Iterator4_Reset_mCD91B789ED27DFCAB4939937BFD8114713AE27ED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CWaitCheckU3Ec__Iterator4_Reset_mCD91B789ED27DFCAB4939937BFD8114713AE27ED_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BrightnessControl_SetUseLine_m5CCF7F07EF01A1E5799AD2441F441460E2E7608E_inline (BrightnessControl_tA529166B28D8D1AF0623F845838D41A5269CA984 * __this, bool ___useLine0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___useLine0;
		__this->set_m_useLine_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  VisibilityControlStatic_GetMatrix_m72C2A4EB4D860A0B6581A8633B671FD5F73419DD_inline (VisibilityControlStatic_tEC673D3C93F4968399648324F2BCA45B68AC611D * __this, const RuntimeMethod* method)
{
	{
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_0 = __this->get_m_originalMatrix_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void VectorLine_set_drawTransform_m64A6D4FFBC0B49EFC72504521A235DB828F62309_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___value0;
		__this->set_m_drawTransform_51(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool VectorLine_get_active_m3B3628759C5C506D16EEFB12D8BB6539B95DB2C5_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_active_23();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* VectorLine_get_name_m64CCA734DE0CA69299EE64228DF70046AF208C99_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_name_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* VectorLine_get_lineVertices_mC4D7969376459F3E60138EDDCA9D581F0702D70C_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method)
{
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_0 = __this->get_m_lineVertices_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* VectorLine_get_lineUVs_m9F9C4DFCB7EB6D5E2A4FFDE0D435BAD6647B3C71_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method)
{
	{
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_0 = __this->get_m_lineUVs_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* VectorLine_get_lineColors_m6723E4CBCB634208C658EFEEDBBAFAC5F4F8E5C1_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method)
{
	{
		Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* L_0 = __this->get_m_lineColors_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * VectorLine_get_lineTriangles_m6FE31B265CC9E175DBC5BD5D91256EAA7ADE78C2_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = __this->get_m_lineTriangles_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * VectorLine_get_texture_m80B0BB07297D6E6A53789EFF36AFA678FACCA27E_inline (VectorLine_tB0E51C27CCA32BE811633B7EAA63EE31CF94B775 * __this, const RuntimeMethod* method)
{
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = __this->get_m_texture_22();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get__items_1();
		int32_t L_3 = ___index0;
		int32_t L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_gshared_inline (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_2 = (ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)L_2, (int32_t)L_3);
		return L_4;
	}
}
